**Level**: 1

**Description**: Water suffused with enchantments

**Effects**

- [Befuddlement](Befuddlement): -1~2

- [Nature Lore Exp](Nature-Lore): 1

- [Water Lore Exp](Water-Lore): 1

- [Arcana](Arcana): 0.001

**Result**

- [Water Gem](Water-Gem): 25%

