```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["Hydromancer > 0 or Wind Mage > 0"])
  t2 ---> t2r ---> stormcaller
  subgraph tier3 [Tier 3]
    stormcaller([Stormcaller])
  end
```
**Requirements**: [Hydromancer](Hydromancer) > 0 or [Wind Mage](Wind-Mage) > 0

**Tags**: T_Tier3

**Cost to acquire**

- [Research](Research): 3000

- [Arcana](Arcana): 25

- [Air Gem](Air-Gem): 20

- [Water Gem](Water-Gem): 20

- [Tomes](Tomes): 10

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 3](tier3): True

- [Lore Max](Lore): 1

- [Mana Max](Mana): 2

- [Wind Lore Max](Wind-Lore): 3

- [Water Lore Max](Water-Lore): 3

- [Wind Lore Rate](Wind-Lore): 15%

- [Water Lore Rate](Water-Lore): 15%

**Flavor**: I am the storm

