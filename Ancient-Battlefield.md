**Description**: Blasted hills and scraps of armor still mark an ancient site of battle.

**Effects**

- [Madness](Madness): 0.5

- [Weariness](Weariness): 2~7

- [Unease](Unease): 2~5

- [World Lore Exp](World-Lore): 0.5

**Result**

- [World Lore Max](World-Lore): 0.0002

