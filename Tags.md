|Tag Name|Tagged Items|
|:-------|:-----------|
|airsource|[Casement Window](Casement-Window), [Cloud Chamber](Cloud-Chamber), [Evanescence](Evanescence), [Lattice Window](Lattice-Window), [Swirling Globe](Swirling-Globe), [Tower](Tower), [Windchime](Windchime)|<br>
|armssource|[Armory](Armory), [Weapon Rack](Weapon-Rack)|<br>
|automata|[Autocaster](Autocaster)|<br>
|bed|[Burning Coals](Burning-Coals), [Cot](Cot), [Fourposter](Fourposter), [Wooden Bed](Wooden-Bed)|<br>
|book|[Annals of Arazor](Annals-of-Arazor), [Annals of Orrem](Annals-of-Orrem), [Coporis Fabrica](Coporis-Fabrica)|<br>
|candle|[Heart Candle](Heart-Candle), [Holy Candle](Holy-Candle), [Somber Candle](Somber-Candle), [Unholy Candle](Unholy-Candle), [Wax Candle](Wax-Candle)|<br>
|earthsource|[Cavern](Cavern), [Earth Soul](Earth-Soul), [Grotesque](Grotesque), [Heart of Stone](Heart-of-Stone), [Sand Garden](Sand-Garden), [Terrarium](Terrarium), [Vivarium](Vivarium)|<br>
|element|[Air](Air), [Earth](Earth), [Fire](Fire), [Water](Water)|<br>
|elemental|[Geomancy](Geomancy), [Pyromancy](Pyromancy), [Water Lore](Water-Lore), [Wind Lore](Wind-Lore)|<br>
|elementalgems|[Air Gem](Air-Gem), [Earth Gem](Earth-Gem), [Fire Gem](Fire-Gem), [Water Gem](Water-Gem)|<br>
|elementalrunes|[Air Runes](Air-Runes), [Earth Runes](Earth-Runes), [Flame Runes](Flame-Runes), [Water Runes](Water-Runes)|<br>
|enchantsource|[Enchanting Table](Enchanting-Table)|<br>
|fetish|[Fetish](Fetish)|<br>
|firesource|[Bonfire](Bonfire), [Brazier](Brazier), [Campfire](Campfire), [Fireplace](Fireplace), [Inner Fire](Inner-Fire), [Lake of Fire](Lake-of-Fire), [Volcanic Lair](Volcanic-Lair)|<br>
|gemimbue|[Coagulate Gem (Blood)](Coagulate-Gem-(Blood)), [Imbue Gem (Air)](Imbue-Gem-(Air)), [Imbue Gem (Arcane)](Imbue-Gem-(Arcane)), [Imbue Gem (Fire)](Imbue-Gem-(Fire)), [Imbue Gem (Light)](Imbue-Gem-(Light)), [Imbue Gem (Nature)](Imbue-Gem-(Nature)), [Imbue Gem (Shadow)](Imbue-Gem-(Shadow)), [Imbue Gem (Spirit)](Imbue-Gem-(Spirit)), [Imbue Gem (Water)](Imbue-Gem-(Water)), [Imbue Stone (Earth)](Imbue-Stone-(Earth))|<br>
|greatstaff|[Greatstaff](Greatstaff)|<br>
|hammer|[Titan's Hammer](Titan's-Hammer)|<br>
|herb|[Belladonna](Belladonna), [Blackrot](Blackrot), [Fire Thistle](Fire-Thistle), [Goldfel](Goldfel), [Icethorn](Icethorn), [Lavender](Lavender), [Marigold](Marigold), [Milkweed](Milkweed), [Nightshade](Nightshade), [Oleander](Oleander), [Snowdrop](Snowdrop), [Stinging Nettle](Stinging-Nettle)|<br>
|lightsource|[Casement Window](Casement-Window), [Lattice Window](Lattice-Window), [Pavilion](Pavilion), [Prism](Prism), [Shrine](Shrine)|<br>
|magic|[Fetish](Fetish), [Greatstaff](Greatstaff), [Orb](Orb), [Rod](Rod), [Wand](Wand)|<br>
|magicgems|[Air Gem](Air-Gem), [Arcane Gem](Arcane-Gem), [Blood Gem](Blood-Gem), [Chaos Gem](Chaos-Gem), [Earth Gem](Earth-Gem), [Fire Gem](Fire-Gem), [Light Gem](Light-Gem), [Nature Gem](Nature-Gem), [Shadow Gem](Shadow-Gem), [Spirit Gem](Spirit-Gem), [Time Gem](Time-Gem), [Void Gem](Void-Gem), [Water Gem](Water-Gem)|<br>
|manas|[Air](Air), [Chaos](Chaos), [Earth](Earth), [Fire](Fire), [Ice](Ice), [Light](Light), [Mana](Mana), [Nature](Nature), [Shadow](Shadow), [Spirit](Spirit), [Tempus](Tempus), [Void](Void), [Water](Water)|<br>
|naturesource|[Garden](Garden), [Vivarium](Vivarium)|<br>
|orb|[Orb](Orb)|<br>
|outdoors|[Camp](Camp), [Cave](Cave), [Cove](Cove), [Enchanted Grove](Enchanted-Grove), [Idrasil Seedling](Idrasil-Seedling), [Shrouded Isle](Shrouded-Isle), [Tinker's Wagon](Tinker's-Wagon), [Wild's Encampment](Wild's-Encampment)|<br>
|patron|[Bloodbane's Anvil](Bloodbane's-Anvil), [Kanna's Dervish Dance](Kanna's-Dervish-Dance), [Nathaniel's Sanctuary](Nathaniel's-Sanctuary), [Wild's Encampment](Wild's-Encampment)|<br>
|plantsource|[Creeping Vine](Creeping-Vine), [Flower Pot](Flower-Pot), [Garden](Garden), [Potted Milkweed](Potted-Milkweed), [Wild's Encampment](Wild's-Encampment)|<br>
|poison|[Belladonna](Belladonna), [Milkweed](Milkweed), [Nightshade](Nightshade), [Oleander](Oleander)|<br>
|potsource|[Cauldron](Cauldron), [Cooking Pot](Cooking-Pot), [Potions Room](Potions-Room)|<br>
|primaticrunes|[Air Runes](Air-Runes), [Earth Runes](Earth-Runes), [Flame Runes](Flame-Runes), [Light Runes](Light-Runes), [Mana Runes](Mana-Runes), [Nature Runes](Nature-Runes), [Shadow Runes](Shadow-Runes), [Spirit Runes](Spirit-Runes), [Water Runes](Water-Runes)|<br>
|primordial|[Chaos](Chaos), [Tempus](Tempus), [Void](Void)|<br>
|primordialgems|[Chaos Gem](Chaos-Gem), [Time Gem](Time-Gem), [Void Gem](Void-Gem)|<br>
|primordialrunes|[Chaos Runes](Chaos-Runes), [Time Runes](Time-Runes)|<br>
|prismatic|[Air](Air), [Earth](Earth), [Fire](Fire), [Light](Light), [Mana](Mana), [Nature](Nature), [Shadow](Shadow), [Spirit](Spirit), [Water](Water)|<br>
|prismaticgems|[Air Gem](Air-Gem), [Arcane Gem](Arcane-Gem), [Earth Gem](Earth-Gem), [Fire Gem](Fire-Gem), [Light Gem](Light-Gem), [Nature Gem](Nature-Gem), [Shadow Gem](Shadow-Gem), [Spirit Gem](Spirit-Gem), [Water Gem](Water-Gem)|<br>
|prison|[Binding Circle](Binding-Circle), [Crypt](Crypt), [Demonic Pentagram](Demonic-Pentagram), [Dungeon](Dungeon), [Iron Cell](Iron-Cell)|<br>
|reagent|[Belladonna](Belladonna), [Blackrot](Blackrot), [Crocodile Tears](Crocodile-Tears), [Dragon Scales](Dragon-Scales), [Eye of Newt](Eye-of-Newt), [Fire Thistle](Fire-Thistle), [Goldfel](Goldfel), [Icethorn](Icethorn), [Ichor](Ichor), [Lavender](Lavender), [Marigold](Marigold), [Milkweed](Milkweed), [Nightshade](Nightshade), [Oleander](Oleander), [Snowdrop](Snowdrop), [Stinging Nettle](Stinging-Nettle)|<br>
|rez|[Greater Reanimate](Greater-Reanimate), [Magic Tending](Magic-Tending), [Mend Wood](Mend-Wood), [Reanimate](Reanimate), [Repair Machina](Repair-Machina), [Revive](Revive), [Tune Automata](Tune-Automata), [Wild Tending](Wild-Tending)|<br>
|rod|[Rod](Rod)|<br>
|shadowsource|[Blood Fountain](Blood-Fountain), [Dark Shrine](Dark-Shrine), [Graveyard](Graveyard), [Iron Cell](Iron-Cell), [Ossuary](Ossuary), [Rusty Cage](Rusty-Cage), [Torture Chamber](Torture-Chamber)|<br>
|soulart|[Soul Card: Soul Lure](Soul-Card:-Soul-Lure)|<br>
|spiritart|[Spirit Art: Soul River](Spirit-Art:-Soul-River), [Spirit Art: Soulstrike](Spirit-Art:-Soulstrike)|<br>
|spiritsource|[Brazier](Brazier), [Cairn](Cairn), [Crystal Ball](Crystal-Ball), [Dream Catcher](Dream-Catcher), [Graveyard](Graveyard), [Haunted Manse](Haunted-Manse), [Ossuary](Ossuary), [Reliquary](Reliquary), [Third Eye](Third-Eye), [Windchime](Windchime)|<br>
|stables|[Menagerie](Menagerie), [Stables](Stables)|<br>
|starsource|[Camp](Camp), [Orrery](Orrery), [Sextant](Sextant), [Telescope](Telescope), [Ziggurat](Ziggurat)|<br>
|steed|[Demure Gelding](Demure-Gelding), [Fine Bay](Fine-Bay), [Fire Charger](Fire-Charger), [Gryffon](Gryffon), [Mule](Mule), [Old Nag](Old-Nag), [Pegasus](Pegasus), [Skeletal Charger](Skeletal-Charger)|<br>
|stress|[Befuddlement](Befuddlement), [Frustration](Frustration), [Madness](Madness), [Unease](Unease), [Weariness](Weariness)|<br>
|swordart|[Lost Sword Art: Flying Swords](Lost-Sword-Art:-Flying-Swords), [Lost Sword Art: Giant Slayer](Lost-Sword-Art:-Giant-Slayer)|<br>
|t_automata|[Autoslayer](Autoslayer)|<br>
|t_hall|[Wizard's Hall](Wizard's-Hall)|<br>
|t_job|[Falconer](Falconer), [Herbalist](Herbalist), [Scribe](Scribe)|<br>
|t_machina|[Mecha-Charger](Mecha-Charger), [Mecha-Mender](Mecha-Mender)|<br>
|t_mine|[Mineshaft](Mineshaft)|<br>
|t_pets|[Birdcage](Birdcage), [Rusty Cage](Rusty-Cage), [Terrarium](Terrarium), [Tinker's Wagon](Tinker's-Wagon), [Vivarium](Vivarium), [Wild's Encampment](Wild's-Encampment)|<br>
|t_puppet|[Puppet Aggressor](Puppet-Aggressor), [Puppet Bulwark](Puppet-Bulwark)|<br>
|t_rest|[Chant](Chant), [Commune](Commune), [Eat Children](Eat-Children), [Indulge](Indulge), [Rest](Rest), [Slumber](Slumber), [Spring Rites](Spring-Rites), [Time Siphon](Time-Siphon), [Warm Yourself](Warm-Yourself)|<br>
|t_ritual|[Greater Ritual of Cultivation](Greater-Ritual-of-Cultivation), [Greater Ritual of Cursing](Greater-Ritual-of-Cursing), [Greater Ritual of Energy](Greater-Ritual-of-Energy), [Greater Ritual of Healing](Greater-Ritual-of-Healing), [Greater Ritual of the Arcane](Greater-Ritual-of-the-Arcane), [Ritual of Air](Ritual-of-Air), [Ritual of Cultivation](Ritual-of-Cultivation), [Ritual of Cursing](Ritual-of-Cursing), [Ritual of Dawn](Ritual-of-Dawn), [Ritual of Dreams](Ritual-of-Dreams), [Ritual of Dusk](Ritual-of-Dusk), [Ritual of Earth](Ritual-of-Earth), [Ritual of Fire](Ritual-of-Fire), [Ritual of Healing](Ritual-of-Healing), [Ritual of Light Binding](Ritual-of-Light-Binding), [Ritual of Mana](Ritual-of-Mana), [Ritual of Mandalas](Ritual-of-Mandalas), [Ritual of Penance](Ritual-of-Penance), [Ritual of Refreshment](Ritual-of-Refreshment), [Ritual of Time](Ritual-of-Time), [Ritual of Void](Ritual-of-Void), [Ritual of Water](Ritual-of-Water), [Ritual of the Arcane](Ritual-of-the-Arcane)|<br>
|t_runes|[Air Runes](Air-Runes), [Earth Runes](Earth-Runes), [Flame Runes](Flame-Runes), [Light Runes](Light-Runes), [Mana Runes](Mana-Runes), [Nature Runes](Nature-Runes), [Rune Stones](Rune-Stones), [Shadow Runes](Shadow-Runes), [Spirit Runes](Spirit-Runes), [Water Runes](Water-Runes)|<br>
|t_school|[Chronomancy](Chronomancy), [Geomancy](Geomancy), [Lore](Lore), [Lumenology](Lumenology), [Nature Lore](Nature-Lore), [Pyromancy](Pyromancy), [Shadowlore](Shadowlore), [Spirit Lore](Spirit-Lore), [Water Lore](Water-Lore), [Wind Lore](Wind-Lore)|<br>
|t_tier0|[Adept](Adept), [Blue Adept](Blue-Adept), [Mystic Savant](Mystic-Savant)|<br>
|t_tier1|[Arcane Trickster](Arcane-Trickster), [Dark Magician](Dark-Magician), [Elementalist](Elementalist), [Magician](Magician), [Oracle](Oracle), [Puppeteer](Puppeteer), [Reanimator](Reanimator), [Witchcraft](Witchcraft)|<br>
|t_tier2|[Alchemist](Alchemist), [Battle Mage](Battle-Mage), [Bonemonger](Bonemonger), [Enchanter](Enchanter), [Geomancer](Geomancer), [Hydromancer](Hydromancer), [Mage](Mage), [Mystic Madcap](Mystic-Madcap), [Pyromancer](Pyromancer), [Ritualist](Ritualist), [Seer](Seer), [Warden](Warden), [Wind Mage](Wind-Mage)|<br>
|t_tier3|[Blood Mage](Blood-Mage), [Doomsayer](Doomsayer), [Dreadlord](Dreadlord), [Druid](Druid), [High Mage](High-Mage), [Highelemental](Highelemental), [Mechanist](Mechanist), [Spell Blade](Spell-Blade), [Stormcaller](Stormcaller), [Summoner](Summoner), [Thanophage](Thanophage)|<br>
|t_tier4|[Bladeweaver](Bladeweaver), [Earthshaker](Earthshaker), [Fey](Fey), [Necromancer](Necromancer), [Shian Anchorite](Shian-Anchorite), [Sorcerer](Sorcerer), [Thaumaturge](Thaumaturge), [Warlock](Warlock), [Wizard](Wizard)|<br>
|t_tier5|[Arcane Dervish](Arcane-Dervish), [Archlock](Archlock), [Dhrunic Wizard](Dhrunic-Wizard), [Grey Necromancer](Grey-Necromancer), [Heresiarch](Heresiarch), [Kell](Kell), [Master Ritualist](Master-Ritualist), [Mechamancer](Mechamancer)|<br>
|t_tier6|[Astral Seer](Astral-Seer), [Avatar](Avatar), [Death Lock](Death-Lock), [Eldritch Knight](Eldritch-Knight), [High Kell](High-Kell), [Mythic Wizard](Mythic-Wizard), [Titan](Titan), [Vile Necromancer](Vile-Necromancer)|<br>
|timesource|[Clepsydra](Clepsydra), [Metronome](Metronome), [Pendulum](Pendulum)|<br>
|tricksource|[Bag of Tricks](Bag-of-Tricks), [Card Deck](Card-Deck), [Three-Sided Coin](Three-Sided-Coin)|<br>
|wand|[Wand](Wand)|<br>
|watersource|[Basin](Basin), [Bottomless Pitcher](Bottomless-Pitcher), [Cataract](Cataract), [Fountain](Fountain), [Gargoyle](Gargoyle), [Pond](Pond)|<br>
|weap_orb|[Orb](Orb)|<br>
|weavesource|[Loom](Loom), [Spinning Wheel](Spinning-Wheel)|<br>
|workspace|[Crafting Table](Crafting-Table), [Enchanting Table](Enchanting-Table), [Machina Workshop](Machina-Workshop), [Shaping Laboratory](Shaping-Laboratory), [Workbench](Workbench), [Workshop](Workshop), [Writing Desk](Writing-Desk)|