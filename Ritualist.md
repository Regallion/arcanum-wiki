```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Rituals ≥ 5"])
  t1 ---> t1r ---> ritualist
  subgraph tier2 [Tier 2]
    ritualist([Ritualist])
  end
```
**Description**: Perform arcane rituals to boost your powers.

**Tags**: T_Tier2

**Requirements**: [Tier 1](tier1) > 0 and [Rituals](Rituals) ≥ 5 and [Tier 2](tier2) = 0

**Cost to acquire**

- [Research](Research): 700

- [Arcana](Arcana): 10

- [Gold](Gold): 500

**Modifiers**

- [Tier 2](tier2): True

- [Rituals Max](Rituals): 2

- [Ritual Wards Rate](Ritual-Wards): 0.0002

- [Arcane Paper Rate](Arcane-Paper): 0.001

**Flavor**: The first rule of ritual magic is thus: 'Make sure you are prepared to contain the explosions.'

