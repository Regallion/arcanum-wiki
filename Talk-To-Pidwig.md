**Description**: Pidwig is sparse with words, but has seen many mysteries in his travels.

**Effects**

- [Befuddlement](Befuddlement): 0~2

- [Weariness](Weariness): 0~2

- [Unease](Unease): 0~2

- [World Lore Exp](World-Lore): 2

- [Astronomy Exp](Astronomy): 1

- [Lore Exp](Lore): 1

**Result**

- [Arcana](Arcana): 0.05

