[[_TOC_]]


## About the Game

This game is about wiznerds, magic, quests, advancement in the society and a lot of time waiting for bars or resources. However, there is also a lot of confusion which I am gonna try to fix with this guide.

If there is anything that you wonder about even after reading this guide then ask on discord in the #apprenticeship chat. This guide is partitioned into parts based on your title.

## Essentials
+ Holding enter after activating an action makes it activate at a rate of twenty times per second on most computers. Only works on the white actions that are instant.
+ The quickslots can be used on everything from [Spells](spells) to [tasks](tasks). Don't hesitate to use them! Hover over the activation button (or name) and press shift+number you want it on. [Enchanting](Enchanting) have issues with quickslot.
+ Most buffs stack, with the exception of [Mana](mana) and [Stamina](stamina) ([Fount](fount)) ones.
For info on the Different Tabs check out their section in the sidebar.

## Stages
```mermaid
graph TD
    A[Waif] -->|Get Gold and Research| B{Apprentice}
    B -->|Birdcage| C[Animal Handling]
    C --> CC[Falconer]
    B -->|Potted Milkweed| D[Herbalism]
    D --> DC[Herbalist]
    B -->|Workbench| E[Craft]
    E -->|Scribe Scroll| EC[Scribe]
    CC --> F{Neophyte}
    DC --> F
    EC --> F
    F --> G[Tier0]
```

### Waif
So you start the game by producing [Gold](Gold) and buying the various [Upgrades](Upgrades) that pops up. For now, buy the 2 [Pouch](Pouches) and 2 [Purse](Purses). You must also buy the 10 [Scroll](Scrolls) you have [Space](Space) for. No need to be stingy, you will need them later (and a whole lot more). When you get to 10 [Scroll](Scrolls), an upgrade called [Satchel](Satchel) appears. This [Upgrades](upgrade) increases your [Scroll](Scroll) limit to 25. Now max them!

After being done with the first, and arguably one of the shorter grinds in the game, you're now ready for [Apprentice](apprenticeship)! This is irreversible, as are many things in this game, but right now you have no other path.
