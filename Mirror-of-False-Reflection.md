**Description**: Something is wrong with the glass.

**Effects**

- [Trickery Exp](Trickery): 1

- [Frustration](Frustration): 1~3

- [Befuddlement](Befuddlement): 1~2

**Result**

