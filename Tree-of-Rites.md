**Level**: 5

**Length**: 30

**Requirements**: [Preparation](Preparation) ≥ 1

**Description**: A fir older than the mountains stands alone in the midst of a clearing. You feel its age, heavier than the fog itself. The embodiment of the living winter.

**Run Cost**

- [Stamina](Stamina): 0.5

**Effects**

- [Frost](Frost): 1

**Modifiers**

- [Cryomancy Max](Cryomancy): 1%

**Result**

- Event: [Snow Double](evt_snowcopy): 1

**Encounters**

- [Siphon Lifeforce](Siphon-Lifeforce)

- [Drawing Mystic Schemes](Drawing-Mystic-Schemes)

- [Placing Gems](Placing-Gems)

- [Connecting Focal Points](Connecting-Focal-Points)

- [Attuning To Local Magic](Attuning-To-Local-Magic)

**Symbol**: ❄️

