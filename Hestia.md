**Description**: Hestia has no interest in the world outside Thangmor, but will teach those who come to her.

**Effects**

- [Unease](Unease): 2~4

- [Befuddlement](Befuddlement): -2~3

- [Herbalism Exp](Herbalism): 1

**Result**

- [Scrying Max](Scrying): 0.001

- [World Lore Max](World-Lore): 0.001

- [Potions Exp](Potions): 0.001

- [Arcana](Arcana): 0.01

