**Description**: Humanoid figure formed from ice and snow. The right enchantment will give it life.

**Level**: 2

**Effects**

- [Weariness](Weariness): 1~4

- [Ice](Ice): -0.4

- [Cryomancy Max](Cryomancy): 0.001

**Loot**

- [Essence of Winter](Essence-of-Winter): 0.1

- [Snomunculus](Snomunculus): 1

**Symbol**: ❄️

