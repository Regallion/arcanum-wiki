**Level**: 15

**Requirements**: [Potions](Potions) ≥ 10

**Cost to Buy**

- [Gold](Gold): 500

- [Tomes](Tomes): 5

- [Research](Research): 500

**Cost to acquire**

- [Air Gem](Air-Gem): 1

- [Ichor](Ichor): 1

**Effect of using**

- Dot: {'mod': {'speed': 25}, 'duration': 1800}

