**Description**: From spring spices to dwarven garnets, all manner of luxuries are bartered at the fair.

**Effects**

- [Gold](Gold): -0.5

- [Weariness](Weariness): 1~3

- [Befuddlement](Befuddlement): 0~1

**Result**

- [Herbs](Herbs): 4~10

**Symbol**: 🌼

