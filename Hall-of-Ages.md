**Description**: The lost Hall of Ages was a gathering place of great and powerful patron mages.

**Requirements**: [Tier 3](tier3) > 0

**Title**: Mark of Ages

**Length**: 100

**Modifiers**

- [World Lore Max](World-Lore): ?1

**Encounters**

- [Kaidi](Kaidi)

- [View Stars With Cyril](View-Stars-With-Cyril)

- [Jeremi](Jeremi)

- [Study Under Kanna](Study-Under-Kanna)

- [Phrenesis](Phrenesis)

- [Sinae](Sinae)

- [Stags](Stags)

- [Tainted, Archon of the Void](Tainted,-Archon-of-the-Void)

- [A Joker](A-Joker)

- [Vondrey](Vondrey)

- [Mister Reaper](Mister-Reaper)

- [Master Wild](Master-Wild)

**Flavor**: the knowledge within may be beyond price.

**Symbol**: ⭐

