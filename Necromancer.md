```mermaid
graph LR
  t3{Tier 3 Class}
  t3r(["Vile > 0,<br>Shadowlore ≥ 17"])
  t3 ---> t3r ---> necromancer
  subgraph tier4 [Tier 4]
    necromancer([Necromancer])
  end
```
**Tags**: T_Tier4

**Description**: A master of death magic

**Requirements**: [Tier 3](tier3) > 0 and Event: [Vile](evil) > 0 and [Shadowlore](Shadowlore) ≥ 17

**Cost to acquire**

- [Research](Research): 2000

- [Bones](Bones): 100

- [Bodies](Bodies): 50

- [Souls](Souls): 40

- [Arcana](Arcana): 10

- [Rune Stones](Rune-Stones): 5

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 4](tier4): True

- [Embalming Max](Embalming): 1

- [Reanimation Max](Reanimation): 2

- [Reanimation Rate](Reanimation): 10%

- [Shadowlore Max](Shadowlore): 2

- [Shadowlore Rate](Shadowlore): 0.2+5%

- [Potions Max](Potions): 1

- [Necromancy Max](Necromancy): 3

- [Necromancy Rate](Necromancy): 10%

- [Spirit Lore Max](Spirit-Lore): 2

- [Spirit Lore Rate](Spirit-Lore): 10%

