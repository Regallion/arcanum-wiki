**Description**: Still maintaining the workshop after centuries... as much as they can

**Effects**

- [Weariness](Weariness): 1~3

- [Mechanical Magic Exp](Mechanical-Magic): 3

- [Puppetry Exp](Puppetry): 3

**Result**

- [Machinae](Machinae): 5%

- [Puppets](Puppets): 5%

**Symbol**: None

