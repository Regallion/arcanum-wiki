**Description**: Much that was is lost.

**Effects**

- [Befuddlement](Befuddlement): -1~4

- [Frustration](Frustration): 0~4

- [World Lore Exp](World-Lore): 2

**Result**

- [World Lore Max](World-Lore): 0.0001

- [Arcana](Arcana): 0.01

**Flavor**: For none now live, who remember it.

