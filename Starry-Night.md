**Description**: A clear sky affords an endless view of the firmament.

**Effects**

- [Befuddlement](Befuddlement): 1~2

- [Unease](Unease): -1~2

- [Madness](Madness): -1~1

- [Astronomy Exp](Astronomy): 1

- [Planes Lore Exp](Planes-Lore): 1

- [Scrying Exp](Scrying): 1

