**Description**: At the center of the menagerie a single massive scale merges with the tree.

**Effects**

- [Unease](Unease): 3~4

- [Madness](Madness): 4~5

**Flavor**: Best not disturb it.

