```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["(Battle Mage > 0 or hp.max > 150)"])
  t2 ---> t2r ---> bloodmage
  subgraph tier3 [Tier 3]
    bloodmage([Blood Mage])
  end
```
**Tags**: T_Tier3

**Description**: The magic in your veins

**Requirements**: [Tier 2](tier2) > 0 and ([Battle Mage](Battle-Mage) > 0 or [Life Max](Life) > 150) and [Tier 3](tier3) = 0

**Cost to acquire**

- [Research](Research): 1500

- [Arcana](Arcana): 8

- [Gold](Gold): 700

- [Blood Gem](Blood-Gem): 20

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 3](tier3): True

- [Research Max](Research): -50

- [Blood Gem Max](Blood-Gem): 10

- [Mana Max](Mana): 1

- [Mana Rate](Mana): 0.1

- [Anatomy Max](Anatomy): 3

- [Anatomy Rate](Anatomy): 10%

- [Geomancy Max](Geomancy): 2

- [Geomancy Rate](Geomancy): 5%

- [Water Lore Max](Water-Lore): 1

- [Water Lore Rate](Water-Lore): 5%

