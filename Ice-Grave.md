**Description**: A traveller here was frozen alive. The body may still be of use.

**Effects**

- [Madness](Madness): 2~3

- [Weariness](Weariness): 1~4

- [Frost](Frost): 1~2

- [Geomancy Max](Geomancy): 0.001

**Loot**

- [Essence of Winter](Essence-of-Winter): 0.1

- Body: 4%

- [Bones](Bones): {'val': '0~2', 'pct': '40%'}

- [Blood Gem](Blood-Gem): 5%

**Symbol**: ❄️

