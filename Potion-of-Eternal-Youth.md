**Description**: Ensures the drinker never becomes old. For sale only

**Requirements**: [Potions](Potions) ≥ 1 and Event: [Vile](evil) > 0

**Cost to Buy**

- [Gold](Gold): 250

- [Research](Research): 500

**Cost to acquire**

- [Herbs](Herbs): 25

- [Bodies](Bodies): 15

- [Shadow Gem](Shadow-Gem): 2

- [Potion Base](Potion-Base): 1

**Gains from selling**: {'gold': 2000, 'gems': 25}

**Effect of using**

- Dot: {'id': 'fakeeternalyouth', 'name': 'eternal youth', 'duration': 150, 'effect': {'hp': -50}}

