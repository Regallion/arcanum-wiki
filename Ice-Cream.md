**Description**: A batch of ice cream flavored with wintery herbs and spices

**Requirements**: [Potions](Potions) ≥ 1

**Cost to Buy**

- [Gold](Gold): 250

- [Ice](Ice): 5

**Cost to acquire**

- [Herbs](Herbs): 15

- [Ice](Ice): 5

**Gains from selling**: {'gold': 100}

**Effect of using**

- Dot: {'id': 'icecream', 'name': 'ice cream high', 'duration': 600, 'effect': {'hp': 0.5, 'stress': -0.1}}

