**Description**: Violently expells all mana from user's body for a time.

**Requirements**: [Potions](Potions) ≥ 999

**Cost to Buy**

- [Gold](Gold): 250

- [Research](Research): 500

**Cost to acquire**

- [Herbs](Herbs): 10

**Effect of using**

- Dot: {'id': 'manaburn', 'name': 'mana burn', 'duration': 600, 'mod': {'manas.max': -1000}, 'effect': {'manas': -50}}

