**Description**: The fog here is so thick, you can't see your own hands.

**Effects**

- [Befuddlement](Befuddlement): 1~2

- [Frustration](Frustration): 0~1

- [Unease](Unease): 1~2

- [Wind Lore Exp](Wind-Lore): 2

- [Spirit Lore Exp](Spirit-Lore): 1

