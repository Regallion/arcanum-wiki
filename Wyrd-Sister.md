**Description**: The Wyrd sister Hecubah waits by the roadside.

**Effects**

- [Scrying Exp](Scrying): 1

- [Divination Exp](Divination): 1

- [Madness](Madness): 2

- [Befuddlement](Befuddlement): 3~7

- [Unease](Unease): 2~4

**Result**

**Flavor**: Blank verse is hard to interpret.

