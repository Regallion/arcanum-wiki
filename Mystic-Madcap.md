```mermaid
graph LR
  t1{Tier 1 Class}
  t1r([Trickery >= 7,<br>tier2==0])
  t1 ---> t1r ---> madcap
  subgraph tier2 [Tier 2]
    madcap([Mystic Madcap])
  end
```
