**Description**: The placid poglers angle their backs of bushy leaves to the sun.

**Effects**

- [Befuddlement](Befuddlement): 2~3

- [Frustration](Frustration): -0.5

- [Nature Mastery Exp](Nature-Mastery): 1

