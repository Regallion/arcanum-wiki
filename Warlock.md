```mermaid
graph LR
  t3{Tier 3 Class}
  t3r(["Vile > 0,<br>Demonology ≥ 10"])
  t3 ---> t3r ---> warlock
  subgraph tier4 [Tier 4]
    warlock([Warlock])
  end
```
**Tags**: T_Tier4

**Description**: Master of demons and demonic magic

**Requirements**: [Tier 3](tier3) > 0 and Event: [Vile](evil) > 0 and [Demonology](Demonology) ≥ 10 and [Tier 4](tier4) = 0

**Cost to acquire**

- [Research](Research): 4000

- [Souls](Souls): 50

- [Arcana](Arcana): 25

- [Tomes](Tomes): 10

- [Rune Stones](Rune-Stones): 5

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 4](tier4): True

- [Anatomy Max](Anatomy): 3

- [Lore Rate](Lore): 10%

- [Lore Max](Lore): 2

- [Pyromancy Max](Pyromancy): 1

- [Necromancy Max](Necromancy): 1

- [Pyromancy Rate](Pyromancy): 0.2

- [Shadowlore Max](Shadowlore): 2

- [Demonology Max](Demonology): 3

