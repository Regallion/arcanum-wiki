**Description**: A cooling spring and frond trees relieve the monotony of the wastes.

**Effects**

- [Weariness](Weariness): -2

- [Madness](Madness): -1

- [Unease](Unease): -1

