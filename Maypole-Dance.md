**Description**: Young women from Aylesmeade weave patterns of colored ribbons around a bare trunk.

**Effects**

- [Weaving Exp](Weaving): 2

- [Befuddlement](Befuddlement): 1~2

- [Weariness](Weariness): 1~2

**Loot**

- [Lily of the Vale](Lily-of-the-Vale): 10%

**Symbol**: 🌼

