**Description**: A pristine tapestry nearby displays a partial index of the menagerie.

**Effects**

- [Unease](Unease): 1~2

- [Madness](Madness): 1~2

- [Frustration](Frustration): 1~2

- [Life](Life): -3

- [Research](Research): 1

