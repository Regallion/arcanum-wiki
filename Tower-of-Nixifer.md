**Level**: 3

**Length**: 50

**Requirements**: [Winter Lore](Winter-Lore) ≥ 4 and [Tier 3](tier3) > 0

**Description**: The frozen tower once belonged to a wizard who mastered the mysteries of ice

**Run Cost**

- [Stamina](Stamina): 0.5

**Effects**

- [Frost](Frost): 1

**Result**

- Title: Nixifer's Disciple

- [Essence of Winter](Essence-of-Winter): 5

**Encounters**

- [Cold Snap](Cold-Snap)

- [Cold Snap](Cold-Snap)

- [Cold Snap](Cold-Snap)

- [Ebin's Notes](Ebin's-Notes)

- [Ebin's Notes](Ebin's-Notes)

- [Ebin's Notes](Ebin's-Notes)

- [Tome of Winters Past](Tome-of-Winters-Past)

- [Tome of Winters Past](Tome-of-Winters-Past)

- [Frozen Bookcase](Frozen-Bookcase)

- [Frozen Bookcase](Frozen-Bookcase)

- [Dusty Chest](Dusty-Chest)

- [Ice Sculpture](Ice-Sculpture)

**Symbol**: ❄️

