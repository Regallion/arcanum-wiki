**Description**: Tenwick is often on the trail, but always glad to stop and trade tales.

**Effects**

- [Travel Skill Exp](Travel): 1

- [Weariness](Weariness): 2~3

