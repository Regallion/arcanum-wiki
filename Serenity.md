**Level**: 11

**Requirements**: [Potions](Potions) ≥ 10

**Cost to Buy**

- [Gold](Gold): 500

- [Tomes](Tomes): 5

- [Research](Research): 500

**Cost to acquire**

- [Light Gem](Light-Gem): 1

- [Ichor](Ichor): 1

**Effect of using**

- Dot: {'mod': {'stress.rate': -2}, 'duration': 1800}

