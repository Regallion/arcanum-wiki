```mermaid
graph LR
  t0{Tier 0 Class}
  t0r(["Lore ≥ 10"])
  t0 ---> t0r ---> magician
  subgraph tier1 [Tier 1]
    magician([Magician])
  end
```
**Tags**: T_Tier1

**Description**: Magic in its purest form.

**Action Name**: The Prestige

**Action Description**: Pursue the path of pure magic.

**Requirements**: [Tier 0](tier0) > 0 and [Lore](Lore) ≥ 10

**Cost to acquire**

- [Research](Research): 500

- [Arcana](Arcana): 15

- [Gold](Gold): 500

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 1](tier1): True

- [Research Max](Research): 100

- [Lore Max](Lore): 1

- [Lore Rate](Lore): 5%

- [Astronomy Max](Astronomy): 1

- [Languages Max](Languages): 1

- [Arcana Rate](Arcana): 0.001

- [Mana Max](Mana): 2

- [Mana Rate](Mana): 0.02

