Open up the project on [gitlab](https://gitlab.com/mathiashjelm/arcanum) it will look like this:\
![image](uploads/8b8d946cde86591ed2e24574624b438e/image.png) \
Press the fork button, as shown in the upper right of the picture above.\
Then choose the name of the project (can still be arcanum) and which namespace you want it at, this can be your user or a group you have made if you have multiple forks of arcanum.\
![image](uploads/db95966448a0ced60894d2bdbc8459be/image.png) \
Then choose Visibility level, note if anyone else are going to play on it, then you need to have it on public.\
Then move to your new project:\
![image](uploads/4fa600d0c13a2593a9b730e49caad087/image.png) \
Open the one just made:\
![image](uploads/df3b0e5dc09ec0ad3e15efeade399ad9/image.png) \
Head into the CI/CD and pipelines:\
![image](uploads/90220ad177e57603aa88b8ea949b06ec/image.png) \
Then setup a pipeline:\
![image](uploads/176352a8b1bb75e68419b46b796b4163/image.png) \
Leave all the parameters alone, and run it on master. NOTE: it won't run on any other branch, if you want to know how to change it, contact us on discord.\
![image](uploads/abb8d1683223b384490e87e6a1bb11ce/image.png) \
Wait till the pipeline is done, should take 5-10 min, will take about 2 min after it shows as done

Now we will figure out what your link is, first of, look at your projects handle whats in the URL:\
![image](uploads/30386c578a5c04aa4d4df5c865c69057/image.png) \
This name will be whatever your namespace is called if its unique, else it will be something random as shown above.\
Then find you projects name, usually arcanum, unless you changed it to something else:\
![image](uploads/39917c79a089de39bf8941d23a98c52c/image.png) \
Combine these into a URL on the form of:\
https://{name_of_namespace}.gitlab.io/{project_name}/


With the examples above:\
[[https://f3224.gitlab.io/arcanum/]]