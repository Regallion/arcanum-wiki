```mermaid
graph LR
  t5{Tier 5 Class}
  t5r(["(Fey + Kell) > 0"])
  t5 ---> t5r ---> highkell
  subgraph tier6 [Tier 6]
    highkell([High Kell])
  end
```
**Description**: The ranks of the Kell range from the lesser Fey and forest Delki, to ancient spirits, old and indominable.

**Tags**: T_Tier6

**Requirements**: [Tier 5](tier5) > 0 and ([Fey](Fey) + [Kell](Kell)) > 0

**Cost to acquire**

- [Research](Research): 15000

- [Nature Gem](Nature-Gem): 50

- [Herbs](Herbs): 75

- [Arcana](Arcana): 35

- [Water Runes](Water-Runes): 15

- [Rune Stones](Rune-Stones): 15

**Modifiers**

- [Tier 6](tier6): True

- [Wind Lore Max](Wind-Lore): 2

- [Wind Lore Rate](Wind-Lore): 10%

- [Nature Lore Max](Nature-Lore): 3

- [Nature Lore Rate](Nature-Lore): 15%

- [Animal Handling Max](Animal-Handling): 2

- [Animal Handling Rate](Animal-Handling): 10%

- [Geomancy Max](Geomancy): 1

- [Geomancy Rate](Geomancy): 15%

**Flavor**: 'It's of no consequence to me,' replied Jora. 'I do not die Trill, and there are few things in this world that could do the trick.

