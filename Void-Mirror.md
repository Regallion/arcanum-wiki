**Description**: You see all shapes clearly reflected; but completely black.

**Effects**

- [Void Lore Exp](Void-Lore): 1

- [Void](Void): 1

- [Madness](Madness): 3~5

- [Unease](Unease): 3~5

**Flavor**: For now you see in a mirror, darkly.

