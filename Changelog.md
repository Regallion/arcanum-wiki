Game at https://mathiashjelm.gitlab.io/arcanum/

[[_TOC_]]
## 0.8.3:
*Made by Abraxas*

TLDR:
**Bugfixes** potions, focus and home hover fixes. Also a small visual change to the resource bars.

Made potions sellable and stackable.

Made it easier to script focus, and also made mousedown work better on it.

Increased the height of the bars, so it avoids a visual glitch

You can now hover homes you can't buy yet for information.
## 0.8.3b:
*Made by Abraxas*

**Bugfixes:** loot bug, prestige bug and magic blade not appearing.

**Balancing:** falconer and herbalist got a buff, while language also got buffed.
## 0.9.0:
*Made by Abraxas and Regallion*

**Content:** A extra action was added, its very late game and requires a weekend of idling. Added event to puppeteer.

**Bugs:** Fixed selling bugs (and created some), fixed a bug in wizards hall. Tenwick stick is finally fixed.