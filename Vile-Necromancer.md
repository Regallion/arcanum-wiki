```mermaid
graph LR
  t5{Tier 5 Class}
  t5r(["Phylactory > 0,<br>Spirit Lore ≥ 15,<br>Shadowlore ≥ 24"])
  t5 ---> t5r ---> necro3
  subgraph tier6 [Tier 6]
    necro3([Vile Necromancer])
  end
```
**Tags**: T_Tier6

**Description**: In every epoch, Death appoints her viceroy.

**Requirements**: [Tier 5](tier5) > 0 and [Phylactory](Phylactory) > 0 and [Spirit Lore](Spirit-Lore) ≥ 15 and [Shadowlore](Shadowlore) ≥ 24

**Cost to acquire**

- [Research](Research): 17000

- [Bones](Bones): 100

- [Bodies](Bodies): 50

- [Souls](Souls): 100

- [Arcana](Arcana): 25

- [Earth Runes](Earth-Runes): 10

- [Rune Stones](Rune-Stones): 10

**Modifiers**

- [Tier 6](tier6): True

- [Lore Max](Lore): 1

- [Spellcraft Max](Spellcraft): 1

- [Embalming Max](Embalming): 1

- [Necromancy Max](Necromancy): 3

- [Necromancy Rate](Necromancy): 10%

- [Reanimation Max](Reanimation): 3

- [Reanimation Rate](Reanimation): 10%

- [Shadowlore Max](Shadowlore): 2

- [Shadowlore Rate](Shadowlore): 5%

- [Spirit Lore Max](Spirit-Lore): 2

- [Spirit Lore Rate](Spirit-Lore): 10%

**Flavor**: The greys bowed their heads in fear.

