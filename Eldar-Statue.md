**Description**: Its weathered face has gazed upon the world since the Wind Age.

**Effects**

- [Madness](Madness): 1

- [Befuddlement](Befuddlement): 3~7

- [Unease](Unease): 3~7

- [World Lore Exp](World-Lore): 2

**Result**

- [Rune Stones](Rune-Stones): 10%

- [World Lore Max](World-Lore): 0.0001

