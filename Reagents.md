This page is a default list of the items in this category.<br>
If it doesn't meet your needs, please let a developer (probably 'simple') know, and they'll get around to updating it.<br>

[Belladonna](Belladonna)<br>
[Blackrot](Blackrot)<br>
[Blood Grass](Blood-Grass)<br>
[Crocodile Tears](Crocodile-Tears)<br>
[Dragon Scales](Dragon-Scales)<br>
[Eye of Newt](Eye-of-Newt)<br>
[Fire Thistle](Fire-Thistle)<br>
[Foxfire](Foxfire)<br>
[Goldfel](Goldfel)<br>
[Grimwort](Grimwort)<br>
[Icethorn](Icethorn)<br>
[King's Foil](King's-Foil)<br>
[Lavender](Lavender)<br>
[Manabell](Manabell)<br>
[Marigold](Marigold)<br>
[Mercury](Mercury)<br>
[Milkweed](Milkweed)<br>
[Nightshade](Nightshade)<br>
[Oleander](Oleander)<br>
[Stinging Nettle](Stinging-Nettle)<br>
[Sulphur](Sulphur)<br>
[Vika Root](Vika-Root)