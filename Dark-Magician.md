```mermaid
graph LR
  t0{Tier 0 Class}
  t0r(["(Shadowlore ≥ 7)"])
  t0 ---> t0r ---> darkmagician
  subgraph tier1 [Tier 1]
    darkmagician([Dark Magician])
  end
```
**Tags**: T_Tier1

**Description**: A devotee of black magic.

**Requirements**: [Tier 0](tier0) > 0 and ([Shadowlore](Shadowlore) ≥ 7)

**Cost to acquire**

- [Research](Research): 400

- [Arcana](Arcana): 13

- [Gold](Gold): 300

- [Bones](Bones): 10

- [Gems](Gems): 10

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 1](tier1): True

- [Evil](Evil): 5

- [Research Max](Research): 100

- [Demonology Max](Demonology): 2

- [Lore Max](Lore): 1

- [Lore Rate](Lore): 5%

- [Mana Max](Mana): 1

- [Mana Rate](Mana): 0.01

- [Shadowlore Max](Shadowlore): 1

- [Shadowlore Rate](Shadowlore): 0.1+5%

