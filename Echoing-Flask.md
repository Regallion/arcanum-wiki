**Description**: Amplifies even significantly muffled sounds.

**Level**: 7

**Requirements**: [Potions](Potions) ≥ 5

**Cost to acquire**

- [Herbs](Herbs): 1

- [Air Gem](Air-Gem): 1

**Effect of using**: [Speak](Speak)

