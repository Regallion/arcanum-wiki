**Description**: A sturdy, oaken chest.

**Effects**

- [Frustration](Frustration): 2~4

- [Weariness](Weariness): 1~3

**Loot**

- [Gold](Gold): 0~300

- [Flame Runes](Flame-Runes): 5%

- [Earth Runes](Earth-Runes): 5%

- [Water Runes](Water-Runes): 5%

- [Air Runes](Air-Runes): 5%

- [Tomes](Tomes): 50%

