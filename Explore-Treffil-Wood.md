**Level**: 7

**Length**: 40

**Description**: The woods posses many features of interest to the magical scholar.

**Run Cost**

- [Stamina](Stamina): 0.25

**Result**

- [Arcana](Arcana): 0.1

- [Nature Lore Exp](Nature-Lore): 10

- [Herbalism Exp](Herbalism): 5

- [Animal Handling Exp](Animal-Handling): 5

**Loot**

- [Herbs](Herbs): 5~10

**Encounters**

- [Mana Tree](Mana-Tree)

- [Mystic Waters](Mystic-Waters)

- [Bright Vista](Bright-Vista)

- [Haunted Glade](Haunted-Glade)

- [Babbling Delki](Babbling-Delki)

- [Hidden Cache](Hidden-Cache)

