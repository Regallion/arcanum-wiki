**Level**: 10

**Requirements**: [Potions](Potions) ≥ 10

**Cost to Buy**

- [Gold](Gold): 500

- [Tomes](Tomes): 3

- [Research](Research): 500

**Cost to acquire**

- [Spirit Gem](Spirit-Gem): 1

- [Ichor](Ichor): 1

**Effect of using**

- Dot: {'mod': {'player.tohit': 25}, 'duration': 1800}

