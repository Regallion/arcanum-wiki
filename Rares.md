This page is a default list of the items in this category.<br>
If it doesn't meet your needs, please let a developer (probably 'simple') know, and they'll get around to updating it.<br>

[Crown of Karnivex](Crown-of-Karnivex)<br>
[Gloves of the Botanist](Gloves-of-the-Botanist)<br>
[Icy Dagger](Icy-Dagger)<br>
[Jazid's Compass](Jazid's-Compass)<br>
[Laurel Crown](Laurel-Crown)<br>
[Lillit's Cord](Lillit's-Cord)<br>
[Lily of the Vale](Lily-of-the-Vale)<br>
[Loose Cog](Loose-Cog)<br>
[Orb of Winters](Orb-of-Winters)<br>
[Ring of the Patron](Ring-of-the-Patron)<br>
[Soulflag](Soulflag)<br>
[Spr_Honey_Jar](Spr_Honey_Jar)<br>
[Spr_Jar_Bees](Spr_Jar_Bees)<br>
[Tenwick's Walking Stick](Tenwick's-Walking-Stick)<br>
[Titan's Hammer](Titan's-Hammer)<br>
[Veldran's Earring](Veldran's-Earring)<br>
[Wraps of the Ecdysiast](Wraps-of-the-Ecdysiast)