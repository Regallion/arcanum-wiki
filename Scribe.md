**Tags**: T_Job

**Action Description**: Become your master's personal scribe.

**Requirements**: [Scribe Scroll](Scribe-Scroll)

**Cost to acquire**

- [Gold](Gold): 100

- [Research](Research): 100

- [Arcana](Arcana): 2

**Result**

- Event: [Promotion](evt_helper): True

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Lore Max](Lore): 1.5

- [Arcana Rate](Arcana): 0.0001

- [Research Max](Research): 10

- [World Lore Max](World-Lore): 1

- [Gold Rate](Gold): 0.1

- [Lore Rate](Lore): 0.2

**Flavor**: When I get a little money I buy books, and if any is left I buy food and clothes.

