```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["minions > 4,<br>Lore ≥ 20"])
  t2 ---> t2r ---> summoner
  subgraph tier3 [Tier 3]
    summoner([Summoner])
  end
```
**Tags**: T_Tier3

**Description**: Alone or with friends, a summoner is never without an army.

**Requirements**: Minions > 4 and [Tier 2](tier2) > 0 and [Lore](Lore) ≥ 20

**Cost to acquire**

- [Research](Research): 2000

- [Arcana](Arcana): 20

- [Tomes](Tomes): 5

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 3](tier3): True

- Minions Max: 1

- [Conjuration Max](Conjuration): 2

- [Conjuration Rate](Conjuration): 2%

- [Charms Max](Charms): 1

- [Lore Max](Lore): 2

- [Lore Rate](Lore): 5%

- [Mana Max](Mana): 2

- [Summoning Max](Summoning): 4

- [Summoning Rate](Summoning): 20%

**Flavor**: How you do I; hope you've met my...

