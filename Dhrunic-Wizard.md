```mermaid
graph LR
  t4{Tier 4 Class}
  t4r(["Lore ≥ 30"])
  t4 ---> t4r ---> wizard2
  subgraph tier5 [Tier 5]
    wizard2([Dhrunic Wizard])
  end
```
**Alias**: dhrunic wizard

**Description**: Raw magic power

**Tags**: T_Tier5

**Requirements**: [Tier 4](tier4) > 0 and [Lore](Lore) ≥ 30

**Cost to acquire**

- [Research](Research): 20000

- [Arcana](Arcana): 30

- [Tomes](Tomes): 30

- [Rune Stones](Rune-Stones): 10

**Modifiers**

- [Tier 5](tier5): True

- [Languages Max](Languages): 2

- [Pyromancy Max](Pyromancy): 1

- [Pyromancy Rate](Pyromancy): 1%

- [Water Lore Max](Water-Lore): 1

- [Wind Lore Max](Wind-Lore): 1

- [Wind Lore Rate](Wind-Lore): 1%

- [Lumenology Max](Lumenology): 1

- [Crafting Max](Crafting): 2

- [Lore Max](Lore): 5

- [Lore Rate](Lore): 10%

- [Mana Max](Mana): 3

**Flavor**: In the end it was three Thule Wizards and The Necromancer who brought the locks to heel.

