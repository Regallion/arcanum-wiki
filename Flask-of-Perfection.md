**Description**: Reshapes your body to make it ever so slightly more ideal.

**Level**: 25

**Requirements**: [Potions](Potions) ≥ 999

**Cost to Buy**

- [Gold](Gold): 5000

- [Tomes](Tomes): 50

- [Research](Research): 5000

- [Fire Gem](Fire-Gem): 50

- [Air Gem](Air-Gem): 50

- [Water Gem](Water-Gem): 50

- [Earth Gem](Earth-Gem): 50

**Cost to acquire**

- [Herbs](Herbs): 50

- [Life](Life): 150

- [Research](Research): 15000

- [Gold](Gold): 10000

- [Blood Gem](Blood-Gem): 50

- [Potion Base](Potion-Base): 5

**Effect of using**

- Effect: {'hp.max': 2}

