**Description**: Applied to a weapon, lets the wielder utilize his skills to the fullest extent.

**Level**: 12

**Requirements**: [Potions](Potions) ≥ 10 and ([Bladelore](Bladelore) + [Hammerlore](Hammerlore)) > 5

**Cost to Buy**

- [Gold](Gold): 500

- [Tomes](Tomes): 5

- [Research](Research): 500

**Cost to acquire**

- [Herbs](Herbs): 50

- [Life](Life): 150

- [Earth](Earth): 15

- [Blood Gem](Blood-Gem): 50

- [Potion Base](Potion-Base): 3

**Effect of using**

- Dot: {'mod': {'bladelore.mod.player.bonuses.slash': 0.5, 'bladelore.mod.player.hits.slash': 0.5, 'hammerlore.mod.player.bonuses.blunt': 0.5, 'hammerlore.mod.player.hits.blunt': 0.5}, 'duration': 600}

