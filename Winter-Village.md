**Level**: 2

**Length**: 50

**Requirements**: [Emblem of Ice](Emblem-of-Ice) ≥ 1

**Description**: Now that you command the power of winter, the villagers ask you to help with their preparations

**Run Cost**

- [Stamina](Stamina): 0.4

**Effects**

- [Fire](Fire): 0.2

**Modifiers**

- [Cryomancy Max](Cryomancy): 1%

**Result**

- [Livingsnow](Livingsnow): 10

- [Essence of Winter](Essence-of-Winter): 5

**Loot**

- [Snomunculus](Snomunculus): 0~2

**Encounters**

- [Playing In the Snow](Playing-In-the-Snow)

- [Windbarrier](Windbarrier)

- [Conjuring](Conjuring)

- [Crafting Candles](Crafting-Candles)

- [Stories](Stories)

**Symbol**: ❄️

