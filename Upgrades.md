This page is a default list of the items in this category.<br>
If it doesn't meet your needs, please let a developer (probably 'simple') know, and they'll get around to updating it.<br>

[Abstraction](Abstraction)<br>
[Advanced Gemcraft](Advanced-Gemcraft)<br>
[Airaffinity](Airaffinity)<br>
[Alchemical Formula](Alchemical-Formula)<br>
[Alembic](Alembic)<br>
[Alkahest](Alkahest)<br>
[An Accurate Magical Index](An-Accurate-Magical-Index)<br>
[Apprentice](Apprentice)<br>
[Arcane Body](Arcane-Body)<br>
[Arcane Isolation Chamber](Arcane-Isolation-Chamber)<br>
[Arcane Orb](Arcane-Orb)<br>
[Armillary Sphere](Armillary-Sphere)<br>
[Automata Agents](Automata-Agents)<br>
[Automata Mastery](Automata-Mastery)<br>
[Automata Studies](Automata-Studies)<br>
[Automated Appraiser](Automated-Appraiser)<br>
[Automated Laboratory](Automated-Laboratory)<br>
[Automated Miner](Automated-Miner)<br>
[Autonomic Mending](Autonomic-Mending)<br>
[Bag of Holding](Bag-of-Holding)<br>
[Bag of Tricks](Bag-of-Tricks)<br>
[Basement](Basement)<br>
[Battered Ledger](Battered-Ledger)<br>
[Beeswax Candle](Beeswax-Candle)<br>
[Bone Collector](Bone-Collector)<br>
[Bottomless Pitcher](Bottomless-Pitcher)<br>
[Breakthrough](Breakthrough)<br>
[Breviary](Breviary)<br>
[Card Deck](Card-Deck)<br>
[Casgair's Conjured Canopy](Casgair's-Conjured-Canopy)<br>
[Cat Familiar](Cat-Familiar)<br>
[Cauldron](Cauldron)<br>
[Ceaseless Bellows](Ceaseless-Bellows)<br>
[Celerity](Celerity)<br>
[Celestial Globe](Celestial-Globe)<br>
[Cellar](Cellar)<br>
[Chakravarti](Chakravarti)<br>
[Chariot of Fire](Chariot-of-Fire)<br>
[Cheerful Recording](Cheerful-Recording)<br>
[Chirography](Chirography)<br>
[Clockwork Butler](Clockwork-Butler)<br>
[Clockwork Expansion](Clockwork-Expansion)<br>
[Clockwork Expansion](Clockwork-Expansion)<br>
[Clockwork Expansion](Clockwork-Expansion)<br>
[Coin Tricks](Coin-Tricks)<br>
[Cold Calculation](Cold-Calculation)<br>
[Cooking Pot](Cooking-Pot)<br>
[Core Tap](Core-Tap)<br>
[Crystal Ball](Crystal-Ball)<br>
[Crystal Mind](Crystal-Mind)<br>
[Crystal Solidifier](Crystal-Solidifier)<br>
[Deep Incisions](Deep-Incisions)<br>
[Deep Pockets](Deep-Pockets)<br>
[Demonic Pact](Demonic-Pact)<br>
[Demure Gelding](Demure-Gelding)<br>
[Dew of Icy Fire](Dew-of-Icy-Fire)<br>
[Discarded Missives](Discarded-Missives)<br>
[Disperse Wards](Disperse-Wards)<br>
[Disperse Wards](Disperse-Wards)<br>
[Donate Copy](Donate-Copy)<br>
[Dragon Tongue](Dragon-Tongue)<br>
[Dream Catcher](Dream-Catcher)<br>
[Earth Sign](Earth-Sign)<br>
[Earth Soul](Earth-Soul)<br>
[Earth Werry](Earth-Werry)<br>
[Earthaffinity](Earthaffinity)<br>
[Earthworks](Earthworks)<br>
[Ebonwood Broomstick](Ebonwood-Broomstick)<br>
[Ebonwood Supply](Ebonwood-Supply)<br>
[Efficient Algorithms](Efficient-Algorithms)<br>
[Efficient Rituals](Efficient-Rituals)<br>
[Elemental Rituals](Elemental-Rituals)<br>
[End Apprenticeship](End-Apprenticeship)<br>
[Endless Expansion](Endless-Expansion)<br>
[Energy Transfusion](Energy-Transfusion)<br>
[Enhanced Elemental Rituals](Enhanced-Elemental-Rituals)<br>
[Evanescence](Evanescence)<br>
[Expanded Cellars](Expanded-Cellars)<br>
[Expanded Hearth](Expanded-Hearth)<br>
[Expansive Lungs](Expansive-Lungs)<br>
[Fatalist](Fatalist)<br>
[Favored Pursuit](Favored-Pursuit)<br>
[Fine Bay](Fine-Bay)<br>
[Fine Crucible](Fine-Crucible)<br>
[Fire Charger](Fire-Charger)<br>
[Fire Sign](Fire-Sign)<br>
[Fire Werry](Fire-Werry)<br>
[Fireaffinity](Fireaffinity)<br>
[Fly](Fly)<br>
[Flying Carpet](Flying-Carpet)<br>
[Forewarned](Forewarned)<br>
[Four Leaf Clover](Four-Leaf-Clover)<br>
[Gem Box](Gem-Box)<br>
[Gemcraft](Gemcraft)<br>
[Ghost Chains](Ghost-Chains)<br>
[Ghost Compiler](Ghost-Compiler)<br>
[Goblin Market](Goblin-Market)<br>
[Greater Arcane Body](Greater-Arcane-Body)<br>
[Grimoire](Grimoire)<br>
[Gryffon](Gryffon)<br>
[Guiding Star](Guiding-Star)<br>
[Heart Candle](Heart-Candle)<br>
[Heart of Stone](Heart-of-Stone)<br>
[Herb Pouch](Herb-Pouch)<br>
[Hidden Chamber](Hidden-Chamber)<br>
[Hiddenpockets](Hiddenpockets)<br>
[Holy Candle](Holy-Candle)<br>
[Ice Mastery](Ice-Mastery)<br>
[Ice Proficiency](Ice-Proficiency)<br>
[Imp Familiar](Imp-Familiar)<br>
[Improve Ice Shard](Improve-Ice-Shard)<br>
[Infused Body](Infused-Body)<br>
[Inner Fire](Inner-Fire)<br>
[Inner Light](Inner-Light)<br>
[Inner Sanctum](Inner-Sanctum)<br>
[Kobold Familiar](Kobold-Familiar)<br>
[Letter of Request](Letter-of-Request)<br>
[Long-Stem Pipe](Long-Stem-Pipe)<br>
[Loremaster's Triumph](Loremaster's-Triumph)<br>
[Magic Broomstick](Magic-Broomstick)<br>
[Magic Horseshoes](Magic-Horseshoes)<br>
[Magic Quarry](Magic-Quarry)<br>
[Magic Skull](Magic-Skull)<br>
[Master of Winter](Master-of-Winter)<br>
[Mastery of Form](Mastery-of-Form)<br>
[Membranous Pouch](Membranous-Pouch)<br>
[Mendicant](Mendicant)<br>
[Metronome](Metronome)<br>
[Minerals Collection](Minerals-Collection)<br>
[Morbid Trinkets](Morbid-Trinkets)<br>
[Mortar And Pestle](Mortar-And-Pestle)<br>
[Mule](Mule)<br>
[Mysterious Sword Arts](Mysterious-Sword-Arts)<br>
[Mythic Anvil](Mythic-Anvil)<br>
[Occult Endurance](Occult-Endurance)<br>
[Odd Spellchart](Odd-Spellchart)<br>
[Oil of Slipping](Oil-of-Slipping)<br>
[Old Forms](Old-Forms)<br>
[Old Nag](Old-Nag)<br>
[Oracular Insight](Oracular-Insight)<br>
[Owl Familiar](Owl-Familiar)<br>
[Pegasus](Pegasus)<br>
[Pendulum](Pendulum)<br>
[Poison Affinity](Poison-Affinity)<br>
[Poison Veins](Poison-Veins)<br>
[Portal of Trees](Portal-of-Trees)<br>
[Pouch](Pouch)<br>
[Powerful Rituals](Powerful-Rituals)<br>
[Precision Engineering](Precision-Engineering)<br>
[Preparation](Preparation)<br>
[Prism](Prism)<br>
[Proxy Amplification](Proxy-Amplification)<br>
[Puppet Proxies](Puppet-Proxies)<br>
[Puppet Spies](Puppet-Spies)<br>
[Puppet Strings](Puppet-Strings)<br>
[Purse](Purse)<br>
[Queen Mab's Carriage](Queen-Mab's-Carriage)<br>
[Raise Mountain](Raise-Mountain)<br>
[Refinement Techniques](Refinement-Techniques)<br>
[Reinforced Folders](Reinforced-Folders)<br>
[Restless Nights](Restless-Nights)<br>
[Rune Pouch](Rune-Pouch)<br>
[Runecrafter](Runecrafter)<br>
[Runic Reinforcement](Runic-Reinforcement)<br>
[Satchel](Satchel)<br>
[Siege Perilous](Siege-Perilous)<br>
[Siver's Gate](Siver's-Gate)<br>
[Skeletal Charger](Skeletal-Charger)<br>
[Slay Master](Slay-Master)<br>
[Somber Candle](Somber-Candle)<br>
[Somnambulist](Somnambulist)<br>
[Speak To Birds](Speak-To-Birds)<br>
[Specialization](Specialization)<br>
[Spillthrough Ink](Spillthrough-Ink)<br>
[Study Ritual Notes](Study-Ritual-Notes)<br>
[Subterranean Pathways](Subterranean-Pathways)<br>
[Swirling Globe](Swirling-Globe)<br>
[Tamed Pathway](Tamed-Pathway)<br>
[Tenwick's Travel Map](Tenwick's-Travel-Map)<br>
[The Five Seals](The-Five-Seals)<br>
[The Purpose](The-Purpose)<br>
[Third Eye](Third-Eye)<br>
[Three-Sided Coin](Three-Sided-Coin)<br>
[Time Spiral](Time-Spiral)<br>
[Timesplit](Timesplit)<br>
[Tome Familiar](Tome-Familiar)<br>
[Touching the Void](Touching-the-Void)<br>
[Transfusions](Transfusions)<br>
[Transportation Sigils](Transportation-Sigils)<br>
[Travel Map](Travel-Map)<br>
[Travel Pack](Travel-Pack)<br>
[Travel Tent](Travel-Tent)<br>
[Truesight](Truesight)<br>
[Two-Way Portal](Two-Way-Portal)<br>
[Unholy Candle](Unholy-Candle)<br>
[Unholy Specimens](Unholy-Specimens)<br>
[Unseen Scribe](Unseen-Scribe)<br>
[Urn](Urn)<br>
[Velvet Bag](Velvet-Bag)<br>
[Water Breathing](Water-Breathing)<br>
[Water Sign](Water-Sign)<br>
[Water Werry](Water-Werry)<br>
[Wateraffinity](Wateraffinity)<br>
[Wax Candle](Wax-Candle)<br>
[Wind Sign](Wind-Sign)<br>
[Wind Werry](Wind-Werry)<br>
[Windchime](Windchime)<br>
[Winter Fog](Winter-Fog)<br>
[Winter Fog](Winter-Fog)<br>
[Winter's Boon](Winter's-Boon)<br>
[Winter's Breath](Winter's-Breath)<br>
[Winter's Chill](Winter's-Chill)<br>
[Winter's Envoy](Winter's-Envoy)<br>
[Winter's Howl](Winter's-Howl)<br>
[Winter's Reward](Winter's-Reward)<br>
[Wishing Wells](Wishing-Wells)<br>
[Withered Designs](Withered-Designs)<br>
[Wizard's Hall](Wizard's-Hall)<br>
[Wood Pipe](Wood-Pipe)<br>
[Workforce](Workforce)