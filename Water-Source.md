**Description**: Some of the ice here is melting. How it fights the cold is a mystery.

**Effects**

- [Madness](Madness): 1~2

- [Frost](Frost): -1~0

- [Water](Water): 0.2~0.3

- [Cryomancy Max](Cryomancy): 0.01

**Symbol**: ❄️

