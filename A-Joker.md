**Description**: A crafty expression suggests trouble.

**Effects**

- [Frustration](Frustration): 1~5

- [Befuddlement](Befuddlement): 2~5

- [Trickery Exp](Trickery): 2

- Player Exp: 1

**Symbol**: ⭐

