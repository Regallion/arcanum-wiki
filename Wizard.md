```mermaid
graph LR
  t3{Tier 3 Class}
  t3r(["Vile ≤ 0,<br>Lore ≥ 25"])
  t3 ---> t3r ---> wizard
  subgraph tier4 [Tier 4]
    wizard([Wizard])
  end
```
**Description**: Raw magic power

**Tags**: T_Tier4

**Requirements**: Event: [Vile](evil) ≤ 0 and [Tier 3](tier3) > 0 and [Lore](Lore) ≥ 25

**Cost to acquire**

- [Research](Research): 5000

- [Arcana](Arcana): 25

- [Tomes](Tomes): 20

- [Rune Stones](Rune-Stones): 5

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 4](tier4): True

- [Languages Max](Languages): 2

- [Conjuration Max](Conjuration): 1

- [Crafting Max](Crafting): 1

- [Lore Max](Lore): 3

- [Spellcraft Max](Spellcraft): 3

- [Spellcraft Rate](Spellcraft): 0.5

- [Lore Rate](Lore): 10%

- [Mana Max](Mana): 3

**Flavor**: 'Do not take me for some conjurer of cheap tricks.'

