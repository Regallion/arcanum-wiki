**Description**: Extended travel beneath the hot sun produces visceral visions in the sand.

**Effects**

- [Madness](Madness): 1~3

- [Unease](Unease): 0~2

- [Befuddlement](Befuddlement): 0~2

- [Lumenology Exp](Lumenology): 1

**Result**

- [Lumenology Max](Lumenology): 0.0001

