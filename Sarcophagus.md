**Description**: Ancient coffin marked by faded runes

**Level**: 4

**Effects**

- [Unease](Unease): 0~2

- [Madness](Madness): 0~2

- [Reanimation Exp](Reanimation): 2

- [Embalming Exp](Embalming): 2

**Loot**

- [Rune Stones](Rune-Stones): 10%

- [Shadow Gem](Shadow-Gem): 30%

- [Spirit Gem](Spirit-Gem): 30%

- [Bodies](Bodies): 50%

