**Description**: A solid iron chest.

**Effects**

- [Befuddlement](Befuddlement): -1~1

- [Frustration](Frustration): 0~1

**Loot**

- [Gold](Gold): 5~14

- [Codices](Codices): 0~2

- [Scrolls](Scrolls): 2~4

