**Description**: A crushed, mangled corpse of a critter lies in the fields, cut apart by an uncaring plow.

**Effects**

- [Unease](Unease): 1~2

- [Frustration](Frustration): 2

- [Animal Handling Exp](Animal-Handling): 2

- [Composure Exp](Composure): 2

**Symbol**: None

