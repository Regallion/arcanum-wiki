```mermaid
graph LR
  t4{Tier 4 Class}
  t4r(["Magiphysics ≥ 5,<br>Mechanical Magic ≥ 9,<br>(Puppeteer + Mechanist) > 0"])
  t4 ---> t4r ---> mechamancer
  subgraph tier5 [Tier 5]
    mechamancer([Mechamancer])
  end
```
**Description**: Master of magical constructs

**Tags**: T_Tier5

**Requirements**: [Tier 4](tier4) > 0 and [Magiphysics](Magiphysics) ≥ 5 and [Mechanical Magic](Mechanical-Magic) ≥ 9 and ([Puppeteer](Puppeteer) + [Mechanist](Mechanist)) > 0 and [Tier 5](tier5) = 0

**Cost to acquire**

- [Research](Research): 20000

- [Arcana](Arcana): 27

- [Tomes](Tomes): 30

- [Rune Stones](Rune-Stones): 30

**Modifiers**

- [Tier 5](tier5): True

- [Puppetry Max](Puppetry): 1

- [Animation Max](Animation): 1

- [Mechanical Magic Max](Mechanical-Magic): 2

- [Automata Sculpting Max](Automata-Sculpting): 3

- Minions Keep: automata

- Allies Max: 5

- [Clockwork Home Space Max](Clockwork-Home): 150

**Flavor**: If you want a better future, make better people.

