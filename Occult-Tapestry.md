**Level**: 2

**Description**: A tapestry with strange, hidden meanings

**Effects**

- [Befuddlement](Befuddlement): -1~2

- [Frustration](Frustration): 0~2

**Result**

- [Research](Research): 1

- [Arcana](Arcana): 0.005

