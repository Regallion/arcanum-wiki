**Description**: Steel-sharp blades of grass bite into your boots.

**Effects**

- [Weariness](Weariness): 1~3

- [Frustration](Frustration): 1~3

- [Life](Life): -2

