**Level**: 12

**Requirements**: [Potions](Potions) ≥ 10

**Cost to Buy**

- [Gold](Gold): 500

- [Tomes](Tomes): 5

- [Research](Research): 500

**Cost to acquire**

- [Herbs](Herbs): 50

- [Ichor](Ichor): 1

**Effect of using**

- Dot: {'mod': {'hp.max': 300}, 'duration': 3600}

