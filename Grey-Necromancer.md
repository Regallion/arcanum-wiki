```mermaid
graph LR
  t4{Tier 4 Class}
  t4r(["Vile > 0,<br>Spirit Lore ≥ 15,<br>Shadowlore ≥ 24"])
  t4 ---> t4r ---> greynecromancer
  subgraph tier5 [Tier 5]
    greynecromancer([Grey Necromancer])
  end
```
**Tags**: T_Tier5

**Description**: Disciple of death

**Requirements**: [Tier 4](tier4) > 0 and Event: [Vile](evil) > 0 and [Spirit Lore](Spirit-Lore) ≥ 15 and [Shadowlore](Shadowlore) ≥ 24

**Cost to acquire**

- [Research](Research): 17000

- [Bones](Bones): 100

- [Bodies](Bodies): 50

- [Souls](Souls): 100

- [Arcana](Arcana): 10

- [Rune Stones](Rune-Stones): 10

**Modifiers**

- [Tier 5](tier5): True

- [Lore Max](Lore): 2

- [Lore Rate](Lore): 10%

- [Embalming Max](Embalming): 2

- [Necromancy Max](Necromancy): 3

- [Necromancy Rate](Necromancy): 10%

- [Reanimation Max](Reanimation): 3

- [Reanimation Rate](Reanimation): 10%

- [Shadowlore Max](Shadowlore): 2

- [Shadowlore Rate](Shadowlore): 5%

- [Spirit Lore Max](Spirit-Lore): 2

- [Spirit Lore Rate](Spirit-Lore): 10%

