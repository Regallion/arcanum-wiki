**Description**: An old hand at astronomical calculations, Pidwig shares his extensive observations.

**Effects**

- [Befuddlement](Befuddlement): 1~3

- [Unease](Unease): -1~2

- [Madness](Madness): -1~1

- [Astronomy Exp](Astronomy): 3

- [Planes Lore Exp](Planes-Lore): 1

- [Scrying Exp](Scrying): 1

**Loot**: [Starcharts](Starcharts)

