**Level**: 2

**Description**: A tree by a magic spring exhibits surprising qualities

**Effects**

- [Befuddlement](Befuddlement): -1~2

- [Nature Lore Exp](Nature-Lore): 2

- [Herbalism Exp](Herbalism): 1

**Result**

- [Arcane Gem](Arcane-Gem): 25%

