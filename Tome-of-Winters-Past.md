**Description**: The year's coldest season is also known for its dark legends.

**Effects**

- [Befuddlement](Befuddlement): 1~2

- [Frustration](Frustration): 0~3

- [Winter Lore Rate](Winter-Lore): 0.04

**Loot**

- [Essence of Winter](Essence-of-Winter): 0.1

- [Codices](Codices): 80%

- Event: [A Cold Legend](evt_w_chillforge): 15%

**Symbol**: ❄️

