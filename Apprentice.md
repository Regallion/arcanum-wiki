**Description**: Apprentice to a notable wizard

**Action Name**: Apprenticeship

**Action Description**: Become an apprentice.

**Requirements**: [Research](Research)

**Class disables**

- [Clean Stables](Clean-Stables)

**Pop-up Warning**: False

**Log Entry**

- Name: Apprentice

- Desc: After paying a small fee, you became apprenticed to a local wizard.

**Cost to acquire**

- [Research](Research): 10

- [Gold](Gold): 20

**Result**

- [Research Max](Research): 10

- Player Exp: 5

- [Skill Points](Skill-Points): 1

