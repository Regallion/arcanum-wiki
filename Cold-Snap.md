**Description**: A sudden burst of icy winds chills you to the bone.

**Effects**

- [Weariness](Weariness): -1~3

- [Unease](Unease): -1~3

- [Frost](Frost): 1~2

- [Cryomancy Exp](Cryomancy): 1.5

- [Life](Life): -2~-4

- [Fire](Fire): -0.2~-0.3

**Symbol**: ❄️

