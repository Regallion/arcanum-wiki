**Description**: Building a barrier against the harsh winds

**Effects**

- [Weariness](Weariness): 1~2

- [Wind Lore Exp](Wind-Lore): 2

**Level**: 2

**Result**

- [Essence of Winter](Essence-of-Winter): 0.1

**Symbol**: ❄️

