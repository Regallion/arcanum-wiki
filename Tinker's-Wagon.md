**Description**: Tinker wagons from distant lands creak along the roads, to trade their wares at the fair.

**Effects**

- [Frustration](Frustration): 1~3

**Symbol**: 🌼

