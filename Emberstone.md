**Description**: Some latent heat springs here from the ground.

**Effects**

- [Madness](Madness): 1~2

- [Life](Life): -1~-2

- [Frost](Frost): -2~-3

- [Fire](Fire): 0.2~0.3

**Symbol**: ❄️

