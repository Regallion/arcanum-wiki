|Name|Level|HP|Defense<br>Bonus|Regen|To Hit<br>Bonus|Speed<br>Bonus|Unique|Attack|
|:---|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| [Abholos](home/Monsters/Abholos)<br>*A grey mass of pustulous malevolence. (Devourer in the Mist)*|30|None|None|None|None|None|No||
| [Adamant Golem](home/Monsters/Adamant-Golem)<br>*Unbreakable*|20|450|130|None|15|None|No|Smash: 80~100 blunt damage|
| [Air Elemental](home/Monsters/Air-Elemental)<br>*Wispy.*|8|13~22|48|None|13|None|No|Air Whip: 5~12 slash damage|
| [Alala](home/Monsters/Alala)<br>*Entity of living sound.*|30|None|None|None|None|None|No||
| [Ancient Lich](home/Monsters/Ancient-Lich)<br>*Not one lich in a hundred live to reach such heights.*|25|550|120|15|27|None|No|Death Touch: 80~120 shadow damage|
| [Ancient Vampire](home/Monsters/Ancient-Vampire)|17|150|75|7|25|None|No|Bite: 30~50 shadow damage|
| [Angel](home/Monsters/Angel)<br>*It has wings and a halo of light.*|14|170|70|None|10|None|No|Light Sword: 20~50 light damage|
| [Angry Bird](home/Monsters/Angry-Bird)|2|2|1|None|4|3|No|Peck: 0~2 pierce damage|
| [Arcane Serpent](home/Monsters/Arcane-Serpent)<br>*Fiercely loyal when summoned by magic.*|6|50|20|2|9|None|No||
| [Archon](home/Monsters/Archon)<br>*It has wings and a halo of light.*|19|270|90|10|23|None|No|Light Sword: 70~120 light damage|
| [Autocaster](home/Monsters/Autocaster)|30|500|100|10|20|None|No||
| [Autoslayer](home/Monsters/Autoslayer)|30|700|50|None|15|None|No||
| [Avatar](home/Monsters/Avatar)<br>*Incarnation of a god*|23|700|120|20|30|None|No|Holy Shine: 75~130 light damage|
| [Azathoth](home/Monsters/Azathoth)<br>*The blind idiot god at the center of infinity.*|100|None|None|None|None|None|No||
| [Balrog](home/Monsters/Balrog)<br>*A servant of Morgoth.*|18|200|80|None|17|None|No|Flame Whip: 90~150 fire damage|
| [Bandit Lord](home/Monsters/Bandit-Lord)<br>*Wants to steal your stuff.*|6|23|17|None|8|None|No|Short Sword: 5~9 slash damage|
| [Bandit](home/Monsters/Bandit)<br>*Wants to steal your stuff.*|5|12|15|None|6|None|No|Short Sword: 4~7 slash damage|
| [Barracuda](home/Monsters/Barracuda)<br>*It's bitey.*|5|12|18|None|7|6|No|Bite: 3~5 pierce damage|
| [Basilisk](home/Monsters/Basilisk)<br>*A large lizard with a petrifying bite.*|11|64|32|None|5|8|No||
| [Bat](home/Monsters/Bat)|1|3|1|None|3|None|No|Bite: 1~2 pierce damage|
| [Bear](home/Monsters/Bear)<br>*He's angry now!!!*|7|55|30|None|8|None|No|Claws: 5~14 slash damage|
| [Behemoth](home/Monsters/Behemoth)|15|325|75|1|15|None|No|Trample: 40~70 blunt damage|
| [Beholder](home/Monsters/Beholder)<br>*It seeeeees you.*|14|119|50|None|12|None|No||
| [Beserker](home/Monsters/Beserker)|7|25|29|None|10|None|No|Claymore: 3~10 slash damage|
| [Big Stone](home/Monsters/Big-Stone)|0|5|2|None|0|-20|No||
| [Blasphemer](home/Monsters/Blasphemer)<br>*She screams black obscenities.*|7|75|40|None|7|None|No|Screech: 5~9 evil damage|
| [Bloated Corpse](home/Monsters/Bloated-Corpse)|4|13|14|None|2|None|No|Bite: 2~3 physical damage|
| [Blood Golem](home/Monsters/Blood-Golem)<br>*A creature of pure blood.*|12|90|45|None|None|None|No|Slam: 15~20 blunt damage|
| [Bodias the Grim](home/Monsters/Bodias-the-Grim)|25|2000|150|None|50|None|Yes||
| [Bog Witch](home/Monsters/Bog-Witch)<br>*Vicious witch in the waters of the Great Bog*|13|500|48|2|14|None|Yes|Black Blood: 1~4 poison damage for 20 seconds; Claws: 12~20 slash damage|
| [Brittelwort the Watcher](home/Monsters/Brittelwort-the-Watcher)<br>*The silent Brittelwort watches in the Felkill mountains, punishing all transgressions.*|27|4000|100|None|34|None|Yes||
| [Bugbear](home/Monsters/Bugbear)|6|43|25|None|2|None|No|Mace: 5~12 blunt damage|
| [Burlap Golem](home/Monsters/Burlap-Golem)<br>*Woven golem filled with heavy sand*|14|400|None|None|None|None|No|Sand Punch: 20~30 earth damage|
| [Callodiper](home/Monsters/Callodiper)<br>*Their globed hands glow with pale, unearthly light.*|5|40|None|None|None|None|No|Light Burst: 3~7 light damage|
| [Cave Troll](home/Monsters/Cave-Troll)|9|90|35|2|None|None|No|Club: 15~27 blunt damage|
| [Clay Golem](home/Monsters/Clay-Golem)<br>*I made you out of clay...*|8|65|33|None|-1|None|No|Smash: 2~7 blunt damage|
| [Cockatrice](home/Monsters/Cockatrice)<br>*The stare of the cockatrice is death.*|15|300|None|None|None|8|No||
| [Cordyceps](home/Monsters/Cordyceps)<br>*A writhing fungus chokes the air with spores.*|13|65|80|None|None|None|No||
| [Coyote](home/Monsters/Coyote)|3|4|2|None|2|None|No|Claws: 2~3 slash damage|
| [Crocodile](home/Monsters/Crocodile)|5|14|20|None|3|None|No|Bite: 4~7 pierce damage|
| [Cthulhu](home/Monsters/Cthulhu)<br>*In his house at R'lyeh dead Cthulhu waits dreaming.*|25|1050|125|5|25|None|Yes|Madness: 70~125 psychic damage|
| [Cutpurse](home/Monsters/Cutpurse)<br>*Wants to steal your stuff.*|4|7|7|None|4|6|No|Dagger: 2~5 pierce damage|
| [Cyclops](home/Monsters/Cyclops)|14|170|65|None|10|None|No|Punch: 17~50 blunt damage|
| [Demon](home/Monsters/Demon)|14|145|65|2|None|None|No|Darksword: 15~40 evil damage|
| [Desilla the Vicious](home/Monsters/Desilla-the-Vicious)<br>*Among the Archlocks, desilla was more inclined to indulgence than conquest.*|20|1500|133|None|27|None|Yes||
| [Desilla's Concubine](home/Monsters/Desilla's-Concubine)<br>*A devoted follower of the sorceress Desilla.*|11|90|50|None|20|None|No|Claws: 12~22 slash damage|
| [Dhrozenknight](home/Monsters/Dhrozenknight)<br>*An immortal knight of Dhroz. Of fifty slain, one is knighted.*|26|1200|None|None|None|None|No|Nether Blade: 130~150 nether damage|
| [Dire Bat](home/Monsters/Dire-Bat)<br>*That's one large bat.*|4|8|10|None|5|None|No|Bite: 3~7 pierce damage|
| [Dire Bear](home/Monsters/Dire-Bear)<br>*He's angrier now!!!*|8|85|30|None|11|None|No|Claws: 17~27 slash damage|
| [Dire Crocodile](home/Monsters/Dire-Crocodile)|8|30|32|None|5|None|No|Bite: 7~15 pierce damage|
| [Dire Rat](home/Monsters/Dire-Rat)<br>*Red eyes... that's different.*|5|11|9|None|3|None|No|Bite: 1~6 pierce damage|
| [Dire Wolf](home/Monsters/Dire-Wolf)|5|17|20|None|7|None|No|Bite: 3~6 pierce damage|
| [Doppelganger](home/Monsters/Doppelganger)<br>*Such a good-looking fellow!*|6|37|32|None|8|None|No|Slam: 3~8 blunt damage|
| [Druid](home/Monsters/Druid)<br>*Ardent of nature*|7|27|25|None|7|None|No|Spellstorm: 9~17 nature damage|
| [Drunken Boxer](home/Monsters/Drunken-Boxer)<br>*This monk has had a bit too much to drink...*|6|15|29|None|7|None|No|Fists: 4~8 blunt damage|
| [Drunken Master](home/Monsters/Drunken-Master)<br>*Master Wong Fei-Hung.*|10|53|44|None|10|None|No|Fists: 7~17 blunt damage|
| [Dullahan](home/Monsters/Dullahan)<br>*It doesn't have a head. But you do.*|8|55|45|None|None|None|No|Vorpal Sword: 7~15 slash damage|
| [Dwarp](home/Monsters/Dwarp)<br>*Its contorted purple body can hide in the smallest crevices.*|7|35|20|None|9|9|No|Strangle: 3~7 blunt damage|
| [Earth Elemental](home/Monsters/Earth-Elemental)<br>*How do you fight dirt?*|8|58|30|None|5|None|No|Pound: 8~12 blunt damage|
| [Eel](home/Monsters/Eel)<br>*So wriggly.*|3|3|12|None|3|None|No|Bite: 1~4 pierce damage|
| [Electric Eel](home/Monsters/Electric-Eel)<br>*So wriggly.*|5|5|15|None|3|7|No|Zap: 2~4 pierce damage|
| [Eledin Strongbow](home/Monsters/Eledin-Strongbow)<br>*When Tenwick told Eledin the Ettinmoors couldn't be crossed in safety, the ranger picked up his bow and headed east.*|12|140|24|None|None|None|Yes|Greatbow: 10~15 pierce damage|
| [Elf](home/Monsters/Elf)<br>*So lithe. So graceful.*|4|8|10|None|1|None|No||
| [Ent](home/Monsters/Ent)<br>*Out cruising for some ent-wives.*|13|138|50|3|2|None|No|Pound: 20~30 blunt damage|
| [Ettin](home/Monsters/Ettin)<br>*Two heads are better than one.*|10|65|30|5|11|None|No|Spiked Club: 7~13 blunt damage|
| [Evil Priest](home/Monsters/Evil-Priest)<br>*He worships evil.*|6|45|32|None|5|None|No|Harm: 5~8 evil damage|
| [Failed Experiment](home/Monsters/Failed-Experiment)<br>*Apparently some sort of long lost arcane experiment, with a mind of its own. It doesn't look friendly.*|3|4|5|None|2|None|No|Psychic Blast: 1~2 mana damage|
| [Fetigern](home/Monsters/Fetigern)<br>*The terror before the Wind Age*|30|9000|400|30|None|None|Yes|Bite: 100~200 poison damage|
| [Fire Elemental](home/Monsters/Fire-Elemental)<br>*Too hot to get close to.*|8|40|55|None|10|None|No|Scorch: 5~15 fire damage|
| [Fire Golem](home/Monsters/Fire-Golem)<br>*Crafted from only the finest phlogiston.*|11|100|53|None|8|None|No|Fire Punch: 15~23 fire damage|
| [Frixie](home/Monsters/Frixie)<br>*A nasty fey of cold disposition.*|7|35|17|None|10|12|No|Frost Bolt: 4~17 water damage|
| [Frost Giant](home/Monsters/Frost-Giant)|16|160|65|None|17|None|No|Punch: 40~60 cold damage|
| [Garden Gnome](home/Monsters/Garden-Gnome)<br>*Standard variety garden gnome.*|1|3~6|4|None|2|None|No|Dot: wink|
| [Gem Cutter](home/Monsters/Gem-Cutter)|18|400|None|None|None|None|No||
| [Ghast](home/Monsters/Ghast)|7|42|26|2|3|None|No|Claws: 7~13 slash damage|
| [Ghatanothoa](home/Monsters/Ghatanothoa)|30|None|None|None|None|None|No||
| [Ghost](home/Monsters/Ghost)|6|20|30|None|7|None|No|Death Touch: 3~9 drain damage|
| [Ghoul](home/Monsters/Ghoul)|5|21|18|1|2|None|No|Claws: 3~6 slash damage|
| [Giant Centipede](home/Monsters/Giant-Centipede)<br>*Creepy crawly*|5|16|15|None|None|None|No|Sting: 5~8 pierce damage|
| [Giant Snake](home/Monsters/Giant-Snake)|6|25|20|None|7|None|No|Constrict: 8~17 blunt damage|
| [Giant Spider](home/Monsters/Giant-Spider)|5|9|5|None|5|None|No|Bite: 4~7 pierce damage|
| [Gildella the Lighthearted](home/Monsters/Gildella-the-Lighthearted)<br>*The elf Gildella had roamed the Ettinmoors for centuries when Eledin first crossed her path. Their path has been the same since then.*|12|120|24|None|None|17|Yes|Greatbow: 10~15 pierce damage|
| [Gobchamp](home/Monsters/Gobchamp)|6|14|16|None|7|None|No||
| [Gobchief](home/Monsters/Gobchief)|5|11|13|None|6|None|No||
| [Gobking](home/Monsters/Gobking)|7|18|23|None|8|None|No|Magic Missle: 5~9 mana damage|
| [Goblin Warrior](home/Monsters/Goblin-Warrior)|3|8|7|None|5|None|No|Shortsword: 2~3 pierce damage|
| [Goblin](home/Monsters/Goblin)|2|4|5|None|None|None|No|Dagger: 1~2 pierce damage|
| [Gobpriest](home/Monsters/Gobpriest)|4|5|5|None|5|None|No|Club: 2~4 blunt damage|
| [Gol-Goroth the Forgotten](home/Monsters/Gol-Goroth-the-Forgotten)|30|None|None|None|None|None|No||
| [Greater Basilisk](home/Monsters/Greater-Basilisk)<br>*A massive lizard with a petrifying bite.*|17|300|None|None|5|None|No||
| [Greater Zombie](home/Monsters/Greater-Zombie)<br>*It ate up all the smaller zombies.*|7|42|22|1|5|None|No|Slam: 7~12 blunt damage|
| [Green Dragon](home/Monsters/Green-Dragon)<br>*Just your standard green dragon.*|14|145|55|None|10|None|No||
| [Gremlin](home/Monsters/Gremlin)|1|3~5|2|0.25|None|None|No|Claws: 1 slash damage|
| [Grimstalk](home/Monsters/Grimstalk)<br>*A wooden demon with fingers like blades.*|11|53|35|7|15|None|No|Claws: 12~28 slash damage|
| [Gryffon](home/Monsters/Gryffon)<br>*Body of a lion. Face of a giant eagle.*|12|123|40|None|5|None|No|Beak: 21~27 pierce damage|
| [Guild Thug](home/Monsters/Guild-Thug)<br>*The guild's policies on unauthorized pouch sales are strictly enforced.*|10|70|None|None|None|None|No|Sand Bag: 10~20 blunt damage|
| [Harpy](home/Monsters/Harpy)<br>*She doesn't look harpy to see you.*|6|17|22|None|9|None|No|Claws: 4~7 slash damage|
| [Hastur](home/Monsters/Hastur)|30|None|None|None|None|None|No||
| [Hawk](home/Monsters/Hawk)|3|3|2|None|5|None|No|Claws: 2.2~4.2 slash damage|
| [High Elf](home/Monsters/High-Elf)<br>*Other elves are as newborns to their elder kin.*|9|80|70|None|15|15|No||
| [Hill Giant](home/Monsters/Hill-Giant)<br>*You've seen bigger.*|11|120|45|None|2|None|No|Club: 15~24 blunt damage|
| [Hobbit Slinger](home/Monsters/Hobbit-Slinger)|5|7|5|None|8|None|No|Sling: 2~5 blunt damage|
| [Hobgoblin](home/Monsters/Hobgoblin)|5|15|17|None|2|None|No|Dagger: 1~5 pierce damage|
| [Homunculus](home/Monsters/Homunculus)<br>*Must have escaped its jar.*|1|10|5|None|1|None|No|Nasty Bite: 0~2 pierce damage|
| [Ice Elemental](home/Monsters/Ice-Elemental)<br>*What's so scary about a bunch of icicles?*|7|72|35|None|9|None|No|Ice Shard: 8~15 cold damage|
| [Icewalker](home/Monsters/Icewalker)<br>*They say those who die on the cold heights search the passes for mortals to join them.*|4|15|14|None|None|3|No|Frozen Claws: 3~6 cold damage|
| [Idh-Yaa the Worm](home/Monsters/Idh-Yaa-the-Worm)<br>*First bride of Cthulhu*|30|None|None|None|None|None|No||
| [Imp](home/Monsters/Imp)<br>*Little demonic bastards*|4|17|30|1|None|None|No||
| [Incubus](home/Monsters/Incubus)<br>*You could bounce a coin off those abs.*|8|33|33|None|10|None|No|Seduction: 5~8 sexy damage|
| [Invisible Stalker](home/Monsters/Invisible-Stalker)<br>*You don't see a thing.*|7|24|42|None|26|None|No|Slam: 5~11 blunt damage|
| [Iron Golem](home/Monsters/Iron-Golem)<br>*It moves with heavy clonking steps.*|9|120|45|None|1|None|No|Smash: 9~14 blunt damage|
| [Jabberwocky](home/Monsters/Jabberwocky)|25|732|42|None|32|None|Yes||
| [Jackal](home/Monsters/Jackal)<br>*They're tricksy.*|1|11|8|None|2|None|No|Bite: 1~3 pierce damage|
| [Jazid](home/Monsters/Jazid)<br>*Jazid's sole purpose is to kill the bearer of his compass.*|26|2500|None|None|30|30|Yes|Claws: 90~120 slash damage|
| [Jellyfish](home/Monsters/Jellyfish)<br>*Don't touch.*|3|4|15|None|5|5|No|Sting: 1~3 poison damage|
| [Kakapo](home/Monsters/Kakapo)|2|5|2|None|3|None|No|Beak: 1~4 pierce damage|
| [Karnivex the Bloodlock](home/Monsters/Karnivex-the-Bloodlock)<br>*The weakest of the Archlocks built his reputation upon sheer slaughter.*|19|1200|130|None|20|None|Yes||
| [Kobold](home/Monsters/Kobold)|2|2~5|7|None|None|None|No|Pointy Stick: 1~4 pierce damage|
| [Large Mouse](home/Monsters/Large-Mouse)|1|4|2|None|2|None|No|Nibble: 0.2~0.7 pierce damage|
| [Large Rat](home/Monsters/Large-Rat)|2|10|2|None|2|None|No|Bite: 0~2.5 pierce damage|
| [Large Snail](home/Monsters/Large-Snail)|2|14|4|None|1|-1|No|Slime: 1~3 nature damage|
| [Large Spider](home/Monsters/Large-Spider)<br>*You can't tell if she looks hungry.*|3|8|15|None|None|None|No|Poison: 1~2 venom damage for 3 seconds; Bite: 4~9 pierce damage|
| [Leech](home/Monsters/Leech)<br>*Blood sucking leech*|4|6|None|None|None|None|No|Suck: 1~3 pierce damage|
| [Lesser Wyrm](home/Monsters/Lesser-Wyrm)<br>*A cross between a worm and a dragon.*|11|80|42|None|5|None|No|Bite: 9~15 pierce damage|
| [Leviathan](home/Monsters/Leviathan)<br>*A mountain moves in the depths of the sea.*|18|400|75|None|20|None|No|Crush: 80~110 blunt damage|
| [Lich Lord](home/Monsters/Lich-Lord)<br>*It was a wizard, once.*|16|150|71|7|20|None|No|Death Touch: 30~40 drain damage|
| [Lich](home/Monsters/Lich)<br>*It was a wizard, once.*|14|166|68|3|20|None|No|Death Touch: 26~36 drain damage|
| [Light Elemental](home/Monsters/Light-Elemental)<br>*Too bright to see.*|8|30|65|None|13|None|No|Dot: blind; Light: 4~10 light damage|
| [Lightning Elemental](home/Monsters/Lightning-Elemental)|10|28|63|None|14|None|No|Lightning: 13~25 light damage|
| [Lion](home/Monsters/Lion)|6|20|18|None|None|8|No|Claws: 7~10 slash damage|
| [Magic Blade](home/Monsters/Magic-Blade)<br>*It just floats there...*|6|45|35|None|9|None|No|Magic Sword: 4~10 slash damage|
| [Magic Mirror](home/Monsters/Magic-Mirror)|7|47|40|None|7|None|No|Light: 8~15 light damage|
| [Magma Elemental](home/Monsters/Magma-Elemental)<br>*A small mountain of magma.*|16|170|60|None|15|None|No|Lava Slam: 50~75 fire damage|
| [Manticore](home/Monsters/Manticore)|12|130|48|None|10|None|No|Sting: 15~25 pierce damage|
| [Master Swordsman](home/Monsters/Master-Swordsman)<br>*He's looking for a six-fingered man.*|8|35|35|None|14|None|No|Longsword: 7~13 slash damage|
| [Mature Red Dragon](home/Monsters/Mature-Red-Dragon)|20|350|90|None|25|None|No|Fire Breath: 90~120 fire damage|
| [Mecha-Charger](home/Monsters/Mecha-Charger)|20|500|120|None|20|None|No||
| [Mecha-Mender](home/Monsters/Mecha-Mender)|20|500|100|None|20|None|No||
| [Mechanical Swordsman](home/Monsters/Mechanical-Swordsman)<br>*Durable, but not very good at its job.*|5|22|28|None|6|None|No|Short Sword: 3~7 slash damage|
| [Medusa](home/Monsters/Medusa)<br>*Kind of ugly.*|8|56|40|None|7|None|No|Stare: 17~25 magic damage|
| [Mind Slayer](home/Monsters/Mind-Slayer)<br>*It licks your brain with its mind.*|11|49|44|None|15|None|No|Mind Flay: 11~19 psionic damage|
| [Minotaur](home/Monsters/Minotaur)|6|25|18|None|None|None|No|Mallet: 7~11 blunt damage|
| [Mithril Golem](home/Monsters/Mithril-Golem)<br>*Would be worth a fortune if you could even make a dent in it.*|17|200|100|None|8|None|No|Pound: 50~95 blunt damage|
| [Mummer](home/Monsters/Mummer)<br>*A voiceless form with a bare and twisted skull.*|26|700|70|None|100|None|No||
| [Mutant Rat](home/Monsters/Mutant-Rat)|3|4|3|None|3|None|No|Bite: 2~4 pierce damage|
| [Mythic Vampire](home/Monsters/Mythic-Vampire)|19|175|110|10|25|None|No|Bite: 50~70 shadow damage|
| [Naga](home/Monsters/Naga)<br>*From the waist up, it doesn't look that bad.*|8|38|48|None|8|None|No|Poison: 1~4 poison damage for 5 seconds; Scimitars: 5~8 slash damage|
| [Narz, the Black Spindle](home/Monsters/Narz,-the-Black-Spindle)|27|1700|200|None|50|None|Yes||
| [Nature's Wrath](home/Monsters/Nature's-Wrath)|22|400|120|15|25|None|No|Lifestorm: 100~125 nature damage|
| [Netherwraith](home/Monsters/Netherwraith)<br>*A spirit cloaked in noxious nether.*|27|1000|40|None|100|None|No|Nether Touch: 50~80 spirit damage|
| [Newt](home/Monsters/Newt)|0.2|1~2|1|None|None|None|No|Newting: 1~2 squiggly damage|
| [Nyarlathotep](home/Monsters/Nyarlathotep)|30|None|None|None|None|None|No||
| [Ogre Chief](home/Monsters/Ogre-Chief)|10|70|23|None|12|None|No||
| [Ogre Warrior](home/Monsters/Ogre-Warrior)|8|70|23|None|12|None|No|Spiked Club: 16~34 blunt damage|
| [Ogre](home/Monsters/Ogre)|7|45|20|None|10|None|No|Club: 8~17 blunt damage|
| [Orc Champion](home/Monsters/Orc-Champion)<br>*A champion among orcs.*|8|100|40|None|10|None|No|Broadsword: 15~20 slash damage|
| [Orc Chieftain](home/Monsters/Orc-Chieftain)<br>*The strongest orc in the whole tribe.*|7|80|30|None|8|None|No|Broadsword: 12~17 slash damage|
| [Orc Shaman](home/Monsters/Orc-Shaman)|6|14|5|None|5|None|No||
| [Orc Warrior](home/Monsters/Orc-Warrior)|5|17|15|None|6|None|No|Orc Hammer: 2~7 blunt damage|
| [Orc](home/Monsters/Orc)<br>*For the Horde!*|4|14|8|None|3|None|No|Spear: 3~6 pierce damage|
| [Ouroboros](home/Monsters/Ouroboros)<br>*The serpent that grips the earth in its scales*|34|130000|200|None|30|None|Yes|Crush: 200~350 blunt damage|
| [Palus the Sinner](home/Monsters/Palus-the-Sinner)<br>*Palus was a mere dabbler before unearthing the remains of a Dhrozen knight. Now he leads the Barrow priests.*|14|80|75|None|10|None|Yes|Harm: 10~20 evil damage|
| [Pegasus](home/Monsters/Pegasus)<br>*It's a unicorn with wings and no horn.*|8|54|27|None|3|None|No|Trample: 9~15 blunt damage|
| [Phoenix](home/Monsters/Phoenix)<br>*It's more fire than bird.*|7|47|40|None|None|None|No|Flame Wing: 11~20 fire damage|
| [Piranha](home/Monsters/Piranha)<br>*It's bitey.*|3|4|15|None|5|7|No|Bite: 2~4 pierce damage|
| [Poltergeist](home/Monsters/Poltergeist)<br>*Only the objects flying at your head alert you to its presence.*|8|44|40|None|9|None|No|Chill Touch: 3~10 drain damage|
| [Pouch Merchant](home/Monsters/Pouch-Merchant)<br>*The guild hires merchants to drain gold from the pouches of others.*|14|300|None|None|None|20|No|Transfer Gold: None None damage|
| [Puppet Aggressor](home/Monsters/Puppet-Aggressor)|10|75|7|None|10|None|No|Stab: 20~30 pierce damage|
| [Puppet Bulwark](home/Monsters/Puppet-Bulwark)|10|300|20|None|7|None|No||
| [Quasit](home/Monsters/Quasit)<br>*Like a demonic humanoid vulture.*|7|22|30|1|8|None|No|Claws: 4~9 slash damage|
| [Ranger](home/Monsters/Ranger)<br>*Rangers aren't very good with people.*|5|20|19|None|None|None|No|Longbow: 3~6 pierce damage|
| [Rat King](home/Monsters/Rat-King)|3|8|4|0.5|2|None|Yes|Kingly Bite: 1~3 pierce damage|
| [Raven](home/Monsters/Raven)|2|3|3|None|4|4|No|Peck: 1~2 pierce damage|
| [Reaper](home/Monsters/Reaper)<br>*a raw manifestation of death is fleeting, but terrible beyond measure.*|30|6666|200|-100|50|None|No||
| [Red Dragon](home/Monsters/Red-Dragon)|15|137|60|None|10|None|No|Fire Breath: 50~70 fire damage|
| [Roc](home/Monsters/Roc)<br>*An avian terror of mythic repute.*|8|41|32|None|None|None|No|Beak: 11~17 pierce damage|
| [Rumpelstiltskin](home/Monsters/Rumpelstiltskin)<br>*A strange imp that spins magic threads.*|23|900|None|None|None|None|Yes|Wooden Cane: 40~100 blunt damage|
| [Sack Guild Enforcer](home/Monsters/Sack-Guild-Enforcer)|17|None|None|None|None|None|No||
| [Sackmaker Apprentice](home/Monsters/Sackmaker-Apprentice)<br>*They have no respect for wizard apprentices.*|2|50|None|None|None|None|No|Sewing Needle: 5~10 None damage|
| [Sackmaker Journeyman](home/Monsters/Sackmaker-Journeyman)<br>*Travels to towns, selling the finest quality bags and pouches.*|16|75|None|None|None|None|No|Punch: 40~50 None damage|
| [Sackmaker's Lodge Master](home/Monsters/Sackmaker's-Lodge-Master)|20|900|None|None|None|None|No|Adamant Needle: 40~75 pierce damage|
| [Sackweaver](home/Monsters/Sackweaver)<br>*Uses flows of magic to weave bags for sale.*|19|400|None|None|None|None|No||
| [Salamander](home/Monsters/Salamander)|6|34|23|None|5|None|No|Burning: 1~3 fire damage for 3 seconds; Flame Claws: 3~9 fire damage|
| [Sand Mite](home/Monsters/Sand-Mite)<br>*Oversized parasites of the dunes*|5|16|None|None|7|7|No|Burrow: 7~14 pierce damage|
| [Sand Wyrm](home/Monsters/Sand-Wyrm)<br>*Terror of the desert wastes*|14|130|50|None|15|None|No|Crush: 12~24 blunt damage|
| [Scarab](home/Monsters/Scarab)<br>*Massive shimmering beetle with a projecting horn*|4|5|7|None|None|None|No|Charge: 3~5 blunt damage|
| [Scuttling Crab](home/Monsters/Scuttling-Crab)<br>*A massive scuttling crab*|5|12|25|None|3|None|No|Claws: 5~8 pierce damage|
| [Shade](home/Monsters/Shade)|9|20|38|None|3|None|No|Cold Touch: 10~20 cold damage|
| [Shark](home/Monsters/Shark)<br>*It's bitey.*|9|58|18|None|11|11|No|Bite: 10~15 pierce damage|
| [Shoggoth](home/Monsters/Shoggoth)|27|None|None|None|None|None|No||
| [Shrieking Eel](home/Monsters/Shrieking-Eel)<br>*Oversized eel with bladed teeth and a terrifying screach.*|7|37|20|None|20|None|No|Scream: 7~11 sonic damage|
| [Skeletal Rat](home/Monsters/Skeletal-Rat)<br>*Even the dead rats are a problem...*|1|3|1|None|3|None|No|Bite: 0~2 pierce damage|
| [Skeleton Lord](home/Monsters/Skeleton-Lord)<br>*Are those rubies in its eyes?*|13|53|34|None|15|None|No|Longsword: 36~45 slash damage|
| [Skeleton](home/Monsters/Skeleton)<br>*It appears to be missing all its fleshy bits.*|3|4|3|None|5|None|No|Slash: 1~4 slash damage|
| [Slime](home/Monsters/Slime)|4|10|8|None|None|None|No|Squelch: 2~4 acid damage|
| [Small Chest](home/Monsters/Small-Chest)|1|5|3|None|None|-20|No||
| [Small Snake](home/Monsters/Small-Snake)|2|5|2|None|2|None|No|Venom: 0.2~0.5 poison damage for 3 seconds|
| [Snow Leopard](home/Monsters/Snow-Leopard)|5|14|18|None|None|9|No|Claws: 4~7 cold damage|
| [Spider Queen](home/Monsters/Spider-Queen)|8|70|5|None|6|None|No|Venom: 3~5 poison damage for 30 seconds|
| [Stingy Bee](home/Monsters/Stingy-Bee)|1|2|2|None|4|3|No|Sting: 0.5~1.5 poison damage|
| [Stone Golem](home/Monsters/Stone-Golem)<br>*The head stone's connected to the... spine stone.*|9|88|40|None|7|None|No|Smash: 9~13 blunt damage|
| [Stranglevine](home/Monsters/Stranglevine)<br>*Writhing tendrils seek to ensnare fresh prey.*|17|165|None|None|None|None|No|Dot: strangle; Strangle: 10~20 blunt damage|
| [Strativax Enraged](home/Monsters/Strativax-Enraged)<br>*Strativax now furious at your impetuous intrusion.*|27|20000|200|None|50|None|Yes||
| [Strativax Slumbering](home/Monsters/Strativax-Slumbering)<br>*He does not appear to notice your presence.*|24|10000|500|None|None|None|Yes||
| [Strativax Waking](home/Monsters/Strativax-Waking)<br>*The dragon's eyes begin to open.*|27|50000|300|None|50|None|Yes||
| [Succubus](home/Monsters/Succubus)|9|69|35|None|20|None|No|Seduction: 9~13 sexy damage|
| [Thangmor Ranger](home/Monsters/Thangmor-Ranger)<br>*All rangers travel far. Some farther than others.*|8|70|25|None|None|None|No|Grandbow: 10~20 pierce damage|
| [The Gorborung](home/Monsters/The-Gorborung)<br>*The gargantuan entity in the depths of Mt. Gorbu never seen by mortal eyes.*|25|5000|100|None|34|None|Yes|Gorboring: 50~75 sonic damage|
| [Thrall](home/Monsters/Thrall)<br>*One isn't too bad. 100 can be a problem.*|4|14|5|None|2|None|No|Axe: 5~9 slash damage|
| [Troll](home/Monsters/Troll)|11|100|50|5|3|None|No|Claws: 17~23 slash damage|
| [Trow](home/Monsters/Trow)<br>*A towering giant of stone*|12|225|50|None|10|None|No|Kick: 14~20 blunt damage|
| [Twik-Man](home/Monsters/Twik-Man)<br>*Tiny vicious humanoid riding a dragonfly.*|1|3~5|1|None|2|1|No|Spear: 0.5~1.5 slash damage|
| [Undead Crow](home/Monsters/Undead-Crow)<br>*Little more than tatters and bones*|0.5|4|2|None|None|None|No|Pecking: 0.2~1 necrotic damage|
| [Undead Horror](home/Monsters/Undead-Horror)<br>*You need to stop looking at it before you go insane.*|8|45|50|None|10|None|No|Chill Touch: 6~16 drain damage|
| [Unicorn](home/Monsters/Unicorn)<br>*Its horn is full of magic.*|9|77|35|None|14|None|No|Horn: 7~14 pierce damage|
| [Valkyrie](home/Monsters/Valkyrie)<br>*She'll take you to Valhalla.*|12|155|54|None|15|None|No|Claymore: 10~17 slash damage|
| [Vampire Bat](home/Monsters/Vampire-Bat)<br>*It vants to zuck your blood.*|4|12|25|None|2|None|No|Bite: 5~7 pierce damage|
| [Vampire Lord](home/Monsters/Vampire-Lord)<br>*Master Vampires are its lackeys.*|15|120|70|5|15|None|No|Bite: 20~40 drain damage|
| [Vampire Master](home/Monsters/Vampire-Master)<br>*Worked his way up the vampire ranks, and has his very own brood now.*|11|87|42|2|10|None|No|Bite: 10~25 drain damage|
| [Vampire](home/Monsters/Vampire)<br>*Standard blood sucking variety.*|9|66|30|None|18|None|No|Bite: 9~18 drain damage|
| [Vicious Wyvern](home/Monsters/Vicious-Wyvern)|20|332|42|None|32|None|No||
| [Vrrek](home/Monsters/Vrrek)<br>*An intelligent scavenger with the features of a vulture.*|5|25|None|None|9|None|No|Talons: 7~12 slash damage|
| [Vulture](home/Monsters/Vulture)|3|None|1|None|7|5|No|Peck: 4~8 pierce damage|
| [Warg](home/Monsters/Warg)|6|15|4|None|7|None|No|Bite: 7~9 pierce damage|
| [Warthog](home/Monsters/Warthog)|3|7|4|None|2|None|No|Bite: 1~4 pierce damage|
| [Werewolf](home/Monsters/Werewolf)|6|22|25|1.5|7|None|No|Claws: 3~6 slash damage|
| [Wicked Hierophant](home/Monsters/Wicked-Hierophant)<br>*Leader of wicked congregations.*|9|100|50|None|9|None|No|Harm: 10~20 evil damage|
| [Wight](home/Monsters/Wight)|6|18|22|1|5|None|No|Claws: 3~9 slash damage|
| [Will O' Wisp](home/Monsters/Will-O'-Wisp)<br>*A gleaming orb of light*|3|4|3|None|None|4|No|Flicker: 1~2 light damage|
| [Wisp](home/Monsters/Wisp)<br>*A ghostly wisp of air.*|1|1~2|1|None|None|None|No|Dot: shivering; Shiver: 0.2~1 spirit damage|
| [Wolf](home/Monsters/Wolf)|4|13|12|None|None|None|No|Bite: 2~5 pierce damage|
| [Wombat](home/Monsters/Wombat)<br>*Just wombatting around.*|2|7~10|3|None|None|None|No|Wombatting: 1~4 blunt damage|
| [Wooden Golem](home/Monsters/Wooden-Golem)<br>*A golem crafted on a budget.*|7|32|28|None|5|None|No|Punch: 2~11 blunt damage|
| [Wraith](home/Monsters/Wraith)<br>*A ghostly black form.*|9|30|40|None|10|None|No|Death Touch: 5~8 drain damage|
| [Wyvern](home/Monsters/Wyvern)<br>*Not a dragon. But it will kill you like one.*|13|132|42|None|12|None|No|Claws: 20~30 slash damage|
| [Yeti](home/Monsters/Yeti)|10|60|25|None|11|None|No|Claws: 8~12 slash damage|
| [Yog-Sothoth](home/Monsters/Yog-Sothoth)|90|None|None|None|None|None|No||
| [Ythogtha](home/Monsters/Ythogtha)|30|None|None|None|None|None|No||
| [Zombie](home/Monsters/Zombie)<br>*Slow and rotting corpse.*|3|4|4|None|-1|None|No|Smash: 3~8 blunt damage|
| [Zoth-Ommog](home/Monsters/Zoth-Ommog)<br>*Third son of cthulhu with a razor fanged, reptilian head.*|30|None|None|None|None|None|No||
|<span style="font-size:2em;">⚔️</span> [Armored Dummy](home/Monsters/Armored-Dummy)<br>*Any normal attack leaves no mark*|1|10|20|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Dodging Dummy](home/Monsters/Dodging-Dummy)<br>*Any normal means of attack is useless*|0|1|None|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Dummy1](home/Monsters/Dummy1)<br>*Even fists would do*|0|10|None|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Dummy](home/Monsters/Dummy)<br>*Can't even hit back*|1|10|3|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Hard Dummy](home/Monsters/Hard-Dummy)<br>*Fists don't work against this guy*|0|5|None|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Instructor](home/Monsters/Instructor)<br>*Is he toying with you?*|1|50|None|None|None|3|No|Mockery: 0~2 slashing damage|
|<span style="font-size:2em;">⚔️</span> [Spellabsorber](home/Monsters/Spellabsorber)<br>*Use a weapon for this guy*|0|5|None|None|None|-10|No||
|<span style="font-size:2em;">❄️</span> [Avalanche](home/Monsters/Avalanche)<br>*The mass of snow seems to aim directly for you as it tumbles down the mountain*|2|100|-10|None|None|None|No|Chilling: None ice damage|
|<span style="font-size:2em;">❄️</span> [Blizzard Guardian](home/Monsters/Blizzard-Guardian)<br>*Winter holds its own secrets, and summons guardians to watch them.*|9|50|55|None|12|None|No|Freeze: 10~20 ice damage|
|<span style="font-size:2em;">❄️</span> [Frost Elemental](home/Monsters/Frost-Elemental)<br>*A spiral of deadly icicles*|1|20|4|None|None|None|No|Icicles: 5~8 ice damage|
|<span style="font-size:2em;">❄️</span> [Frost Golem](home/Monsters/Frost-Golem)<br>*A hulking Golem sculpted in ice*|1|50|8|2|None|10|No|Snowball: 3~5 ice damage|
|<span style="font-size:2em;">❄️</span> [Ice Troll](home/Monsters/Ice-Troll)<br>*A huge dumb looking creature with a club of ice*|3|40|10|None|None|None|No|Smash: 10~15 blunt damage|
|<span style="font-size:2em;">❄️</span> [Icy Winds](home/Monsters/Icy-Winds)<br>*A chilling wind*|2|30|5|None|None|None|No|Icy Winds: 2~3 ice damage|
|<span style="font-size:2em;">❄️</span> [Yeti](home/Monsters/Yeti)<br>*A mythical creature, found only in legends*|5|200|20|None|None|1|No|Icybreath: 35~55 ice damage|
