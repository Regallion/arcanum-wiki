**Description**: The ranks of the Kell range from the lesser Fey and forest Delki, to ancient spirits, old and indominable.

**Tags**: [T_Tier6](home/Tags)

**Requirements**: [Dhrunic Magic](home/Events/Dhrunic-Magic) > 0 and ([Fey](home/Classes/Fey) + [Kell](home/Classes/Kell)) > 0

**Cost to acquire**

- [Research](home/Resources/Research): 15000

- [Nature Gem](home/Resources/Nature-Gem): 50

- [Herbs](home/Resources/Herbs): 75

- [Arcana](home/Resources/Arcana): 35

- [Water Runes](home/Resources/Water-Runes): 15

- [Rune Stones](home/Resources/Rune-Stones): 15

**Modifiers**

- [Ageless Magic](home/Events/Ageless-Magic): True

- [Aeromancy Max](home/Skills/Aeromancy): 2

- [Aeromancy Rate](home/Skills/Aeromancy): 10%

- [Nature Studies Max](home/Skills/Nature-Studies): 3

- [Nature Studies Rate](home/Skills/Nature-Studies): 15%

- [Animal Handling Max](home/Skills/Animal-Handling): 2

- [Animal Handling Rate](home/Skills/Animal-Handling): 10%

- [Geomancy Max](home/Skills/Geomancy): 1

- [Geomancy Rate](home/Skills/Geomancy): 15%

**Flavor**: 'It's of no consequence to me,' replied Jora. 'I do not die Trill, and there are few things in this world that could do the trick.

