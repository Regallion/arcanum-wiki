**Description**: Steel and magic

**Tags**: [T_Tier3](home/Tags)

**Requirements**: [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and [Mechanical Magic](home/Skills/Mechanical-Magic) ≥ 5 and [Magical Mastery](home/Events/Magical-Mastery) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 3000

- [Arcana](home/Resources/Arcana): 25

- [Blood Gem](home/Resources/Blood-Gem): 20

- [Air Gem](home/Resources/Air-Gem): 20

- [Tomes](home/Resources/Tomes): 10

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- [Puppetry Max](home/Skills/Puppetry): 1

- Minions Keep: machina

- [Animation Max](home/Skills/Animation): 2

- [Mechanical Magic Max](home/Skills/Mechanical-Magic): 2

- Allies Max: 5

- [Clockwork Home Space Max](home/Homes/Clockwork-Home): 100

**Flavor**: What is a man? An inefficient form of labor.

