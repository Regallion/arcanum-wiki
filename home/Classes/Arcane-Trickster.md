**Description**: Use magic for deceit and trickery.

**Secret**: True

**Tags**: [T_Tier1](home/Tags)

**Requirements**: [Trickery](home/Skills/Trickery) ≥ 4 and [Solitary Magic](home/Events/Solitary-Magic) > 0 and [Professional Magick](home/Events/Professional-Magick) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 250

- [Gemstones](home/Resources/Gemstones): 10

- [Gold](home/Resources/Gold): 300

- [Arcana](home/Resources/Arcana): 5

**Class disables**: [Pace](home/Tasks/Pace)

**Result**

- [Skill Points](home/Player/Skill-Points): 3

**Modifiers**

- [Professional Magick](home/Events/Professional-Magick): True

- [Gold Max](home/Resources/Gold): 10

- [Research Max](home/Resources/Research): 2

- [Trickery Max](home/Skills/Trickery): 2

- [Trickery Rate](home/Skills/Trickery): 10%

- [Conjuration Max](home/Skills/Conjuration): 1

- [Mana Max](home/Resources/Mana): 2

- [Chaos Max](home/Resources/Chaos): 3

- [Chaos Rate](home/Resources/Chaos): 0.1

- [Potions Max](home/Skills/Potions): 1

- [Spirit Communion Max](home/Skills/Spirit-Communion): 1

