**Tags**: [T_Tier6](home/Tags)

**Description**: You have reached the pinnacle of blending steel and sorcery, and your sword strokes seem to cleave the air itself.

**Requirements**: [Dhrunic Magic](home/Events/Dhrunic-Magic) > 0 and [Spell Blade](home/Classes/Spell-Blade) > 0 and [Ageless Magic](home/Events/Ageless-Magic) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 15000

- [Arcana](home/Resources/Arcana): 20

- [Gold](home/Resources/Gold): 7500

- [Air Gem](home/Resources/Air-Gem): 40

- [Fire Gem](home/Resources/Fire-Gem): 20

- [Earth Gem](home/Resources/Earth-Gem): 20

- [Water Gem](home/Resources/Water-Gem): 20

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Ageless Magic](home/Events/Ageless-Magic): True

- tohit: 15%

- [World Lore Max](home/Skills/World-Lore): 1

- [Enchanting Max](home/Skills/Enchanting): 1

- [Swordplay Max](home/Skills/Swordplay): 2

- [Elemental Max](home/Tags): 1

- [Anatomy Max](home/Skills/Anatomy): 1

- [Greater Arcane Body Max](home/Upgrades/Greater-Arcane-Body): 1

