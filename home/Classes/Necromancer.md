**Tags**: [T_Tier4](home/Tags)

**Description**: A master of death magic

**Requirements**: [Magical Mastery](home/Events/Magical-Mastery) > 0 and [Vile](home/Events/Vile) > 0 and [Umbramancy](home/Skills/Umbramancy) ≥ 17

**Cost to acquire**

- [Research](home/Resources/Research): 2000

- [Bones](home/Resources/Bones): 100

- [Bodies](home/Resources/Bodies): 50

- [Souls](home/Resources/Souls): 40

- [Arcana](home/Resources/Arcana): 10

- [Rune Stones](home/Resources/Rune-Stones): 5

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Supreme Sorcery](home/Events/Supreme-Sorcery): True

- [Embalming Max](home/Skills/Embalming): 1

- [Reanimation Max](home/Skills/Reanimation): 2

- [Reanimation Rate](home/Skills/Reanimation): 10%

- [Umbramancy Max](home/Skills/Umbramancy): 2

- [Umbramancy Rate](home/Skills/Umbramancy): 0.2+5%

- [Potions Max](home/Skills/Potions): 1

- [Necromancy Max](home/Skills/Necromancy): 3

- [Necromancy Rate](home/Skills/Necromancy): 10%

- [Spirit Communion Max](home/Skills/Spirit-Communion): 2

- [Spirit Communion Rate](home/Skills/Spirit-Communion): 10%

