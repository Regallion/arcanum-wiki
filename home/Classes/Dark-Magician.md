**Tags**: [T_Tier1](home/Tags)

**Description**: A devotee of black magic.

**Requirements**: [Solitary Magic](home/Events/Solitary-Magic) > 0 and ([Umbramancy](home/Skills/Umbramancy) ≥ 7)

**Cost to acquire**

- [Research](home/Resources/Research): 400

- [Arcana](home/Resources/Arcana): 13

- [Gold](home/Resources/Gold): 300

- [Bones](home/Resources/Bones): 10

- [Gemstones](home/Resources/Gemstones): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Professional Magick](home/Events/Professional-Magick): True

- [Evil](home/Player/Evil): 5

- [Research Max](home/Resources/Research): 100

- [Demonology Max](home/Skills/Demonology): 2

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Mage Lore Rate](home/Skills/Mage-Lore): 5%

- [Mana Max](home/Resources/Mana): 1

- [Mana Rate](home/Resources/Mana): 0.01

- [Umbramancy Max](home/Skills/Umbramancy): 1

- [Umbramancy Rate](home/Skills/Umbramancy): 0.1+5%

