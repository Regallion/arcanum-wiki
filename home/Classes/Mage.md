**Description**: Member of an arcane order.

**Tags**: [T_Tier2](home/Tags)

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Mage Lore](home/Skills/Mage-Lore) ≥ 17

**Cost to acquire**

- [Research](home/Resources/Research): 2000

- [Arcana](home/Resources/Arcana): 15

- [Tomes](home/Resources/Tomes): 5

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Research Max](home/Resources/Research): 40

- [Pyromancy Max](home/Skills/Pyromancy): 1

- [Mysticism Max](home/Skills/Mysticism): 2

- [Enchanting Max](home/Skills/Enchanting): 1

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Mage Lore Rate](home/Skills/Mage-Lore): 0.4

- [Mana Max](home/Resources/Mana): 3

