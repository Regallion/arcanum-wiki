**Description**: An ancient magic from beyond the sea, combining spirit and blade.

**Secret**: True

**Tags**: [T_Tier4](home/Tags)

**Requirements**: [Magical Mastery](home/Events/Magical-Mastery) > 0 and [The Unstable Spire](home/Dungeons/The-Unstable-Spire) > 0

**Cost to acquire**

- [Research](home/Resources/Research): 25000

- [Arcana](home/Resources/Arcana): 25

- [Tomes](home/Resources/Tomes): 100

- [Ichor](home/Resources/Ichor): 2

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Supreme Sorcery](home/Events/Supreme-Sorcery): True

- [Spirit Communion Max](home/Skills/Spirit-Communion): 3

- [Swordplay](home/Skills/Swordplay): True

**Flavor**: Contemplation of the small as well as great.

