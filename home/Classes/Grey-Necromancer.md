**Tags**: [T_Tier5](home/Tags)

**Description**: Disciple of death

**Requirements**: [Supreme Sorcery](home/Events/Supreme-Sorcery) > 0 and [Vile](home/Events/Vile) > 0 and [Spirit Communion](home/Skills/Spirit-Communion) ≥ 15 and [Umbramancy](home/Skills/Umbramancy) ≥ 24

**Cost to acquire**

- [Research](home/Resources/Research): 17000

- [Bones](home/Resources/Bones): 100

- [Bodies](home/Resources/Bodies): 50

- [Souls](home/Resources/Souls): 100

- [Arcana](home/Resources/Arcana): 10

- [Rune Stones](home/Resources/Rune-Stones): 10

**Modifiers**

- [Dhrunic Magic](home/Events/Dhrunic-Magic): True

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Mage Lore Rate](home/Skills/Mage-Lore): 10%

- [Embalming Max](home/Skills/Embalming): 2

- [Necromancy Max](home/Skills/Necromancy): 3

- [Necromancy Rate](home/Skills/Necromancy): 10%

- [Reanimation Max](home/Skills/Reanimation): 3

- [Reanimation Rate](home/Skills/Reanimation): 10%

- [Umbramancy Max](home/Skills/Umbramancy): 2

- [Umbramancy Rate](home/Skills/Umbramancy): 5%

- [Spirit Communion Max](home/Skills/Spirit-Communion): 2

- [Spirit Communion Rate](home/Skills/Spirit-Communion): 10%

