**Description**: A wielder of raw magical power.

**Requirements**: [Magical Mastery](home/Events/Magical-Mastery) > 0 and ([Geomancy](home/Skills/Geomancy) ≥ 15 or [Aquamancy](home/Skills/Aquamancy) ≥ 15 or [Pyromancy](home/Skills/Pyromancy) ≥ 15) and ([Elementalist](home/Classes/Elementalist) + [Highelemental](home/Classes/Highelemental)) > 0

**Tags**: [T_Tier4](home/Tags)

**Cost to acquire**

- [Research](home/Resources/Research): 3000

- [Arcane Gem](home/Resources/Arcane-Gem): 5

- [Fire Gem](home/Resources/Fire-Gem): 15

- [Earth Gem](home/Resources/Earth-Gem): 15

- [Water Gem](home/Resources/Water-Gem): 15

- [Arcana](home/Resources/Arcana): 25

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Supreme Sorcery](home/Events/Supreme-Sorcery): True

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Mana Max](home/Resources/Mana): 1

- [Geomancy Max](home/Skills/Geomancy): 2

- [Geomancy Rate](home/Skills/Geomancy): 10%

- [Aquamancy Max](home/Skills/Aquamancy): 2

- [Aquamancy Rate](home/Skills/Aquamancy): 10%

- [Pyromancy Max](home/Skills/Pyromancy): 2

- [Pyromancy Rate](home/Skills/Pyromancy): 10%

