**Tags**: [T_Tier1](home/Tags)

**Description**: Create and ressurect living materials.

**Requirements**: [Solitary Magic](home/Events/Solitary-Magic) > 0 and [Reanimation](home/Skills/Reanimation) > 2

**Cost to acquire**

- [Research](home/Resources/Research): 500

- [Bones](home/Resources/Bones): 10

- [Bone Dust](home/Resources/Bone-Dust): 10

- [Arcana](home/Resources/Arcana): 11

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Class disables**: [Pace](home/Tasks/Pace)

**Modifiers**

- [Professional Magick](home/Events/Professional-Magick): True

- [Mana Max](home/Resources/Mana): 1

- [Research Max](home/Resources/Research): 10

- [Embalming Max](home/Skills/Embalming): 1

- [Reanimation Max](home/Skills/Reanimation): 3

- [Umbramancy Max](home/Skills/Umbramancy): 2

- [Potions Max](home/Skills/Potions): 3

- [Spirit Communion Max](home/Skills/Spirit-Communion): 2

**Flavor**: Even the dead have their uses

