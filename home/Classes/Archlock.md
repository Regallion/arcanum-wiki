**Description**: Master of demons and demonic magic

**Tags**: [T_Tier5](home/Tags)

**Requirements**: [Warlock](home/Classes/Warlock) > 0 and [Demonology](home/Skills/Demonology) ≥ 20

**Cost to acquire**

- [Research](home/Resources/Research): 15000

- [Souls](home/Resources/Souls): 75

- [Bodies](home/Resources/Bodies): 10

- [Tomes](home/Resources/Tomes): 25

- [Arcana](home/Resources/Arcana): 25

- [Rune Stones](home/Resources/Rune-Stones): 10

**Modifiers**

- [Dhrunic Magic](home/Events/Dhrunic-Magic): True

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Necromancy Max](home/Skills/Necromancy): 1

- [Mage Lore Rate](home/Skills/Mage-Lore): 10%

- [Pyromancy Max](home/Skills/Pyromancy): 3

- [Pyromancy Rate](home/Skills/Pyromancy): 20%

- [Demonology Max](home/Skills/Demonology): 5

- [Demonology Rate](home/Skills/Demonology): 20%

**Flavor**: In the War of the Locks the Seven raged for Power Absolute, and Dhrune trembled in their wake.

