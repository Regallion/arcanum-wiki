**Tags**: [T_Tier4](home/Tags)

**Description**: Master of demons and demonic magic

**Requirements**: [Magical Mastery](home/Events/Magical-Mastery) > 0 and [Vile](home/Events/Vile) > 0 and [Demonology](home/Skills/Demonology) ≥ 10 and [Supreme Sorcery](home/Events/Supreme-Sorcery) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 4000

- [Souls](home/Resources/Souls): 50

- [Arcana](home/Resources/Arcana): 25

- [Tomes](home/Resources/Tomes): 10

- [Rune Stones](home/Resources/Rune-Stones): 5

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Supreme Sorcery](home/Events/Supreme-Sorcery): True

- [Anatomy Max](home/Skills/Anatomy): 3

- [Mage Lore Rate](home/Skills/Mage-Lore): 10%

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Pyromancy Max](home/Skills/Pyromancy): 1

- [Necromancy Max](home/Skills/Necromancy): 1

- [Pyromancy Rate](home/Skills/Pyromancy): 0.2

- [Umbramancy Max](home/Skills/Umbramancy): 2

- [Demonology Max](home/Skills/Demonology): 3

