**Description**: A diviner.

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Divination](home/Skills/Divination) ≥ 10

**Tags**: [T_Tier2](home/Tags)

**Cost to acquire**

- [Codices](home/Resources/Codices): 20

- [Tomes](home/Resources/Tomes): 5

- [Arcana](home/Resources/Arcana): 10

- [Research](home/Resources/Research): 2000

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Virtue](home/Player/Virtue): 5

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Mana Max](home/Resources/Mana): 3

- [Divination Max](home/Skills/Divination): 2

- [Scrying Max](home/Skills/Scrying): 2

- [Spirit Communion Max](home/Skills/Spirit-Communion): 2

- [Spirit Communion Rate](home/Skills/Spirit-Communion): 0.1

