**Tags**: [T_Tier1](home/Tags)

**Description**: Magic in its purest form.

**Action Name**: The Prestige

**Action Description**: Pursue the path of pure magic.

**Requirements**: [Solitary Magic](home/Events/Solitary-Magic) > 0 and [Mage Lore](home/Skills/Mage-Lore) ≥ 10

**Cost to acquire**

- [Research](home/Resources/Research): 500

- [Arcana](home/Resources/Arcana): 15

- [Gold](home/Resources/Gold): 500

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Professional Magick](home/Events/Professional-Magick): True

- [Research Max](home/Resources/Research): 100

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Mage Lore Rate](home/Skills/Mage-Lore): 5%

- [Astronomy Max](home/Skills/Astronomy): 1

- [Languages Max](home/Skills/Languages): 1

- [Arcana Rate](home/Resources/Arcana): 0.001

- [Mana Max](home/Resources/Mana): 2

- [Mana Rate](home/Resources/Mana): 0.02

