**Description**: Harbinger of ruin

**Requirements**: [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and [Spirit](home/Resources/Spirit) ≥ 12 and [Divination](home/Skills/Divination) ≥ 12

**Tags**: [T_Tier3](home/Tags)

**Cost to acquire**

- [Research](home/Resources/Research): 2000

- [Arcana](home/Resources/Arcana): 15

- [Tomes](home/Resources/Tomes): 7

- [Spirit Gem](home/Resources/Spirit-Gem): 15

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- [Spirit Communion Max](home/Skills/Spirit-Communion): 2

- [Divination Max](home/Skills/Divination): 3

- [Planes Lore Max](home/Skills/Planes-Lore): 1

- [Scrying Max](home/Skills/Scrying): 2

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Mage Lore Rate](home/Skills/Mage-Lore): 10%

- [Mana Max](home/Resources/Mana): 1

**Flavor**: You call it an empire, but in the gloaming I see only heaps of crumbled stone.

