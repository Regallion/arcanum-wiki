**Tags**: [T_Tier1](home/Tags)

**Description**: Witch

**Action Name**: The Craft

**Action Description**: Enter into the practice of witchcraft.

**Requirements**: [Solitary Magic](home/Events/Solitary-Magic) > 0 and [Animal Handling](home/Skills/Animal-Handling) ≥ 2 and [Potions](home/Skills/Potions) ≥ 3 and [Professional Magick](home/Events/Professional-Magick) = 0

**Flavor**: 'What is't you do?' 'A deed without a name'

**Cost to acquire**

- [Research](home/Resources/Research): 350

- [Gold](home/Resources/Gold): 200

- [Arcana](home/Resources/Arcana): 13

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Class disables**: [Pace](home/Tasks/Pace)

**Modifiers**

- [Professional Magick](home/Events/Professional-Magick): True

- [Research Max](home/Resources/Research): 10

- [Nature Studies Max](home/Skills/Nature-Studies): 1

- [Nature Studies Rate](home/Skills/Nature-Studies): 10%

- [Animal Handling Max](home/Skills/Animal-Handling): 1

- [Animal Handling Rate](home/Skills/Animal-Handling): 10%

- [Potions Max](home/Skills/Potions): 2

- [Potions Rate](home/Skills/Potions): 15%

- [Spirit Communion Max](home/Skills/Spirit-Communion): 2

