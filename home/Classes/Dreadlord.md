**Description**: Harbinger of destruction

**Tags**: [T_Tier3](home/Tags)

**Requirements**: [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and [Vile](home/Events/Vile) > 0 and [Life](home/Player/Life) ≥ 100 and [Level](home/Player/Level) ≥ 17 and [Magical Mastery](home/Events/Magical-Mastery) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 1500

- [Bones](home/Resources/Bones): 100

- [Bodies](home/Resources/Bodies): 50

- [Arcana](home/Resources/Arcana): 20

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- tohit: 10%

- [Life Max](home/Player/Life): 10%

- [Swordplay Max](home/Skills/Swordplay): 1

- [Umbramancy Max](home/Skills/Umbramancy): 2

- [Umbramancy Rate](home/Skills/Umbramancy): 1%

- [Arcane Body Max](home/Upgrades/Arcane-Body): 1

- [Greater Arcane Body Max](home/Upgrades/Greater-Arcane-Body): 1

