**Description**: Elite of an arcane order.

**Tags**: [T_Tier3](home/Tags)

**Requirements**: [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and [Mage Lore](home/Skills/Mage-Lore) ≥ 21

**Cost to acquire**

- [Research](home/Resources/Research): 5000

- [Arcana](home/Resources/Arcana): 20

- [Tomes](home/Resources/Tomes): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- [Research Max](home/Resources/Research): 50

- [Pyromancy Max](home/Skills/Pyromancy): 1

- [Mysticism Max](home/Skills/Mysticism): 2

- [Alchemy Max](home/Skills/Alchemy): 1

- [Spellcraft Max](home/Skills/Spellcraft): 2

- [Crafting Max](home/Skills/Crafting): 1

- [Mage Lore Max](home/Skills/Mage-Lore): 2

