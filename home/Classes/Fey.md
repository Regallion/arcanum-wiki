**Description**: A being of wild and natural magic

**Tags**: [T_Tier4](home/Tags)

**Requirements**: [Magical Mastery](home/Events/Magical-Mastery) > 0 and [Nature Studies](home/Skills/Nature-Studies) ≥ 19 and ([Witchcraft](home/Classes/Witchcraft) + [Druid](home/Classes/Druid) + [Elementalist](home/Classes/Elementalist)) > 0

**Cost to acquire**

- [Research](home/Resources/Research): 5000

- [Nature Gem](home/Resources/Nature-Gem): 30

- [Herbs](home/Resources/Herbs): 75

- [Arcana](home/Resources/Arcana): 15

- [Rune Stones](home/Resources/Rune-Stones): 5

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Supreme Sorcery](home/Events/Supreme-Sorcery): True

- [Aquamancy Max](home/Skills/Aquamancy): 2

- [Geomancy Max](home/Skills/Geomancy): 2

- [Aeromancy Max](home/Skills/Aeromancy): 2

- [Nature Studies Max](home/Skills/Nature-Studies): 3

- [Nature Studies Rate](home/Skills/Nature-Studies): 1+10%

- [Animal Handling Max](home/Skills/Animal-Handling): 2

- [Animal Handling Rate](home/Skills/Animal-Handling): 1+10%

- [Herbalism Max](home/Skills/Herbalism): 2

- [Herbalism Rate](home/Skills/Herbalism): 1+10%

**Flavor**: 'Will you drink from the goblet?' she asked. 'The transformation cannot be undone.'

