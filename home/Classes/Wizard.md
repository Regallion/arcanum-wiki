**Description**: Raw magic power

**Tags**: [T_Tier4](home/Tags)

**Requirements**: [Vile](home/Events/Vile) ≤ 0 and [Magical Mastery](home/Events/Magical-Mastery) > 0 and [Mage Lore](home/Skills/Mage-Lore) ≥ 25

**Need**: G.Evil<=0

**Cost to acquire**

- [Research](home/Resources/Research): 5000

- [Arcana](home/Resources/Arcana): 25

- [Tomes](home/Resources/Tomes): 20

- [Rune Stones](home/Resources/Rune-Stones): 5

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Supreme Sorcery](home/Events/Supreme-Sorcery): True

- [Languages Max](home/Skills/Languages): 2

- [Conjuration Max](home/Skills/Conjuration): 1

- [Crafting Max](home/Skills/Crafting): 1

- [Mage Lore Max](home/Skills/Mage-Lore): 3

- [Spellcraft Max](home/Skills/Spellcraft): 3

- [Spellcraft Rate](home/Skills/Spellcraft): 0.5

- [Mage Lore Rate](home/Skills/Mage-Lore): 10%

- [Mana Max](home/Resources/Mana): 3

**Flavor**: 'Do not take me for some conjurer of cheap tricks.'

