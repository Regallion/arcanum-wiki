**Description**: Immortal spirits of nature, even the weakest Kell are beings to rival wizards

**Tags**: [T_Tier5](home/Tags)

**Requirements**: [Supreme Sorcery](home/Events/Supreme-Sorcery) > 0 and [Nature Studies](home/Skills/Nature-Studies) ≥ 23 and ([Witchcraft](home/Classes/Witchcraft) + [Fey](home/Classes/Fey) + [Highelemental](home/Classes/Highelemental)) > 0

**Cost to acquire**

- [Research](home/Resources/Research): 15000

- [Nature Gem](home/Resources/Nature-Gem): 50

- [Herbs](home/Resources/Herbs): 75

- [Arcana](home/Resources/Arcana): 20

- [Rune Stones](home/Resources/Rune-Stones): 15

**Modifiers**

- [Dhrunic Magic](home/Events/Dhrunic-Magic): True

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Mage Lore Rate](home/Skills/Mage-Lore): 10%

- [Aeromancy Max](home/Skills/Aeromancy): 2

- [Aeromancy Rate](home/Skills/Aeromancy): 10%

- [Nature Studies Max](home/Skills/Nature-Studies): 3

- [Nature Studies Rate](home/Skills/Nature-Studies): 15%

- [Animal Handling Max](home/Skills/Animal-Handling): 2

- [Animal Handling Rate](home/Skills/Animal-Handling): 10%

- [Herbalism Max](home/Skills/Herbalism): 1

- [Herbalism Rate](home/Skills/Herbalism): 15%

**Flavor**: Kell are not of mortal blood, and only potent magics can imbue a mortal with their essence.

