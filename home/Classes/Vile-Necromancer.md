**Tags**: [T_Tier6](home/Tags)

**Description**: In every epoch, Death appoints her viceroy.

**Requirements**: [Dhrunic Magic](home/Events/Dhrunic-Magic) > 0 and [Phylactory](home/Tasks/Phylactory) > 0 and [Spirit Communion](home/Skills/Spirit-Communion) ≥ 15 and [Umbramancy](home/Skills/Umbramancy) ≥ 24

**Cost to acquire**

- [Research](home/Resources/Research): 17000

- [Bones](home/Resources/Bones): 100

- [Bodies](home/Resources/Bodies): 50

- [Souls](home/Resources/Souls): 100

- [Arcana](home/Resources/Arcana): 25

- [Earth Runes](home/Resources/Earth-Runes): 10

- [Rune Stones](home/Resources/Rune-Stones): 10

**Modifiers**

- [Ageless Magic](home/Events/Ageless-Magic): True

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Spellcraft Max](home/Skills/Spellcraft): 1

- [Embalming Max](home/Skills/Embalming): 1

- [Necromancy Max](home/Skills/Necromancy): 3

- [Necromancy Rate](home/Skills/Necromancy): 10%

- [Reanimation Max](home/Skills/Reanimation): 3

- [Reanimation Rate](home/Skills/Reanimation): 10%

- [Umbramancy Max](home/Skills/Umbramancy): 2

- [Umbramancy Rate](home/Skills/Umbramancy): 5%

- [Spirit Communion Max](home/Skills/Spirit-Communion): 2

- [Spirit Communion Rate](home/Skills/Spirit-Communion): 10%

**Flavor**: The greys bowed their heads in fear.

