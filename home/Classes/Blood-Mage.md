**Tags**: [T_Tier3](home/Tags)

**Description**: The magic in your veins

**Requirements**: [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and ([Battle Mage](home/Classes/Battle-Mage) > 0 or [Life Max](home/Player/Life) > 150) and [Magical Mastery](home/Events/Magical-Mastery) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 1500

- [Arcana](home/Resources/Arcana): 8

- [Gold](home/Resources/Gold): 700

- [Blood Gem](home/Resources/Blood-Gem): 20

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- [Research Max](home/Resources/Research): -50

- [Blood Gem Max](home/Resources/Blood-Gem): 10

- [Mana Max](home/Resources/Mana): 1

- [Mana Rate](home/Resources/Mana): 0.1

- [Anatomy Max](home/Skills/Anatomy): 3

- [Anatomy Rate](home/Skills/Anatomy): 10%

- [Geomancy Max](home/Skills/Geomancy): 2

- [Geomancy Rate](home/Skills/Geomancy): 5%

- [Aquamancy Max](home/Skills/Aquamancy): 1

- [Aquamancy Rate](home/Skills/Aquamancy): 5%

