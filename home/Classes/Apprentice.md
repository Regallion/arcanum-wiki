**Description**: Apprentice to a notable wizard

**Action Name**: Apprenticeship

**Action Description**: Become an apprentice.

**Requirements**: [Research](home/Resources/Research)

**Class disables**

- [Clean Stables](home/Tasks/Clean-Stables)

**Pop-up Warning**: False

**Log Entry**

- Name: Apprentice

- Desc: After paying a small fee, you became apprenticed to a local wizard.

**Cost to acquire**

- [Research](home/Resources/Research): 10

- [Gold](home/Resources/Gold): 20

**Result**

- [Research Max](home/Resources/Research): 10

- Player Exp: 5

- [Skill Points](home/Player/Skill-Points): 1

