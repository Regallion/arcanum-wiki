**Description**: An incarnation of raw, elemental forces.

**Tags**: [T_Tier6](home/Tags)

**Requirements**: [Dhrunic Magic](home/Events/Dhrunic-Magic) > 0 and ([Highelemental](home/Classes/Highelemental) + [Kell](home/Classes/Kell) + [Heresiarch](home/Classes/Heresiarch) + [Thaumaturge](home/Classes/Thaumaturge)) > 0

**Cost to acquire**

- [Research](home/Resources/Research): 20000

- [Star Shard](home/Resources/Star-Shard): 3

- [Arcane Gem](home/Resources/Arcane-Gem): 70

- [Air Gem](home/Resources/Air-Gem): 40

- [Earth Gem](home/Resources/Earth-Gem): 40

- [Fire Gem](home/Resources/Fire-Gem): 40

- [Water Gem](home/Resources/Water-Gem): 40

- [Skill Points](home/Player/Skill-Points): 10

- [Arcana](home/Resources/Arcana): 35

- [Rune Stones](home/Resources/Rune-Stones): 15

**Modifiers**

- [Ageless Magic](home/Events/Ageless-Magic): True

- [Aeromancy Max](home/Skills/Aeromancy): 2

- [Aeromancy Rate](home/Skills/Aeromancy): 10%

- [Geomancy Max](home/Skills/Geomancy): 3

- [Geomancy Rate](home/Skills/Geomancy): 15%

- [Aquamancy Max](home/Skills/Aquamancy): 2

- [Aquamancy Rate](home/Skills/Aquamancy): 10%

- [Pyromancy Max](home/Skills/Pyromancy): 1

- [Pyromancy Rate](home/Skills/Pyromancy): 15%

