**Description**: Lord of rocks.

**Tags**: [T_Tier2](home/Tags)

**Action Description**: Become a geomancer, a master of perseverance.

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Geomancy](home/Skills/Geomancy) ≥ 10 and [Arcane Ascendant](home/Events/Arcane-Ascendant) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 1000

- [Arcana](home/Resources/Arcana): 15

- [Tomes](home/Resources/Tomes): 10

- [Earth Gem](home/Resources/Earth-Gem): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Geomancy Max](home/Skills/Geomancy): 2

- [Geosculpting Max](home/Skills/Geosculpting): 2

- [Stamina Max](home/Player/Stamina): 10%

- [Mana Max](home/Resources/Mana): 2

- [Earthen Spire Space Max](home/Homes/Earthen-Spire): 50

**Flavor**: Your empire of dirt

