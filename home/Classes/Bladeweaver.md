**Tags**: [T_Tier4](home/Tags)

**Description**: You have taken your training further, weaving magic and steel with a flourish of magical grace.

**Requirements**: [Magical Mastery](home/Events/Magical-Mastery) > 0 and [Spell Blade](home/Classes/Spell-Blade) > 0 and [Supreme Sorcery](home/Events/Supreme-Sorcery) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 5000

- [Arcana](home/Resources/Arcana): 20

- [Gold](home/Resources/Gold): 2500

- [Air Gem](home/Resources/Air-Gem): 10

- [Fire Gem](home/Resources/Fire-Gem): 5

- [Earth Gem](home/Resources/Earth-Gem): 5

- [Water Gem](home/Resources/Water-Gem): 5

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Supreme Sorcery](home/Events/Supreme-Sorcery): True

- tohit: 15%

- [World Lore Max](home/Skills/World-Lore): 1

- [Enchanting Max](home/Skills/Enchanting): 1

- [Elemental Max](home/Tags): 1

- [Swordplay Max](home/Skills/Swordplay): 2

- [Anatomy Max](home/Skills/Anatomy): 1

- [Greater Arcane Body Max](home/Upgrades/Greater-Arcane-Body): 1

