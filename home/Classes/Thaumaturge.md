**Description**: A maker of miracles

**Requirements**: [Magical Mastery](home/Events/Magical-Mastery) > 0 and [Spirit](home/Resources/Spirit) ≥ 15 and [Divination](home/Skills/Divination) ≥ 15

**Tags**: [T_Tier4](home/Tags)

**Cost to acquire**

- [Research](home/Resources/Research): 5000

- [Arcana](home/Resources/Arcana): 25

- [Tomes](home/Resources/Tomes): 7

- [Spirit Gem](home/Resources/Spirit-Gem): 20

- [Rune Stones](home/Resources/Rune-Stones): 5

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Supreme Sorcery](home/Events/Supreme-Sorcery): True

- [Spirit Communion Max](home/Skills/Spirit-Communion): 2

- [Divination Max](home/Skills/Divination): 3

- [Spellcraft Max](home/Skills/Spellcraft): 2

- [Spellcraft Rate](home/Skills/Spellcraft): 0.3+5%

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Light Max](home/Resources/Light): 2

- [Light Rate](home/Resources/Light): 0.2+5%

- [Mage Lore Rate](home/Skills/Mage-Lore): 0.4+5%

- [Mana Max](home/Resources/Mana): 3

