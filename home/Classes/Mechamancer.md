**Description**: Master of magical constructs

**Tags**: [T_Tier5](home/Tags)

**Requirements**: [Supreme Sorcery](home/Events/Supreme-Sorcery) > 0 and [Magiphysics](home/Skills/Magiphysics) ≥ 5 and [Mechanical Magic](home/Skills/Mechanical-Magic) ≥ 9 and ([Puppeteer](home/Classes/Puppeteer) + [Mechanist](home/Classes/Mechanist)) > 0 and [Dhrunic Magic](home/Events/Dhrunic-Magic) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 20000

- [Arcana](home/Resources/Arcana): 27

- [Tomes](home/Resources/Tomes): 30

- [Rune Stones](home/Resources/Rune-Stones): 30

**Modifiers**

- [Dhrunic Magic](home/Events/Dhrunic-Magic): True

- [Puppetry Max](home/Skills/Puppetry): 1

- [Animation Max](home/Skills/Animation): 1

- [Mechanical Magic Max](home/Skills/Mechanical-Magic): 2

- [Automata Sculpting Max](home/Skills/Automata-Sculpting): 3

- Minions Keep: automata

- Allies Max: 5

- [Clockwork Home Space Max](home/Homes/Clockwork-Home): 150

**Flavor**: If you want a better future, make better people.

