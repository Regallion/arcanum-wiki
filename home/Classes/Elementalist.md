**Description**: Master of elemental forces.

**Requirements**: [Solitary Magic](home/Events/Solitary-Magic) > 0 and [Aeromancy](home/Skills/Aeromancy) > 2 and [Pyromancy](home/Skills/Pyromancy) > 2 and [Geomancy](home/Skills/Geomancy) > 2 and [Aquamancy](home/Skills/Aquamancy) > 2

**Tags**: [T_Tier1](home/Tags)

**Cost to acquire**

- [Arcana](home/Resources/Arcana): 12

- [Research](home/Resources/Research): 1000

- [Codices](home/Resources/Codices): 20

- [Gemstones](home/Resources/Gemstones): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Professional Magick](home/Events/Professional-Magick): True

- [Research Max](home/Resources/Research): 5

- [Elemental Max](home/Tags): 2

- [Elemental Rate](home/Tags): 0.2

