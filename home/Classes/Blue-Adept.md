**Description**: Dedicate yourself to putting the world in order.

**Secret**: True

**Requirements**: [Neophyte](home/Classes/Neophyte) > 0 and [Virtue](home/Player/Virtue) ≥ 200

**Tags**: [T_Tier0](home/Tags)

**Cost to acquire**

- [Arcana](home/Resources/Arcana): 7

**Modifiers**

- [Solitary Magic](home/Events/Solitary-Magic): True

- [World Lore Max](home/Skills/World-Lore): 1

- [Aquamancy Max](home/Skills/Aquamancy): 1

- [Aeromancy Max](home/Skills/Aeromancy): 1

- [Dissection Max](home/Skills/Dissection): -1

- [Nature Studies Max](home/Skills/Nature-Studies): -1

