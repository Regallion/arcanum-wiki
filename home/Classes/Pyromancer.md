**Description**: Shaper of fire

**Tags**: [T_Tier2](home/Tags)

**Action Description**: Become a pyromancer.

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Pyromancy](home/Skills/Pyromancy) ≥ 10

**Cost to acquire**

- [Research](home/Resources/Research): 1000

- [Arcana](home/Resources/Arcana): 15

- [Tomes](home/Resources/Tomes): 10

- [Fire Gem](home/Resources/Fire-Gem): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Pyromancy Max](home/Skills/Pyromancy): 2

- [Pyromancy Rate](home/Skills/Pyromancy): 10%

- [Dodge](home/Player/Dodge): 10%

- [Mana Max](home/Resources/Mana): 2

