**Description**: An unholy synthesis of wizardry and demonology.

**Tags**: [T_Tier6](home/Tags)

**Requirements**: [Dhrunic Magic](home/Events/Dhrunic-Magic) > 0 and [Phylactory](home/Tasks/Phylactory) > 0

**Cost to acquire**

- [Research](home/Resources/Research): 20000

- [Souls](home/Resources/Souls): 100

- [Bodies](home/Resources/Bodies): 100

- [Tomes](home/Resources/Tomes): 50

- [Arcana](home/Resources/Arcana): 60

- [Flame Runes](home/Resources/Flame-Runes): 10

- [Rune Stones](home/Resources/Rune-Stones): 20

**Modifiers**

- [Ageless Magic](home/Events/Ageless-Magic): True

- [Necromancy Max](home/Skills/Necromancy): 2

- [Geomancy Max](home/Skills/Geomancy): 2

- [Pyromancy Max](home/Skills/Pyromancy): 3

- [Pyromancy Rate](home/Skills/Pyromancy): 20%

- [Umbramancy Max](home/Skills/Umbramancy): 3

- [Umbramancy Rate](home/Skills/Umbramancy): 0.1+5%

- [Demonology Max](home/Skills/Demonology): 5

- [Demonology Rate](home/Skills/Demonology): 20%

**Flavor**: Narz, Bodias, Vezial. Even among the Archlocks, some names stood in abject terror.

