**Tags**: [T_Tier3](home/Tags)

**Requirements**: [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and [Enchanting](home/Skills/Enchanting) ≥ 7 and Tohit ≥ 10 and [Magical Mastery](home/Events/Magical-Mastery) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 3000

- [Arcana](home/Resources/Arcana): 20

- [Gold](home/Resources/Gold): 1000

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- tohit: 15%

- [World Lore Max](home/Skills/World-Lore): 1

- [Enchanting Max](home/Skills/Enchanting): 2

- [Swordplay Max](home/Skills/Swordplay): 2

- [Anatomy Max](home/Skills/Anatomy): 1

- [Greater Arcane Body Max](home/Upgrades/Greater-Arcane-Body): 1

