**Description**: The stars are an open book, to those who can read.

**Tags**: [T_Tier6](home/Tags)

**Requirements**: [Dhrunic Magic](home/Events/Dhrunic-Magic) > 0 and [Astronomy](home/Skills/Astronomy) ≥ 15 and [Planes Lore](home/Skills/Planes-Lore) ≥ 15

**Cost to acquire**

- [Research](home/Resources/Research): 17000

- [Arcana](home/Resources/Arcana): 75

- [Tomes](home/Resources/Tomes): 50

- [Star Shard](home/Resources/Star-Shard): 3

- [Starcharts](home/Resources/Starcharts): 50

**Modifiers**

- [Ageless Magic](home/Events/Ageless-Magic): True

- [Spellcraft Max](home/Skills/Spellcraft): 1

- [Planes Lore Max](home/Skills/Planes-Lore): 2

- [Planes Lore Rate](home/Skills/Planes-Lore): 2%

- bonuses: 2

- [Astronomy Max](home/Skills/Astronomy): 2

- [Astronomy Rate](home/Skills/Astronomy): 1%

- [Scrying Max](home/Skills/Scrying): 1

- [Scrying Rate](home/Skills/Scrying): 2%

- [Star Shard Rate](home/Resources/Star-Shard): 5%

- [Mysticism Max](home/Skills/Mysticism): 2

- [Mysticism Rate](home/Skills/Mysticism): 0.5

- [Crafting Max](home/Skills/Crafting): 2

