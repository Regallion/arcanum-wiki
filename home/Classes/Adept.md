**Tags**: [T_Tier0](home/Tags)

**Description**: Your master behind you, you set off into the world alone. You had better find a new place to stay.

**Modifiers**: [Solitary Magic](home/Events/Solitary-Magic)

