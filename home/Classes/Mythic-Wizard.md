**Description**: Their names reappear throughout the ages, and few believe they ever existed.

**Tags**: [T_Tier6](home/Tags)

**Requirements**: [Dhrunic Magic](home/Events/Dhrunic-Magic) > 0 and [Mage Lore](home/Skills/Mage-Lore) ≥ 35 and [Spellcraft](home/Skills/Spellcraft) ≥ 10 and [Vile](home/Events/Vile) = 0

**Need**: G.Evil<=0

**Cost to acquire**

- [Research](home/Resources/Research): 20000

- [Arcana](home/Resources/Arcana): 75

- [Tomes](home/Resources/Tomes): 50

- [Star Shard](home/Resources/Star-Shard): 3

- [Rune Stones](home/Resources/Rune-Stones): 10

**Modifiers**

- [Ageless Magic](home/Events/Ageless-Magic): True

- [Spellcraft Max](home/Skills/Spellcraft): 1

- [Pyromancy Max](home/Skills/Pyromancy): 1

- [Enchanting Max](home/Skills/Enchanting): 1

- [Aquamancy Max](home/Skills/Aquamancy): 1

- [Aquamancy Rate](home/Skills/Aquamancy): 1%

- [Geomancy Max](home/Skills/Geomancy): 1

- [Crafting Max](home/Skills/Crafting): 2

- [World Lore Max](home/Skills/World-Lore): 1

- [Potions Max](home/Skills/Potions): 1

- [Alchemy Max](home/Skills/Alchemy): 1

