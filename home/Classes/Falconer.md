**Tags**: [T_Job](home/Tags)

**Action Description**: Tend your master's owls and falcons.

**Requirements**: [Animal Handling](home/Skills/Animal-Handling)

**Cost to acquire**

- [Gold](home/Resources/Gold): 100

- [Research](home/Resources/Research): 100

- [Arcana](home/Resources/Arcana): 2

**Result**

- [Promotion](home/Events/Promotion): True

- [Skill Points](home/Player/Skill-Points): 1.25

- Player Exp: 10

**Modifiers**

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Gold Rate](home/Resources/Gold): 0.07

- [Animal Handling Max](home/Skills/Animal-Handling): 3

- [Animal Handling Rate](home/Skills/Animal-Handling): 0.2

- [Nature Max](home/Resources/Nature): 2

- [Nature Rate](home/Resources/Nature): 0.1

