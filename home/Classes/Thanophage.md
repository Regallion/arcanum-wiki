**Tags**: [T_Tier3](home/Tags)

**Description**: Drain corpses of their lingering life-force to prolong your bleak existence.

**Requirements**: [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and [Vile](home/Events/Vile) > 0 and [Magical Mastery](home/Events/Magical-Mastery) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 1500

- [Bones](home/Resources/Bones): 100

- [Bodies](home/Resources/Bodies): 50

- [Bone Dust](home/Resources/Bone-Dust): 25

- [Arcana](home/Resources/Arcana): 15

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- [Evil](home/Player/Evil): 20

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Research Max](home/Resources/Research): 25

- [Embalming Max](home/Skills/Embalming): 1

- [Reanimation Max](home/Skills/Reanimation): 2

- [Reanimation Rate](home/Skills/Reanimation): 5%

- [Umbramancy Max](home/Skills/Umbramancy): 2

- [Umbramancy Rate](home/Skills/Umbramancy): 5%

