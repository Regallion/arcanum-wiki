**Tags**: [T_Tier2](home/Tags)

**Description**: Extend your life with spare parts from the dead.

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Reanimator](home/Classes/Reanimator) ≥ 1 and [Vile](home/Events/Vile) > 0 and [Arcane Ascendant](home/Events/Arcane-Ascendant) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 800

- [Bodies](home/Resources/Bodies): 3

- [Bones](home/Resources/Bones): 10

- [Bone Dust](home/Resources/Bone-Dust): 15

- [Arcana](home/Resources/Arcana): 7

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Evil](home/Player/Evil): 10

- [Research Max](home/Resources/Research): 10

- [Embalming Max](home/Skills/Embalming): 1

- [Reanimation Max](home/Skills/Reanimation): 2

- [Umbramancy Max](home/Skills/Umbramancy): 3

- [Potions Max](home/Skills/Potions): 2

- [Spirit Communion Max](home/Skills/Spirit-Communion): 1

**Flavor**: Even the dead have their uses

