**Description**: A penchant for chaos

**Secret**: True

**Tags**: [T_Tier2](home/Tags)

**Requirements**: [Trickery](home/Skills/Trickery) ≥ 7 and [Professional Magick](home/Events/Professional-Magick) > 0 and [Arcane Ascendant](home/Events/Arcane-Ascendant) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 777

- [Gemstones](home/Resources/Gemstones): 7

- [Arcane Gem](home/Resources/Arcane-Gem): 7

- [Air Gem](home/Resources/Air-Gem): 7

- [Water Gem](home/Resources/Water-Gem): 7

- [Tomes](home/Resources/Tomes): 7

- [Gold](home/Resources/Gold): 777

- [Arcana](home/Resources/Arcana): 7

**Result**

- [Skill Points](home/Player/Skill-Points): 3

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Gold Max](home/Resources/Gold): 50

- [Trickery Max](home/Skills/Trickery): 2

- [Trickery Rate](home/Skills/Trickery): 10%

- [Conjuration Max](home/Skills/Conjuration): 1

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Potions Max](home/Skills/Potions): 1

- [Mysticism Max](home/Skills/Mysticism): 1

- [Mysticism Rate](home/Skills/Mysticism): 5%

