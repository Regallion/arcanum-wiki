**Description**: A reckoning comes.

**Requirements**: [Supreme Sorcery](home/Events/Supreme-Sorcery) > 0 and [Spirit](home/Resources/Spirit) ≥ 15 and [Divination](home/Skills/Divination) ≥ 17

**Tags**: [T_Tier5](home/Tags)

**Cost to acquire**

- [Research](home/Resources/Research): 15000

- [Arcana](home/Resources/Arcana): 25

- [Tomes](home/Resources/Tomes): 15

- [Spirit Gem](home/Resources/Spirit-Gem): 50

- [Rune Stones](home/Resources/Rune-Stones): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Dhrunic Magic](home/Events/Dhrunic-Magic): True

- [Spirit Communion Max](home/Skills/Spirit-Communion): 3

- [Spirit Communion Rate](home/Skills/Spirit-Communion): 12%

- [Divination Max](home/Skills/Divination): 3

- [Divination Rate](home/Skills/Divination): 10%

- [Scrying Max](home/Skills/Scrying): 2

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Mage Lore Rate](home/Skills/Mage-Lore): 10%

**Flavor**: It waits in time

