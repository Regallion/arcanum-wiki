**Action Description**: The final step on the twisting stairwell of apprenticeship.

**Requirements**: ([Promotion](home/Events/Promotion) > 0) and [Research Max](home/Resources/Research) ≥ 125 and [Spellbook](home/Tasks/Spellbook) > 0

**Log Entry**

- Name: Neophyte

- Desc: Your master has decided you are ready to take the next step on the path of wizardry.

**Flavor**: Why does Master charge so many fees?

**Cost to acquire**

- [Gold](home/Resources/Gold): 150

- [Research](home/Resources/Research): 175

- [Arcana](home/Resources/Arcana): 3

**Result**

- [Skill Points](home/Player/Skill-Points): 1

- Player Exp: 10

**Modifiers**

- [Research Max](home/Resources/Research): 10

- [Arcana Rate](home/Resources/Arcana): 0.0001

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Mage Lore Rate](home/Skills/Mage-Lore): 0.2

