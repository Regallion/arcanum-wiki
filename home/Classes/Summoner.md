**Tags**: [T_Tier3](home/Tags)

**Description**: Alone or with friends, a summoner is never without an army.

**Requirements**: Minions > 4 and [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and [Mage Lore](home/Skills/Mage-Lore) ≥ 20

**Cost to acquire**

- [Research](home/Resources/Research): 2000

- [Arcana](home/Resources/Arcana): 20

- [Tomes](home/Resources/Tomes): 5

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- Minions Max: 1

- [Conjuration Max](home/Skills/Conjuration): 2

- [Conjuration Rate](home/Skills/Conjuration): 2%

- [Charms Max](home/Skills/Charms): 1

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Mage Lore Rate](home/Skills/Mage-Lore): 5%

- [Mana Max](home/Resources/Mana): 2

- [Summoning Max](home/Skills/Summoning): 4

- [Summoning Rate](home/Skills/Summoning): 20%

**Flavor**: How you do I; hope you've met my...

