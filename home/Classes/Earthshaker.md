**Description**: Master of the tectonics.

**Tags**: [T_Tier4](home/Tags)

**Action Description**: Become an earthshaker.

**Requirements**: [Magical Mastery](home/Events/Magical-Mastery) > 0 and [Geomancy](home/Skills/Geomancy) ≥ 20 and [Supreme Sorcery](home/Events/Supreme-Sorcery) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 4000

- [Arcana](home/Resources/Arcana): 25

- [Tomes](home/Resources/Tomes): 30

- [Earth Gem](home/Resources/Earth-Gem): 50

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Supreme Sorcery](home/Events/Supreme-Sorcery): True

- [Geomancy Max](home/Skills/Geomancy): 3

- defense: 15

- [Stamina Max](home/Player/Stamina): 20%

- [Earth Rate](home/Resources/Earth): 0.3

- [Earthen Spire Space Max](home/Homes/Earthen-Spire): 50

**Flavor**: Finally, a subduction license!

