**Description**: A master of charms and enchantments.

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Enchanting](home/Skills/Enchanting) ≥ 6 and [Charms](home/Skills/Charms) ≥ 3

**Tags**: [T_Tier2](home/Tags)

**Cost to acquire**

- [Research](home/Resources/Research): 1200

- [Arcane Gem](home/Resources/Arcane-Gem): 10

- [Tomes](home/Resources/Tomes): 5

- [Arcana](home/Resources/Arcana): 20

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Research Max](home/Resources/Research): 15

- [Enchanting Max](home/Skills/Enchanting): 3

- [Arcana Max](home/Resources/Arcana): 2

- [Nature Studies Max](home/Skills/Nature-Studies): 2

- [Mana Max](home/Resources/Mana): 3

- [Aeromancy Max](home/Skills/Aeromancy): 2

- [Aquamancy Max](home/Skills/Aquamancy): 2

