**Tags**: [T_Tier1](home/Tags)

**Description**: A diviner.

**Requirements**: [Solitary Magic](home/Events/Solitary-Magic) > 0 and [Spirit Communion](home/Skills/Spirit-Communion) ≥ 5 and [Divination](home/Skills/Divination) ≥ 5

**Cost to acquire**

- [Research](home/Resources/Research): 750

- [Gemstones](home/Resources/Gemstones): 10

- [Codices](home/Resources/Codices): 20

- [Arcana](home/Resources/Arcana): 5

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Professional Magick](home/Events/Professional-Magick): True

- [Virtue](home/Player/Virtue): 1

- [Research Max](home/Resources/Research): 15

- [Mage Lore Max](home/Skills/Mage-Lore): 2

- [Scrying Max](home/Skills/Scrying): 2

- [Mana Max](home/Resources/Mana): 3

- [Divination Max](home/Skills/Divination): 2

- [Divination Rate](home/Skills/Divination): 0.1

- [Aeromancy Max](home/Skills/Aeromancy): 1

- [Spirit Communion Max](home/Skills/Spirit-Communion): 1

- [Spirit Communion Rate](home/Skills/Spirit-Communion): 0.1

