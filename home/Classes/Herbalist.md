**Tags**: [T_Job](home/Tags)

**Action Description**: Gather herbs for your master's potions and alchemy.

**Requirements**: [Herbalism](home/Skills/Herbalism)

**Cost to acquire**

- [Gold](home/Resources/Gold): 100

- [Research](home/Resources/Research): 100

- [Arcana](home/Resources/Arcana): 2

**Result**

- [Promotion](home/Events/Promotion): True

- [Skill Points](home/Player/Skill-Points): 1.5

- Player Exp: 10

**Modifiers**

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Gold Rate](home/Resources/Gold): 0.08

- [Herbalism Rate](home/Skills/Herbalism): 0.2

- [Herbalism Max](home/Skills/Herbalism): 3

- [Alchemy Rate](home/Skills/Alchemy): 0.1

- [Alchemy Max](home/Skills/Alchemy): 2

