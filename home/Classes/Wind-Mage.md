**Description**: To walk on the wind

**Tags**: [T_Tier2](home/Tags)

**Action Description**: Become a wind mage.

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Aeromancy](home/Skills/Aeromancy) ≥ 10

**Cost to acquire**

- [Research](home/Resources/Research): 1000

- [Arcana](home/Resources/Arcana): 15

- [Tomes](home/Resources/Tomes): 10

- [Air Gem](home/Resources/Air-Gem): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Aeromancy Max](home/Skills/Aeromancy): 2

- [Aeromancy Rate](home/Skills/Aeromancy): 10%

- [Dodge](home/Player/Dodge): 10%

- [Mana Max](home/Resources/Mana): 2

**Flavor**: There's a storm coming

