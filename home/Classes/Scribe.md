**Tags**: [T_Job](home/Tags)

**Action Description**: Become your master's personal scribe.

**Requirements**: [Scribe Scroll](home/Tasks/Scribe-Scroll)

**Cost to acquire**

- [Gold](home/Resources/Gold): 100

- [Research](home/Resources/Research): 100

- [Arcana](home/Resources/Arcana): 2

**Result**

- [Promotion](home/Events/Promotion): True

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Mage Lore Max](home/Skills/Mage-Lore): 1.5

- [Arcana Rate](home/Resources/Arcana): 0.0001

- [Research Max](home/Resources/Research): 10

- [World Lore Max](home/Skills/World-Lore): 1

- [Gold Rate](home/Resources/Gold): 0.1

- [Mage Lore Rate](home/Skills/Mage-Lore): 0.2

**Flavor**: When I get a little money I buy books, and if any is left I buy food and clothes.

