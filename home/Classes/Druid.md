**Description**: A spellcaster dedicated to the natural world

**Tags**: [T_Tier3](home/Tags)

**Requirements**: [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and [Nature Studies](home/Skills/Nature-Studies) ≥ 15

**Cost to acquire**

- [Research](home/Resources/Research): 1000

- [Nature Gem](home/Resources/Nature-Gem): 20

- [Herbs](home/Resources/Herbs): 50

- [Arcana](home/Resources/Arcana): 20

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- [Nature Studies Max](home/Skills/Nature-Studies): 3

- [Nature Studies Rate](home/Skills/Nature-Studies): 0.2+5%

- [Animal Handling Max](home/Skills/Animal-Handling): 2

- [Herbalism Max](home/Skills/Herbalism): 2

- [Herbalism Rate](home/Skills/Herbalism): 10%

- [Potions Max](home/Skills/Potions): 1

- [Potions Rate](home/Skills/Potions): 0.4

