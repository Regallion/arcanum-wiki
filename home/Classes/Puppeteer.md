**Description**: Assemble and control mechanical minions.

**Tags**: [T_Tier1](home/Tags)

**Requirements**: [Solitary Magic](home/Events/Solitary-Magic) > 0 and [Puppetry](home/Skills/Puppetry) ≥ 5

**Cost to acquire**

- [Research](home/Resources/Research): 700

- [Arcana](home/Resources/Arcana): 10

- [Gold](home/Resources/Gold): 500

**Modifiers**

- [Professional Magick](home/Events/Professional-Magick): True

- [Puppetry Max](home/Skills/Puppetry): 2

- Minions Keep: puppet

- [Animation Max](home/Skills/Animation): 1

- Allies Max: 5

- [Clockwork Home Space Max](home/Homes/Clockwork-Home): 50

**Flavor**: No matter what, the strings would not break.

