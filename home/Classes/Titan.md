**Description**: A mantle of metal and stone.

**Tags**: [T_Tier6](home/Tags)

**Action Description**: Become a titan.

**Requirements**: [Dhrunic Magic](home/Events/Dhrunic-Magic) > 0 and ([Earthshaker](home/Classes/Earthshaker) + [Geomancer](home/Classes/Geomancer)) > 0 and [Hammer Proficiency](home/Skills/Hammer-Proficiency) ≥ 10 and [Geosculpting](home/Skills/Geosculpting) ≥ 10 and [Ageless Magic](home/Events/Ageless-Magic) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 10000

- [Arcana](home/Resources/Arcana): 50

- [Tomes](home/Resources/Tomes): 40

- [Earth Gem](home/Resources/Earth-Gem): 100

- [Earth Runes](home/Resources/Earth-Runes): 20

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Ageless Magic](home/Events/Ageless-Magic): True

- [Geomancy Max](home/Skills/Geomancy): 3

- defense: 30

- [Stamina Max](home/Player/Stamina): 20%

- [Hammer Proficiency Max](home/Skills/Hammer-Proficiency): 3

- [Earthen Spire Space Max](home/Homes/Earthen-Spire): 100

**Flavor**: No stone left unturned

