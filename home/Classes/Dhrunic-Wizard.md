**Alias**: dhrunic wizard

**Description**: Raw magic power

**Tags**: [T_Tier5](home/Tags)

**Requirements**: [Supreme Sorcery](home/Events/Supreme-Sorcery) > 0 and [Mage Lore](home/Skills/Mage-Lore) ≥ 30

**Cost to acquire**

- [Research](home/Resources/Research): 20000

- [Arcana](home/Resources/Arcana): 30

- [Tomes](home/Resources/Tomes): 30

- [Rune Stones](home/Resources/Rune-Stones): 10

**Modifiers**

- [Dhrunic Magic](home/Events/Dhrunic-Magic): True

- [Languages Max](home/Skills/Languages): 2

- [Pyromancy Max](home/Skills/Pyromancy): 1

- [Pyromancy Rate](home/Skills/Pyromancy): 1%

- [Aquamancy Max](home/Skills/Aquamancy): 1

- [Aeromancy Max](home/Skills/Aeromancy): 1

- [Aeromancy Rate](home/Skills/Aeromancy): 1%

- [Lumenology Max](home/Skills/Lumenology): 1

- [Crafting Max](home/Skills/Crafting): 2

- [Mage Lore Max](home/Skills/Mage-Lore): 5

- [Mage Lore Rate](home/Skills/Mage-Lore): 10%

- [Mana Max](home/Resources/Mana): 3

**Flavor**: In the end it was three Thule Wizards and The Necromancer who brought the locks to heel.

