**Description**: Perform arcane rituals to boost your powers.

**Tags**: [T_Tier2](home/Tags)

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Rituals](home/Skills/Rituals) ≥ 5 and [Arcane Ascendant](home/Events/Arcane-Ascendant) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 700

- [Arcana](home/Resources/Arcana): 10

- [Gold](home/Resources/Gold): 500

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Rituals Max](home/Skills/Rituals): 2

- [Ritual Wards Rate](home/Resources/Ritual-Wards): 0.0002

- [Arcane Paper Rate](home/Resources/Arcane-Paper): 0.001

**Flavor**: The first rule of ritual magic is thus: 'Make sure you are prepared to contain the explosions.'

