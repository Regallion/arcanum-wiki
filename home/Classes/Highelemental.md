**Tags**: [T_Tier3](home/Tags)

**Description**: Lord of the elements

**Requirements**: [Arcane Ascendant](home/Events/Arcane-Ascendant) > 0 and [Aeromancy](home/Skills/Aeromancy) > 7 and [Pyromancy](home/Skills/Pyromancy) > 7 and [Aquamancy](home/Skills/Aquamancy) > 7 and [Geomancy](home/Skills/Geomancy) > 7

**Cost to acquire**

- [Arcana](home/Resources/Arcana): 15

- [Tomes](home/Resources/Tomes): 10

- [Air Gem](home/Resources/Air-Gem): 20

- [Fire Gem](home/Resources/Fire-Gem): 20

- [Earth Gem](home/Resources/Earth-Gem): 20

- [Water Gem](home/Resources/Water-Gem): 20

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- [Aeromancy Max](home/Skills/Aeromancy): 2

- [Pyromancy Max](home/Skills/Pyromancy): 2

- [Geomancy Max](home/Skills/Geomancy): 2

- [Aquamancy Max](home/Skills/Aquamancy): 2

- [Research Max](home/Resources/Research): 25

- [Elemental Max](home/Tags): 2

- [Elemental Rate](home/Tags): 0.2

