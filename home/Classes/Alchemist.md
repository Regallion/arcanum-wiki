**Tags**: [T_Tier2](home/Tags)

**Description**: An alchemist uses magical knowledge to craft wonders.

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Potions](home/Skills/Potions) ≥ 4 and [Alchemy](home/Skills/Alchemy) ≥ 5

**Cost to acquire**

- [Research](home/Resources/Research): 2000

- [Arcane Gem](home/Resources/Arcane-Gem): 5

- [Fire Gem](home/Resources/Fire-Gem): 2

- [Water Gem](home/Resources/Water-Gem): 2

- [Earth Gem](home/Resources/Earth-Gem): 2

- [Arcana](home/Resources/Arcana): 20

**Result**

- [Skill Points](home/Player/Skill-Points): 3

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Research Max](home/Resources/Research): 100

- [Crafting Max](home/Skills/Crafting): 3

- [Crafting Rate](home/Skills/Crafting): 0.3

- [Potions Max](home/Skills/Potions): 3

- [Potions Rate](home/Skills/Potions): 5%

- [Alchemy Max](home/Skills/Alchemy): 3

- [Alchemy Rate](home/Skills/Alchemy): 5%

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Mage Lore Rate](home/Skills/Mage-Lore): 0.3

