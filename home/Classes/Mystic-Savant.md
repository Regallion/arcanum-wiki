**Description**: You have no need of these dry tomes and musty old books.

**Secret**: True

**Tags**: [T_Tier0](home/Tags)

**Requirements**: [Study](home/Tasks/Study) ≤ 0 and [Neophyte](home/Classes/Neophyte) > 0 and [Arcana](home/Resources/Arcana) ≥ 5

**Cost to acquire**

- [Arcana](home/Resources/Arcana): 7

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Solitary Magic](home/Events/Solitary-Magic): True

- [Mage Lore Max](home/Skills/Mage-Lore): -1

- [World Lore Max](home/Skills/World-Lore): -1

- [Mage Lore Rate](home/Skills/Mage-Lore): -2%

- [Research Rate](home/Resources/Research): 0.2

- [Conjuration Max](home/Skills/Conjuration): 1

- [Mana Max](home/Resources/Mana): 1

- [Spirit Communion Max](home/Skills/Spirit-Communion): 1

- [Mysticism Max](home/Skills/Mysticism): 2

- [Mysticism Rate](home/Skills/Mysticism): 0.1

