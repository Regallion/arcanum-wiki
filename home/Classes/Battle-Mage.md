**Tags**: [T_Tier2](home/Tags)

**Action Description**: Pursue the path of battle.

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Level](home/Player/Level) ≥ 10 and [Armory](home/Furnitures/Armory) > 0 and [Arcane Ascendant](home/Events/Arcane-Ascendant) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 1500

- [Arcana](home/Resources/Arcana): 7

- [Gold](home/Resources/Gold): 800

- [Gemstones](home/Resources/Gemstones): 20

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Research Max](home/Resources/Research): -50

- [Mana Max](home/Resources/Mana): 1

- [Mana Rate](home/Resources/Mana): 0.1

- [Pyromancy Max](home/Skills/Pyromancy): 2

- [Pyromancy Rate](home/Skills/Pyromancy): 0.1

- [Geomancy Max](home/Skills/Geomancy): 1

