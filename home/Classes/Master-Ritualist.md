**Description**: There is no ritual you fear to attempt.

**Tags**: [T_Tier5](home/Tags)

**Requirements**: [Supreme Sorcery](home/Events/Supreme-Sorcery) > 0 and [Rituals](home/Skills/Rituals) ≥ 10 and [Dhrunic Magic](home/Events/Dhrunic-Magic) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 3000

- [Arcana](home/Resources/Arcana): 25

- [Ritual Paper](home/Resources/Ritual-Paper): 25

- [Ritual Wards](home/Resources/Ritual-Wards): 10

- [Ritual Notes](home/Resources/Ritual-Notes): 15

**Modifiers**

- [Dhrunic Magic](home/Events/Dhrunic-Magic): True

- [Rituals Max](home/Skills/Rituals): 2

- [Ritual Wards Rate](home/Resources/Ritual-Wards): 0.0003

- [Arcane Paper Rate](home/Resources/Arcane-Paper): 0.002

**Flavor**: The last rule of ritual magic is thus: 'No cost is too great if it guarantees success.'

