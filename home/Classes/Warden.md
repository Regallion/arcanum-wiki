**Description**: A keeper of the forests, mountains, and hills. Only those living in the wild may become wardens.

**Tags**: [T_Tier2](home/Tags)

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Nature Studies](home/Skills/Nature-Studies) ≥ 12

**Need**: (G.Outdoors+G.Lodge)>0

**Cost to acquire**

- [Research](home/Resources/Research): 1500

- [Nature Gem](home/Resources/Nature-Gem): 15

- [Herbs](home/Resources/Herbs): 50

- [Arcana](home/Resources/Arcana): 15

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Research Max](home/Resources/Research): 5

- [Nature Studies Max](home/Skills/Nature-Studies): 3

- [Nature Studies Rate](home/Skills/Nature-Studies): 1+15%

- [Animal Handling Max](home/Skills/Animal-Handling): 3

- [Potions Max](home/Skills/Potions): 1

