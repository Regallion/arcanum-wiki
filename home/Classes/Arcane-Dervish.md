**Tags**: [T_Tier5](home/Tags)

**Description**: Your skills with blade and magic blend together in a blur, each stroke seeming like a step in a dance.

**Requirements**: [Supreme Sorcery](home/Events/Supreme-Sorcery) > 0 and [Spell Blade](home/Classes/Spell-Blade) > 0 and [Dhrunic Magic](home/Events/Dhrunic-Magic) = 0

**Cost to acquire**

- [Research](home/Resources/Research): 10000

- [Arcana](home/Resources/Arcana): 20

- [Gold](home/Resources/Gold): 4000

- [Air Gem](home/Resources/Air-Gem): 20

- [Fire Gem](home/Resources/Fire-Gem): 10

- [Earth Gem](home/Resources/Earth-Gem): 10

- [Water Gem](home/Resources/Water-Gem): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Dhrunic Magic](home/Events/Dhrunic-Magic): True

- tohit: 15%

- [World Lore Max](home/Skills/World-Lore): 1

- [Enchanting Max](home/Skills/Enchanting): 1

- [Elemental Max](home/Tags): 1

- [Swordplay Max](home/Skills/Swordplay): 2

- [Anatomy Max](home/Skills/Anatomy): 1

- [Greater Arcane Body Max](home/Upgrades/Greater-Arcane-Body): 1

