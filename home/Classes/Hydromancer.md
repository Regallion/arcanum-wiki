**Description**: Master of water

**Tags**: [T_Tier2](home/Tags)

**Action Description**: Become a hydromancer.

**Requirements**: [Professional Magick](home/Events/Professional-Magick) > 0 and [Aquamancy](home/Skills/Aquamancy) ≥ 10

**Cost to acquire**

- [Research](home/Resources/Research): 1000

- [Arcana](home/Resources/Arcana): 15

- [Tomes](home/Resources/Tomes): 10

- [Water Gem](home/Resources/Water-Gem): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 2

**Modifiers**

- [Arcane Ascendant](home/Events/Arcane-Ascendant): True

- [Aquamancy Max](home/Skills/Aquamancy): 2

- [Aquamancy Rate](home/Skills/Aquamancy): 10%

- [Dodge](home/Player/Dodge): 10%

- [Mana Max](home/Resources/Mana): 2

