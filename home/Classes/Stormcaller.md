**Requirements**: [Hydromancer](home/Classes/Hydromancer) > 0 or [Wind Mage](home/Classes/Wind-Mage) > 0

**Tags**: [T_Tier3](home/Tags)

**Cost to acquire**

- [Research](home/Resources/Research): 3000

- [Arcana](home/Resources/Arcana): 25

- [Air Gem](home/Resources/Air-Gem): 20

- [Water Gem](home/Resources/Water-Gem): 20

- [Tomes](home/Resources/Tomes): 10

**Result**

- [Skill Points](home/Player/Skill-Points): 1

**Modifiers**

- [Magical Mastery](home/Events/Magical-Mastery): True

- [Mage Lore Max](home/Skills/Mage-Lore): 1

- [Mana Max](home/Resources/Mana): 2

- [Aeromancy Max](home/Skills/Aeromancy): 3

- [Aquamancy Max](home/Skills/Aquamancy): 3

- [Aeromancy Rate](home/Skills/Aeromancy): 15%

- [Aquamancy Rate](home/Skills/Aquamancy): 15%

**Flavor**: I am the storm

