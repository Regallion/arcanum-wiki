**Level**: 3

**Length**: 50

**Requirements**: [Winter Lore](home/Skills/Winter-Lore) ≥ 4 and [Magical Mastery](home/Events/Magical-Mastery) > 0

**Description**: The frozen tower once belonged to a wizard who mastered the mysteries of ice

**Run Cost**

- [Stamina](home/Player/Stamina): 0.5

**Effects**

- [Frost](home/Resources/Frost): 1

**Result**

- Title: Nixifer's Disciple

- [Essence of Winter](home/Resources/Essence-of-Winter): 5

**Encounters**

- [Cold Snap](home/Encounters/Cold-Snap)

- [Cold Snap](home/Encounters/Cold-Snap)

- [Cold Snap](home/Encounters/Cold-Snap)

- [Ebin's Notes](home/Encounters/Ebin's-Notes)

- [Ebin's Notes](home/Encounters/Ebin's-Notes)

- [Ebin's Notes](home/Encounters/Ebin's-Notes)

- [Tome of Winters Past](home/Encounters/Tome-of-Winters-Past)

- [Tome of Winters Past](home/Encounters/Tome-of-Winters-Past)

- [Frozen Bookcase](home/Encounters/Frozen-Bookcase)

- [Frozen Bookcase](home/Encounters/Frozen-Bookcase)

- [Dusty Chest (enc_chest3)](home/Encounters/Dusty-Chest-(enc_chest3))

- [Ice Sculpture](home/Encounters/Ice-Sculpture)

**Symbol**: ❄️

