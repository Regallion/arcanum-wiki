**Description**: At the center of the world the great idrasil stretches to the heavens.

**Flavor**: A single seed once fell from Yggrasil.

**Symbol**: 🌼

