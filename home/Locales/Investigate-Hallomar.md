**Description**: Some people just vanish in thin air, and pop up unexpectedly

**Requirements**: [Strange Village](home/Events/Strange-Village)

**Length**: 20

**Level**: 3

**Run Cost**

- [Stamina](home/Player/Stamina): 0.5

- [Apple](home/Resources/Apple): 0.1

**Result**

- [Treat Max](home/Resources/Treat): 10

- [Apple Max](home/Resources/Apple): 10

**Encounters**

- [Berta](home/Encounters/Berta)

- [Old Harold](home/Encounters/Old-Harold)

- [Fox](home/Encounters/Fox)

- [Grumpy Granny](home/Encounters/Grumpy-Granny)

- [Hank Is Missing](home/Encounters/Hank-Is-Missing)

**Symbol**: 🎃

