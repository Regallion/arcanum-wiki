**Description**: The lost Hall of Ages was a gathering place of great and powerful patron mages.

**Requirements**: [Magical Mastery](home/Events/Magical-Mastery) > 0

**Title**: Mark of Ages

**Length**: 100

**Modifiers**

- [World Lore Max](home/Skills/World-Lore): ?1

**Encounters**

- [Kaidi](home/Encounters/Kaidi)

- [View Stars With Cyril](home/Encounters/View-Stars-With-Cyril)

- [Jeremi](home/Encounters/Jeremi)

- [Study Under Kanna](home/Encounters/Study-Under-Kanna)

- [Phrenesis](home/Encounters/Phrenesis)

- [Sinae](home/Encounters/Sinae)

- [Stags](home/Encounters/Stags)

- [Tainted, Archon of the Void](home/Encounters/Tainted,-Archon-of-the-Void)

- [A Joker](home/Encounters/A-Joker)

- [Vondrey](home/Encounters/Vondrey)

- [Mister Reaper](home/Encounters/Mister-Reaper)

- [Master Wild](home/Encounters/Master-Wild)

**Flavor**: the knowledge within may be beyond price.

**Symbol**: ⭐

