**Level**: 23

**Requirements**: [Dhrunic Magic](home/Events/Dhrunic-Magic) > 0 and [Magic Beasts](home/Skills/Magic-Beasts) ≥ 5

**Length**: 200

**Description**: Beneath the tallest idrasil a great menagerie thrives beneath a second sky.

**Run Cost**

- [Stamina](home/Player/Stamina): 3

**Modifiers**

- [Magic Beasts Max](home/Skills/Magic-Beasts): ?1

**Effects**

- [Magic Beasts Exp](home/Skills/Magic-Beasts): 1

**Result**

- Title: Wild Compiler

**Loot**

- [Gemstones](home/Resources/Gemstones): 5~10

- [Herbs](home/Resources/Herbs): 10~20

**Encounters**

- [Agolith](home/Encounters/Agolith)

- [Balmuth](home/Encounters/Balmuth)

- [Barghest](home/Encounters/Barghest)

- [Menagerie Hanging](home/Encounters/Menagerie-Hanging)

- [Bestiary](home/Encounters/Bestiary)

- [Scale of Ouroboros](home/Encounters/Scale-of-Ouroboros)

- [Cockatrice](home/Encounters/Cockatrice)

- [Flithy](home/Encounters/Flithy)

- [Gryffon](home/Encounters/Gryffon)

- [Hydra](home/Encounters/Hydra)

- [Phoenix](home/Encounters/Phoenix)

- [Pod of Poglers](home/Encounters/Pod-of-Poglers)

- [Snake Swarm](home/Encounters/Snake-Swarm)

- [Moss-Stone Portal](home/Encounters/Moss-Stone-Portal)

- [Spider Swarm](home/Encounters/Spider-Swarm)

- [Trumple](home/Encounters/Trumple)

- [Wyvern](home/Encounters/Wyvern)

**Flavor**: Its creator and purpose are unknown to history. Perhaps all creatures exist within.

