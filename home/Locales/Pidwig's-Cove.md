**Level**: 9

**Requirements**: [World Lore](home/Skills/World-Lore) ≥ 5 and [Eryl Eyot](home/Locales/Eryl-Eyot) > 0

**Length**: 25

**Description**: The half-goblin Pidwig has sailed the waters around his bay for hundreds of years. In that time he has collected a myriad of interesting artifacts.

**Run Cost**

- [Stamina](home/Player/Stamina): 0.3

**Result**

- [Arcana](home/Resources/Arcana): 0.1

**Loot**

- [Gold](home/Resources/Gold): 20~30

- [Gemstones](home/Resources/Gemstones): 4~10

**Encounters**

- [Dusty Chest (enc_chest3)](home/Encounters/Dusty-Chest-(enc_chest3))

- [Pidwig's Starcharts](home/Encounters/Pidwig's-Starcharts)

- [Talk To Pidwig](home/Encounters/Talk-To-Pidwig)

- [Murky Waters](home/Encounters/Murky-Waters)

- [Pidwig's Hoard](home/Encounters/Pidwig's-Hoard)

- [Dusty Chest (enc_chest1)](home/Encounters/Dusty-Chest-(enc_chest1))

- [Somber Sunset](home/Encounters/Somber-Sunset)

- [Starry Night](home/Encounters/Starry-Night)

