**Level**: 0

**Length**: 100

**Description**: Nestled in the fog of the midlands, the village of Hallomar holds a yearly celebration in its pumpkin patch.

**Requirements**: [Hallomar Festivities](home/Events/Hallomar-Festivities)

**Run Cost**

- [Stamina](home/Player/Stamina): 0.2

**Result**

- Title: harvest reveller

- [Pumpkins](home/Resources/Pumpkins): 1~3

- [Cobwebs](home/Resources/Cobwebs): 1

**Loot**

- [Gold](home/Resources/Gold): 10~25

- [Gemstones](home/Resources/Gemstones): 2~5

- [Jack O' Lantern](home/Resources/Jack-O'-Lantern): 1

**Encounters**

- [Outsider](home/Encounters/Outsider)

- [Pumpkin Patch](home/Encounters/Pumpkin-Patch)

- [Haunted Glade](home/Encounters/Haunted-Glade)

- [Pumpkin Patch](home/Encounters/Pumpkin-Patch)

- [Pumpkin Patch](home/Encounters/Pumpkin-Patch)

- [Pumpkin Patch](home/Encounters/Pumpkin-Patch)

- [Foggy Dale](home/Encounters/Foggy-Dale)

- [Dusty Chest](home/Encounters/Dusty-Chest)

- [Hidden Cache](home/Encounters/Hidden-Cache)

- [Starry Night](home/Encounters/Starry-Night)

**Flavor**: Just don't wander too far from the village.

**Symbol**: 🎃

