**Level**: 7

**Length**: 40

**Description**: The deeper woods posses many features of interest to the magical scholar. The spirits of the place make sure to keep the troublemakers out.

**Run Cost**

- [Stamina](home/Player/Stamina): 0.25

**Result**

- [Arcana](home/Resources/Arcana): 0.1

- [Nature Studies Exp](home/Skills/Nature-Studies): 10

- [Herbalism Exp](home/Skills/Herbalism): 5

- [Animal Handling Exp](home/Skills/Animal-Handling): 5

**Loot**

- [Herbs](home/Resources/Herbs): 5~10

**Encounters**

- [Mana Tree](home/Encounters/Mana-Tree)

- [Mystic Waters](home/Encounters/Mystic-Waters)

- [Bright Vista](home/Encounters/Bright-Vista)

- [Haunted Glade](home/Encounters/Haunted-Glade)

- [Babbling Delki](home/Encounters/Babbling-Delki)

- [Hidden Cache](home/Encounters/Hidden-Cache)

