**Level**: 2

**Length**: 50

**Requirements**: [Emblem of Ice](home/Resources/Emblem-of-Ice) ≥ 1

**Description**: Now that you command the power of winter, the villagers ask you to help with their preparations

**Run Cost**

- [Stamina](home/Player/Stamina): 0.4

**Effects**

- [Fire](home/Resources/Fire): 0.2

**Modifiers**

- [Cryomancy Max](home/Skills/Cryomancy): 1%

**Result**

- [Livingsnow](home/Resources/Livingsnow): 10

- [Essence of Winter](home/Resources/Essence-of-Winter): 5

**Loot**

- [Snomunculus](home/Resources/Snomunculus): 0~2

**Encounters**

- [Playing In the Snow](home/Encounters/Playing-In-the-Snow)

- [Windbarrier](home/Encounters/Windbarrier)

- [Conjuring](home/Encounters/Conjuring)

- [Crafting Candles](home/Encounters/Crafting-Candles)

- [Stories](home/Encounters/Stories)

**Symbol**: ❄️

