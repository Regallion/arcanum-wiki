**Requirements**: [World Lore](home/Skills/World-Lore) ≥ 7 and [Scrying](home/Skills/Scrying) ≥ 5

**Start Announcement**

- Name: Cities of Orrem

- Desc: A vision in a crystal orb reveals to you the ruins of the lost cities of Orrem.

**Description**: Only a small trading post marks the boundary of the lost cities of Orrem. Weathered masonry jutting from the sands beyond bespeak monuments of unimaginable proportions.

**Length**: 400

**Level**: 19

**Run Cost**

- [Stamina](home/Player/Stamina): 4

**Result**

- Title: Nomad

- [Notoriety](home/Player/Notoriety): 1

**Loot**

- [Jazid's Compass](home/Rares/Jazid's-Compass): 5%

**Encounters**

- [Aeon Clock](home/Encounters/Aeon-Clock)

- [Caravan](home/Encounters/Caravan)

- [Oasis](home/Encounters/Oasis)

- [Orrem Rains](home/Encounters/Orrem-Rains)

- [Winds of Madness](home/Encounters/Winds-of-Madness)

- [Hidden Cave](home/Encounters/Hidden-Cave)

- [Sandstorm](home/Encounters/Sandstorm)

- [Mirage](home/Encounters/Mirage)

- [Eldar Statue](home/Encounters/Eldar-Statue)

- [Strange Bones](home/Encounters/Strange-Bones)

- [Crumbled Statue](home/Encounters/Crumbled-Statue)

- [Starry Night](home/Encounters/Starry-Night)

- [Dusty Chest (enc_chest3)](home/Encounters/Dusty-Chest-(enc_chest3))

- [Lost Trading Post](home/Encounters/Lost-Trading-Post)

- [Scrap Pile](home/Encounters/Scrap-Pile)

- [Pile of Wood?](home/Encounters/Pile-of-Wood_)

