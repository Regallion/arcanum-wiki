**Level**: 12

**Requirements**: [Scrying](home/Skills/Scrying) ≥ 999

**Length**: 50

**Symbol**: 📖

**Run Cost**

- [Stamina](home/Player/Stamina): 0.4

**Result**

- [Arcana](home/Resources/Arcana): 0.1

- [Research](home/Resources/Research): 10

**Loot**

- [Scrolls](home/Resources/Scrolls): 1~4

**Encounters**

- [Occult Tapestry](home/Encounters/Occult-Tapestry)

- [Dusty Chest (enc_chest1)](home/Encounters/Dusty-Chest-(enc_chest1))

- [Mirror of Introspection](home/Encounters/Mirror-of-Introspection)

- [Mirror of False Reflection](home/Encounters/Mirror-of-False-Reflection)

- [Mirror of Foretelling](home/Encounters/Mirror-of-Foretelling)

- [Seething Mirror](home/Encounters/Seething-Mirror)

- [Reflecting Hallways](home/Encounters/Reflecting-Hallways)

- [Void Mirror](home/Encounters/Void-Mirror)

- [Mirror of History](home/Encounters/Mirror-of-History)

- [A Distant Mirror](home/Encounters/A-Distant-Mirror)

- [Water Mirror](home/Encounters/Water-Mirror)

