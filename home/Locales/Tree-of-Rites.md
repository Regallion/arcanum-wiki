**Level**: 5

**Length**: 30

**Requirements**: [Preparation](home/Upgrades/Preparation) ≥ 1

**Description**: A fir older than the mountains stands alone in the midst of a clearing. You feel its age, heavier than the fog itself. The embodiment of the living winter.

**Run Cost**

- [Stamina](home/Player/Stamina): 0.5

**Effects**

- [Frost](home/Resources/Frost): 1

**Modifiers**

- [Cryomancy Max](home/Skills/Cryomancy): 1%

**Result**

- [Snow Double](home/Events/Snow-Double): 1

**Encounters**

- [Siphon Lifeforce](home/Encounters/Siphon-Lifeforce)

- [Drawing Mystic Schemes](home/Encounters/Drawing-Mystic-Schemes)

- [Placing Gems](home/Encounters/Placing-Gems)

- [Connecting Focal Points](home/Encounters/Connecting-Focal-Points)

- [Attuning To Local Magic](home/Encounters/Attuning-To-Local-Magic)

**Symbol**: ❄️

