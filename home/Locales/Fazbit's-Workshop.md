**Level**: 9

**Length**: 50

**Symbol**: 📖

**Requirements**: ([Alchemy](home/Skills/Alchemy) + [Scrying](home/Skills/Scrying)) ≥ 17

**Description**: You could spend a century in Fazbit's lost workshop and not exhaust its creations.

**Loot**

- [Tomes](home/Resources/Tomes): 2~3

- [Gemstones](home/Resources/Gemstones): 3~5

- [Air Gem](home/Resources/Air-Gem): 50%

- [Fire Gem](home/Resources/Fire-Gem): 50%

- [Fazbit's Fixations](home/Tasks/Fazbit's-Fixations): True

**Encounters**

- [Alchemical Equipment](home/Encounters/Alchemical-Equipment)

- [Dusty Chest (enc_chest1)](home/Encounters/Dusty-Chest-(enc_chest1))

- [Iron Chest](home/Encounters/Iron-Chest)

- [Fazbit's Furnace](home/Encounters/Fazbit's-Furnace)

- [Workbook](home/Encounters/Workbook)

- [Alchemical Equipment](home/Encounters/Alchemical-Equipment)

- [Master's Workbook](home/Encounters/Master's-Workbook)

- [Fazbit's Helpers](home/Encounters/Fazbit's-Helpers)

