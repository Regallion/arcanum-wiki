**Level**: 1

**Length**: 15

**Symbol**: 📖

**Requirements**: [Promotion](home/Events/Promotion)

**Description**: A good apprentice spends all their free time in the library. After chores, of course.

**Run Cost**

- [Stamina](home/Player/Stamina): 0.2

**Result**

- [Arcana](home/Resources/Arcana): 0.1

- [Research](home/Resources/Research): 10

**Loot**

- [Scrolls](home/Resources/Scrolls): 1~4

**Encounters**

- [Bookworm](home/Encounters/Bookworm)

- [Occult Tapestry](home/Encounters/Occult-Tapestry)

- [Magical Primer](home/Encounters/Magical-Primer)

- [Dusty Chest (enc_chest1)](home/Encounters/Dusty-Chest-(enc_chest1))

- [Magical Primer](home/Encounters/Magical-Primer)

- [Workbook](home/Encounters/Workbook)

