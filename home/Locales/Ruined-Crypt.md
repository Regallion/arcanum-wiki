**Level**: 4

**Length**: 30

**Requirements**: [Solitary Magic](home/Events/Solitary-Magic)

**Description**: An ancient crypt of crumbling stone.

**Run Cost**

- [Stamina](home/Player/Stamina): 0.35

**Result**

- [Arcana](home/Resources/Arcana): 0.15

- [Research](home/Resources/Research): 20

**Loot**

- [Scrolls](home/Resources/Scrolls): 2~4

**Encounters**

- [Occult Tapestry](home/Encounters/Occult-Tapestry)

- [Mummified Remains](home/Encounters/Mummified-Remains)

- [Sarcophagus](home/Encounters/Sarcophagus)

- [Plague Rats](home/Encounters/Plague-Rats)

- [Strange Bones](home/Encounters/Strange-Bones)

- [Embalming Apparatus](home/Encounters/Embalming-Apparatus)

- [Iron Chest](home/Encounters/Iron-Chest)

- [Eerie Moans](home/Encounters/Eerie-Moans)

- [Pile of Wood?](home/Encounters/Pile-of-Wood_)

