**Description**: Each spring Aylesmeade holds its fabled fair in sight of the World Tree, Aesilwyr.

**Start Announcement**

**Encounters**

- [Maypole Dance](home/Encounters/Maypole-Dance)

- [Aylesmeade Market](home/Encounters/Aylesmeade-Market)

- [Tinker's Wagon](home/Encounters/Tinker's-Wagon)

- [Idle Rumors](home/Encounters/Idle-Rumors)

**Symbol**: 🌼

