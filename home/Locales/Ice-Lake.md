**Level**: 3

**Length**: 50

**Requirements**: [Emblem of Ice](home/Resources/Emblem-of-Ice) ≥ 1

**Description**: At the bottom of the frozen lake, numerous caves can be found, full of curiosities and wonders.

**Run Cost**

- [Stamina](home/Player/Stamina): 0.5

**Effects**

- [Frost](home/Resources/Frost): 1

**Result**

- [Essence of Winter](home/Resources/Essence-of-Winter): 5

**Encounters**

- Movingsnow

- [Cold Snap](home/Encounters/Cold-Snap)

- [Emberstone](home/Encounters/Emberstone)

- [Cold Snap](home/Encounters/Cold-Snap)

- [Water Source](home/Encounters/Water-Source)

- [Frozen Herbs](home/Encounters/Frozen-Herbs)

- [Sparkling Crystals](home/Encounters/Sparkling-Crystals)

- [Dusty Chest (enc_chest3)](home/Encounters/Dusty-Chest-(enc_chest3))

- [Ice Grave](home/Encounters/Ice-Grave)

- [Ice Sculpture](home/Encounters/Ice-Sculpture)

**Symbol**: ❄️

