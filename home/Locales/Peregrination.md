**Level**: 19

**Requirements**: [Travel](home/Skills/Travel) ≥ 5 and [Travel Task](home/Tasks/Travel) ≥ 100

**Description**: Tenwick's trail runs a circuit about the notable areas of Dhrune. On foot it takes the better part of a year to complete.

**Length**: 10000

**Run Cost**

- [Stamina](home/Player/Stamina): 2

**Result**

- Title: Dhrunic Wayfarer

**Loot**

- [Tenwick's Walking Stick](home/Rares/Tenwick's-Walking-Stick): 1

**Encounters**

- [Bright Vista](home/Encounters/Bright-Vista)

- [Pod of Poglers](home/Encounters/Pod-of-Poglers)

- [Agolith](home/Encounters/Agolith)

- [Ancient Battlefield](home/Encounters/Ancient-Battlefield)

- [Black Cat](home/Encounters/Black-Cat)

- [Oak Chest](home/Encounters/Oak-Chest)

- [Babbling Delki](home/Encounters/Babbling-Delki)

- [Crumbled Statue](home/Encounters/Crumbled-Statue)

- [Eldar Statue](home/Encounters/Eldar-Statue)

- [Markhul's Gap](home/Encounters/Markhul's-Gap)

- [Blood Grass](home/Encounters/Blood-Grass)

- [Gibbering](home/Encounters/Gibbering)

- [Affable Gnome](home/Encounters/Affable-Gnome)

- [Thandhrun Pass](home/Encounters/Thandhrun-Pass)

- [The Old Stone](home/Encounters/The-Old-Stone)

- [Sandstorm](home/Encounters/Sandstorm)

- [Star Shrine](home/Encounters/Star-Shrine)

- [Tenwick](home/Encounters/Tenwick)

- [Wyrd Sister](home/Encounters/Wyrd-Sister)

- [Haunted Glade](home/Encounters/Haunted-Glade)

- [Foggy Dale](home/Encounters/Foggy-Dale)

- [Hidden Cache](home/Encounters/Hidden-Cache)

- [Mana Tree](home/Encounters/Mana-Tree)

- [Murky Waters](home/Encounters/Murky-Waters)

- [Mystic Waters](home/Encounters/Mystic-Waters)

- [Somber Sunset](home/Encounters/Somber-Sunset)

- [Starry Night](home/Encounters/Starry-Night)

- [Strange Bones](home/Encounters/Strange-Bones)

