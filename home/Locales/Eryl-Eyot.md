**Level**: 5

**Length**: 30

**Description**: The small and tranquil island is inhabited by an old hermit named thyffr

**Run Cost**

- [Stamina](home/Player/Stamina): 0.22

**Result**

- [Arcana](home/Resources/Arcana): 0.1

- [Research](home/Resources/Research): 10

**Loot**

- [Scrolls](home/Resources/Scrolls): 1~4

- [Herbs](home/Resources/Herbs): 2~5

**Encounters**

- [Dusty Chest (enc_chest3)](home/Encounters/Dusty-Chest-(enc_chest3))

- [Bright Vista](home/Encounters/Bright-Vista)

- [Talking To Thyffr](home/Encounters/Talking-To-Thyffr)

- [Starry Night](home/Encounters/Starry-Night)

- [Mystic Waters](home/Encounters/Mystic-Waters)

