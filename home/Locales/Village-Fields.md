**Level**: 1

**Length**: 25

**Requirements**: [Promotion](home/Events/Promotion)

**Description**: The nearby village's fields are vast and full of opportunity for an aspiring mage.

**Run Cost**

- [Stamina](home/Player/Stamina): 0.2

**Result**

- [Arcana](home/Resources/Arcana): 0.1

- [Gold](home/Resources/Gold): 5

**Loot**

- [Herbs](home/Resources/Herbs): 1~3

- [Loose Cog](home/Rares/Loose-Cog): 5%

**Encounters**

- [Automated Plower](home/Encounters/Automated-Plower)

- [Pile of Wood?](home/Encounters/Pile-of-Wood_)

- [Chatty Farmer](home/Encounters/Chatty-Farmer)

- [Blood](home/Encounters/Blood)

- [Scarecrow](home/Encounters/Scarecrow)

- [Scarecrow](home/Encounters/Scarecrow)

- [Upturned Soil](home/Encounters/Upturned-Soil)

- [Rusted Sickle](home/Encounters/Rusted-Sickle)

**Symbol**: None

