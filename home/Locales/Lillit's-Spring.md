**Level**: 2

**Length**: 20

**Requirements**: [Musty Library](home/Locales/Musty-Library)

**Description**: Tradition tells of a spring with enchanted waters. Investigations might prove revealing.

**Run Cost**

- [Stamina](home/Player/Stamina): 0.2

**Result**

- [Arcana](home/Resources/Arcana): 0.1

- [Research](home/Resources/Research): 10

**Loot**

- [Herbs](home/Resources/Herbs): 2~5

- [Lillit's Cord](home/Rares/Lillit's-Cord): 5%

**Encounters**

- [Mana Tree](home/Encounters/Mana-Tree)

- [Heathered Copse](home/Encounters/Heathered-Copse)

- [Mystic Waters](home/Encounters/Mystic-Waters)

- [Mana Tree](home/Encounters/Mana-Tree)

- [Hidden Cache](home/Encounters/Hidden-Cache)

- [Starry Night](home/Encounters/Starry-Night)

