**Description**: Deep in Thangmor forest, the great witch Hestia keeps a small, secluded cabin. On rare occasions she even takes visitors.

**Level**: 13

**Distance**: 50

**Length**: 50

**Requirements**: [Hallomar](home/Locales/Hallomar)

**Result**

- [Purple Amulet](home/Events/Purple-Amulet): True

**Loot**

- [Jack O' Lantern](home/Resources/Jack-O'-Lantern): 1

- [Hestia's Homebrew](home/Potions/Hestia's-Homebrew): 1

- [Hallow Broomstick](home/Upgrades/Hallow-Broomstick): 5%

- [Cobwebs](home/Resources/Cobwebs): 3

**Encounters**

- [Black Cat](home/Encounters/Black-Cat)

- [Hestia](home/Encounters/Hestia)

- [Talk To Hestia](home/Encounters/Talk-To-Hestia)

- [Warding Charm](home/Encounters/Warding-Charm)

- [Bubbling Cauldron](home/Encounters/Bubbling-Cauldron)

- [Iron Chest](home/Encounters/Iron-Chest)

**Symbol**: 🎃

