**Level**: 14

**Description**: This tiny outpost in the center of thangmor outlasted even the pillars

**Requirements**: [Vile](home/Events/Vile) ≤ 0 and [World Lore](home/Skills/World-Lore) ≥ 7 and [Explore Treffil Wood](home/Locales/Explore-Treffil-Wood) > 0

**Run Cost**

- [Stamina](home/Player/Stamina): 0.33

**Result**

- [Arcana](home/Resources/Arcana): 0.1

- [Research](home/Resources/Research): 10

**Loot**

- [Scrolls](home/Resources/Scrolls): 1~4

**Encounters**

- [Iron Chest](home/Encounters/Iron-Chest)

- [Affable Gnome](home/Encounters/Affable-Gnome)

- [Advanced Primer](home/Encounters/Advanced-Primer)

- [Hettie](home/Encounters/Hettie)

- [Babbling Delki](home/Encounters/Babbling-Delki)

- [Bright Vista](home/Encounters/Bright-Vista)

