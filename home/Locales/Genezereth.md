**Level**: 15

**Requirements**: [World Lore](home/Skills/World-Lore) ≥ 7

**Length**: 100

**Symbol**: 📖

**Description**: An ancient library at the bridge spanning Markhul's gap.

**Flavor**: Third of the Four Weirs of Dhrune.

**Result**

- Title: Historian

**Run Cost**

- [Stamina](home/Player/Stamina): 0.4

**Loot**

- [Tomes](home/Resources/Tomes): 3~5

- [Markhul's Codex](home/Tasks/Markhul's-Codex): True

**Encounters**

- [Occult Tapestry](home/Encounters/Occult-Tapestry)

- [Runic Tome](home/Encounters/Runic-Tome)

- [Dusty Chest (enc_chest1)](home/Encounters/Dusty-Chest-(enc_chest1))

- [Markhul's Gap](home/Encounters/Markhul's-Gap)

- [Crumbled Statue](home/Encounters/Crumbled-Statue)

- [Dhrunic Manuscript](home/Encounters/Dhrunic-Manuscript)

- [Magical Primer](home/Encounters/Magical-Primer)

- [Scrap Pile](home/Encounters/Scrap-Pile)

- [Pulsating Crystal](home/Encounters/Pulsating-Crystal)

