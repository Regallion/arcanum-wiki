**Value**: 0

**Only Integer Values**: True

**Statistics**: True

**Locked**: False

**Hidden**: True

**Modifiers**

- Spelllist Max: 1

- [Stamina Max](home/Player/Stamina): 1

- tohit: 1

- [Life Max](home/Player/Life): 1

- Allies Max: :5

- [Speed](home/Player/Speed): :4

- [Skill Points](home/Player/Skill-Points): :3

**Result**

- Hallpoints: 0.1

