This page is a default list of the items in this category.<br>
[Age](home/Player/Age)<br>
[Breath](home/Player/Breath)<br>
[Damage Reduction](home/Player/Damage-Reduction)<br>
[Distance](home/Player/Distance)<br>
[Dodge](home/Player/Dodge)<br>
[Evil](home/Player/Evil)<br>
[Exp](home/Player/Exp)<br>
[Level](home/Player/Level)<br>
[Life](home/Player/Life)<br>
[Luck](home/Player/Luck)<br>
[Notoriety](home/Player/Notoriety)<br>
[Skill Points](home/Player/Skill-Points)<br>
[Speed](home/Player/Speed)<br>
[Spellcraft Power](home/Player/Spellcraft-Power)<br>
[Stamina](home/Player/Stamina)<br>
[Virtue](home/Player/Virtue)