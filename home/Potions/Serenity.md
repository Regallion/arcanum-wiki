**Level**: 11

**Requirements**: [Potions](home/Skills/Potions) ≥ 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 500

- [Tomes](home/Resources/Tomes): 5

- [Research](home/Resources/Research): 500

- [Ichor](home/Resources/Ichor): 1

**Cost to acquire**

- [Light Gem](home/Resources/Light-Gem): 1

- [Ichor](home/Resources/Ichor): 1

**Effect of using**

- Dot: {'mod': {'stress.rate': -2}, 'duration': 1800}

