**Description**: Ensures the drinker never becomes old. For sale only

**Requirements**: [Potions](home/Skills/Potions) ≥ 1 and [Vile](home/Events/Vile) > 0

**Cost to Buy**

- [Gold](home/Resources/Gold): 250

- [Research](home/Resources/Research): 500

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 25

- [Bodies](home/Resources/Bodies): 15

- [Shadow Gem](home/Resources/Shadow-Gem): 2

- [Potion Base](home/Resources/Potion-Base): 1

**Gains from selling**: {'gold': 2000, 'gems': 25}

**Effect of using**

- Dot: {'id': 'fakeeternalyouth', 'name': 'eternal youth', 'duration': 150, 'effect': {'hp': -50}}

