**Description**: Raw mana injected right into the veins of a mage can allow for sustained casting, if one disregards the side-effects

**Requirements**: [Potions](home/Skills/Potions) ≥ 8

**Cost to Buy**

- [Gold](home/Resources/Gold): 250

- [Research](home/Resources/Research): 500

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 25

- [Arcane Gem](home/Resources/Arcane-Gem): 5

**Effect of using**

- Dot: {'id': 'dotmana', 'name': 'mana drip', 'duration': 900, 'effect': {'mana': 1, 'hp': -1}}

