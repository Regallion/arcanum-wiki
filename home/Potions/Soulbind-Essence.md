**Description**: A measure of this sickly green substance can significantly improve the effects of soul traps.

**Requirements**: [Potions](home/Skills/Potions) ≥ 6 and [Vile](home/Events/Vile) > 0

**Level**: 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 2500

- [Research](home/Resources/Research): 3000

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Souls](home/Resources/Souls): 15

- [Gold](home/Resources/Gold): 500

- [Spirit Gem](home/Resources/Spirit-Gem): 5

- [Spirit](home/Resources/Spirit): 15

- [Shadow Gem](home/Resources/Shadow-Gem): 5

- [Potion Base](home/Resources/Potion-Base): 1

**Effect of using**

- Dot: {'duration': 1800, 'mod': {'trapsoul.run.bonedust': -0.2, 'trapsoul.length': -5, 'trapsoul.result.souls.max': 0.25}}

