**Description**: Cures most common ailments and fortifies the body.

**Requirements**: [Potions](home/Skills/Potions) ≥ 8

**Level**: 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 2500

- [Research](home/Resources/Research): 3000

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 50

- [Nature](home/Resources/Nature): 10

- [Nature Gem](home/Resources/Nature-Gem): 15

- [Gemstones](home/Resources/Gemstones): 5

- [Potion Base](home/Resources/Potion-Base): 1

**Effect of using**

- Action: {'cure': ['paralysis', 'enrage', 'charmed', 'fear', 'silence', 'sick'], 'targets': 'self'}

- Dot: {'duration': 300, 'effect': {'hp': 1}, 'mod': {'hp.max': 100}}

