**Description**: Amplifies even significantly muffled sounds.

**Level**: 7

**Requirements**: [Potions](home/Skills/Potions) ≥ 5

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 1

- [Air Gem](home/Resources/Air-Gem): 1

**Effect of using**: [Speak](home/Spells/Speak)

