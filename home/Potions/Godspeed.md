**Level**: 15

**Requirements**: [Potions](home/Skills/Potions) ≥ 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 500

- [Tomes](home/Resources/Tomes): 5

- [Research](home/Resources/Research): 500

- [Ichor](home/Resources/Ichor): 1

**Cost to acquire**

- [Air Gem](home/Resources/Air-Gem): 1

- [Ichor](home/Resources/Ichor): 1

**Effect of using**

- Dot: {'mod': {'speed': 25}, 'duration': 1800}

