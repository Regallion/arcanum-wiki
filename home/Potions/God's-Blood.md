**Level**: 12

**Requirements**: [Potions](home/Skills/Potions) ≥ 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 500

- [Tomes](home/Resources/Tomes): 5

- [Research](home/Resources/Research): 500

- [Ichor](home/Resources/Ichor): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 50

- [Ichor](home/Resources/Ichor): 1

**Effect of using**

- Dot: {'mod': {'hp.max': 300}, 'duration': 3600}

