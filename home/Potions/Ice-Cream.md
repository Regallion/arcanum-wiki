**Description**: A batch of ice cream flavored with wintery herbs and spices

**Requirements**: [Potions](home/Skills/Potions) ≥ 1

**Cost to Buy**

- [Gold](home/Resources/Gold): 250

- [Ice](home/Resources/Ice): 5

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 15

- [Ice](home/Resources/Ice): 5

**Gains from selling**: {'gold': 100}

**Effect of using**

- Dot: {'id': 'icecream', 'name': 'ice cream high', 'duration': 3600, 'effect': {'hp': 0.5, 'stress': -0.5}}

