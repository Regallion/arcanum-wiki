**Requirements**: [Potions](home/Skills/Potions) > 0

**Level**: 1

**Cost to Buy**

- [Gold](home/Resources/Gold): 25

- [Research](home/Resources/Research): 50

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 10

**Effect of using**

- [Stamina](home/Player/Stamina): 10

