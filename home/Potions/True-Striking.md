**Level**: 10

**Requirements**: [Potions](home/Skills/Potions) ≥ 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 500

- [Tomes](home/Resources/Tomes): 3

- [Research](home/Resources/Research): 500

- [Ichor](home/Resources/Ichor): 1

**Cost to acquire**

- [Spirit Gem](home/Resources/Spirit-Gem): 1

- [Ichor](home/Resources/Ichor): 1

**Effect of using**

- Dot: {'mod': {'player.tohit': 25}, 'duration': 1800}

