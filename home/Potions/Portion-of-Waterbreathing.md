**Description**: Unusually, prepared by baking special herbs into a brownie.

**Requirements**: [Potions](home/Skills/Potions) ≥ 1

**Cost to Buy**

- [Gold](home/Resources/Gold): 25

- [Research](home/Resources/Research): 50

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 10

**Effect of using**

- Dot: {'duration': 120, 'effect': {'breath': 3, 'water': 0.1}}

