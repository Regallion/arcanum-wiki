**Description**: Applied to a weapon, lets the wielder utilize his skills to the fullest extent.

**Level**: 12

**Requirements**: [Potions](home/Skills/Potions) ≥ 10 and ([Swordplay](home/Skills/Swordplay) + [Hammer Proficiency](home/Skills/Hammer-Proficiency)) > 5

**Cost to Buy**

- [Gold](home/Resources/Gold): 500

- [Tomes](home/Resources/Tomes): 5

- [Research](home/Resources/Research): 500

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 50

- [Life](home/Player/Life): 150

- [Earth](home/Resources/Earth): 15

- [Blood Gem](home/Resources/Blood-Gem): 50

- [Potion Base](home/Resources/Potion-Base): 3

**Effect of using**

- Dot: {'mod': {'bladelore.mod.player.bonuses.slash': 0.5, 'bladelore.mod.player.hits.slash': 0.5, 'hammerlore.mod.player.bonuses.blunt': 0.5, 'hammerlore.mod.player.hits.blunt': 0.5}, 'duration': 600}

