**Description**: Converts passive information into short-term benefit

**Requirements**: [Potions](home/Skills/Potions) ≥ 9

**Level**: 5

**Cost to Buy**

- [Gold](home/Resources/Gold): 2500

- [Research](home/Resources/Research): 5000

- [Skill Points](home/Player/Skill-Points): 100

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Skill Points](home/Player/Skill-Points): 50

- [Potion Base](home/Resources/Potion-Base): 1

**Effect of using**

- [Research](home/Resources/Research): 2000

- [Manas](home/Tags): 250

- [Stamina](home/Player/Stamina): 250

