**Requirements**: [Witchcraft](home/Classes/Witchcraft)

**Level**: 4

**Cost to Buy**

- [Gold](home/Resources/Gold): 25

- [Research](home/Resources/Research): 150

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 15

**Effect of using**

- Dot: {'duration': 50, 'effect': {'hp': 2, 'nature': 0.5}}

