**Description**: A flask of metal that stays liquid even in the coldest weather. Converts organic materials to more liquid metal. Becomes inert after some time.

**Requirements**: Puppet_Enhancedlabs > 0 and [Potions](home/Skills/Potions) > 0

**Level**: 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 4500

- [Research](home/Resources/Research): 3000

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 50

- [Gold](home/Resources/Gold): 2500

- [Gemstones](home/Resources/Gemstones): 25

- [Mana](home/Resources/Mana): 15

- [Potion Base](home/Resources/Potion-Base): 1

**Effect of using**

- Dot: {'id': 'metallicize', 'name': 'liquidation', 'duration': 120, 'effect': {'bodies': -1, 'herbs': -1, 'hp': -2}, 'mod': {'gatherherbs.result.herbs': -0.2, 'gatherherbs.result.gold': 4, 'murder.result.bodies': -1, 'murder.result.gold': 200}}

