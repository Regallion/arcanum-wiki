**Description**: Temporarily grants you an impeccable ability to acquire valuables.

**Requirements**: [Potions](home/Skills/Potions) ≥ 9

**Level**: 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 5000

- [Research](home/Resources/Research): 6000

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 50

- [Gold](home/Resources/Gold): 2000

- [Gemstones](home/Resources/Gemstones): 40

- [Spirit](home/Resources/Spirit): 15

- [Potion Base](home/Resources/Potion-Base): 2

**Effect of using**

- Dot: {'duration': 1200, 'effect': {'gold': 5, 'gems': 0.1}}

