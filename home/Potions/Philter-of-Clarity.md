**Description**: A master's skill in liquid form

**Requirements**: [Potions](home/Skills/Potions) > 0

**Level**: 1

**Cost to Buy**

- [Gold](home/Resources/Gold): 250

- [Research](home/Resources/Research): 500

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 10

- [Potion Base](home/Resources/Potion-Base): 4

**Effect of using**

- [Skill Points](home/Player/Skill-Points): 1

