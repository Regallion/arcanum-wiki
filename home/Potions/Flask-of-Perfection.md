**Description**: Reshapes your body to make it ever so slightly more ideal.

**Level**: 25

**Requirements**: [Potions](home/Skills/Potions) ≥ 999

**Cost to Buy**

- [Gold](home/Resources/Gold): 5000

- [Tomes](home/Resources/Tomes): 50

- [Research](home/Resources/Research): 5000

- [Fire Gem](home/Resources/Fire-Gem): 50

- [Air Gem](home/Resources/Air-Gem): 50

- [Water Gem](home/Resources/Water-Gem): 50

- [Earth Gem](home/Resources/Earth-Gem): 50

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 50

- [Life](home/Player/Life): 150

- [Research](home/Resources/Research): 15000

- [Gold](home/Resources/Gold): 10000

- [Blood Gem](home/Resources/Blood-Gem): 50

- [Potion Base](home/Resources/Potion-Base): 5

**Effect of using**

- Effect: {'hp.max': 2}

