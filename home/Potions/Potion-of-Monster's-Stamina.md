**Requirements**: [Potions](home/Skills/Potions) ≥ 5

**Level**: 5

**Cost to Buy**

- [Gold](home/Resources/Gold): 300

- [Research](home/Resources/Research): 500

- [Codices](home/Resources/Codices): 30

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 30

- [Water Gem](home/Resources/Water-Gem): 1

**Effect of using**

- [Stamina](home/Player/Stamina): 50

**Flavor**: Magnum

