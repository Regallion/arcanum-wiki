**Description**: A batch of ice cream, for children

**Requirements**: [Potions](home/Skills/Potions) ≥ 1 and ([Witchcraft](home/Classes/Witchcraft) + [Thanophage](home/Classes/Thanophage)) > 0

**Cost to Buy**

- [Gold](home/Resources/Gold): 250

- [Ice](home/Resources/Ice): 5

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 15

- [Ice](home/Resources/Ice): 5

**Gains from selling**: {'gold': 100}

**Effect of using**

- Dot: {'id': 'icecreamlure', 'name': 'ice cream lure', 'duration': 3600, 'mod': {'eatchildren.effect.stress': -0.2, 'eatchildren.effect.prismatic': 0.1, 'eatchildren.effect.stamina': 0.3, 'eatchildren.effect.hp': 1}}

