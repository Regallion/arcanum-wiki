**Stackable**: True

**Description**: A natural repast.

**Cost to acquire**

- [Earth Gem](home/Resources/Earth-Gem): 3

- [Herbs](home/Resources/Herbs): 10

**Effect of using**

- Dot: {'mod': {'earth.rate': '10%', 'duration': 600}}

