**Description**: Removes most forms of inisidious motion impairment.

**Level**: 5

**Requirements**: [Potions](home/Skills/Potions) ≥ 5

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 1

- [Arcane Gem](home/Resources/Arcane-Gem): 1

**Effect of using**: [Cure Paralysis](home/Spells/Cure-Paralysis)

