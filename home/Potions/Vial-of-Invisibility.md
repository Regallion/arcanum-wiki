**Description**: Appears to contain nothing at all.

**Requirements**: [Mystic Madcap](home/Classes/Mystic-Madcap) > 0 and [Potions](home/Skills/Potions) > 0

**Level**: 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 2500

- [Research](home/Resources/Research): 3000

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 50

- [Gold](home/Resources/Gold): 2500

- [Spirit Gem](home/Resources/Spirit-Gem): 25

- [Life](home/Player/Life): 150

- [Spirit](home/Resources/Spirit): 15

- [Potion Base](home/Resources/Potion-Base): 1

**Effect of using**

- Dot: {'id': 'pot_invisibility_effect', 'name': 'invisibility', 'duration': 1800, 'mod': {'player.dodge': 25, 'player.speed': 15, 'heist.result.gems.min': 1, 'heist.result.gems.max': 1, 'heist.length': -25}}

