**Description**: This reddish potion causes a surge of energy in the imbiber, at the expense of health

**Requirements**: [Potions](home/Skills/Potions) ≥ 8

**Cost to Buy**

- [Gold](home/Resources/Gold): 250

- [Research](home/Resources/Research): 500

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 25

- [Arcane Gem](home/Resources/Arcane-Gem): 5

- [Blood Gem](home/Resources/Blood-Gem): 5

**Effect of using**

- Dot: {'id': 'fount', 'name': "bull's tenacity", 'duration': 900, 'effect': {'stamina': 2.25, 'hp': -1}}

