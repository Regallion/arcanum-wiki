**Description**: Causes you to become more attuned to the world's elements. Harmful to one's physical body.

**Level**: 25

**Requirements**: [Potions](home/Skills/Potions) ≥ 999

**Cost to Buy**

- [Gold](home/Resources/Gold): 5000

- [Tomes](home/Resources/Tomes): 50

- [Research](home/Resources/Research): 5000

- [Fire Gem](home/Resources/Fire-Gem): 50

- [Air Gem](home/Resources/Air-Gem): 50

- [Water Gem](home/Resources/Water-Gem): 50

- [Earth Gem](home/Resources/Earth-Gem): 50

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 50

- [Life](home/Player/Life): 150

- [Research](home/Resources/Research): 15000

- [Gold](home/Resources/Gold): 10000

- [Fire Gem](home/Resources/Fire-Gem): 50

- [Air Gem](home/Resources/Air-Gem): 50

- [Water Gem](home/Resources/Water-Gem): 50

- [Earth Gem](home/Resources/Earth-Gem): 50

- [Potion Base](home/Resources/Potion-Base): 5

**Effect of using**

- Effect: {'hp.max': -4, 'element.max': 0.25}

