**Requirements**: [Potions](home/Skills/Potions) ≥ 9

**Cost to Buy**

- [Gold](home/Resources/Gold): 100

- [Light Gem](home/Resources/Light-Gem): 1

- [Research](home/Resources/Research): 500

**Level**: 7

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 25

- [Gemstones](home/Resources/Gemstones): 3

**Effect of using**

- [Life](home/Player/Life): 200

