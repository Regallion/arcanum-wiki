**Cost to Buy**

- [Gold](home/Resources/Gold): 25

- [Research](home/Resources/Research): 50

**Level**: 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 10

**Requirements**: [Potions](home/Skills/Potions) > 0

**Effect of using**

- [Life](home/Player/Life): 75

