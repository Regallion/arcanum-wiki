**Description**: Holding it seems dangerous. Not holding it, even more so.

**Requirements**: [Potions](home/Skills/Potions) ≥ 999

**Level**: 5

**Cost to Buy**

- [Gold](home/Resources/Gold): 1500

- [Research](home/Resources/Research): 1200

**Cost to acquire**

- [Fire](home/Resources/Fire): 5

- [Air](home/Resources/Air): 5

- [Herbs](home/Resources/Herbs): 25

- [Water](home/Resources/Water): 5

**Effect of using**

- Attack: {'targets': 'all', 'dmg': '50~90'}

