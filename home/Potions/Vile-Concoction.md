**Description**: What was theirs is now yours.

**Level**: 25

**Requirements**: [Potions](home/Skills/Potions) ≥ 999 and [Vile](home/Events/Vile) > 0

**Cost to Buy**

- [Gold](home/Resources/Gold): 5000

- [Tomes](home/Resources/Tomes): 50

- [Research](home/Resources/Research): 5000

- [Souls](home/Resources/Souls): 150

- [Potion Base](home/Resources/Potion-Base): 1

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 50

- [Research](home/Resources/Research): 15000

- [Bodies](home/Resources/Bodies): 100

- [Souls](home/Resources/Souls): 100

- [Blood Gem](home/Resources/Blood-Gem): 50

- [Potion Base](home/Resources/Potion-Base): 5

**Effect of using**

- Effect: {'hp.max': 1, 'element.max': 0.1}

