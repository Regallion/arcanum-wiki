**Requirements**: [Potions](home/Skills/Potions) ≥ 7

**Level**: 10

**Cost to Buy**

- [Gold](home/Resources/Gold): 2500

- [Tomes](home/Resources/Tomes): 15

- [Earth Gem](home/Resources/Earth-Gem): 5

**Cost to acquire**

- [Herbs](home/Resources/Herbs): 30

- [Gemstones](home/Resources/Gemstones): 1

**Effect of using**

- Dot: {'id': 'toughskin', 'name': 'adamant salve', 'duration': 1800, 'mod': {'self.defense': 50}}

