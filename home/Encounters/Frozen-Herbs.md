**Description**: A few herbs still survive in the frozen loam.

**Level**: 3

**Effects**

- [Frustration](home/Stressors/Frustration): 1~3

- [Weariness](home/Stressors/Weariness): 1~4

**Loot**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

- [Herbs](home/Resources/Herbs): 2~5

- [Snowdrop](home/Resources/Snowdrop): 20%

**Symbol**: ❄️

