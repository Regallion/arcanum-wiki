**Description**: Legend has it that Thurn himself crafted the ten leagues of clockworks in the Orrem Sands. Year by year, it ticks away the endless Aeons.

**Effects**

- [Madness](home/Stressors/Madness): 0~3

- [Unease](home/Stressors/Unease): 0~2

- [Befuddlement](home/Stressors/Befuddlement): 1~3

- [Chronomancy Exp](home/Skills/Chronomancy): 1

- [Crafting Exp](home/Skills/Crafting): 1

- [World Lore Exp](home/Skills/World-Lore): 1

**Result**

- [World Lore Max](home/Skills/World-Lore): 0.001

**Flavor**: Its vast face is nothing to what is reputed to lie buried below.

