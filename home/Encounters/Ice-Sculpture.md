**Description**: Humanoid figure formed from ice and snow. The right enchantment will give it life.

**Level**: 2

**Effects**

- [Weariness](home/Stressors/Weariness): 1~4

- [Ice](home/Resources/Ice): -0.4

- [Cryomancy Max](home/Skills/Cryomancy): 0.001

**Loot**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

- [Snomunculus](home/Resources/Snomunculus): 1

**Symbol**: ❄️

