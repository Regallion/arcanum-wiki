**Description**: A pile of scrap metal sits on the floor, once a useful mechanical servant

**Effects**

- [Weariness](home/Stressors/Weariness): 2~3

- [Mechanical Magic Exp](home/Skills/Mechanical-Magic): 1

**Result**

- [Machinae](home/Resources/Machinae): 15%

**Symbol**: None

