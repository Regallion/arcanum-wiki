**Description**: Its weathered face has gazed upon the world since the Wind Age.

**Effects**

- [Madness](home/Stressors/Madness): 1

- [Befuddlement](home/Stressors/Befuddlement): 3~7

- [Unease](home/Stressors/Unease): 3~7

- [World Lore Exp](home/Skills/World-Lore): 2

**Result**

- [Rune Stones](home/Resources/Rune-Stones): 10%

- [World Lore Max](home/Skills/World-Lore): 0.0001

