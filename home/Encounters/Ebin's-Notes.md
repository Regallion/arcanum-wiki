**Description**: A detailed study of the properties of ice.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~3

- [Cryomancy Exp](home/Skills/Cryomancy): 1

- [Cryomancy Max](home/Skills/Cryomancy): 0.001

**Result**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

- [A Cold Legend](home/Events/A-Cold-Legend): 10%

**Symbol**: ❄️

