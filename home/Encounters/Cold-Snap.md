**Description**: A sudden burst of icy winds chills you to the bone.

**Effects**

- [Weariness](home/Stressors/Weariness): -1~3

- [Unease](home/Stressors/Unease): -1~3

- [Frost](home/Resources/Frost): 1~2

- [Cryomancy Exp](home/Skills/Cryomancy): 1.5

- [Life](home/Player/Life): -2~-4

- [Fire](home/Resources/Fire): -0.2~-0.3

**Symbol**: ❄️

