**Description**: You see all shapes clearly reflected; but completely black.

**Effects**

- [Abyss Gazing Exp](home/Skills/Abyss-Gazing): 1

- [Void](home/Resources/Void): 1

- [Madness](home/Stressors/Madness): 3~5

- [Unease](home/Stressors/Unease): 3~5

**Flavor**: For now you see in a mirror, darkly.

