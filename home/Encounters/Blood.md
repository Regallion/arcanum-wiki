**Description**: A crushed, mangled corpse of a critter lies in the fields, cut apart by an uncaring plow.

**Effects**

- [Unease](home/Stressors/Unease): 1~2

- [Frustration](home/Stressors/Frustration): 2

- [Animal Handling Exp](home/Skills/Animal-Handling): 2

- [Composure Exp](home/Skills/Composure): 2

**Symbol**: None

