**Description**: More advanced magical theory.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~3

- [Weariness](home/Stressors/Weariness): 0~2

- [Frustration](home/Stressors/Frustration): 0~2

- [Mage Lore Exp](home/Skills/Mage-Lore): 2

- [Languages Exp](home/Skills/Languages): 1

**Result**

- [Codices](home/Resources/Codices): 10%

- [Research](home/Resources/Research): 10

- [Arcana](home/Resources/Arcana): 0.02

