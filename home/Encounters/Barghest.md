**Description**: The great dog-like beast is a portent of death.

**Effects**

- [Unease](home/Stressors/Unease): 3~4

- [Madness](home/Stressors/Madness): 1~4

- [Shadow Mastery Exp](home/Skills/Shadow-Mastery): 1

