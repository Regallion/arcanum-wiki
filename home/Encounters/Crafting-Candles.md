**Description**: Crafting everburning candles

**Effects**

- [Unease](home/Stressors/Unease): 1~2

- [Crafting Exp](home/Skills/Crafting): 3

**Level**: 2

**Result**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

**Symbol**: ❄️

