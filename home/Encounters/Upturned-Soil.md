**Description**: A patch of uneven ground with the soil exposed to the elements, likely to become barren. What kind of farmer misses something like this?

**Effects**

- [Unease](home/Stressors/Unease): 1~3

- [Befuddlement](home/Stressors/Befuddlement): 1~3

- [Geomancy Exp](home/Skills/Geomancy): 5

- [Nature Studies Exp](home/Skills/Nature-Studies): 5

**Result**

- [Herbs](home/Resources/Herbs): 10%

**Symbol**: None

