**Description**: A lone mechanism plows the fields for it's rich master.

**Effects**

- [Weariness](home/Stressors/Weariness): 1~3

- [Mechanical Magic Exp](home/Skills/Mechanical-Magic): 5

**Result**

- [Herbs](home/Resources/Herbs): 0~2

**Symbol**: None

