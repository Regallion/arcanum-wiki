**Description**: Ancient coffin marked by faded runes

**Level**: 4

**Effects**

- [Unease](home/Stressors/Unease): 0~2

- [Madness](home/Stressors/Madness): 0~2

- [Reanimation Exp](home/Skills/Reanimation): 2

- [Embalming Exp](home/Skills/Embalming): 2

**Loot**

- [Rune Stones](home/Resources/Rune-Stones): 10%

- [Shadow Gem](home/Resources/Shadow-Gem): 30%

- [Spirit Gem](home/Resources/Spirit-Gem): 30%

- [Bodies](home/Resources/Bodies): 50%

