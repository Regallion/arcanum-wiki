**Description**: The massive head of stone and moss is said to be as old as the world itself.

**Effects**

- [Unease](home/Stressors/Unease): 3~4

- [Befuddlement](home/Stressors/Befuddlement): 0~-2

- [Madness](home/Stressors/Madness): 0~2

- [World Lore Exp](home/Skills/World-Lore): 2

**Flavor**: A voice seems to grumble from the ground.

