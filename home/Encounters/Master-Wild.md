**Description**: A mage of the wilds makes a perplexing teacher.

**Effects**

- [Madness](home/Stressors/Madness): 1~5

- [Weariness](home/Stressors/Weariness): 2~5

- [Nature Studies Exp](home/Skills/Nature-Studies): 2

- [Animal Handling Exp](home/Skills/Animal-Handling): 2

**Symbol**: ⭐

