**Description**: Piled higher than your head, an eclectic mass of skittering spiders bars the path.

**Effects**

- [Unease](home/Stressors/Unease): 2~3

- [Madness](home/Stressors/Madness): 2~4

- [Frustration](home/Stressors/Frustration): 1~2

- [Life](home/Player/Life): -1

