**Description**: The placid poglers angle their backs of bushy leaves to the sun.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 2~3

- [Frustration](home/Stressors/Frustration): -0.5

- [Nature Mastery Exp](home/Skills/Nature-Mastery): 1

