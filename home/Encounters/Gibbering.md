**Description**: They hide in small cracks and in return for a wicked deed, will reveal one secret.

**Effects**

- [Weariness](home/Stressors/Weariness): 0~2

- [Madness](home/Stressors/Madness): 0~2

- [Mysticism Exp](home/Skills/Mysticism): 1

- [Research](home/Resources/Research): 1

**Result**

- [Shadow Gem](home/Resources/Shadow-Gem): 5%

- [Arcana](home/Resources/Arcana): 0.01

**Flavor**: Legend tells of a massive gibbering, who will answer any question in return for a murder.

