**Description**: Steel-sharp blades of grass bite into your boots.

**Effects**

- [Weariness](home/Stressors/Weariness): 1~3

- [Frustration](home/Stressors/Frustration): 1~3

- [Life](home/Player/Life): -2

