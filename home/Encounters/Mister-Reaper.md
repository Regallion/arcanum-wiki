**Description**: A grim name for an otherwise respectable mage.

**Effects**

- [Madness](home/Stressors/Madness): 1~5

- [Befuddlement](home/Stressors/Befuddlement): 2~5

- [World Lore Exp](home/Skills/World-Lore): 2

- [Umbramancy Exp](home/Skills/Umbramancy): 2

**Symbol**: ⭐

