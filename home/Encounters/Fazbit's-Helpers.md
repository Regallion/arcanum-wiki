**Description**: Still maintaining the workshop after centuries... as much as they can

**Effects**

- [Weariness](home/Stressors/Weariness): 1~3

- [Mechanical Magic Exp](home/Skills/Mechanical-Magic): 3

- [Puppetry Exp](home/Skills/Puppetry): 3

**Result**

- [Machinae](home/Resources/Machinae): 5%

- [Puppets](home/Resources/Puppets): 5%

**Symbol**: None

