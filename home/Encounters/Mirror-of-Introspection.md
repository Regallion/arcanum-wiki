**Description**: It's just you.

**Effects**

- [Lumenology Exp](home/Skills/Lumenology): 1

- [Weariness](home/Stressors/Weariness): 1~3

- [Unease](home/Stressors/Unease): 1~2

**Result**

