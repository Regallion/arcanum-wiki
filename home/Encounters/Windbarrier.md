**Description**: Building a barrier against the harsh winds

**Effects**

- [Weariness](home/Stressors/Weariness): 1~2

- [Aeromancy Exp](home/Skills/Aeromancy): 2

**Level**: 2

**Result**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

**Symbol**: ❄️

