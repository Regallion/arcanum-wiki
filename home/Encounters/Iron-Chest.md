**Description**: A solid iron chest.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~1

- [Frustration](home/Stressors/Frustration): 0~1

**Loot**

- [Gold](home/Resources/Gold): 5~14

- [Codices](home/Resources/Codices): 0~2

- [Scrolls](home/Resources/Scrolls): 2~4

