**Description**: A manual of beastly lore rests atop an ancient pedestal.

**Effects**

- [Unease](home/Stressors/Unease): 1~2

- [Madness](home/Stressors/Madness): 1~2

- [Frustration](home/Stressors/Frustration): 1~2

- [Life](home/Player/Life): -3

**Result**

- [Animal Handling Max](home/Skills/Animal-Handling): 0.001

