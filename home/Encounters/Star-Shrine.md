**Description**: A glowing star cinder embedded in a shrine.

**Effects**

- [Lumenology Exp](home/Skills/Lumenology): 1

- [Befuddlement](home/Stressors/Befuddlement): 2~5

- [Weariness](home/Stressors/Weariness): 0~-2

- [Unease](home/Stressors/Unease): 1~5

- [World Lore Exp](home/Skills/World-Lore): 1

**Result**

- [Lumenology Max](home/Skills/Lumenology): 0.0001

**Flavor**: You can't seem to pry it loose.

