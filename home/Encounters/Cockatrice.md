**Description**: An unnatural blend of serpent and rooster. As its stare is death, you avert your gaze as you approach.

**Effects**

- [Unease](home/Stressors/Unease): 3~5

- [Befuddlement](home/Stressors/Befuddlement): 2~3

- [Madness](home/Stressors/Madness): 0~2

- [Magic Beasts Exp](home/Skills/Magic-Beasts): 1

