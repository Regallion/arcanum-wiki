**Description**: Extended travel beneath the hot sun produces visceral visions in the sand.

**Effects**

- [Madness](home/Stressors/Madness): 1~3

- [Unease](home/Stressors/Unease): 0~2

- [Befuddlement](home/Stressors/Befuddlement): 0~2

- [Lumenology Exp](home/Skills/Lumenology): 1

**Result**

- [Lumenology Max](home/Skills/Lumenology): 0.0001

