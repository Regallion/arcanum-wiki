**Description**: Aylesmeade villagers arrange their spring blooms for sale.

**Effects**

- [Herbalism Exp](home/Skills/Herbalism): 2

- [Weariness](home/Stressors/Weariness): 2~3

**Loot**

- [Hyacinth](home/Resources/Hyacinth): 1

**Symbol**: 🌼

