**Description**: Every year rumors abound of lights and music from the nearby woods.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Unease](home/Stressors/Unease): 1~2

**Result**

**Symbol**: 🌼

