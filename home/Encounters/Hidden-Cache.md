**Description**: You sense something buried nearby.

**Effects**

- [Frustration](home/Stressors/Frustration): 1~2

- [Weariness](home/Stressors/Weariness): 0~2

- [Befuddlement](home/Stressors/Befuddlement): -1~2

- [Nature Studies Exp](home/Skills/Nature-Studies): 1

**Loot**

- [Gold](home/Resources/Gold): 0~50

- [Gemstones](home/Resources/Gemstones): 2~5

- [Scrolls](home/Resources/Scrolls): 2~5

- [Codices](home/Resources/Codices): 0~4

