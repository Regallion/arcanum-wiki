**Description**: A hysterical, repeating laugh fills the halls.

**Effects**

- [Madness](home/Stressors/Madness): 2~3

- [Unease](home/Stressors/Unease): 3~5

**Result**

- [Fazbit's Knowledge](home/Resources/Fazbit's-Knowledge): 1~3

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

**Symbol**: ❄️

