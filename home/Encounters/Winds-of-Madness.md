**Description**: Voices on the wind scream and echo in your mind.

**Effects**

- [Madness](home/Stressors/Madness): 2~5

- [Mana](home/Resources/Mana): 0.5

- [Spirit Communion Exp](home/Skills/Spirit-Communion): 1

- [Spirit](home/Resources/Spirit): 1

- [Aeromancy Exp](home/Skills/Aeromancy): 1

