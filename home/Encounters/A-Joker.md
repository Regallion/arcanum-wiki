**Description**: A crafty expression suggests trouble.

**Effects**

- [Frustration](home/Stressors/Frustration): 1~5

- [Befuddlement](home/Stressors/Befuddlement): 2~5

- [Trickery Exp](home/Skills/Trickery): 2

- Player Exp: 1

**Symbol**: ⭐

