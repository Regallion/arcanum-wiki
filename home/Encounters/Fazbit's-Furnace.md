**Description**: Its embers still glow after centuries

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~4

- [Madness](home/Stressors/Madness): 0~2

- [Unease](home/Stressors/Unease): 1~3

- [Pyromancy Exp](home/Skills/Pyromancy): 3

**Result**

- [Pyromancy Max](home/Skills/Pyromancy): 0.0001

