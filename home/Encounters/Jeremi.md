**Description**: Nothing is known about this mage.

**Effects**

- [Frustration](home/Stressors/Frustration): 1~5

- [Weariness](home/Stressors/Weariness): 2~5

- [Research](home/Resources/Research): 1

**Result**

**Symbol**: ⭐

