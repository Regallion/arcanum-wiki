**Description**: Phrenesis was forever preoccupied with the cycles of death and rebirth.

**Effects**

- [Stress](home/Tags): -0.1

- [Life](home/Player/Life): -4

**Symbol**: ⭐

