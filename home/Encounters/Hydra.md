**Description**: A multi-headed beast rears its heads from the water.

**Effects**

- [Unease](home/Stressors/Unease): 2~3

- [Frustration](home/Stressors/Frustration): 0.5

- [Water Mastery Exp](home/Skills/Water-Mastery): 1

