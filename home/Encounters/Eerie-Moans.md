**Description**: You hear a faint moaning, but are unable to determine its source.

**Effects**

- [Spirit Communion Exp](home/Skills/Spirit-Communion): 1

- [Madness](home/Stressors/Madness): 1

