**Description**: Blasted hills and scraps of armor still mark an ancient site of battle.

**Effects**

- [Madness](home/Stressors/Madness): 0.5

- [Weariness](home/Stressors/Weariness): 2~7

- [Unease](home/Stressors/Unease): 2~5

- [World Lore Exp](home/Skills/World-Lore): 0.5

**Result**

- [World Lore Max](home/Skills/World-Lore): 0.0002

