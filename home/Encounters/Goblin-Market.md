**Description**: In the woods at night, goblins hold a market of their own.

**Effects**

- [Gold](home/Resources/Gold): -10

- [Frustration](home/Stressors/Frustration): 1~3

- [Madness](home/Stressors/Madness): 3~4

**Result**

- [Herbs](home/Resources/Herbs): 4~10

- [Gemstones](home/Resources/Gemstones): 5~10

- [Arcane Gem](home/Resources/Arcane-Gem): 40%

**Symbol**: 🌼

