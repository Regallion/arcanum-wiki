**Description**: Hidden in a deep pond, it lures its prey with a fruiting branch growing from its head.

**Effects**

- [Unease](home/Stressors/Unease): 1~2

- [Weariness](home/Stressors/Weariness): 0~1

- [Herbalism Exp](home/Skills/Herbalism): 1

- [Aquamancy Exp](home/Skills/Aquamancy): 1

