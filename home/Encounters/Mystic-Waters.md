**Level**: 1

**Description**: Water suffused with enchantments

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~2

- [Nature Studies Exp](home/Skills/Nature-Studies): 1

- [Aquamancy Exp](home/Skills/Aquamancy): 1

- [Arcana](home/Resources/Arcana): 0.001

**Result**

- [Water Gem](home/Resources/Water-Gem): 25%

