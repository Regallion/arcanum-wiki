**Description**: A violent echo still wanders the halls it never walked

**Effects**

- [Madness](home/Stressors/Madness): 2~4

- [Unease](home/Stressors/Unease): 2~4

- [Tempus](home/Resources/Tempus): -1

- [Chronomancy Exp](home/Skills/Chronomancy): 1

- [Abyss Gazing Exp](home/Skills/Abyss-Gazing): 1

**Loot**

- Voidrune: 1%

- [Void Gem](home/Resources/Void-Gem): 1%

- [Time Runes](home/Resources/Time-Runes): 1%

- [Time Gem](home/Resources/Time-Gem): 1%

**Flavor**: Did that actually happen?

**Symbol**: ⭐

