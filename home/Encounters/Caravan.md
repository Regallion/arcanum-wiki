**Description**: All features of the riders are hidden in robes. They communicate with gutteral sounds and gestures.

**Effects**

- [Unease](home/Stressors/Unease): 1~3

**Result**

- [Travel Max](home/Skills/Travel): 0.001

