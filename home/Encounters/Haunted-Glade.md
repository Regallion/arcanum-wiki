**Description**: Trees here have an eerie aspect

**Effects**

- [Madness](home/Stressors/Madness): 1~2

- [Unease](home/Stressors/Unease): 0~2

- [Umbramancy Exp](home/Skills/Umbramancy): 1

- [Spirit Communion Exp](home/Skills/Spirit-Communion): 1

**Result**

- [Spirit Communion Max](home/Skills/Spirit-Communion): 0.001

