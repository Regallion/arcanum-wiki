**Description**: Writhing in heaps and drooping from branches, a mass of snakes covers the vicinity.

**Effects**

- [Unease](home/Stressors/Unease): 1~2

- [Madness](home/Stressors/Madness): 2~3

- [Frustration](home/Stressors/Frustration): 2~3

- [Life](home/Player/Life): -3

