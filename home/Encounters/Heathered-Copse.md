**Level**: 2

**Description**: You rest in the shade amid flowers of heather.

**Effects**

- [Frustration](home/Stressors/Frustration): -2~0

- [Stamina](home/Player/Stamina): 1

- [Mana](home/Resources/Mana): 1

