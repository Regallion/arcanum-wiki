**Description**: A corpse preserved by the stale air

**Effects**

- [Madness](home/Stressors/Madness): 0~2

- [Unease](home/Stressors/Unease): 1~3

- [Embalming Exp](home/Skills/Embalming): 1

- [Spirit Communion Exp](home/Skills/Spirit-Communion): 1

- [Anatomy Exp](home/Skills/Anatomy): 1

**Result**

- [Anatomy Max](home/Skills/Anatomy): 0.001

**Loot**

- [Gold](home/Resources/Gold): 0~100

- [Bone Dust](home/Resources/Bone-Dust): 0~2

- [Gemstones](home/Resources/Gemstones): 0~2

- [Bodies](home/Resources/Bodies): 30%

