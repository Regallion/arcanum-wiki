**Description**: The Delki of the woods know many things, but are seldom coherent.

**Effects**

- [Frustration](home/Stressors/Frustration): 1~2

- [Weariness](home/Stressors/Weariness): 1~3

- [Nature Studies Exp](home/Skills/Nature-Studies): 1

- [Research](home/Resources/Research): 1

- [Mage Lore Exp](home/Skills/Mage-Lore): 1

**Result**

- [Nature Gem](home/Resources/Nature-Gem): 10%

- [Arcana](home/Resources/Arcana): 0.01

