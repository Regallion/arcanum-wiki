**Description**: The shifting sands reveal a rocky cave. Perhaps some artifacts remain within.

**Effects**

- [Unease](home/Stressors/Unease): 1~3

- [Void](home/Resources/Void): 0.1

**Result**

- [Codices](home/Resources/Codices): 2~4

- [Tomes](home/Resources/Tomes): 0~3

- [Rune Stones](home/Resources/Rune-Stones): 10%

