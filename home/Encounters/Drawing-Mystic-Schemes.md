**Description**: the patterns must be laid precisely to direct the magic weaves.

**Level**: 2

**Effects**

- [Madness](home/Stressors/Madness): 1~2

- [Befuddlement](home/Stressors/Befuddlement): 1~3

- [Frost](home/Resources/Frost): 0~1

**Symbol**: ❄️

