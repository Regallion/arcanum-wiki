**Description**: Even deciphering the runes is a tedious chore.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~4

- [Frustration](home/Stressors/Frustration): 0~4

- [Unease](home/Stressors/Unease): 0~2

- [Mage Lore Exp](home/Skills/Mage-Lore): 2

**Result**

- [Codices](home/Resources/Codices): 1

- [Arcana](home/Resources/Arcana): 0.01

