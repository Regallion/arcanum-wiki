**Description**: A battered, wooden chest.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~2

**Loot**

- [Gold](home/Resources/Gold): 2~10

- [Scrolls](home/Resources/Scrolls): 1~2

