**Description**: The uses of these instruments are beyond you.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~4

- [Madness](home/Stressors/Madness): 0~2

- [Frustration](home/Stressors/Frustration): 1~3

- [Alchemy Exp](home/Skills/Alchemy): 3

**Result**

- [Alchemy Max](home/Skills/Alchemy): 0.0001

