**Description**: A scarecrow winks at you.

**Effects**

- [Unease](home/Stressors/Unease): 1~2

- [Puppetry Exp](home/Skills/Puppetry): 5

**Symbol**: None

