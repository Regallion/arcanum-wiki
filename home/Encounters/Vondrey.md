**Description**: The mage does not appear inclined to speak.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~5

- [Frustration](home/Stressors/Frustration): 1~5

- [Weariness](home/Stressors/Weariness): 2~5

- [Concentration Exp](home/Skills/Concentration): 1

- [Composure Exp](home/Skills/Composure): 1

**Symbol**: ⭐

