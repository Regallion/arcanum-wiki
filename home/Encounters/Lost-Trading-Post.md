**Description**: In the midst of a wide desolation, a tall grey-skinned creature with no eyes offers you its wares.

**Effects**

- [Unease](home/Stressors/Unease): 2~3

- [Madness](home/Stressors/Madness): 1~3

- [Mana](home/Resources/Mana): -1

**Result**

- [Blood Gem](home/Resources/Blood-Gem): 20%

- [Arcane Gem](home/Resources/Arcane-Gem): 10%

- [Codices](home/Resources/Codices): 15%

