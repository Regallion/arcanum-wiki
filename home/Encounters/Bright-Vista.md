**Description**: A small knoll reveals lands far into the distance

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~-2

- [Weariness](home/Stressors/Weariness): -1~-2

- [Frustration](home/Stressors/Frustration): -1~-2

- [Nature Studies Exp](home/Skills/Nature-Studies): 1

- [Lumenology Exp](home/Skills/Lumenology): 1

