**Description**: An ancient gryffon lives atop the Rithel's solitary turret, and Hettie spends her days grooming and feeding the beast.

**Effects**

- [Weariness](home/Stressors/Weariness): 1~3

- [Befuddlement](home/Stressors/Befuddlement): 1~2

**Result**

- [Magic Beasts Max](home/Skills/Magic-Beasts): 0.001

