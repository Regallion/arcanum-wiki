**Description**: Listening to elders recite their winter legends.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Unease](home/Stressors/Unease): 0~2

- [World Lore Exp](home/Skills/World-Lore): 3

**Level**: 2

**Result**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

- [Legend Tells](home/Events/Legend-Tells): 10%

**Symbol**: ❄️

