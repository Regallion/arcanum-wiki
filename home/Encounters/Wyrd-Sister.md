**Description**: The Wyrd sister Hecubah waits by the roadside.

**Effects**

- [Scrying Exp](home/Skills/Scrying): 1

- [Divination Exp](home/Skills/Divination): 1

- [Madness](home/Stressors/Madness): 2

- [Befuddlement](home/Stressors/Befuddlement): 3~7

- [Unease](home/Stressors/Unease): 2~4

**Result**

**Flavor**: Blank verse is hard to interpret.

