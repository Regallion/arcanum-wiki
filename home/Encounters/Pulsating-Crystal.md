**Description**: An odd crystal pulses rhythmically in the darkness. .... . .-.. .-.. ---

**Effects**

- [Weariness](home/Stressors/Weariness): 1~3

- [Unease](home/Stressors/Unease): 1~3

- [Befuddlement](home/Stressors/Befuddlement): 1~3

- [Automata Sculpting Exp](home/Skills/Automata-Sculpting): 5

**Result**

- [Rune Stones](home/Resources/Rune-Stones): 15%

**Symbol**: None

