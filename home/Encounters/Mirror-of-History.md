**Description**: The mirror shows scenes from long ago.

**Effects**

- [World Lore Exp](home/Skills/World-Lore): 1

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Unease](home/Stressors/Unease): 1~2

