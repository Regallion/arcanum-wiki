**Description**: You've never seen such bones before.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~2

- [Unease](home/Stressors/Unease): 0~3

- [Anatomy Exp](home/Skills/Anatomy): 1

**Result**

- [Bones](home/Resources/Bones): 1~4

