**Description**: A small but solid chest.

**Effects**

- [Frustration](home/Stressors/Frustration): 1~3

**Loot**

- [Gold](home/Resources/Gold): 0~50

- [Gemstones](home/Resources/Gemstones): 2~5

- [Rune Stones](home/Resources/Rune-Stones): 0~2

- [Codices](home/Resources/Codices): 0~4

