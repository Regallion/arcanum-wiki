**Description**: Hestia has no interest in the world outside Thangmor, but will teach those who come to her.

**Effects**

- [Unease](home/Stressors/Unease): 2~4

- [Befuddlement](home/Stressors/Befuddlement): -2~3

- [Herbalism Exp](home/Skills/Herbalism): 1

**Result**

- [Scrying Max](home/Skills/Scrying): 0.001

- [World Lore Max](home/Skills/World-Lore): 0.001

- [Potions Exp](home/Skills/Potions): 0.001

- [Arcana](home/Resources/Arcana): 0.01

