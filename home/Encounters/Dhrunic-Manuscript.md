**Description**: Much that was is lost.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~4

- [Frustration](home/Stressors/Frustration): 0~4

- [World Lore Exp](home/Skills/World-Lore): 2

**Result**

- [World Lore Max](home/Skills/World-Lore): 0.0001

- [Arcana](home/Resources/Arcana): 0.01

**Flavor**: For none now live, who remember it.

