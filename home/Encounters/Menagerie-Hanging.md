**Description**: A pristine tapestry nearby displays a partial index of the menagerie.

**Effects**

- [Unease](home/Stressors/Unease): 1~2

- [Madness](home/Stressors/Madness): 1~2

- [Frustration](home/Stressors/Frustration): 1~2

- [Life](home/Player/Life): -3

- [Research](home/Resources/Research): 1

