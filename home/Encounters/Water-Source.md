**Description**: Some of the ice here is melting. How it fights the cold is a mystery.

**Effects**

- [Madness](home/Stressors/Madness): 1~2

- [Frost](home/Resources/Frost): -1~0

- [Water](home/Resources/Water): 0.2~0.3

- [Cryomancy Max](home/Skills/Cryomancy): 0.01

**Symbol**: ❄️

