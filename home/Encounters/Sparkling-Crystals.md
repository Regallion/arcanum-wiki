**Description**: A vein of crystals embedded in the cave wall, sparkling in the dark.

**Level**: 3

**Effects**

- [Frustration](home/Stressors/Frustration): 1~3

- [Weariness](home/Stressors/Weariness): 1~4

- [Geomancy Exp](home/Skills/Geomancy): 3

**Loot**

- [Gemstones](home/Resources/Gemstones): 1~3

- [Water Gem](home/Resources/Water-Gem): 0~1

- [Fire Gem](home/Resources/Fire-Gem): 20%

**Symbol**: ❄️

