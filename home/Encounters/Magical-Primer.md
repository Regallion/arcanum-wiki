**Description**: An introduction to magical theory.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~2

- [Mage Lore Exp](home/Skills/Mage-Lore): 1

**Result**

- [Research](home/Resources/Research): 1

- [Arcana](home/Resources/Arcana): 0.01

