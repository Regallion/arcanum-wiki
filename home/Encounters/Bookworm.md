**Description**: A studious annelid to choose a home within a tome's pages.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Mage Lore Exp](home/Skills/Mage-Lore): 3

