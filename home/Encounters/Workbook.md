**Description**: A book on the practical applications of magic

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~2

- [Frustration](home/Stressors/Frustration): 0~2

- [Crafting Exp](home/Skills/Crafting): 2

