**Description**: Through the mirror you appear to be drowning.

**Effects**

- [Breath](home/Player/Breath): -3

- [Water](home/Resources/Water): 1

- [Frustration](home/Stressors/Frustration): 2~4

- [Unease](home/Stressors/Unease): 3~5

