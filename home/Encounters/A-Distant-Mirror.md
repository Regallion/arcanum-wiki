**Description**: The mirror shows scenes from far away.

**Effects**

- [Scrying Exp](home/Skills/Scrying): 1

- Travel Exp: 1

- [Befuddlement](home/Stressors/Befuddlement): 1~2

