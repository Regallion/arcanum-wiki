**Description**: Something is wrong with the glass.

**Effects**

- [Trickery Exp](home/Skills/Trickery): 1

- [Frustration](home/Stressors/Frustration): 1~3

- [Befuddlement](home/Stressors/Befuddlement): 1~2

**Result**

