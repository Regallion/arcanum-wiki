**Description**: A rusted sickle lays abandoned. Curiously, it seems to have never had a handle.

**Effects**

- [Unease](home/Stressors/Unease): 0~2

- [Befuddlement](home/Stressors/Befuddlement): 0~2

- [Puppetry Exp](home/Skills/Puppetry): 2

- [Mechanical Magic Exp](home/Skills/Mechanical-Magic): 2

**Symbol**: None

