**Description**: A traveller here was frozen alive. The body may still be of use.

**Effects**

- [Madness](home/Stressors/Madness): 2~3

- [Weariness](home/Stressors/Weariness): 1~4

- [Frost](home/Resources/Frost): 1~2

- [Geomancy Max](home/Skills/Geomancy): 0.001

**Loot**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

- Body: 4%

- [Bones](home/Resources/Bones): {'val': '0~2', 'pct': '40%'}

- [Blood Gem](home/Resources/Blood-Gem): 5%

**Symbol**: ❄️

