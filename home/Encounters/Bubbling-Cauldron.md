**Description**: The cauldron bubbles and glows with a odorous brew.

**Effects**

- [Unease](home/Stressors/Unease): 0~2

- [Weariness](home/Stressors/Weariness): 1~3

- [Befuddlement](home/Stressors/Befuddlement): -1~2.5

- [Potions Exp](home/Skills/Potions): 1

- [Alchemy Exp](home/Skills/Alchemy): 1

- [Crafting Exp](home/Skills/Crafting): 1

**Modifiers**

- [Potions Max](home/Skills/Potions): ?1

**Loot**: [Hestia's Homebrew](home/Potions/Hestia's-Homebrew)

