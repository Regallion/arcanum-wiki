**Description**: A fox-girl delightedly teaches you some practical ways to use your weapon more effectively

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 0~2

- [Weariness](home/Stressors/Weariness): 2~5

- [Frustration](home/Stressors/Frustration): 2~5

- [Swordplay Exp](home/Skills/Swordplay): 3

- [Enchanting Exp](home/Skills/Enchanting): 3

**Result**

- [Swordplay Max](home/Skills/Swordplay): 0.001

**Symbol**: ⭐

