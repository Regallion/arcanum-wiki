**Description**: Higher even than Gorboru, the passes of Thandhrun incessantly thunder.

**Effects**

- [Madness](home/Stressors/Madness): 0.5

- [Weariness](home/Stressors/Weariness): 4~7

- [Frustration](home/Stressors/Frustration): 2~7

**Result**

- [Travel Max](home/Skills/Travel): 0.0001

