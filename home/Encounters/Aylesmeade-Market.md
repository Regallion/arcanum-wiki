**Description**: From spring spices to dwarven garnets, all manner of luxuries are bartered at the fair.

**Effects**

- [Gold](home/Resources/Gold): -0.5

- [Weariness](home/Stressors/Weariness): 1~3

- [Befuddlement](home/Stressors/Befuddlement): 0~1

**Result**

- [Herbs](home/Resources/Herbs): 4~10

**Symbol**: 🌼

