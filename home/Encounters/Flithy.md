**Description**: You meet a creeping creature with twenty furry legs and nothing else.

**Effects**

- [Unease](home/Stressors/Unease): 1~2

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Madness](home/Stressors/Madness): 1~2

