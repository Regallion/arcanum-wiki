**Description**: A cooling spring and frond trees relieve the monotony of the wastes.

**Effects**

- [Weariness](home/Stressors/Weariness): -2

- [Madness](home/Stressors/Madness): -1

- [Unease](home/Stressors/Unease): -1

