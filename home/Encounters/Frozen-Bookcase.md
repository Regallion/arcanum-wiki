**Description**: The cracked pages of the books are fused with ice

**Effects**

- [Madness](home/Stressors/Madness): 1~2

- [Fire](home/Resources/Fire): -0.1

- [Weariness](home/Stressors/Weariness): 1~3

- [Frost](home/Resources/Frost): 0~2

- [Cryomancy Exp](home/Skills/Cryomancy): 0.5

**Loot**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

- [Codices](home/Resources/Codices): 2~5

- [Tomes](home/Resources/Tomes): 20%

**Symbol**: ❄️

