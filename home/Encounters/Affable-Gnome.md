**Description**: A gnome is always more than willing to talk over a pint or a pipe.

**Effects**

- [Weariness](home/Stressors/Weariness): 0~2

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Spellcraft Exp](home/Skills/Spellcraft): 1

- [Research](home/Resources/Research): 1

- [Mage Lore Exp](home/Skills/Mage-Lore): 1

**Result**

- [Spellcraft Max](home/Skills/Spellcraft): 0.001

