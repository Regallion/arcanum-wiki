**Description**: Pidwig is sparse with words, but has seen many mysteries in his travels.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 0~2

- [Weariness](home/Stressors/Weariness): 0~2

- [Unease](home/Stressors/Unease): 0~2

- [World Lore Exp](home/Skills/World-Lore): 2

- [Astronomy Exp](home/Skills/Astronomy): 1

- [Mage Lore Exp](home/Skills/Mage-Lore): 1

**Result**

- [Arcana](home/Resources/Arcana): 0.05

