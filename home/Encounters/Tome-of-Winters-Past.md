**Description**: The year's coldest season is also known for its dark legends.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Frustration](home/Stressors/Frustration): 0~3

- [Winter Lore Rate](home/Skills/Winter-Lore): 0.04

**Loot**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

- [Codices](home/Resources/Codices): 80%

- [A Cold Legend](home/Events/A-Cold-Legend): 15%

**Symbol**: ❄️

