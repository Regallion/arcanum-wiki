**Description**: As evening fades to night, revellers kindle fires to continue the festivities.

**Effects**

- [Frustration](home/Stressors/Frustration): 1~3

- Wear: 0~2

**Result**

**Symbol**: 🌼

