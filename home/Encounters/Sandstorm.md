**Description**: The blistering sands peel away your skin.

**Effects**

- [Madness](home/Stressors/Madness): 0~1

- [Weariness](home/Stressors/Weariness): 1~2

- hp: -2

- [Aeromancy Exp](home/Skills/Aeromancy): 1

- [Geomancy Exp](home/Skills/Geomancy): 1

- [Earth](home/Resources/Earth): 0.5

- [Air](home/Resources/Air): 0.5

**Result**

