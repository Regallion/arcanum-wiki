**Description**: Some latent heat springs here from the ground.

**Effects**

- [Madness](home/Stressors/Madness): 1~2

- [Life](home/Player/Life): -1~-2

- [Frost](home/Resources/Frost): -2~-3

- [Fire](home/Resources/Fire): 0.2~0.3

**Symbol**: ❄️

