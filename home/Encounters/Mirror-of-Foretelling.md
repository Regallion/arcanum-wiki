**Description**: Visions of the future play in your reflection.

**Effects**

- [Divination Exp](home/Skills/Divination): 1

- [Madness](home/Stressors/Madness): 1~3

- [Unease](home/Stressors/Unease): 2~3

**Result**

