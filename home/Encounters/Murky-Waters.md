**Description**: Ominous shapes drift and flow beneath the murky waters.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 0~2

- [Unease](home/Stressors/Unease): 2~3

- [Madness](home/Stressors/Madness): 1~2

- [Aquamancy Exp](home/Skills/Aquamancy): 2

- [Umbramancy Exp](home/Skills/Umbramancy): 1

- [Arcana Max](home/Resources/Arcana): 0.05

