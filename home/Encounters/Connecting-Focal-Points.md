**Description**: Snow golems will serve well for this purpose.

**Level**: 2

**Effects**

- [Weariness](home/Stressors/Weariness): 0~2

- [Frustration](home/Stressors/Frustration): 0~1

- [Frost](home/Resources/Frost): 0~2

**Symbol**: ❄️

