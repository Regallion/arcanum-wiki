**Description**: Gazing over the dizzy edge, you see no sign of a bottom

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 0~3

- [Unease](home/Stressors/Unease): 1~4

- [Madness](home/Stressors/Madness): 0~3

- [Aeromancy Exp](home/Skills/Aeromancy): 2

**Result**

- [Air Gem](home/Resources/Air-Gem): 10%

- [Aeromancy Max](home/Skills/Aeromancy): 0.0001

