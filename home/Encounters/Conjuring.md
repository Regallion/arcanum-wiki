**Description**: Conjuring a fire elemental at the magic circle. That should stave away the cold!

**Effects**

- [Mana](home/Resources/Mana): 0.5~1

- [Pyromancy Exp](home/Skills/Pyromancy): 3

- [Conjuration Exp](home/Skills/Conjuration): 5

- [Madness](home/Stressors/Madness): 2~3

**Level**: 2

**Result**

- [Frost Rate](home/Resources/Frost): -0.1

**Symbol**: ❄️

