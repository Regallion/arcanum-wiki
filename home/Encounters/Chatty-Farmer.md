**Description**: You barely manage to escape his endless complaints about his rich neighbor.

**Effects**

- [Weariness](home/Stressors/Weariness): 1~2

- [Frustration](home/Stressors/Frustration): 1~2

- [Charms Exp](home/Skills/Charms): 2

- [Composure Exp](home/Skills/Composure): 2

**Symbol**: None

