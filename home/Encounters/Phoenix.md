**Description**: One of the rarest of birds, they are not uncommon in this place.

**Effects**

- [Unease](home/Stressors/Unease): 2~3

- [Frustration](home/Stressors/Frustration): 0.5

- [Fire Mastery Exp](home/Skills/Fire-Mastery): 1

