**Description**: Rare but torrential, the Orrem rains are accounted a wonder of the world.

**Effects**

- [Stamina](home/Player/Stamina): 1

- [Aquamancy Exp](home/Skills/Aquamancy): 1

- [Water](home/Resources/Water): 0.5

