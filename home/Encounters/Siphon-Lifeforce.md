**Description**: The old tree has lain dormant for centuries. Now it draws life back into its branches.

**Level**: 2

**Run Cost**

- [Life](home/Player/Life): 2

**Effects**

- [Weariness](home/Stressors/Weariness): 1~3

- [Frost](home/Resources/Frost): 0~1

**Symbol**: ❄️

