**Description**: The old stonemasons often chiselled spells into their works.

**Effects**

- [Geomancy Exp](home/Skills/Geomancy): 1

- [Befuddlement](home/Stressors/Befuddlement): 0~2

- [Unease](home/Stressors/Unease): 0~2

- [World Lore Exp](home/Skills/World-Lore): 1

**Result**

- [Geomancy Max](home/Skills/Geomancy): 0.0001

