**Description**: Magical gems will focus the power of the tree of rites.

**Level**: 2

**Effects**

- [Unease](home/Stressors/Unease): 0~2

- [Frost](home/Resources/Frost): 0~1

**Symbol**: ❄️

