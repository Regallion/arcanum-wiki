**Description**: Bright spring skies bring new stars into view.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Weariness](home/Stressors/Weariness): 2~3

**Result**

- [Starcharts](home/Resources/Starcharts): 1~2

**Symbol**: 🌼

