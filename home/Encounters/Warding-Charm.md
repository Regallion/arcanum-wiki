**Description**: A hanging charm wards away the borgles and grimstalks.

**Effects**

- [Unease](home/Stressors/Unease): 1~5

- [Befuddlement](home/Stressors/Befuddlement): 1~5

- [Charms Exp](home/Skills/Charms): 1

- [Enchanting Exp](home/Skills/Enchanting): 1

**Modifiers**

- [Enchanting Max](home/Skills/Enchanting): ?1

- [Charms Max](home/Skills/Charms): ?1

