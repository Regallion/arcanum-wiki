**Description**: Generally peaceful creatures, a gryffon is nevertheless deadly when enraged.

**Effects**

- [Unease](home/Stressors/Unease): 1~2

