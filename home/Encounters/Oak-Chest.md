**Description**: A sturdy, oaken chest.

**Effects**

- [Frustration](home/Stressors/Frustration): 2~4

- [Weariness](home/Stressors/Weariness): 1~3

**Loot**

- [Gold](home/Resources/Gold): 0~300

- [Flame Runes](home/Resources/Flame-Runes): 5%

- [Earth Runes](home/Resources/Earth-Runes): 5%

- [Water Runes](home/Resources/Water-Runes): 5%

- [Air Runes](home/Resources/Air-Runes): 5%

- [Tomes](home/Resources/Tomes): 50%

