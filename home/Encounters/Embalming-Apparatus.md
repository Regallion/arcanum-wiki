**Description**: Fluids and recepticles for the preservation of corpses.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~2

- [Unease](home/Stressors/Unease): 0~2

- [Reanimation Exp](home/Skills/Reanimation): 1

- [Embalming Exp](home/Skills/Embalming): 2

**Result**

- [Bone Dust](home/Resources/Bone-Dust): 0~3

