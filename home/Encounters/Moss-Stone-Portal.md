**Description**: One of a dozen mossy portals at the edges of the menagerie. Their use is unknown.

**Effects**

- [Unease](home/Stressors/Unease): 3~4

- [Befuddlement](home/Stressors/Befuddlement): 3~4

**Result**

- [Arcana](home/Resources/Arcana): 0.01

