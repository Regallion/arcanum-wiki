**Description**: The children seem to enjoy the snowballs you conjure from the air.

**Level**: 2

**Effects**

- [Weariness](home/Stressors/Weariness): 1~2

- [Frustration](home/Stressors/Frustration): -1~0

- [Befuddlement](home/Stressors/Befuddlement): -1~-2

- [Cryomancy Exp](home/Skills/Cryomancy): 3

**Result**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

- [Livingsnow](home/Resources/Livingsnow): 1~3

**Symbol**: ❄️

