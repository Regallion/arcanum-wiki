**Description**: Tenwick is often on the trail, but always glad to stop and trade tales.

**Effects**

- [Travel Exp](home/Skills/Travel): 1

- [Weariness](home/Stressors/Weariness): 2~3

