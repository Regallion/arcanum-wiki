**Level**: 2

**Description**: A tapestry with strange, hidden meanings

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~2

- [Frustration](home/Stressors/Frustration): 0~2

**Result**

- [Research](home/Resources/Research): 1

- [Arcana](home/Resources/Arcana): 0.005

