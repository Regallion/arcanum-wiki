**Description**: Lapping waves in the dying sun hint at secrets beyond your reach.

**Effects**

- [Frustration](home/Stressors/Frustration): -1~-2

- [Weariness](home/Stressors/Weariness): -1~-2

- [Madness](home/Stressors/Madness): -1~-2

- [Aquamancy Exp](home/Skills/Aquamancy): 1

- [Mysticism Exp](home/Skills/Mysticism): 1

- [Divination Exp](home/Skills/Divination): 1

