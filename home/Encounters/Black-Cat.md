**Description**: A black cat is an omen to be studied seriously

**Effects**

- [Unease](home/Stressors/Unease): 1~4

- [Frustration](home/Stressors/Frustration): 2~4

- [Divination Exp](home/Skills/Divination): 1

- [Scrying Exp](home/Skills/Scrying): 1

**Result**

- [Spirit Communion Max](home/Skills/Spirit-Communion): 0.001

- [Arcana](home/Resources/Arcana): 0.04

