**Description**: Young women from Aylesmeade weave patterns of colored ribbons around a bare trunk.

**Effects**

- [Weaving Exp](home/Skills/Weaving): 2

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Weariness](home/Stressors/Weariness): 1~2

**Loot**

- [Lily of the Vale](home/Rares/Lily-of-the-Vale): 10%

**Symbol**: 🌼

