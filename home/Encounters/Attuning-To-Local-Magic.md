**Description**: Attuning to magic of the tree, before the rite proceeds.

**Level**: 2

**Effects**

- [Madness](home/Stressors/Madness): 1~2

- [Frost](home/Resources/Frost): 0.5~2

**Result**

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

- [Cryomancy Max](home/Skills/Cryomancy): 0.1

**Symbol**: ❄️

