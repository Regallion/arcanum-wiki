**Description**: Over the years Pidwig has amassed a sizeable collection of gems and artifacts.

**Effects**

- [Crafting Exp](home/Skills/Crafting): 1

- [World Lore Exp](home/Skills/World-Lore): 1

- [Trickery Exp](home/Skills/Trickery): 1

- [Frustration](home/Stressors/Frustration): 0~2

- [Madness](home/Stressors/Madness): 0~2

**Loot**: [Prismaticgems](home/Tags)

**Flavor**: Perhaps he could be induced to part with some...

