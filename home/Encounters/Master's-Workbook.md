**Description**: A book on the practical applications of magic

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~3

- [Madness](home/Stressors/Madness): 0~3

- [Frustration](home/Stressors/Frustration): 1~3

- [Crafting Exp](home/Skills/Crafting): 4

**Result**

- [Crafting Max](home/Skills/Crafting): 0.0001

