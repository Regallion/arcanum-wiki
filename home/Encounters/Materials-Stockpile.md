**Description**: You happen upon a store of valuable materials.

**Effects**

- [Unease](home/Stressors/Unease): 4~6

**Result**

- [Fazbit's Knowledge](home/Resources/Fazbit's-Knowledge): 1~3

- [Essence of Winter](home/Resources/Essence-of-Winter): 0.1

**Loot**

- [Gemstones](home/Resources/Gemstones): 2~5

- [Gold](home/Resources/Gold): 100~250

**Symbol**: ❄️

