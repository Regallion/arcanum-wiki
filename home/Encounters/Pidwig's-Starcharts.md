**Description**: An old hand at astronomical calculations, Pidwig shares his extensive observations.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~3

- [Unease](home/Stressors/Unease): -1~2

- [Madness](home/Stressors/Madness): -1~1

- [Astronomy Exp](home/Skills/Astronomy): 3

- [Planes Lore Exp](home/Skills/Planes-Lore): 1

- [Scrying Exp](home/Skills/Scrying): 1

**Loot**: [Starcharts](home/Resources/Starcharts)

