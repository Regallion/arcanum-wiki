**Description**: At the center of the menagerie a single massive scale merges with the tree.

**Effects**

- [Unease](home/Stressors/Unease): 3~4

- [Madness](home/Stressors/Madness): 4~5

**Flavor**: Best not disturb it.

