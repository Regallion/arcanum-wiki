**Description**: Within a ramble of mundane details, thyffr often discloses some genuine history.

**Effects**

- [Frustration](home/Stressors/Frustration): 0~2

- [Weariness](home/Stressors/Weariness): 1~3

- [World Lore Exp](home/Skills/World-Lore): 2

- [Research](home/Resources/Research): 1

**Result**

- [Arcana](home/Resources/Arcana): 0.01

