**Description**: A clear sky affords an endless view of the firmament.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Unease](home/Stressors/Unease): -1~2

- [Madness](home/Stressors/Madness): -1~1

- [Astronomy Exp](home/Skills/Astronomy): 1

- [Planes Lore Exp](home/Skills/Planes-Lore): 1

- [Scrying Exp](home/Skills/Scrying): 1

