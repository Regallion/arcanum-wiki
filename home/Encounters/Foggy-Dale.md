**Description**: The fog here is so thick, you can't see your own hands.

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): 1~2

- [Frustration](home/Stressors/Frustration): 0~1

- [Unease](home/Stressors/Unease): 1~2

- [Aeromancy Exp](home/Skills/Aeromancy): 2

- [Spirit Communion Exp](home/Skills/Spirit-Communion): 1

