**Level**: 2

**Description**: A tree by a magic spring exhibits surprising qualities

**Effects**

- [Befuddlement](home/Stressors/Befuddlement): -1~2

- [Nature Studies Exp](home/Skills/Nature-Studies): 2

- [Herbalism Exp](home/Skills/Herbalism): 1

**Result**

- [Arcane Gem](home/Resources/Arcane-Gem): 25%

