**Description**: Higher even than Gorboru, the passes of Thandhrun incessantly thunder.

**Effects**

- [Madness](Madness): 0.5

- [Weariness](Weariness): 4~7

- [Frustration](Frustration): 2~7

**Result**

- [Travel Skill Max](Travel): 0.0001

