```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Level ≥ 10,<br>Armory > 0"])
  t1 ---> t1r ---> battlemage
  subgraph tier2 [Tier 2]
    battlemage([Battle Mage])
  end
```
**Tags**: T_Tier2

**Action Description**: Pursue the path of battle.

**Requirements**: [Tier 1](tier1) > 0 and [Level](Level) ≥ 10 and [Armory](Armory) > 0 and [Tier 2](tier2) = 0

**Cost to acquire**

- [Research](Research): 1500

- [Arcana](Arcana): 7

- [Gold](Gold): 800

- [Gems](Gems): 20

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 2](tier2): True

- [Research Max](Research): -50

- [Mana Max](Mana): 1

- [Mana Rate](Mana): 0.1

- [Pyromancy Max](Pyromancy): 2

- [Pyromancy Rate](Pyromancy): 0.1

- [Geomancy Max](Geomancy): 1

