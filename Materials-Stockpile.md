**Description**: You happen upon a store of valuable materials.

**Effects**

- [Unease](Unease): 4~6

**Result**

- [Fazbit's Knowledge](Fazbit's-Knowledge): 1~3

- [Essence of Winter](Essence-of-Winter): 0.1

**Loot**

- [Gems](Gems): 2~5

- [Gold](Gold): 100~250

**Symbol**: ❄️

