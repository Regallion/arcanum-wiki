**Description**: The uses of these instruments are beyond you.

**Effects**

- [Befuddlement](Befuddlement): -1~4

- [Madness](Madness): 0~2

- [Frustration](Frustration): 1~3

- [Alchemy Exp](Alchemy): 3

**Result**

- [Alchemy Max](Alchemy): 0.0001

