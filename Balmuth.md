**Description**: A gigantic reptile plows its jaw through the earth to eat.

**Effects**

- [Befuddlement](Befuddlement): 1~2

- [Earth Mastery Exp](Earth-Mastery): 1

**Flavor**: It seems to like rocks the best.

