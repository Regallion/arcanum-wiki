**Description**: You meet a creeping creature with twenty furry legs and nothing else.

**Effects**

- [Unease](Unease): 1~2

- [Befuddlement](Befuddlement): 1~2

- [Madness](Madness): 1~2

