**Requirements**: [Potions](Potions) > 0

**Level**: 1

**Cost to Buy**

- [Gold](Gold): 25

- [Research](Research): 50

**Cost to acquire**

- [Herbs](Herbs): 10

**Effect of using**

- [Stamina](Stamina): 10

