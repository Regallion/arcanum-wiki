```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Potions ≥ 4,<br>Alchemy ≥ 5"])
  t1 ---> t1r ---> alchemist
  subgraph tier2 [Tier 2]
    alchemist([Alchemist])
  end
```
**Tags**: T_Tier2

**Description**: An alchemist uses magical knowledge to craft wonders.

**Requirements**: [Tier 1](tier1) > 0 and [Potions](Potions) ≥ 4 and [Alchemy](Alchemy) ≥ 5

**Cost to acquire**

- [Research](Research): 2000

- [Arcane Gem](Arcane-Gem): 5

- [Fire Gem](Fire-Gem): 2

- [Water Gem](Water-Gem): 2

- [Earth Gem](Earth-Gem): 2

- [Arcana](Arcana): 20

**Result**

- [Skill Points](Skill-Points): 3

**Modifiers**

- [Tier 2](tier2): True

- [Research Max](Research): 100

- [Crafting Max](Crafting): 3

- [Crafting Rate](Crafting): 0.3

- [Potions Max](Potions): 3

- [Potions Rate](Potions): 5%

- [Alchemy Max](Alchemy): 3

- [Alchemy Rate](Alchemy): 5%

- [Lore Max](Lore): 1

- [Lore Rate](Lore): 0.3

