**Description**: the patterns must be laid precisely to direct the magic weaves.

**Level**: 2

**Effects**

- [Madness](Madness): 1~2

- [Befuddlement](Befuddlement): 1~3

- [Frost](Frost): 0~1

**Symbol**: ❄️

