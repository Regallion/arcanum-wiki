**Description**: This reddish potion causes a surge of energy in the imbiber, at the expense of health

**Requirements**: [Potions](Potions) ≥ 8

**Cost to Buy**

- [Gold](Gold): 250

- [Research](Research): 500

**Cost to acquire**

- [Herbs](Herbs): 25

- [Arcane Gem](Arcane-Gem): 5

- [Blood Gem](Blood-Gem): 5

**Effect of using**

- Dot: {'id': 'fount', 'name': "bull's tenacity", 'duration': 900, 'effect': {'stamina': 2.25, 'hp': -1}}

