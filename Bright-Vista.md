**Description**: A small knoll reveals lands far into the distance

**Effects**

- [Befuddlement](Befuddlement): -1~-2

- [Weariness](Weariness): -1~-2

- [Frustration](Frustration): -1~-2

- [Nature Lore Exp](Nature-Lore): 1

- [Lumenology Exp](Lumenology): 1

