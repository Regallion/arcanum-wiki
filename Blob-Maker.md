**Description**: The machine throws chunks of metal into a cauldron. Round, bouncy balls emerge.

**Effects**

- [Befuddlement](Befuddlement): 2~3

- [Unease](Unease): 3~5

**Result**

- [Fazbit's Knowledge](Fazbit's-Knowledge): 1~3

- [Essence of Winter](Essence-of-Winter): 0.1

**Symbol**: ❄️

