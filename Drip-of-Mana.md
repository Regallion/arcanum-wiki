**Description**: Raw mana injected right into the veins of a mage can allow for sustained casting, if one disregards the side-effects

**Requirements**: [Potions](Potions) ≥ 8

**Cost to Buy**

- [Gold](Gold): 250

- [Research](Research): 500

**Cost to acquire**

- [Herbs](Herbs): 25

- [Arcane Gem](Arcane-Gem): 5

**Effect of using**

- Dot: {'id': 'dotmana', 'name': 'mana drip', 'duration': 900, 'effect': {'mana': 1, 'hp': -1}}

