**Description**: In the midst of a wide desolation, a tall grey-skinned creature with no eyes offers you its wares.

**Effects**

- [Unease](Unease): 2~3

- [Madness](Madness): 1~3

- [Mana](Mana): -1

**Result**

- [Blood Gem](Blood-Gem): 20%

- [Arcane Gem](Arcane-Gem): 10%

- [Codices](Codices): 15%

