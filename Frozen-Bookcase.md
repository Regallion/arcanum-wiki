**Description**: The cracked pages of the books are fused with ice

**Effects**

- [Madness](Madness): 1~2

- [Fire](Fire): -0.1

- [Weariness](Weariness): 1~3

- [Frost](Frost): 0~2

- [Cryomancy Exp](Cryomancy): 0.5

**Loot**

- [Essence of Winter](Essence-of-Winter): 0.1

- [Codices](Codices): 2~5

- [Tomes](Tomes): 20%

**Symbol**: ❄️

