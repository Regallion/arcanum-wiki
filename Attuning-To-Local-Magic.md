**Description**: Attuning to magic of the tree, before the rite proceeds.

**Level**: 2

**Effects**

- [Madness](Madness): 1~2

- [Frost](Frost): 0.5~2

**Result**

- [Essence of Winter](Essence-of-Winter): 0.1

- [Cryomancy Max](Cryomancy): 0.1

**Symbol**: ❄️

