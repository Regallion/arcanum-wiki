**Tags**: T_Job

**Action Description**: Tend your master's owls and falcons.

**Requirements**: [Animal Handling](Animal-Handling)

**Cost to acquire**

- [Gold](Gold): 100

- [Research](Research): 100

- [Arcana](Arcana): 2

**Result**

- [Skill Points](Skill-Points): 1.25

- Player Exp: 10

**Modifiers**

- Event: [Promotion](evt_helper): True

- [Lore Max](Lore): 1

- [Gold Rate](Gold): 0.07

- [Animal Handling Max](Animal-Handling): 3

- [Animal Handling Rate](Animal-Handling): 0.2

- [Nature Max](Nature): 2

- [Nature Rate](Nature): 0.1

