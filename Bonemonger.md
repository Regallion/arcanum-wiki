```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Reanimator ≥ 1,<br>Vile > 0"])
  t1 ---> t1r ---> bonemonger
  subgraph tier2 [Tier 2]
    bonemonger([Bonemonger])
  end
```
**Tags**: T_Tier2

**Description**: Extend your life with spare parts from the dead.

**Requirements**: [Tier 1](tier1) > 0 and [Reanimator](Reanimator) ≥ 1 and Event: [Vile](evil) > 0 and [Tier 2](tier2) = 0

**Cost to acquire**

- [Research](Research): 800

- [Bodies](Bodies): 3

- [Bones](Bones): 10

- [Bone Dust](Bone-Dust): 15

- [Arcana](Arcana): 7

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 2](tier2): True

- [Evil](Evil): 10

- [Research Max](Research): 10

- [Embalming Max](Embalming): 1

- [Reanimation Max](Reanimation): 2

- [Shadowlore Max](Shadowlore): 3

- [Potions Max](Potions): 2

- [Spirit Lore Max](Spirit-Lore): 1

**Flavor**: Even the dead have their uses

