**Description**: A manual of beastly lore rests atop an ancient pedestal.

**Effects**

- [Unease](Unease): 1~2

- [Madness](Madness): 1~2

- [Frustration](Frustration): 1~2

- [Life](Life): -3

**Result**

- [Animal Handling Max](Animal-Handling): 0.001

