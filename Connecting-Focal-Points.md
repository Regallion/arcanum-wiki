**Description**: Snow golems will serve well for this purpose.

**Level**: 2

**Effects**

- [Weariness](Weariness): 0~2

- [Frustration](Frustration): 0~1

- [Frost](Frost): 0~2

**Symbol**: ❄️

