**Description**: An unnatural blend of serpent and rooster. As its stare is death, you avert your gaze as you approach.

**Effects**

- [Unease](Unease): 3~5

- [Befuddlement](Befuddlement): 2~3

- [Madness](Madness): 0~2

- [Magic Beasts Exp](Magic-Beasts): 1

