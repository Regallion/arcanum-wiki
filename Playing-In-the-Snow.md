**Description**: The children seem to enjoy the snowballs you conjure from the air.

**Level**: 2

**Effects**

- [Weariness](Weariness): 1~2

- [Frustration](Frustration): -1~0

- [Befuddlement](Befuddlement): -1~-2

- [Cryomancy Exp](Cryomancy): 3

**Result**

- [Essence of Winter](Essence-of-Winter): 0.1

- [Livingsnow](Livingsnow): 1~3

**Symbol**: ❄️

