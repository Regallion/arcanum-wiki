```mermaid
graph LR
  t0{Tier 0 Class}
  t0r(["Reanimation > 2"])
  t0 ---> t0r ---> reanimator
  subgraph tier1 [Tier 1]
    reanimator([Reanimator])
  end
```
**Tags**: T_Tier1

**Description**: Create and ressurect living materials.

**Requirements**: [Tier 0](tier0) > 0 and [Reanimation](Reanimation) > 2

**Cost to acquire**

- [Research](Research): 500

- [Bones](Bones): 10

- [Bone Dust](Bone-Dust): 10

- [Arcana](Arcana): 11

**Result**

- [Skill Points](Skill-Points): 2

**Class disables**: [Pace](Pace)

**Modifiers**

- [Tier 1](tier1): True

- [Mana Max](Mana): 1

- [Research Max](Research): 10

- [Embalming Max](Embalming): 1

- [Reanimation Max](Reanimation): 3

- [Shadowlore Max](Shadowlore): 2

- [Potions Max](Potions): 3

- [Spirit Lore Max](Spirit-Lore): 2

**Flavor**: Even the dead have their uses

