**Description**: A black cat is an omen to be studied seriously

**Effects**

- [Unease](Unease): 1~4

- [Frustration](Frustration): 2~4

- [Divination Exp](Divination): 1

- [Scrying Exp](Scrying): 1

**Result**

- [Spirit Lore Max](Spirit-Lore): 0.001

- [Arcana](Arcana): 0.04

