```mermaid
graph LR
  t5{Tier 5 Class}
  t5r(["(Earthshaker + Geomancer) > 0,<br>Hammerlore ≥ 10,<br>Geosculpting ≥ 10"])
  t5 ---> t5r ---> titan
  subgraph tier6 [Tier 6]
    titan([Titan])
  end
```
**Description**: A mantle of metal and stone.

**Tags**: T_Tier6

**Action Description**: Become a titan.

**Requirements**: [Tier 5](tier5) > 0 and ([Earthshaker](Earthshaker) + [Geomancer](Geomancer)) > 0 and [Hammerlore](Hammerlore) ≥ 10 and [Geosculpting](Geosculpting) ≥ 10 and [Tier 6](tier6) = 0

**Cost to acquire**

- [Research](Research): 10000

- [Arcana](Arcana): 50

- [Tomes](Tomes): 40

- [Earth Gem](Earth-Gem): 100

- [Earth Runes](Earth-Runes): 20

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 6](tier6): True

- [Geomancy Max](Geomancy): 3

- defense: 30

- [Stamina Max](Stamina): 20%

- [Hammerlore Max](Hammerlore): 3

- [Earthen Spire Space Max](Earthen-Spire): 100

**Flavor**: No stone left unturned

