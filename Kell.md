```mermaid
graph LR
  t4{Tier 4 Class}
  t4r(["Nature Lore ≥ 23,<br>(Witchcraft + Fey + Highelemental) > 0"])
  t4 ---> t4r ---> kell
  subgraph tier5 [Tier 5]
    kell([Kell])
  end
```
**Description**: Immortal spirits of nature, even the weakest Kell are beings to rival wizards

**Tags**: T_Tier5

**Requirements**: [Tier 4](tier4) > 0 and [Nature Lore](Nature-Lore) ≥ 23 and ([Witchcraft](Witchcraft) + [Fey](Fey) + [Highelemental](Highelemental)) > 0

**Cost to acquire**

- [Research](Research): 15000

- [Nature Gem](Nature-Gem): 50

- [Herbs](Herbs): 75

- [Arcana](Arcana): 20

- [Rune Stones](Rune-Stones): 15

**Modifiers**

- [Tier 5](tier5): True

- [Lore Max](Lore): 2

- [Lore Rate](Lore): 10%

- [Wind Lore Max](Wind-Lore): 2

- [Wind Lore Rate](Wind-Lore): 10%

- [Nature Lore Max](Nature-Lore): 3

- [Nature Lore Rate](Nature-Lore): 15%

- [Animal Handling Max](Animal-Handling): 2

- [Animal Handling Rate](Animal-Handling): 10%

- [Herbalism Max](Herbalism): 1

- [Herbalism Rate](Herbalism): 15%

**Flavor**: Kell are not of mortal blood, and only potent magics can imbue a mortal with their essence.

