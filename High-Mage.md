```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["Lore ≥ 21"])
  t2 ---> t2r ---> highmage
  subgraph tier3 [Tier 3]
    highmage([High Mage])
  end
```
**Description**: Elite of an arcane order.

**Tags**: T_Tier3

**Requirements**: [Tier 2](tier2) > 0 and [Lore](Lore) ≥ 21

**Cost to acquire**

- [Research](Research): 5000

- [Arcana](Arcana): 20

- [Tomes](Tomes): 10

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 3](tier3): True

- [Research Max](Research): 50

- [Pyromancy Max](Pyromancy): 1

- [Mysticism Max](Mysticism): 2

- [Alchemy Max](Alchemy): 1

- [Spellcraft Max](Spellcraft): 2

- [Crafting Max](Crafting): 1

- [Lore Max](Lore): 2

