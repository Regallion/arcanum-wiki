**Description**: You hear a faint moaning, but are unable to determine its source.

**Effects**

- [Spirit Lore Exp](Spirit-Lore): 1

- [Madness](Madness): 1

