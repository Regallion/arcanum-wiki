**Level**: 3

**Length**: 50

**Requirements**: [Emblem of Ice](Emblem-of-Ice) ≥ 1

**Description**: At the bottom of the frozen lake, numerous caves can be found, full of curiosities and wonders.

**Run Cost**

- [Stamina](Stamina): 0.5

**Effects**

- [Frost](Frost): 1

**Result**

- [Essence of Winter](Essence-of-Winter): 5

**Encounters**

- Movingsnow

- [Cold Snap](Cold-Snap)

- [Emberstone](Emberstone)

- [Cold Snap](Cold-Snap)

- [Water Source](Water-Source)

- [Frozen Herbs](Frozen-Herbs)

- [Sparkling Crystals](Sparkling-Crystals)

- [Dusty Chest](Dusty-Chest)

- [Ice Grave](Ice-Grave)

- [Ice Sculpture](Ice-Sculpture)

**Symbol**: ❄️

