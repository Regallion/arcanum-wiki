```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Divination ≥ 10"])
  t1 ---> t1r ---> seer
  subgraph tier2 [Tier 2]
    seer([Seer])
  end
```
**Description**: A diviner.

**Requirements**: [Tier 1](tier1) > 0 and [Divination](Divination) ≥ 10

**Tags**: T_Tier2

**Cost to acquire**

- [Codices](Codices): 20

- [Tomes](Tomes): 5

- [Arcana](Arcana): 10

- [Research](Research): 2000

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 2](tier2): True

- [Virtue](Virtue): 5

- [Lore Max](Lore): 2

- [Mana Max](Mana): 3

- [Divination Max](Divination): 2

- [Scrying Max](Scrying): 2

- [Spirit Lore Max](Spirit-Lore): 2

- [Spirit Lore Rate](Spirit-Lore): 0.1

