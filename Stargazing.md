**Description**: Bright spring skies bring new stars into view.

**Effects**

- [Befuddlement](Befuddlement): 1~2

- [Weariness](Weariness): 2~3

**Result**

- [Starcharts](Starcharts): 1~2

**Symbol**: 🌼

