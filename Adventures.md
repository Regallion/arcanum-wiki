Adventures are divided into two types:

[Locales](home/Locales) without enemies, where the player confronts different [encounters](home/Encounters), gaining a wide variety of benefits and stress as you progress.

[Dungeons](home/Dungeons) with enemies, where the player confronts [monsters](home/Monsters), battling with weapons, armor, spells, and minions to win rewards.
