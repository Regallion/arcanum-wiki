**Description**: Aylesmeade villagers arrange their spring blooms for sale.

**Effects**

- [Herbalism Exp](Herbalism): 2

- [Weariness](Weariness): 2~3

**Loot**

- [Hyacinth](Hyacinth): 1

**Symbol**: 🌼

