**Description**: Amount of damage subtracted from total.

**Value**: 0

**Statistics**: True

**Only Integer Values**: False

**Locked**: False

