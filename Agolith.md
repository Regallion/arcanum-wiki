**Description**: Hidden in a deep pond, it lures its prey with a fruiting branch growing from its head.

**Effects**

- [Unease](Unease): 1~2

- [Weariness](Weariness): 0~1

- [Herbalism Exp](Herbalism): 1

- [Water Lore Exp](Water-Lore): 1

