**Description**: A lone mechanism plows the fields for it's rich master.

**Effects**

- [Weariness](Weariness): 1~3

- [Mechanical Magic Exp](Mechanical-Magic): 5

**Result**

- [Herbs](Herbs): 0~2

**Symbol**: None

