```mermaid
graph LR
  t4{Tier 4 Class}
  t4r(["Rituals ≥ 10"])
  t4 ---> t4r ---> masterritualist
  subgraph tier5 [Tier 5]
    masterritualist([Master Ritualist])
  end
```
**Description**: There is no ritual you fear to attempt.

**Tags**: T_Tier5

**Requirements**: [Tier 4](tier4) > 0 and [Rituals](Rituals) ≥ 10 and [Tier 5](tier5) = 0

**Cost to acquire**

- [Research](Research): 3000

- [Arcana](Arcana): 25

- [Ritual Paper](Ritual-Paper): 25

- [Ritual Wards](Ritual-Wards): 10

- [Ritual Notes](Ritual-Notes): 15

**Modifiers**

- [Tier 5](tier5): True

- [Rituals Max](Rituals): 2

- [Ritual Wards Rate](Ritual-Wards): 0.0003

- [Arcane Paper Rate](Arcane-Paper): 0.002

**Flavor**: The last rule of ritual magic is thus: 'No cost is too great if it guarantees success.'

