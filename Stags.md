**Description**: Nothing is known about the enigmatic mage.

**Effects**

- [Frustration](Frustration): 1~5

- [Weariness](Weariness): 2~5

- [Research](Research): 1

**Result**

**Symbol**: ⭐

