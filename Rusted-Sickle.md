**Description**: A rusted sickle lays abandoned. Curiously, it seems to have never had a handle.

**Effects**

- [Unease](Unease): 0~2

- [Befuddlement](Befuddlement): 0~2

- [Puppetry Exp](Puppetry): 2

- [Mechanical Magic Exp](Mechanical-Magic): 2

**Symbol**: None

