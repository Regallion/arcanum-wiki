```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Geomancy ≥ 10"])
  t1 ---> t1r ---> geomancer
  subgraph tier2 [Tier 2]
    geomancer([Geomancer])
  end
```
**Description**: Lord of rocks.

**Tags**: T_Tier2

**Action Description**: Become a geomancer, a master of perseverance.

**Requirements**: [Tier 1](tier1) > 0 and [Geomancy](Geomancy) ≥ 10 and [Tier 2](tier2) = 0

**Cost to acquire**

- [Research](Research): 1000

- [Arcana](Arcana): 15

- [Tomes](Tomes): 10

- [Earth Gem](Earth-Gem): 10

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 2](tier2): True

- [Geomancy Max](Geomancy): 2

- [Geosculpting Max](Geosculpting): 2

- [Stamina Max](Stamina): 10%

- [Mana Max](Mana): 2

- [Earthen Spire Space Max](Earthen-Spire): 50

**Flavor**: Your empire of dirt

