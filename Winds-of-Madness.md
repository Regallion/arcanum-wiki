**Description**: Voices on the wind scream and echo in your mind.

**Effects**

- [Madness](Madness): 2~5

- [Mana](Mana): 0.5

- [Spirit Lore Exp](Spirit-Lore): 1

- [Spirit](Spirit): 1

- [Wind Lore Exp](Wind-Lore): 1

