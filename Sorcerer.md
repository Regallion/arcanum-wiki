```mermaid
graph LR
  t3{Tier 3 Class}
  t3r(["(Geomancy ≥ 15 or Water Lore ≥ 15 or Pyromancy ≥ 15),<br>(Elementalist + Highelemental) > 0"])
  t3 ---> t3r ---> sorcerer
  subgraph tier4 [Tier 4]
    sorcerer([Sorcerer])
  end
```
**Description**: A wielder of raw magical power.

**Requirements**: [Tier 3](tier3) > 0 and ([Geomancy](Geomancy) ≥ 15 or [Water Lore](Water-Lore) ≥ 15 or [Pyromancy](Pyromancy) ≥ 15) and ([Elementalist](Elementalist) + [Highelemental](Highelemental)) > 0

**Tags**: T_Tier4

**Cost to acquire**

- [Research](Research): 3000

- [Arcane Gem](Arcane-Gem): 5

- [Fire Gem](Fire-Gem): 15

- [Earth Gem](Earth-Gem): 15

- [Water Gem](Water-Gem): 15

- [Arcana](Arcana): 25

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 4](tier4): True

- [Lore Max](Lore): 1

- [Mana Max](Mana): 1

- [Geomancy Max](Geomancy): 2

- [Geomancy Rate](Geomancy): 10%

- [Water Lore Max](Water-Lore): 2

- [Water Lore Rate](Water-Lore): 10%

- [Pyromancy Max](Pyromancy): 2

- [Pyromancy Rate](Pyromancy): 10%

