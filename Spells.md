This page is a default list of the items in this category.<br>
If it doesn't meet your needs, please let a developer (probably 'simple') know, and they'll get around to updating it.<br>

[Abundance](Abundance)<br>
[Adamant Shell](Adamant-Shell)<br>
[Angel of Death](Angel-of-Death)<br>
[Animate Bulwark](Animate-Bulwark)<br>
[Animate Puppet Attacker](Animate-Puppet-Attacker)<br>
[Assemble Mecha-Charger](Assemble-Mecha-Charger)<br>
[Assemble Mecha-Mender](Assemble-Mecha-Mender)<br>
[Blasting Curse](Blasting-Curse)<br>
[Blinding Flash](Blinding-Flash)<br>
[Bloodshot](Bloodshot)<br>
[Bloodspike](Bloodspike)<br>
[Burst](Burst)<br>
[Call Spirit](Call-Spirit)<br>
[Calming Murmurs](Calming-Murmurs)<br>
[Clarity](Clarity)<br>
[Copper Skin](Copper-Skin)<br>
[Cordyceps](Cordyceps)<br>
[Create Burlap Golem](Create-Burlap-Golem)<br>
[Cure Paralysis](Cure-Paralysis)<br>
[Curesick](Curesick)<br>
[Cyril's Celestial Sling](Cyril's-Celestial-Sling)<br>
[Dark Bolt](Dark-Bolt)<br>
[Dark Promptings](Dark-Promptings)<br>
[Dbulm's Apiary](Dbulm's-Apiary)<br>
[Death Bolt](Death-Bolt)<br>
[Dispersal Dream](Dispersal-Dream)<br>
[Doublestrike](Doublestrike)<br>
[Down Swinging](Down-Swinging)<br>
[Drain Life](Drain-Life)<br>
[Dust Devil II](Dust-Devil-II)<br>
[Dust Devil](Dust-Devil)<br>
[Embolden](Embolden)<br>
[Entomb](Entomb)<br>
[Field of Heat](Field-of-Heat)<br>
[Fire Bolt](Fire-Bolt)<br>
[Fire Dart](Fire-Dart)<br>
[Fire Sense](Fire-Sense)<br>
[Firestorm](Firestorm)<br>
[Fissure](Fissure)<br>
[Flamestrike](Flamestrike)<br>
[Fling Stone](Fling-Stone)<br>
[Force Ten](Force-Ten)<br>
[Fount](Fount)<br>
[Frost Bolt III](Frost-Bolt-III)<br>
[Frost Bolt II](Frost-Bolt-II)<br>
[Frost Bolt](Frost-Bolt)<br>
[Frost Golem](Frost-Golem)<br>
[Gloom](Gloom)<br>
[Greater Fount](Greater-Fount)<br>
[Greater Light Spear](Greater-Light-Spear)<br>
[Greater Prismatic Spray](Greater-Prismatic-Spray)<br>
[Greater Quenching Sphere](Greater-Quenching-Sphere)<br>
[Greater Reanimate](Greater-Reanimate)<br>
[Guided Strike](Guided-Strike)<br>
[High Tide](High-Tide)<br>
[Ice Shard](Ice-Shard)<br>
[Icy Flame](Icy-Flame)<br>
[Insight](Insight)<br>
[Iron Skin](Iron-Skin)<br>
[Jinx](Jinx)<br>
[Kanna's Dervish Dance](Kanna's-Dervish-Dance)<br>
[Last Stand](Last-Stand)<br>
[Lesser Mana](Lesser-Mana)<br>
[Light Spear](Light-Spear)<br>
[Lightning](Lightning)<br>
[Lost Sword Art: Flying Swords](Lost-Sword-Art:-Flying-Swords)<br>
[Lost Sword Art: Giant Slayer](Lost-Sword-Art:-Giant-Slayer)<br>
[Mage Bolt](Mage-Bolt)<br>
[Magic Missile](Magic-Missile)<br>
[Magic Tending](Magic-Tending)<br>
[Magma Bolt](Magma-Bolt)<br>
[Mana](Mana)<br>
[Mass Jinx](Mass-Jinx)<br>
[Mend Wood](Mend-Wood)<br>
[Minor Fount](Minor-Fount)<br>
[Minor Mana](Minor-Mana)<br>
[Mud](Mud)<br>
[Perfect Strike](Perfect-Strike)<br>
[Phrenesis' Healing Circle](Phrenesis'-Healing-Circle)<br>
[Phrenesis' Healing Wave](Phrenesis'-Healing-Wave)<br>
[Poison Nettle](Poison-Nettle)<br>
[Poison Ward](Poison-Ward)<br>
[Prismatic Missile](Prismatic-Missile)<br>
[Prismatic Spray](Prismatic-Spray)<br>
[Protection: Fire](Protection:-Fire)<br>
[Protection: Light](Protection:-Light)<br>
[Protection: Nature](Protection:-Nature)<br>
[Protection: Shadow](Protection:-Shadow)<br>
[Protection: Water](Protection:-Water)<br>
[Pulsing Light III](Pulsing-Light-III)<br>
[Pulsing Light II](Pulsing-Light-II)<br>
[Pulsing Light](Pulsing-Light)<br>
[Quake](Quake)<br>
[Quell](Quell)<br>
[Quenching Sphere](Quenching-Sphere)<br>
[Quicksand](Quicksand)<br>
[Raise Zombie](Raise-Zombie)<br>
[Reanimate](Reanimate)<br>
[Repair Machina](Repair-Machina)<br>
[Resist Paralysis](Resist-Paralysis)<br>
[Resist Silence](Resist-Silence)<br>
[Revive](Revive)<br>
[Seal Dreams](Seal-Dreams)<br>
[Seal Heart](Seal-Heart)<br>
[Seal Limbs](Seal-Limbs)<br>
[Seal Mind](Seal-Mind)<br>
[Seal Reason](Seal-Reason)<br>
[Sealing Light III](Sealing-Light-III)<br>
[Sealing Light II](Sealing-Light-II)<br>
[Sealing Light](Sealing-Light)<br>
[Searing Bolt](Searing-Bolt)<br>
[Shape Autocaster](Shape-Autocaster)<br>
[Shape Autoslayer](Shape-Autoslayer)<br>
[Smite](Smite)<br>
[Soothing Breeze](Soothing-Breeze)<br>
[Soul Card: Soul Lure](Soul-Card:-Soul-Lure)<br>
[Speak](Speak)<br>
[Spirit Art: Soul River](Spirit-Art:-Soul-River)<br>
[Spirit Art: Soulstrike](Spirit-Art:-Soulstrike)<br>
[Spiteful Hex](Spiteful-Hex)<br>
[Splendor](Splendor)<br>
[Starfall](Starfall)<br>
[Steel Skin](Steel-Skin)<br>
[Stone Skin](Stone-Skin)<br>
[Summon Blade](Summon-Blade)<br>
[Summon Hobgoblin](Summon-Hobgoblin)<br>
[Summon Murder](Summon-Murder)<br>
[Summon Raven](Summon-Raven)<br>
[Summon Spider](Summon-Spider)<br>
[Summon Unicorn](Summon-Unicorn)<br>
[Thunderstrike](Thunderstrike)<br>
[To Sand](To-Sand)<br>
[Tremor](Tremor)<br>
[True Strike](True-Strike)<br>
[Tune Automata](Tune-Automata)<br>
[Unearth](Unearth)<br>
[Unseen Servant](Unseen-Servant)<br>
[Venom II](Venom-II)<br>
[Venom I](Venom-I)<br>
[Vine Whip](Vine-Whip)<br>
[Wake](Wake)<br>
[Water Sense](Water-Sense)<br>
[Water Spray](Water-Spray)<br>
[Whirling Step III](Whirling-Step-III)<br>
[Whirling Step II](Whirling-Step-II)<br>
[Whirling Step](Whirling-Step)<br>
[Whisper](Whisper)<br>
[Wild Growth](Wild-Growth)<br>
[Wild Tending](Wild-Tending)<br>
[Wind Sense](Wind-Sense)<br>
[Zap](Zap)