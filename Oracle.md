```mermaid
graph LR
  t0{Tier 0 Class}
  t0r(["Spirit Lore ≥ 5,<br>Divination ≥ 5"])
  t0 ---> t0r ---> oracle
  subgraph tier1 [Tier 1]
    oracle([Oracle])
  end
```
**Tags**: T_Tier1

**Description**: A diviner.

**Requirements**: [Tier 0](tier0) > 0 and [Spirit Lore](Spirit-Lore) ≥ 5 and [Divination](Divination) ≥ 5

**Cost to acquire**

- [Research](Research): 750

- [Gems](Gems): 10

- [Codices](Codices): 20

- [Arcana](Arcana): 5

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 1](tier1): True

- [Virtue](Virtue): 1

- [Research Max](Research): 15

- [Lore Max](Lore): 2

- [Scrying Max](Scrying): 2

- [Mana Max](Mana): 3

- [Divination Max](Divination): 2

- [Divination Rate](Divination): 0.1

- [Wind Lore Max](Wind-Lore): 1

- [Spirit Lore Max](Spirit-Lore): 1

- [Spirit Lore Rate](Spirit-Lore): 0.1

