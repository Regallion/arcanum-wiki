**Description**: A glowing star cinder embedded in a shrine.

**Effects**

- [Lumenology Exp](Lumenology): 1

- [Befuddlement](Befuddlement): 2~5

- [Weariness](Weariness): 0~-2

- [Unease](Unease): 1~5

- [World Lore Exp](World-Lore): 1

**Result**

- [Lumenology Max](Lumenology): 0.0001

**Flavor**: You can't seem to pry it loose.

