```mermaid
graph LR
  t3{Tier 3 Class}
  t3r(["Spell Blade > 0"])
  t3 ---> t3r ---> bladeweaver
  subgraph tier4 [Tier 4]
    bladeweaver([Bladeweaver])
  end
```
**Tags**: T_Tier4

**Description**: You have taken your training further, weaving magic and steel with a flourish of magical grace.

**Requirements**: [Tier 3](tier3) > 0 and [Spell Blade](Spell-Blade) > 0 and [Tier 4](tier4) = 0

**Cost to acquire**

- [Research](Research): 5000

- [Arcana](Arcana): 20

- [Gold](Gold): 2500

- [Air Gem](Air-Gem): 10

- [Fire Gem](Fire-Gem): 5

- [Earth Gem](Earth-Gem): 5

- [Water Gem](Water-Gem): 5

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 4](tier4): True

- tohit: 15%

- [World Lore Max](World-Lore): 1

- [Enchanting Max](Enchanting): 1

- Elemental Max: 1

- [Bladelore Max](Bladelore): 2

- [Anatomy Max](Anatomy): 1

- [Greater Arcane Body Max](Greater-Arcane-Body): 1

