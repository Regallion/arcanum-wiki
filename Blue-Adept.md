```mermaid
graph LR
  t-1{Tier -1 Class}
  t-1r([Neophyte,<br>Virtue >= 200])
  t-1 ---> t-1r ---> blueadept
  subgraph tier0 [Tier 0]
    blueadept([Blue Adept])
  end
```
