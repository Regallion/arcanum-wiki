```mermaid
graph LR
  t0{Tier 0 Class}
  t0r(["Animal Handling ≥ 2,<br>Potions ≥ 3"])
  t0 ---> t0r ---> witch
  subgraph tier1 [Tier 1]
    witch([Witchcraft])
  end
```
**Tags**: T_Tier1

**Description**: Witch

**Action Name**: The Craft

**Action Description**: Enter into the practice of witchcraft.

**Requirements**: [Tier 0](tier0) > 0 and [Animal Handling](Animal-Handling) ≥ 2 and [Potions](Potions) ≥ 3 and [Tier 1](tier1) = 0

**Flavor**: 'What is't you do?' 'A deed without a name'

**Cost to acquire**

- [Research](Research): 350

- [Gold](Gold): 200

- [Arcana](Arcana): 13

**Result**

- [Skill Points](Skill-Points): 2

**Class disables**: [Pace](Pace)

**Modifiers**

- [Tier 1](tier1): True

- Witchcraft: True

- [Research Max](Research): 10

- [Nature Lore Max](Nature-Lore): 1

- [Nature Lore Rate](Nature-Lore): 10%

- [Animal Handling Max](Animal-Handling): 1

- [Animal Handling Rate](Animal-Handling): 10%

- [Potions Max](Potions): 2

- [Potions Rate](Potions): 15%

- [Spirit Lore Max](Spirit-Lore): 2

