**Description**: You've never seen such bones before.

**Effects**

- [Befuddlement](Befuddlement): -1~2

- [Unease](Unease): 0~3

- [Anatomy Exp](Anatomy): 1

**Result**

- [Bones](Bones): 1~4

