```mermaid
graph LR
  t3{Tier 3 Class}
  t3r(["Geomancy ≥ 20"])
  t3 ---> t3r ---> earthshaker
  subgraph tier4 [Tier 4]
    earthshaker([Earthshaker])
  end
```
**Description**: Master of the tectonics.

**Tags**: T_Tier4

**Action Description**: Become an earthshaker.

**Requirements**: [Tier 3](tier3) > 0 and [Geomancy](Geomancy) ≥ 20 and [Tier 4](tier4) = 0

**Cost to acquire**

- [Research](Research): 4000

- [Arcana](Arcana): 25

- [Tomes](Tomes): 30

- [Earth Gem](Earth-Gem): 50

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 4](tier4): True

- [Geomancy Max](Geomancy): 3

- defense: 15

- [Stamina Max](Stamina): 20%

- [Earth Rate](Earth): 0.3

- [Earthen Spire Space Max](Earthen-Spire): 50

**Flavor**: Finally, a subduction license!

