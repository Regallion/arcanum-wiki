**Description**: A gnome is always more than willing to talk over a pint or a pipe.

**Effects**

- [Weariness](Weariness): 0~2

- [Befuddlement](Befuddlement): 1~2

- [Spellcraft Exp](Spellcraft): 1

- [Research](Research): 1

- [Lore Exp](Lore): 1

**Result**

- [Spellcraft Max](Spellcraft): 0.001

