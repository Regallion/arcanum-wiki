**Description**: Crafting everburning candles

**Effects**

- [Unease](Unease): 1~2

- [Crafting Exp](Crafting): 3

**Level**: 2

**Result**

- [Essence of Winter](Essence-of-Winter): 0.1

**Symbol**: ❄️

