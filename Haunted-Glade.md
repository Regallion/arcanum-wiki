**Description**: Trees here have an eerie aspect

**Effects**

- [Madness](Madness): 1~2

- [Unease](Unease): 0~2

- [Shadowlore Exp](Shadowlore): 1

- [Spirit Lore Exp](Spirit-Lore): 1

**Result**

- [Spirit Lore Max](Spirit-Lore): 0.001

