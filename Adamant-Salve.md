**Requirements**: [Potions](Potions) ≥ 7

**Level**: 10

**Cost to Buy**

- [Gold](Gold): 2500

- [Tomes](Tomes): 15

- [Earth Gem](Earth-Gem): 5

**Cost to acquire**

- [Herbs](Herbs): 30

- [Gems](Gems): 1

**Effect of using**

- Dot: {'id': 'adamantreinforcement', 'name': 'adamant salve', 'duration': 1800, 'mod': {'self.defense': 50}}

