```mermaid
graph LR
  t5{Tier 5 Class}
  t5r(["Lore ≥ 35,<br>Spellcraft ≥ 10,<br>not Evil"])
  t5 ---> t5r ---> wizard3
  subgraph tier6 [Tier 6]
    wizard3([Mythic Wizard])
  end
```
**Description**: Their names reappear throughout the ages, and few believe they ever existed.

**Tags**: T_Tier6

**Requirements**: [Tier 5](tier5) > 0 and [Lore](Lore) ≥ 35 and [Spellcraft](Spellcraft) ≥ 10 and Event: [Vile](evil) = 0

**Cost to acquire**

- [Research](Research): 20000

- [Arcana](Arcana): 75

- [Tomes](Tomes): 50

- [Star Shard](Star-Shard): 3

- [Rune Stones](Rune-Stones): 10

**Modifiers**

- [Tier 6](tier6): True

- [Spellcraft Max](Spellcraft): 1

- [Pyromancy Max](Pyromancy): 1

- [Enchanting Max](Enchanting): 1

- [Water Lore Max](Water-Lore): 1

- [Water Lore Rate](Water-Lore): 1%

- [Geomancy Max](Geomancy): 1

- [Crafting Max](Crafting): 2

- [World Lore Max](World-Lore): 1

- [Potions Max](Potions): 1

- [Alchemy Max](Alchemy): 1

