**Description**: Gazing over the dizzy edge, you see no sign of a bottom

**Effects**

- [Befuddlement](Befuddlement): 0~3

- [Unease](Unease): 1~4

- [Madness](Madness): 0~3

- [Wind Lore Exp](Wind-Lore): 2

**Result**

- [Air Gem](Air-Gem): 10%

- [Wind Lore Max](Wind-Lore): 0.0001

