**Description**: A violent echo still wanders the halls it never walked

**Effects**

- [Madness](Madness): 2~4

- [Unease](Unease): 2~4

- [Tempus](Tempus): -1

- [Chronomancy Exp](Chronomancy): 1

- [Void Lore Exp](Void-Lore): 1

**Loot**

- Voidrune: 1%

- [Void Gem](Void-Gem): 1%

- [Time Runes](Time-Runes): 1%

- [Time Gem](Time-Gem): 1%

**Flavor**: Did that actually happen?

**Symbol**: ⭐

