**Description**: Writhing in heaps and drooping from branches, a mass of snakes covers the vicinity.

**Effects**

- [Unease](Unease): 1~2

- [Madness](Madness): 2~3

- [Frustration](Frustration): 2~3

- [Life](Life): -3

