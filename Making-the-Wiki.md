If you want to help on the wiki, you need to contact us in the discord: https://discord.gg/bCABC96 \
This is because editing in this wiki is whitelisted through gitlab. So contact mathias or just send a message in #wiki-talk.

The wiki supports [markdown](https://docs.gitlab.com/ee/user/markdown.html#wiki-specific-markdown) and you can make the pages you feel are lacking.\
Changing the auto-generated pages doesn't give any benefit as your changes will be overwritten. So take contact on discord to discuss changes to it.\
The auto-generated pages are made through scripts in this [folder](https://gitlab.com/mathiashjelm/arcanum/-/tree/wikistuff/scripts) Only