**Description**: Removes most forms of inisidious motion impairment.

**Level**: 5

**Requirements**: [Potions](Potions) ≥ 5

**Cost to acquire**

- [Herbs](Herbs): 1

- [Arcane Gem](Arcane-Gem): 1

**Effect of using**: [Cure Paralysis](Cure-Paralysis)

