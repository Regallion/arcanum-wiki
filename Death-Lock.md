```mermaid
graph LR
  t5{Tier 5 Class}
  t5r(["Phylactory > 0"])
  t5 ---> t5r ---> deathlock
  subgraph tier6 [Tier 6]
    deathlock([Death Lock])
  end
```
**Description**: An unholy synthesis of wizardry and demonology.

**Tags**: T_Tier6

**Requirements**: [Tier 5](tier5) > 0 and [Phylactory](Phylactory) > 0

**Cost to acquire**

- [Research](Research): 20000

- [Souls](Souls): 100

- [Bodies](Bodies): 100

- [Tomes](Tomes): 50

- [Arcana](Arcana): 60

- [Flame Runes](Flame-Runes): 10

- [Rune Stones](Rune-Stones): 20

**Modifiers**

- [Tier 6](tier6): True

- [Necromancy Max](Necromancy): 2

- [Geomancy Max](Geomancy): 2

- [Pyromancy Max](Pyromancy): 3

- [Pyromancy Rate](Pyromancy): 20%

- [Shadowlore Max](Shadowlore): 3

- [Shadowlore Rate](Shadowlore): 0.1+5%

- [Demonology Max](Demonology): 5

- [Demonology Rate](Demonology): 20%

**Flavor**: Narz, Bodias, Vezial. Even among the Archlocks, some names stood in abject terror.

