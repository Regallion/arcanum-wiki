```mermaid
graph LR
  t4{Tier 4 Class}
  t4r(["Spell Blade > 0"])
  t4 ---> t4r ---> arcanedervish
  subgraph tier5 [Tier 5]
    arcanedervish([Arcane Dervish])
  end
```
**Tags**: T_Tier5

**Description**: Your skills with blade and magic blend together in a blur, each stroke seeming like a step in a dance.

**Requirements**: [Tier 4](tier4) > 0 and [Spell Blade](Spell-Blade) > 0 and [Tier 5](tier5) = 0

**Cost to acquire**

- [Research](Research): 10000

- [Arcana](Arcana): 20

- [Gold](Gold): 4000

- [Air Gem](Air-Gem): 20

- [Fire Gem](Fire-Gem): 10

- [Earth Gem](Earth-Gem): 10

- [Water Gem](Water-Gem): 10

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 5](tier5): True

- tohit: 15%

- [World Lore Max](World-Lore): 1

- [Enchanting Max](Enchanting): 1

- Elemental Max: 1

- [Bladelore Max](Bladelore): 2

- [Anatomy Max](Anatomy): 1

- [Greater Arcane Body Max](Greater-Arcane-Body): 1

