**Description**: A hanging charm wards away the borgles and grimstalks.

**Effects**

- [Unease](Unease): 1~5

- [Befuddlement](Befuddlement): 1~5

- [Charms Exp](Charms): 1

- [Enchanting Exp](Enchanting): 1

**Result**

- [Enchanting Max](Enchanting): 0.01

- [Charms Max](Charms): 0.01

