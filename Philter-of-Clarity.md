**Description**: A master's skill in liquid form

**Requirements**: [Potions](Potions) > 0

**Level**: 1

**Cost to Buy**

- [Gold](Gold): 250

- [Research](Research): 500

**Cost to acquire**

- [Herbs](Herbs): 10

- [Potion Base](Potion-Base): 4

**Effect of using**

- [Skill Points](Skill-Points): 1

