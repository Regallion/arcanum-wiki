**Level**: 23

**Requirements**: [Tier 5](tier5) > 0 and [Magic Beasts](Magic-Beasts) ≥ 5

**Length**: 200

**Description**: Beneath the tallest idrasil a great menagerie thrives beneath a second sky.

**Run Cost**

- [Stamina](Stamina): 3

**Modifiers**

- [Magic Beasts Max](Magic-Beasts): ?1

**Effects**

- [Magic Beasts Exp](Magic-Beasts): 1

**Result**

- Title: Wild Compiler

**Loot**

- [Gems](Gems): 5~10

- [Herbs](Herbs): 10~20

**Encounters**

- [Agolith](Agolith)

- [Balmuth](Balmuth)

- [Barghest](Barghest)

- [Menagerie Hanging](Menagerie-Hanging)

- [Bestiary](Bestiary)

- [Scale of Ouroboros](Scale-of-Ouroboros)

- [Cockatrice](Cockatrice)

- [Flithy](Flithy)

- [Gryffon](Gryffon)

- [Hydra](Hydra)

- [Phoenix](Phoenix)

- [Pod of Poglers](Pod-of-Poglers)

- [Snake Swarm](Snake-Swarm)

- [Moss-Stone Portal](Moss-Stone-Portal)

- [Spider Swarm](Spider-Swarm)

- [Trumple](Trumple)

- [Wyvern](Wyvern)

**Flavor**: Its creator and purpose are unknown to history. Perhaps all creatures exist within.

