**Description**: With warmer weather and lighter skies, the celebrations last long into the night.

**Effects**

- [Weariness](Weariness): 3~5

**Result**

**Symbol**: 🌼

