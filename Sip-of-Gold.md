**Description**: Temporarily grants you an impeccable ability to acquire valuables.

**Requirements**: [Potions](Potions) ≥ 9

**Level**: 10

**Cost to Buy**

- [Gold](Gold): 5000

- [Research](Research): 6000

**Cost to acquire**

- [Herbs](Herbs): 50

- [Gold](Gold): 2000

- [Gems](Gems): 40

- [Spirit](Spirit): 15

- [Potion Base](Potion-Base): 2

**Effect of using**

- Dot: {'duration': 1200, 'effect': {'gold': 5, 'gems': 0.1}}

