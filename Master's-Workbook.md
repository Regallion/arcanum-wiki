**Description**: A book on the practical applications of magic

**Effects**

- [Befuddlement](Befuddlement): -1~3

- [Madness](Madness): 0~3

- [Frustration](Frustration): 1~3

- [Crafting Exp](Crafting): 4

**Result**

- [Crafting Max](Crafting): 0.0001

