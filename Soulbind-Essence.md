**Description**: A measure of this sickly green substance can significantly improve the effects of soul traps.

**Requirements**: [Potions](Potions) ≥ 6 and Event: [Vile](evil) > 0

**Level**: 10

**Cost to Buy**

- [Gold](Gold): 2500

- [Research](Research): 3000

**Cost to acquire**

- Soul: 15

- [Gold](Gold): 500

- [Spirit Gem](Spirit-Gem): 5

- [Spirit](Spirit): 15

- [Shadow Gem](Shadow-Gem): 5

- [Potion Base](Potion-Base): 1

**Effect of using**

- Dot: {'duration': 1800, 'mod': {'trapsoul.run.bonedust': -0.2, 'trapsoul.length': -5, 'trapsoul.result.souls.max': 0.25}}

