**Description**: A pile of scrap metal sits on the floor, once a useful mechanical servant

**Effects**

- [Weariness](Weariness): 2~3

- [Mechanical Magic Exp](Mechanical-Magic): 1

**Result**

- [Machinae](Machinae): 15%

**Symbol**: None

