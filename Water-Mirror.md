**Description**: Through the mirror you appear to be drowning.

**Effects**

- [Breath](Breath): -3

- [Water](Water): 1

- [Frustration](Frustration): 2~4

- [Unease](Unease): 3~5

