**Description**: More advanced magical theory.

**Effects**

- [Befuddlement](Befuddlement): -1~3

- [Weariness](Weariness): 0~2

- [Frustration](Frustration): 0~2

- [Lore Exp](Lore): 2

- [Languages Exp](Languages): 1

**Result**

- [Codices](Codices): 10%

- [Research](Research): 10

- [Arcana](Arcana): 0.02

