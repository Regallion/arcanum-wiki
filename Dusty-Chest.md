**Description**: A small but solid chest.

**Effects**

- [Frustration](Frustration): 1~3

**Loot**

- [Gold](Gold): 0~50

- [Gems](Gems): 2~5

- [Rune Stones](Rune-Stones): 0~2

- [Codices](Codices): 0~4

