```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Lore ≥ 17"])
  t1 ---> t1r ---> mage
  subgraph tier2 [Tier 2]
    mage([Mage])
  end
```
**Description**: Member of an arcane order.

**Tags**: T_Tier2

**Requirements**: [Tier 1](tier1) > 0 and [Lore](Lore) ≥ 17

**Cost to acquire**

- [Research](Research): 2000

- [Arcana](Arcana): 15

- [Tomes](Tomes): 5

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 2](tier2): True

- [Research Max](Research): 40

- [Pyromancy Max](Pyromancy): 1

- [Mysticism Max](Mysticism): 2

- [Enchanting Max](Enchanting): 1

- [Lore Max](Lore): 2

- [Lore Rate](Lore): 0.4

- [Mana Max](Mana): 3

