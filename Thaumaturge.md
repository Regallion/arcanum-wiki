```mermaid
graph LR
  t3{Tier 3 Class}
  t3r(["Spirit ≥ 15,<br>Divination ≥ 15"])
  t3 ---> t3r ---> thaumaturge
  subgraph tier4 [Tier 4]
    thaumaturge([Thaumaturge])
  end
```
**Description**: A maker of miracles

**Requirements**: [Tier 3](tier3) > 0 and [Spirit](Spirit) ≥ 15 and [Divination](Divination) ≥ 15

**Tags**: T_Tier4

**Cost to acquire**

- [Research](Research): 5000

- [Arcana](Arcana): 25

- [Tomes](Tomes): 7

- [Spirit Gem](Spirit-Gem): 20

- [Rune Stones](Rune-Stones): 5

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 4](tier4): True

- [Spirit Lore Max](Spirit-Lore): 2

- [Divination Max](Divination): 3

- [Spellcraft Max](Spellcraft): 2

- [Spellcraft Rate](Spellcraft): 0.3+5%

- [Lore Max](Lore): 2

- [Light Max](Light): 2

- [Light Rate](Light): 0.2+5%

- [Lore Rate](Lore): 0.4+5%

- [Mana Max](Mana): 3

