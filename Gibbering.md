**Description**: They hide in small cracks and in return for a wicked deed, will reveal one secret.

**Effects**

- [Weariness](Weariness): 0~2

- [Madness](Madness): 0~2

- [Mysticism Exp](Mysticism): 1

- [Research](Research): 1

**Result**

- [Shadow Gem](Shadow-Gem): 5%

- [Arcana](Arcana): 0.01

**Flavor**: Legend tells of a massive gibbering, who will answer any question in return for a murder.

