**Level**: 1

**Length**: 25

**Requirements**: Event: [Promotion](evt_helper)

**Description**: The nearby village's fields are vast and full of opportunity for an aspiring mage.

**Run Cost**

- [Stamina](Stamina): 0.2

**Result**

- [Arcana](Arcana): 0.1

- [Gold](Gold): 5

**Loot**

- [Herbs](Herbs): 1~3

- [Loose Cog](Loose-Cog): 5%

**Encounters**

- [Automated Plower](Automated-Plower)

- [Pile of Wood?](Pile-of-Wood_)

- [Chatty Farmer](Chatty-Farmer)

- [Blood](Blood)

- [Scarecrow](Scarecrow)

- [Scarecrow](Scarecrow)

- [Upturned Soil](Upturned-Soil)

- [Rusted Sickle](Rusted-Sickle)

**Symbol**: None

