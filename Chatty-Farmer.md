**Description**: You barely manage to escape his endless complaints about his rich neighbor.

**Effects**

- [Weariness](Weariness): 1~2

- [Frustration](Frustration): 1~2

- [Charms Exp](Charms): 2

- [Composure Exp](Composure): 2

**Symbol**: None

