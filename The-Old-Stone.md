**Description**: The massive head of stone and moss is said to be as old as the world itself.

**Effects**

- [Unease](Unease): 3~4

- [Befuddlement](Befuddlement): 0~-2

- [Madness](Madness): 0~2

- [World Lore Exp](World-Lore): 2

**Flavor**: A voice seems to grumble from the ground.

