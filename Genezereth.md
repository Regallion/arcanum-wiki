**Level**: 15

**Requirements**: [World Lore](World-Lore) ≥ 7

**Length**: 100

**Symbol**: 📖

**Description**: An ancient library at the bridge spanning Markhul's gap.

**Flavor**: Third of the Four Weirs of Dhrune.

**Result**

- Title: Historian

**Run Cost**

- [Stamina](Stamina): 0.4

**Loot**

- [Tomes](Tomes): 3~5

- [Markhul's Codex](Markhul's-Codex): True

**Encounters**

- [Occult Tapestry](Occult-Tapestry)

- [Runic Tome](Runic-Tome)

- [Dusty Chest](Dusty-Chest)

- [Markhul's Gap](Markhul's-Gap)

- [Crumbled Statue](Crumbled-Statue)

- [Dhrunic Manuscript](Dhrunic-Manuscript)

- [Magical Primer](Magical-Primer)

- [Scrap Pile](Scrap-Pile)

- [Pulsating Crystal](Pulsating-Crystal)

