**Requirements**: [Potions](Potions) ≥ 5

**Level**: 5

**Cost to Buy**

- [Gold](Gold): 300

- [Research](Research): 500

- [Codices](Codices): 30

**Cost to acquire**

- [Herbs](Herbs): 30

- [Water Gem](Water-Gem): 1

**Effect of using**

- [Stamina](Stamina): 50

**Flavor**: Magnum

