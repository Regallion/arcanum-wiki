**Description**: A multi-headed beast rears its heads from the water.

**Effects**

- [Unease](Unease): 2~3

- [Frustration](Frustration): 0.5

- [Water Mastery Exp](Water-Mastery): 1

