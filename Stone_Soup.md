**Stackable**: True

**Description**: A natural repast.

**Cost to acquire**

- [Earth Gem](Earth-Gem): 3

- [Herbs](Herbs): 10

**Effect of using**

- Dot: {'mod': {'earth.rate': '10%', 'duration': 600}}

