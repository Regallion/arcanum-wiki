**Description**: A stout tree with stumpy legs and a face inlaid within its trunk.

**Effects**

- [Befuddlement](Befuddlement): 1~2

- [Weariness](Weariness): 2~3

- [Herbalism Exp](Herbalism): 1

**Flavor**: It can't speak but likes it when you feed it oil.

