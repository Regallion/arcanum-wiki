**Description**: Piled higher than your head, an eclectic mass of skittering spiders bars the path.

**Effects**

- [Unease](Unease): 2~3

- [Madness](Madness): 2~4

- [Frustration](Frustration): 1~2

- [Life](Life): -1

