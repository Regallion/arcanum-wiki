**Description**: Its embers still glow after centuries

**Effects**

- [Befuddlement](Befuddlement): -1~4

- [Madness](Madness): 0~2

- [Unease](Unease): 1~3

- [Pyromancy Exp](Pyromancy): 3

**Result**

- [Pyromancy Max](Pyromancy): 0.0001

