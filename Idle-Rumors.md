**Description**: Every year rumors abound of lights and music from the nearby woods.

**Effects**

- [Befuddlement](Befuddlement): 1~2

- [Unease](Unease): 1~2

**Result**

**Symbol**: 🌼

