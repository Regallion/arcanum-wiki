**Tags**: T_Job

**Action Description**: Gather herbs for your master's potions and alchemy.

**Requirements**: [Herbalism](Herbalism)

**Cost to acquire**

- [Gold](Gold): 100

- [Research](Research): 100

- [Arcana](Arcana): 2

**Result**

- Event: [Promotion](evt_helper): True

- [Skill Points](Skill-Points): 1.5

- Player Exp: 10

**Modifiers**

- [Lore Max](Lore): 1

- [Gold Rate](Gold): 0.08

- [Herbalism Rate](Herbalism): 0.2

- [Herbalism Max](Herbalism): 3

- [Alchemy Rate](Alchemy): 0.1

- [Alchemy Max](Alchemy): 2

