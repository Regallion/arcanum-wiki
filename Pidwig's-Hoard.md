**Description**: Over the years Pidwig has amassed a sizeable collection of gems and artifacts.

**Effects**

- [Crafting Exp](Crafting): 1

- [World Lore Exp](World-Lore): 1

- [Trickery Exp](Trickery): 1

- [Frustration](Frustration): 0~2

- [Madness](Madness): 0~2

**Loot**: Prismaticgems

**Flavor**: Perhaps he could be induced to part with some...

