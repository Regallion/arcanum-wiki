```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Pyromancy ≥ 10"])
  t1 ---> t1r ---> pyromancer
  subgraph tier2 [Tier 2]
    pyromancer([Pyromancer])
  end
```
**Description**: Shaper of fire

**Tags**: T_Tier2

**Action Description**: Become a pyromancer.

**Requirements**: [Tier 1](tier1) > 0 and [Pyromancy](Pyromancy) ≥ 10

**Cost to acquire**

- [Research](Research): 1000

- [Arcana](Arcana): 15

- [Tomes](Tomes): 10

- [Fire Gem](Fire-Gem): 10

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 2](tier2): True

- [Pyromancy Max](Pyromancy): 2

- [Pyromancy Rate](Pyromancy): 10%

- [Dodge](Dodge): 10%

- [Mana Max](Mana): 2

