```mermaid
graph LR
  t3{Tier 3 Class}
  t3r(["Nature Lore ≥ 19,<br>(Witchcraft + Druid + Elementalist) > 0"])
  t3 ---> t3r ---> fey
  subgraph tier4 [Tier 4]
    fey([Fey])
  end
```
**Description**: A being of wild and natural magic

**Tags**: T_Tier4

**Requirements**: [Tier 3](tier3) > 0 and [Nature Lore](Nature-Lore) ≥ 19 and ([Witchcraft](Witchcraft) + [Druid](Druid) + [Elementalist](Elementalist)) > 0

**Cost to acquire**

- [Research](Research): 5000

- [Nature Gem](Nature-Gem): 30

- [Herbs](Herbs): 75

- [Arcana](Arcana): 15

- [Rune Stones](Rune-Stones): 5

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 4](tier4): True

- [Water Lore Max](Water-Lore): 2

- [Geomancy Max](Geomancy): 2

- [Wind Lore Max](Wind-Lore): 2

- [Nature Lore Max](Nature-Lore): 3

- [Nature Lore Rate](Nature-Lore): 1+10%

- [Animal Handling Max](Animal-Handling): 2

- [Animal Handling Rate](Animal-Handling): 1+10%

- [Herbalism Max](Herbalism): 2

- [Herbalism Rate](Herbalism): 1+10%

**Flavor**: 'Will you drink from the goblet?' she asked. 'The transformation cannot be undone.'

