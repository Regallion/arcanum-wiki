**Description**: The cauldron bubbles and glows with a odorous brew.

**Effects**

- [Unease](Unease): 0~2

- [Weariness](Weariness): 1~3

- [Befuddlement](Befuddlement): -1~2.5

- [Potions Exp](Potions): 1

- [Alchemy Exp](Alchemy): 1

- [Crafting Exp](Crafting): 1

**Result**

- [Potions Max](Potions): 0.01

**Loot**: Hestiabrew

