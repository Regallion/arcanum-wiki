**Level**: 14

**Description**: This tiny outpost in the center of thangmor outlasted even the pillars

**Requirements**: Event: [Vile](evil) ≤ 0 and [World Lore](World-Lore) ≥ 7 and [Explore Treffil Wood](Explore-Treffil-Wood) > 0

**Run Cost**

- [Stamina](Stamina): 0.33

**Result**

- [Arcana](Arcana): 0.1

- [Research](Research): 10

**Loot**

- [Scrolls](Scrolls): 1~4

**Encounters**

- [Iron Chest](Iron-Chest)

- [Affable Gnome](Affable-Gnome)

- [Advanced Primer](Advanced-Primer)

- [Hettie](Hettie)

- [Babbling Delki](Babbling-Delki)

- [Bright Vista](Bright-Vista)

