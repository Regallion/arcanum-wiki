**Description**: A patch of uneven ground with the soil exposed to the elements, likely to become barren. What kind of farmer misses something like this?

**Effects**

- [Unease](Unease): 1~3

- [Befuddlement](Befuddlement): 1~3

- [Geomancy Exp](Geomancy): 5

- [Nature Lore Exp](Nature-Lore): 5

**Result**

- [Herbs](Herbs): 10%

**Symbol**: None

