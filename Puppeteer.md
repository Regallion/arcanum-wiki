```mermaid
graph LR
  t0{Tier 0 Class}
  t0r(["Puppetry ≥ 5"])
  t0 ---> t0r ---> puppeteer
  subgraph tier1 [Tier 1]
    puppeteer([Puppeteer])
  end
```
**Description**: Assemble and control mechanical minions.

**Tags**: T_Tier1

**Requirements**: [Tier 0](tier0) > 0 and [Puppetry](Puppetry) ≥ 5

**Cost to acquire**

- [Research](Research): 700

- [Arcana](Arcana): 10

- [Gold](Gold): 500

**Modifiers**

- [Tier 1](tier1): True

- [Puppetry Max](Puppetry): 2

- Minions Keep: puppet

- [Animation Max](Animation): 1

- Allies Max: 5

- [Clockwork Home Space Max](Clockwork-Home): 50

**Flavor**: No matter what, the strings would not break.

