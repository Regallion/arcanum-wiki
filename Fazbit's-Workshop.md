**Level**: 9

**Length**: 50

**Symbol**: 📖

**Requirements**: ([Alchemy](Alchemy) + [Scrying](Scrying)) ≥ 17

**Description**: You could spend a century in Fazbit's lost workshop and not exhaust its creations.

**Loot**

- [Tomes](Tomes): 2~3

- [Gems](Gems): 3~5

- [Air Gem](Air-Gem): 50%

- [Fire Gem](Fire-Gem): 50%

- [Fazbit's Fixations](Fazbit's-Fixations): True

**Encounters**

- [Alchemical Equipment](Alchemical-Equipment)

- [Dusty Chest](Dusty-Chest)

- [Iron Chest](Iron-Chest)

- [Fazbit's Furnace](Fazbit's-Furnace)

- [Workbook](Workbook)

- [Alchemical Equipment](Alchemical-Equipment)

- [Master's Workbook](Master's-Workbook)

- [Fazbit's Helpers](Fazbit's-Helpers)

