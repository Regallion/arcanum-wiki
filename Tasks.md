This page is a default list of the items in this category.<br>
If it doesn't meet your needs, please let a developer (probably 'simple') know, and they'll get around to updating it.<br>

[Advise Notables](Advise-Notables)<br>
[Almagest](Almagest)<br>
[Animal Companion](Animal-Companion)<br>
[Annals of Arazor](Annals-of-Arazor)<br>
[Annals of Orrem](Annals-of-Orrem)<br>
[Arcane Bestiary](Arcane-Bestiary)<br>
[Arcane Thievery](Arcane-Thievery)<br>
[Assemble Machina](Assemble-Machina)<br>
[Assemble Puppet](Assemble-Puppet)<br>
[Attune (Elements)](Attune-(Elements))<br>
[Bag of Demons](Bag-of-Demons)<br>
[Bestiary](Bestiary)<br>
[Bind Codex](Bind-Codex)<br>
[Blood Siphon](Blood-Siphon)<br>
[Build Snomunculus](Build-Snomunculus)<br>
[Buy Scroll](Buy-Scroll)<br>
[Call Hammer](Call-Hammer)<br>
[Chant](Chant)<br>
[Clean Stables](Clean-Stables)<br>
[Coagulate Gem (Blood)](Coagulate-Gem-(Blood))<br>
[Codex Annihilus](Codex-Annihilus)<br>
[Commune](Commune)<br>
[Compile Tome](Compile-Tome)<br>
[Complete Soulflag](Complete-Soulflag)<br>
[Concoct Potions](Concoct-Potions)<br>
[Coporis Fabrica](Coporis-Fabrica)<br>
[Craft Gem](Craft-Gem)<br>
[Craft Rune (Air)](Craft-Rune-(Air))<br>
[Craft Rune (Earth)](Craft-Rune-(Earth))<br>
[Craft Rune (Fire)](Craft-Rune-(Fire))<br>
[Craft Rune (Spirit)](Craft-Rune-(Spirit))<br>
[Craft Rune (Water)](Craft-Rune-(Water))<br>
[Craft Rune](Craft-Rune)<br>
[Craft Schematic](Craft-Schematic)<br>
[Craft Soulflag](Craft-Soulflag)<br>
[Craft Timepiece](Craft-Timepiece)<br>
[Create Ritual Paper](Create-Ritual-Paper)<br>
[Create Still Room](Create-Still-Room)<br>
[Demonic Dictionary](Demonic-Dictionary)<br>
[Dig Out A Bigger Cave](Dig-Out-A-Bigger-Cave)<br>
[Dissect Cadaver](Dissect-Cadaver)<br>
[Do Chores](Do-Chores)<br>
[Dreamweaver](Dreamweaver)<br>
[Dwarven Dirges](Dwarven-Dirges)<br>
[Eat Children](Eat-Children)<br>
[Fazbit's Fixations](Fazbit's-Fixations)<br>
[Focus](Focus)<br>
[Funerary Rites](Funerary-Rites)<br>
[Garden](Garden)<br>
[Gather Herbs](Gather-Herbs)<br>
[Geas](Geas)<br>
[Ghost Catcher](Ghost-Catcher)<br>
[Greater Ritual of Cultivation](Greater-Ritual-of-Cultivation)<br>
[Greater Ritual of Cursing](Greater-Ritual-of-Cursing)<br>
[Greater Ritual of Energy](Greater-Ritual-of-Energy)<br>
[Greater Ritual of Healing](Greater-Ritual-of-Healing)<br>
[Greater Ritual of the Arcane](Greater-Ritual-of-the-Arcane)<br>
[Grind Bones](Grind-Bones)<br>
[Grind](Grind)<br>
[Hat Trick](Hat-Trick)<br>
[Hold Seance](Hold-Seance)<br>
[Imbue Gem (Air)](Imbue-Gem-(Air))<br>
[Imbue Gem (Arcane)](Imbue-Gem-(Arcane))<br>
[Imbue Gem (Fire)](Imbue-Gem-(Fire))<br>
[Imbue Gem (Light)](Imbue-Gem-(Light))<br>
[Imbue Gem (Nature)](Imbue-Gem-(Nature))<br>
[Imbue Gem (Shadow)](Imbue-Gem-(Shadow))<br>
[Imbue Gem (Spirit)](Imbue-Gem-(Spirit))<br>
[Imbue Gem (Water)](Imbue-Gem-(Water))<br>
[Imbue Stone (Earth)](Imbue-Stone-(Earth))<br>
[Imbue Timepiece](Imbue-Timepiece)<br>
[Improve Soulflag](Improve-Soulflag)<br>
[Indulge](Indulge)<br>
[Inscribe Ritual Paper](Inscribe-Ritual-Paper)<br>
[Lease Labor](Lease-Labor)<br>
[Lemurian Lexicon](Lemurian-Lexicon)<br>
[Lich](Lich)<br>
[Magic Heist](Magic-Heist)<br>
[Make A Beautiful Home](Make-A-Beautiful-Home)<br>
[Malleus Maleficarum](Malleus-Maleficarum)<br>
[Map Stars](Map-Stars)<br>
[Markhul's Codex](Markhul's-Codex)<br>
[Melt Snomunculus](Melt-Snomunculus)<br>
[Mine](Mine)<br>
[Murder](Murder)<br>
[Nixifer's Study](Nixifer's-Study)<br>
[Offer Services](Offer-Services)<br>
[Oppress](Oppress)<br>
[Pace](Pace)<br>
[Pet Familiar](Pet-Familiar)<br>
[Phylactory](Phylactory)<br>
[Pillage Graves](Pillage-Graves)<br>
[Plan Future](Plan-Future)<br>
[Prestidigitation](Prestidigitation)<br>
[Puppet Show](Puppet-Show)<br>
[Purchase Gem](Purchase-Gem)<br>
[Read Palms](Read-Palms)<br>
[Rest](Rest)<br>
[Ritual of Air](Ritual-of-Air)<br>
[Ritual of Cultivation](Ritual-of-Cultivation)<br>
[Ritual of Cursing](Ritual-of-Cursing)<br>
[Ritual of Dawn](Ritual-of-Dawn)<br>
[Ritual of Dreams](Ritual-of-Dreams)<br>
[Ritual of Dusk](Ritual-of-Dusk)<br>
[Ritual of Earth](Ritual-of-Earth)<br>
[Ritual of Fire](Ritual-of-Fire)<br>
[Ritual of Healing](Ritual-of-Healing)<br>
[Ritual of Light Binding](Ritual-of-Light-Binding)<br>
[Ritual of Mana](Ritual-of-Mana)<br>
[Ritual of Mandalas](Ritual-of-Mandalas)<br>
[Ritual of Penance](Ritual-of-Penance)<br>
[Ritual of Refreshment](Ritual-of-Refreshment)<br>
[Ritual of Time](Ritual-of-Time)<br>
[Ritual of Void](Ritual-of-Void)<br>
[Ritual of Water](Ritual-of-Water)<br>
[Ritual of the Arcane](Ritual-of-the-Arcane)<br>
[Run Errands](Run-Errands)<br>
[Scribe Scroll](Scribe-Scroll)<br>
[Scry](Scry)<br>
[Sell Gem](Sell-Gem)<br>
[Sell Herbs](Sell-Herbs)<br>
[Sell Scroll](Sell-Scroll)<br>
[Shape Automata](Shape-Automata)<br>
[Slumber](Slumber)<br>
[Spellbook](Spellbook)<br>
[Spin Gold](Spin-Gold)<br>
[Spring Rites](Spring-Rites)<br>
[Starwish](Starwish)<br>
[Study](Study)<br>
[Sublimate Lore](Sublimate-Lore)<br>
[Summon Familiar](Summon-Familiar)<br>
[Sylvan Syllabary](Sylvan-Syllabary)<br>
[Tend Animals](Tend-Animals)<br>
[Time Siphon](Time-Siphon)<br>
[Titan's Hammer](Titan's-Hammer)<br>
[Trap Soul](Trap-Soul)<br>
[Travel](Travel)<br>
[Treat Ailments](Treat-Ailments)<br>
[Unending Codex](Unending-Codex)<br>
[Unending Scroll](Unending-Scroll)<br>
[Unending Tome](Unending-Tome)<br>
[Vile Experiment](Vile-Experiment)<br>
[Warm Yourself](Warm-Yourself)<br>
[Warp Landscape](Warp-Landscape)<br>
[Weave Tapestry](Weave-Tapestry)<br>
[Witch's Sabbat](Witch's-Sabbat)