**Description**: One of the rarest of birds, they are not uncommon in this place.

**Effects**

- [Unease](Unease): 2~3

- [Frustration](Frustration): 0.5

- [Fire Mastery Exp](Fire-Mastery): 1

