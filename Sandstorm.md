**Description**: The blistering sands peel away your skin.

**Effects**

- [Madness](Madness): 0~1

- [Weariness](Weariness): 1~2

- hp: -2

- [Wind Lore Exp](Wind-Lore): 1

- [Geomancy Exp](Geomancy): 1

- [Earth](Earth): 0.5

- [Air](Air): 0.5

**Result**

