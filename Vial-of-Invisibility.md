**Description**: Appears to contain nothing at all.

**Requirements**: [Mystic Madcap](Mystic-Madcap) > 0 and [Potions](Potions) > 0

**Level**: 10

**Cost to Buy**

- [Gold](Gold): 2500

- [Research](Research): 3000

**Cost to acquire**

- [Herbs](Herbs): 50

- [Gold](Gold): 2500

- [Spirit Gem](Spirit-Gem): 25

- [Life](Life): 150

- [Spirit](Spirit): 15

- [Potion Base](Potion-Base): 1

**Effect of using**

- Dot: {'id': 'pot_invisibility_effect', 'name': 'invisibility', 'duration': 1800, 'mod': {'player.dodge': 25, 'player.speed': 15, 'heist.result.gems.min': 1, 'heist.result.gems.max': 1, 'heist.length': -25}}

