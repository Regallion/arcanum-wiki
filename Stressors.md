This page is a default list of the items in this category.<br>
If it doesn't meet your needs, please let a developer (probably 'simple') know, and they'll get around to updating it.<br>

[Befuddlement](Befuddlement)<br>
[Frustration](Frustration)<br>
[Madness](Madness)<br>
[Unease](Unease)<br>
[Weariness](Weariness)