**Action Description**: The final step on the twisting stairwell of apprenticeship.

**Requirements**: (Event: [Promotion](evt_helper) > 0) and [Research Max](Research) ≥ 125 and [Spellbook](Spellbook) > 0

**Log Entry**

- Name: Neophyte

- Desc: Your master has decided you are ready to take the next step on the path of wizardry.

**Flavor**: Why does Master charge so many fees?

**Cost to acquire**

- [Gold](Gold): 150

- [Research](Research): 175

- [Arcana](Arcana): 3

**Result**

- [Skill Points](Skill-Points): 1

- Player Exp: 10

**Modifiers**

- [Research Max](Research): 10

- [Arcana Rate](Arcana): 0.0001

- [Lore Max](Lore): 1

- [Lore Rate](Lore): 0.2

