**Description**: Each spring Aylesmeade holds its fabled fair in sight of the World Tree, Aesilwyr.

**Start Announcement**

**Encounters**

- [Maypole Dance](Maypole-Dance)

- [Aylesmeade Market](Aylesmeade-Market)

- [Tinker's Wagon](Tinker's-Wagon)

- [Idle Rumors](Idle-Rumors)

**Symbol**: 🌼

