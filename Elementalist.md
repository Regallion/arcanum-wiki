```mermaid
graph LR
  t0{Tier 0 Class}
  t0r(["Wind Lore > 2,<br>Pyromancy > 2,<br>Geomancy > 2,<br>Water Lore > 2"])
  t0 ---> t0r ---> elementalist
  subgraph tier1 [Tier 1]
    elementalist([Elementalist])
  end
```
**Description**: Master of elemental forces.

**Requirements**: [Tier 0](tier0) > 0 and [Wind Lore](Wind-Lore) > 2 and [Pyromancy](Pyromancy) > 2 and [Geomancy](Geomancy) > 2 and [Water Lore](Water-Lore) > 2

**Tags**: T_Tier1

**Cost to acquire**

- [Arcana](Arcana): 12

- [Research](Research): 1000

- [Codices](Codices): 20

- [Gems](Gems): 10

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 1](tier1): True

- [Research Max](Research): 5

- Elemental Max: 2

- Elemental Rate: 0.2

