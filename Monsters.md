|Name|Level|HP|Defense<br>Bonus|Regen|To Hit<br>Bonus|Speed<br>Bonus|Unique|Attack|
|:---|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| [Abholos](Abholos)<br>*A grey mass of pustulous malevolence. (Devourer in the Mist)*|30|None|None|None|None|None|No||
| [Adamant Golem](Adamant-Golem)<br>*Unbreakable*|20|450|130|None|15|None|No|Smash: 80~100 blunt damage|
| [Air Elemental](Air-Elemental)<br>*Wispy.*|8|13~22|48|None|13|None|No|Air Whip: 5~12 slash damage|
| [Alala](Alala)<br>*Entity of living sound.*|30|None|None|None|None|None|No||
| [Ancient Lich](Ancient-Lich)<br>*Not one lich in a hundred live to reach such heights.*|25|550|120|15|27|None|No|Death Touch: 80~120 shadow damage|
| [Ancient Vampire](Ancient-Vampire)|17|150|75|7|25|None|No|Bite: 30~50 shadow damage|
| [Angel](Angel)<br>*It has wings and a halo of light.*|14|170|70|None|10|None|No|Light Sword: 20~50 light damage|
| [Angry Bird](Angry-Bird)|2|2|1|None|4|3|No|Peck: 0~2 pierce damage|
| [Arcane Serpent](Arcane-Serpent)<br>*Fiercely loyal when summoned by magic.*|6|50|20|2|9|None|No||
| [Archon](Archon)<br>*It has wings and a halo of light.*|19|270|90|10|23|None|No|Light Sword: 70~120 light damage|
| [Autocaster](Autocaster)|30|500|100|10|20|None|No||
| [Autoslayer](Autoslayer)|30|700|50|None|15|None|No||
| [Avatar](Avatar)<br>*Incarnation of a god*|23|700|120|20|30|None|No|Holy Shine: 75~130 light damage|
| [Azathoth](Azathoth)<br>*The blind idiot god at the center of infinity.*|100|None|None|None|None|None|No||
| [Balrog](Balrog)<br>*A servant of Morgoth.*|18|200|80|None|17|None|No|Flame Whip: 90~150 fire damage|
| [Bandit Lord](Bandit-Lord)<br>*Wants to steal your stuff.*|6|23|17|None|8|None|No|Short Sword: 5~9 slash damage|
| [Bandit](Bandit)<br>*Wants to steal your stuff.*|5|12|15|None|6|None|No|Short Sword: 4~7 slash damage|
| [Barracuda](Barracuda)<br>*It's bitey.*|5|12|18|None|7|6|No|Bite: 3~5 pierce damage|
| [Basilisk](Basilisk)<br>*A large lizard with a petrifying bite.*|11|64|32|None|5|8|No||
| [Bat](Bat)|1|3|1|None|3|None|No|Bite: 1~2 pierce damage|
| [Bear](Bear)<br>*He's angry now!!!*|7|55|30|None|8|None|No|Claws: 5~14 slash damage|
| [Behemoth](Behemoth)|15|325|75|1|15|None|No|Trample: 40~70 blunt damage|
| [Beholder](Beholder)<br>*It seeeeees you.*|14|119|50|None|12|None|No||
| [Beserker](Beserker)|7|25|29|None|10|None|No|Claymore: 3~10 slash damage|
| [Big Stone](Big-Stone)|0|5|2|None|0|-20|No||
| [Blasphemer](Blasphemer)<br>*She screams black obscenities.*|7|75|40|None|7|None|No|Screech: 5~9 evil damage|
| [Bloated Corpse](Bloated-Corpse)|4|13|14|None|2|None|No|Bite: 2~3 physical damage|
| [Blood Golem](Blood-Golem)<br>*A creature of pure blood.*|12|90|45|None|None|None|No|Slam: 15~20 blunt damage|
| [Bodias the Grim](Bodias-the-Grim)|25|2000|150|None|50|None|Yes||
| [Bog Witch](Bog-Witch)<br>*Vicious witch in the waters of the Great Bog*|13|500|48|2|14|None|Yes|Black Blood: 1~4 poison damage for 20 seconds; Claws: 12~20 slash damage|
| [Brittelwort the Watcher](Brittelwort-the-Watcher)<br>*The silent Brittelwort watches in the Felkill mountains, punishing all transgressions.*|27|4000|100|None|34|None|Yes||
| [Bugbear](Bugbear)|6|43|25|None|2|None|No|Mace: 5~12 blunt damage|
| [Burlap Golem](Burlap-Golem)<br>*Woven golem filled with heavy sand*|14|400|None|None|None|None|No|Sand Punch: 20~30 earth damage|
| [Callodiper](Callodiper)<br>*Their globed hands glow with pale, unearthly light.*|5|40|None|None|None|None|No|Light Burst: 3~7 light damage|
| [Cave Troll](Cave-Troll)|9|90|35|2|None|None|No|Club: 15~27 blunt damage|
| [Clay Golem](Clay-Golem)<br>*I made you out of clay...*|8|65|33|None|-1|None|No|Smash: 2~7 blunt damage|
| [Cockatrice](Cockatrice)<br>*The stare of the cockatrice is death.*|15|300|None|None|None|8|No||
| [Cordyceps](Cordyceps)<br>*A writhing fungus chokes the air with spores.*|13|65|80|None|None|None|No||
| [Coyote](Coyote)|3|4|2|None|2|None|No|Claws: 2~3 slash damage|
| [Crocodile](Crocodile)|5|14|20|None|3|None|No|Bite: 4~7 pierce damage|
| [Cthulhu](Cthulhu)<br>*In his house at R'lyeh dead Cthulhu waits dreaming.*|25|1050|125|5|25|None|Yes|Madness: 70~125 psychic damage|
| [Cutpurse](Cutpurse)<br>*Wants to steal your stuff.*|4|7|7|None|4|6|No|Dagger: 2~5 pierce damage|
| [Cyclops](Cyclops)|14|170|65|None|10|None|No|Punch: 17~50 blunt damage|
| [Demon](Demon)|14|145|65|2|None|None|No|Darksword: 15~40 evil damage|
| [Desilla the Vicious](Desilla-the-Vicious)<br>*Among the Archlocks, desilla was more inclined to indulgence than conquest.*|20|1500|133|None|27|None|Yes||
| [Desilla's Concubine](Desilla's-Concubine)<br>*A devoted follower of the sorceress Desilla.*|11|90|50|None|20|None|No|Claws: 12~22 slash damage|
| [Dhrozenknight](Dhrozenknight)<br>*An immortal knight of Dhroz. Of fifty slain, one is knighted.*|26|1200|None|None|None|None|No|Nether Blade: 130~150 nether damage|
| [Dire Bat](Dire-Bat)<br>*That's one large bat.*|4|8|10|None|5|None|No|Bite: 3~7 pierce damage|
| [Dire Bear](Dire-Bear)<br>*He's angrier now!!!*|8|85|30|None|11|None|No|Claws: 17~27 slash damage|
| [Dire Crocodile](Dire-Crocodile)|8|30|32|None|5|None|No|Bite: 7~15 pierce damage|
| [Dire Rat](Dire-Rat)<br>*Red eyes... that's different.*|5|11|9|None|3|None|No|Bite: 1~6 pierce damage|
| [Dire Wolf](Dire-Wolf)|5|17|20|None|7|None|No|Bite: 3~6 pierce damage|
| [Doppelganger](Doppelganger)<br>*Such a good-looking fellow!*|6|37|32|None|8|None|No|Slam: 3~8 blunt damage|
| [Druid](Druid)<br>*Ardent of nature*|7|27|25|None|7|None|No|Spellstorm: 9~17 nature damage|
| [Drunken Boxer](Drunken-Boxer)<br>*This monk has had a bit too much to drink...*|6|15|29|None|7|None|No|Fists: 4~8 blunt damage|
| [Drunken Master](Drunken-Master)<br>*Master Wong Fei-Hung.*|10|53|44|None|10|None|No|Fists: 7~17 blunt damage|
| [Dullahan](Dullahan)<br>*It doesn't have a head. But you do.*|8|55|45|None|None|None|No|Vorpal Sword: 7~15 slash damage|
| [Dwarp](Dwarp)<br>*Its contorted purple body can hide in the smallest crevices.*|7|35|20|None|9|9|No|Strangle: 3~7 blunt damage|
| [Earth Elemental](Earth-Elemental)<br>*How do you fight dirt?*|8|58|30|None|5|None|No|Pound: 8~12 blunt damage|
| [Eel](Eel)<br>*So wriggly.*|3|3|12|None|3|None|No|Bite: 1~4 pierce damage|
| [Electric Eel](Electric-Eel)<br>*So wriggly.*|5|5|15|None|3|7|No|Zap: 2~4 pierce damage|
| [Eledin Strongbow](Eledin-Strongbow)<br>*When Tenwick told Eledin the Ettinmoors couldn't be crossed in safety, the ranger picked up his bow and headed east.*|12|140|24|None|None|None|Yes|Greatbow: 10~15 pierce damage|
| [Elf](Elf)<br>*So lithe. So graceful.*|4|8|10|None|1|None|No||
| [Ent](Ent)<br>*Out cruising for some ent-wives.*|13|138|50|3|2|None|No|Pound: 20~30 blunt damage|
| [Ettin](Ettin)<br>*Two heads are better than one.*|10|65|30|5|11|None|No|Spiked Club: 7~13 blunt damage|
| [Evil Priest](Evil-Priest)<br>*He worships evil.*|6|45|32|None|5|None|No|Harm: 5~8 evil damage|
| [Failed Experiment](Failed-Experiment)<br>*Apparently some sort of long lost arcane experiment, with a mind of its own. It doesn't look friendly.*|3|4|5|None|2|None|No|Psychic Blast: 1~2 mana damage|
| [Fetigern](Fetigern)<br>*The terror before the Wind Age*|30|9000|400|30|None|None|Yes|Bite: 100~200 poison damage|
| [Fire Elemental](Fire-Elemental)<br>*Too hot to get close to.*|8|40|55|None|10|None|No|Scorch: 5~15 fire damage|
| [Fire Golem](Fire-Golem)<br>*Crafted from only the finest phlogiston.*|11|100|53|None|8|None|No|Fire Punch: 15~23 fire damage|
| [Frixie](Frixie)<br>*A nasty fey of cold disposition.*|7|35|17|None|10|12|No|Frost Bolt: 4~17 water damage|
| [Frost Giant](Frost-Giant)|16|160|65|None|17|None|No|Punch: 40~60 cold damage|
| [Garden Gnome](Garden-Gnome)<br>*Standard variety garden gnome.*|1|3~6|4|None|2|None|No|Dot: wink|
| [Gem Cutter](Gem-Cutter)|18|400|None|None|None|None|No||
| [Ghast](Ghast)|7|42|26|2|3|None|No|Claws: 7~13 slash damage|
| [Ghatanothoa](Ghatanothoa)|30|None|None|None|None|None|No||
| [Ghost](Ghost)|6|20|30|None|7|None|No|Death Touch: 3~9 drain damage|
| [Ghoul](Ghoul)|5|21|18|1|2|None|No|Claws: 3~6 slash damage|
| [Giant Centipede](Giant-Centipede)<br>*Creepy crawly*|5|16|15|None|None|None|No|Sting: 5~8 pierce damage|
| [Giant Snake](Giant-Snake)|6|25|20|None|7|None|No|Constrict: 8~17 blunt damage|
| [Giant Spider](Giant-Spider)|5|9|5|None|5|None|No|Bite: 4~7 pierce damage|
| [Gildella the Lighthearted](Gildella-the-Lighthearted)<br>*The elf Gildella had roamed the Ettinmoors for centuries when Eledin first crossed her path. Their path has been the same since then.*|12|120|24|None|None|17|Yes|Greatbow: 10~15 pierce damage|
| [Gobchamp](Gobchamp)|6|14|16|None|7|None|No||
| [Gobchief](Gobchief)|5|11|13|None|6|None|No||
| [Gobking](Gobking)|7|18|23|None|8|None|No|Magic Missle: 5~9 mana damage|
| [Goblin Warrior](Goblin-Warrior)|3|8|7|None|5|None|No|Shortsword: 2~3 pierce damage|
| [Goblin](Goblin)|2|4|5|None|None|None|No|Dagger: 1~2 pierce damage|
| [Gobpriest](Gobpriest)|4|5|5|None|5|None|No|Club: 2~4 blunt damage|
| [Gol-Goroth the Forgotten](Gol-Goroth-the-Forgotten)|30|None|None|None|None|None|No||
| [Greater Basilisk](Greater-Basilisk)<br>*A massive lizard with a petrifying bite.*|17|300|None|None|5|None|No||
| [Greater Zombie](Greater-Zombie)<br>*It ate up all the smaller zombies.*|7|42|22|1|5|None|No|Slam: 7~12 blunt damage|
| [Green Dragon](Green-Dragon)<br>*Just your standard green dragon.*|14|145|55|None|10|None|No||
| [Gremlin](Gremlin)|1|3~5|2|0.25|None|None|No|Claws: 1 slash damage|
| [Grimstalk](Grimstalk)<br>*A wooden demon with fingers like blades.*|11|53|35|7|15|None|No|Claws: 12~28 slash damage|
| [Gryffon](Gryffon)<br>*Body of a lion. Face of a giant eagle.*|12|123|40|None|5|None|No|Beak: 21~27 pierce damage|
| [Guild Thug](Guild-Thug)<br>*The guild's policies on unauthorized pouch sales are strictly enforced.*|10|70|None|None|None|None|No|Sand Bag: 10~20 blunt damage|
| [Harpy](Harpy)<br>*She doesn't look harpy to see you.*|6|17|22|None|9|None|No|Claws: 4~7 slash damage|
| [Hastur](Hastur)|30|None|None|None|None|None|No||
| [Hawk](Hawk)|3|3|2|None|5|None|No|Claws: 2.2~4.2 slash damage|
| [High Elf](High-Elf)<br>*Other elves are as newborns to their elder kin.*|9|80|70|None|15|15|No||
| [Hill Giant](Hill-Giant)<br>*You've seen bigger.*|11|120|45|None|2|None|No|Club: 15~24 blunt damage|
| [Hobbit Slinger](Hobbit-Slinger)|5|7|5|None|8|None|No|Sling: 2~5 blunt damage|
| [Hobgoblin](Hobgoblin)|5|15|17|None|2|None|No|Dagger: 1~5 pierce damage|
| [Homunculus](Homunculus)<br>*Must have escaped its jar.*|1|10|5|None|1|None|No|Nasty Bite: 0~2 pierce damage|
| [Ice Elemental](Ice-Elemental)<br>*What's so scary about a bunch of icicles?*|7|72|35|None|9|None|No|Ice Shard: 8~15 cold damage|
| [Icewalker](Icewalker)<br>*They say those who die on the cold heights search the passes for mortals to join them.*|4|15|14|None|None|3|No|Frozen Claws: 3~6 cold damage|
| [Idh-Yaa the Worm](Idh-Yaa-the-Worm)<br>*First bride of Cthulhu*|30|None|None|None|None|None|No||
| [Imp](Imp)<br>*Little demonic bastards*|4|17|30|1|None|None|No||
| [Incubus](Incubus)<br>*You could bounce a coin off those abs.*|8|33|33|None|10|None|No|Seduction: 5~8 sexy damage|
| [Invisible Stalker](Invisible-Stalker)<br>*You don't see a thing.*|7|24|42|None|26|None|No|Slam: 5~11 blunt damage|
| [Iron Golem](Iron-Golem)<br>*It moves with heavy clonking steps.*|9|120|45|None|1|None|No|Smash: 9~14 blunt damage|
| [Jabberwocky](Jabberwocky)|25|732|42|None|32|None|Yes||
| [Jackal](Jackal)<br>*They're tricksy.*|1|11|8|None|2|None|No|Bite: 1~3 pierce damage|
| [Jazid](Jazid)<br>*Jazid's sole purpose is to kill the bearer of his compass.*|26|2500|None|None|30|30|Yes|Claws: 90~120 slash damage|
| [Jellyfish](Jellyfish)<br>*Don't touch.*|3|4|15|None|5|5|No|Sting: 1~3 poison damage|
| [Kakapo](Kakapo)|2|5|2|None|3|None|No|Beak: 1~4 pierce damage|
| [Karnivex the Bloodlock](Karnivex-the-Bloodlock)<br>*The weakest of the Archlocks built his reputation upon sheer slaughter.*|19|1200|130|None|20|None|Yes||
| [Kobold](Kobold)|2|2~5|7|None|None|None|No|Pointy Stick: 1~4 pierce damage|
| [Large Mouse](Large-Mouse)|1|4|2|None|2|None|No|Nibble: 0.2~0.7 pierce damage|
| [Large Rat](Large-Rat)|2|10|2|None|2|None|No|Bite: 0~2.5 pierce damage|
| [Large Snail](Large-Snail)|2|14|4|None|1|-1|No|Slime: 1~3 nature damage|
| [Large Spider](Large-Spider)<br>*You can't tell if she looks hungry.*|3|8|15|None|None|None|No|Poison: 1~2 venom damage for 3 seconds; Bite: 4~9 pierce damage|
| [Leech](Leech)<br>*Blood sucking leech*|4|6|None|None|None|None|No|Suck: 1~3 pierce damage|
| [Lesser Wyrm](Lesser-Wyrm)<br>*A cross between a worm and a dragon.*|11|80|42|None|5|None|No|Bite: 9~15 pierce damage|
| [Leviathan](Leviathan)<br>*A mountain moves in the depths of the sea.*|18|400|75|None|20|None|No|Crush: 80~110 blunt damage|
| [Lich Lord](Lich-Lord)<br>*It was a wizard, once.*|16|150|71|7|20|None|No|Death Touch: 30~40 drain damage|
| [Lich](Lich)<br>*It was a wizard, once.*|14|166|68|3|20|None|No|Death Touch: 26~36 drain damage|
| [Light Elemental](Light-Elemental)<br>*Too bright to see.*|8|30|65|None|13|None|No|Dot: blind; Light: 4~10 light damage|
| [Lightning Elemental](Lightning-Elemental)|10|28|63|None|14|None|No|Lightning: 13~25 light damage|
| [Lion](Lion)|6|20|18|None|None|8|No|Claws: 7~10 slash damage|
| [Magic Blade](Magic-Blade)<br>*It just floats there...*|6|45|35|None|9|None|No|Magic Sword: 4~10 slash damage|
| [Magic Mirror](Magic-Mirror)|7|47|40|None|7|None|No|Light: 8~15 light damage|
| [Magma Elemental](Magma-Elemental)<br>*A small mountain of magma.*|16|170|60|None|15|None|No|Lava Slam: 50~75 fire damage|
| [Manticore](Manticore)|12|130|48|None|10|None|No|Sting: 15~25 pierce damage|
| [Master Swordsman](Master-Swordsman)<br>*He's looking for a six-fingered man.*|8|35|35|None|14|None|No|Longsword: 7~13 slash damage|
| [Mature Red Dragon](Mature-Red-Dragon)|20|350|90|None|25|None|No|Fire Breath: 90~120 fire damage|
| [Mecha-Charger](Mecha-Charger)|20|500|200|None|20|None|No||
| [Mecha-Mender](Mecha-Mender)|20|500|100|None|20|None|No||
| [Mechanical Swordsman](Mechanical-Swordsman)<br>*Durable, but not very good at its job.*|5|22|28|None|6|None|No|Short Sword: 3~7 slash damage|
| [Medusa](Medusa)<br>*Kind of ugly.*|8|56|40|None|7|None|No|Stare: 17~25 magic damage|
| [Mind Slayer](Mind-Slayer)<br>*It licks your brain with its mind.*|11|49|44|None|15|None|No|Mind Flay: 11~19 psionic damage|
| [Minotaur](Minotaur)|6|25|18|None|None|None|No|Mallet: 7~11 blunt damage|
| [Mithril Golem](Mithril-Golem)<br>*Would be worth a fortune if you could even make a dent in it.*|17|200|100|None|8|None|No|Pound: 50~95 blunt damage|
| [Mummer](Mummer)<br>*A voiceless form with a bare and twisted skull.*|26|700|70|None|100|None|No||
| [Mutant Rat](Mutant-Rat)|3|4|3|None|3|None|No|Bite: 2~4 pierce damage|
| [Mythic Vampire](Mythic-Vampire)|19|175|110|10|25|None|No|Bite: 50~70 shadow damage|
| [Naga](Naga)<br>*From the waist up, it doesn't look that bad.*|8|38|48|None|8|None|No|Poison: 1~4 poison damage for 5 seconds; Scimitars: 5~8 slash damage|
| [Narz, the Black Spindle](Narz,-the-Black-Spindle)|27|1700|200|None|50|None|Yes||
| [Nature's Wrath](Nature's-Wrath)|22|400|120|15|25|None|No|Lifestorm: 100~125 nature damage|
| [Netherwraith](Netherwraith)<br>*A spirit cloaked in noxious nether.*|27|1000|40|None|100|None|No|Nether Touch: 50~80 spirit damage|
| [Newt](Newt)|0.2|1~2|1|None|None|None|No|Newting: 1~2 squiggly damage|
| [Nyarlathotep](Nyarlathotep)|30|None|None|None|None|None|No||
| [Ogre Chief](Ogre-Chief)|10|70|23|None|12|None|No||
| [Ogre Warrior](Ogre-Warrior)|8|70|23|None|12|None|No|Spiked Club: 16~34 blunt damage|
| [Ogre](Ogre)|7|45|20|None|10|None|No|Club: 8~17 blunt damage|
| [Orc Champion](Orc-Champion)<br>*A champion among orcs.*|8|100|40|None|10|None|No|Broadsword: 15~20 slash damage|
| [Orc Chieftain](Orc-Chieftain)<br>*The strongest orc in the whole tribe.*|7|80|30|None|8|None|No|Broadsword: 12~17 slash damage|
| [Orc Shaman](Orc-Shaman)|6|14|5|None|5|None|No||
| [Orc Warrior](Orc-Warrior)|5|17|15|None|6|None|No|Orc Hammer: 2~7 blunt damage|
| [Orc](Orc)<br>*For the Horde!*|4|14|8|None|3|None|No|Spear: 3~6 pierce damage|
| [Ouroboros](Ouroboros)<br>*The serpent that grips the earth in its scales*|34|130000|200|None|30|None|Yes|Crush: 200~350 blunt damage|
| [Palus the Sinner](Palus-the-Sinner)<br>*Palus was a mere dabbler before unearthing the remains of a Dhrozen knight. Now he leads the Barrow priests.*|14|80|75|None|10|None|Yes|Harm: 10~20 evil damage|
| [Pegasus](Pegasus)<br>*It's a unicorn with wings and no horn.*|8|54|27|None|3|None|No|Trample: 9~15 blunt damage|
| [Phoenix](Phoenix)<br>*It's more fire than bird.*|7|47|40|None|None|None|No|Flame Wing: 11~20 fire damage|
| [Piranha](Piranha)<br>*It's bitey.*|3|4|15|None|5|7|No|Bite: 2~4 pierce damage|
| [Poltergeist](Poltergeist)<br>*Only the objects flying at your head alert you to its presence.*|8|44|40|None|9|None|No|Chill Touch: 3~10 drain damage|
| [Pouch Merchant](Pouch-Merchant)<br>*The guild hires merchants to drain gold from the pouches of others.*|14|300|None|None|None|20|No|Transfer Gold: None None damage|
| [Puppet Aggressor](Puppet-Aggressor)|10|75|7|None|10|None|No|Stab: 20~30 pierce damage|
| [Puppet Bulwark](Puppet-Bulwark)|10|300|20|None|7|None|No||
| [Quasit](Quasit)<br>*Like a demonic humanoid vulture.*|7|22|30|1|8|None|No|Claws: 4~9 slash damage|
| [Ranger](Ranger)<br>*Rangers aren't very good with people.*|5|20|19|None|None|None|No|Longbow: 3~6 pierce damage|
| [Rat King](Rat-King)|3|8|4|0.5|2|None|Yes|Kingly Bite: 1~3 pierce damage|
| [Raven](Raven)|2|3|3|None|4|4|No|Peck: 1~2 pierce damage|
| [Reaper](Reaper)<br>*a raw manifestation of death is fleeting, but terrible beyond measure.*|30|6666|200|-100|50|None|No||
| [Red Dragon](Red-Dragon)|15|137|60|None|10|None|No|Fire Breath: 50~70 fire damage|
| [Roc](Roc)<br>*An avian terror of mythic repute.*|8|41|32|None|None|None|No|Beak: 11~17 pierce damage|
| [Rumpelstiltskin](Rumpelstiltskin)<br>*A strange imp that spins magic threads.*|23|900|None|None|None|None|Yes|Wooden Cane: 40~100 blunt damage|
| [Sack Guild Enforcer](Sack-Guild-Enforcer)|17|None|None|None|None|None|No||
| [Sackmaker Apprentice](Sackmaker-Apprentice)<br>*They have no respect for wizard apprentices.*|2|50|None|None|None|None|No|Sewing Needle: 5~10 None damage|
| [Sackmaker Journeyman](Sackmaker-Journeyman)<br>*Travels to towns, selling the finest quality bags and pouches.*|16|75|None|None|None|None|No|Punch: 40~50 None damage|
| [Sackmaker's Lodge Master](Sackmaker's-Lodge-Master)|20|900|None|None|None|None|No|Adamant Needle: 40~75 pierce damage|
| [Sackweaver](Sackweaver)<br>*Uses flows of magic to weave bags for sale.*|19|400|None|None|None|None|No||
| [Salamander](Salamander)|6|34|23|None|5|None|No|Burning: 1~3 fire damage for 3 seconds; Flame Claws: 3~9 fire damage|
| [Sand Mite](Sand-Mite)<br>*Oversized parasites of the dunes*|5|16|None|None|7|7|No|Burrow: 7~14 pierce damage|
| [Sand Wyrm](Sand-Wyrm)<br>*Terror of the desert wastes*|14|130|50|None|15|None|No|Crush: 12~24 blunt damage|
| [Scarab](Scarab)<br>*Massive shimmering beetle with a projecting horn*|4|5|7|None|None|None|No|Charge: 3~5 blunt damage|
| [Scuttling Crab](Scuttling-Crab)<br>*A massive scuttling crab*|5|12|25|None|3|None|No|Claws: 5~8 pierce damage|
| [Shade](Shade)|9|20|38|None|3|None|No|Cold Touch: 10~20 cold damage|
| [Shark](Shark)<br>*It's bitey.*|9|58|18|None|11|11|No|Bite: 10~15 pierce damage|
| [Shoggoth](Shoggoth)|27|None|None|None|None|None|No||
| [Shrieking Eel](Shrieking-Eel)<br>*Oversized eel with bladed teeth and a terrifying screach.*|7|37|20|None|20|None|No|Scream: 7~11 sonic damage|
| [Skeletal Rat](Skeletal-Rat)<br>*Even the dead rats are a problem...*|1|3|1|None|3|None|No|Bite: 0~2 pierce damage|
| [Skeleton Lord](Skeleton-Lord)<br>*Are those rubies in its eyes?*|13|53|34|None|15|None|No|Longsword: 36~45 slash damage|
| [Skeleton](Skeleton)<br>*It appears to be missing all its fleshy bits.*|3|4|3|None|5|None|No|Slash: 1~4 slash damage|
| [Slime](Slime)|4|10|8|None|None|None|No|Squelch: 2~4 acid damage|
| [Small Chest](Small-Chest)|1|5|3|None|None|-20|No||
| [Small Snake](Small-Snake)|2|5|2|None|2|None|No|Venom: 0.2~0.5 poison damage for 3 seconds|
| [Snow Leopard](Snow-Leopard)|5|14|18|None|None|9|No|Claws: 4~7 cold damage|
| [Spider Queen](Spider-Queen)|8|70|5|None|6|None|No|Venom: 3~5 poison damage for 30 seconds|
| [Stingy Bee](Stingy-Bee)|1|2|2|None|4|3|No|Sting: 0.5~1.5 poison damage|
| [Stone Golem](Stone-Golem)<br>*The head stone's connected to the... spine stone.*|9|88|40|None|7|None|No|Smash: 9~13 blunt damage|
| [Stranglevine](Stranglevine)<br>*Writhing tendrils seek to ensnare fresh prey.*|17|165|None|None|None|None|No|Dot: strangle; Strangle: 10~20 blunt damage|
| [Strativax Enraged](Strativax-Enraged)<br>*Strativax now furious at your impetuous intrusion.*|27|20000|200|None|50|None|Yes||
| [Strativax Slumbering](Strativax-Slumbering)<br>*He does not appear to notice your presence.*|24|10000|500|None|None|None|Yes||
| [Strativax Waking](Strativax-Waking)<br>*The dragon's eyes begin to open.*|27|50000|300|None|50|None|Yes||
| [Succubus](Succubus)|9|69|35|None|20|None|No|Seduction: 9~13 sexy damage|
| [Thangmor Ranger](Thangmor-Ranger)<br>*All rangers travel far. Some farther than others.*|8|70|25|None|None|None|No|Grandbow: 10~20 pierce damage|
| [The Gorborung](The-Gorborung)<br>*The gargantuan entity in the depths of Mt. Gorbu never seen by mortal eyes.*|25|5000|100|None|34|None|Yes|Gorboring: 50~75 sonic damage|
| [Thrall](Thrall)<br>*One isn't too bad. 100 can be a problem.*|4|14|5|None|2|None|No|Axe: 5~9 slash damage|
| [Troll](Troll)|11|100|50|5|3|None|No|Claws: 17~23 slash damage|
| [Trow](Trow)<br>*A towering giant of stone*|12|225|50|None|10|None|No|Kick: 14~20 blunt damage|
| [Twik-Man](Twik-Man)<br>*Tiny vicious humanoid riding a dragonfly.*|1|3~5|1|None|2|1|No|Spear: 0.5~1.5 slash damage|
| [Undead Crow](Undead-Crow)<br>*Little more than tatters and bones*|0.5|4|2|None|None|None|No|Pecking: 0.2~1 necrotic damage|
| [Undead Horror](Undead-Horror)<br>*You need to stop looking at it before you go insane.*|8|45|50|None|10|None|No|Chill Touch: 6~16 drain damage|
| [Unicorn](Unicorn)<br>*Its horn is full of magic.*|9|77|35|None|14|None|No|Horn: 7~14 pierce damage|
| [Valkyrie](Valkyrie)<br>*She'll take you to Valhalla.*|12|155|54|None|15|None|No|Claymore: 10~17 slash damage|
| [Vampire Bat](Vampire-Bat)<br>*It vants to zuck your blood.*|4|12|25|None|2|None|No|Bite: 5~7 pierce damage|
| [Vampire Lord](Vampire-Lord)<br>*Master Vampires are its lackeys.*|15|120|70|5|15|None|No|Bite: 20~40 drain damage|
| [Vampire Master](Vampire-Master)<br>*Worked his way up the vampire ranks, and has his very own brood now.*|11|87|42|2|10|None|No|Bite: 10~25 drain damage|
| [Vampire](Vampire)<br>*Standard blood sucking variety.*|9|66|30|None|18|None|No|Bite: 9~18 drain damage|
| [Vicious Wyvern](Vicious-Wyvern)|20|332|42|None|32|None|No||
| [Vrrek](Vrrek)<br>*An intelligent scavenger with the features of a vulture.*|5|25|None|None|9|None|No|Talons: 7~12 slash damage|
| [Vulture](Vulture)|3|None|1|None|7|5|No|Peck: 4~8 pierce damage|
| [Warg](Warg)|6|15|4|None|7|None|No|Bite: 7~9 pierce damage|
| [Warthog](Warthog)|3|7|4|None|2|None|No|Bite: 1~4 pierce damage|
| [Werewolf](Werewolf)|6|22|25|1.5|7|None|No|Claws: 3~6 slash damage|
| [Wicked Hierophant](Wicked-Hierophant)<br>*Leader of wicked congregations.*|9|100|50|None|9|None|No|Harm: 10~20 evil damage|
| [Wight](Wight)|6|18|22|1|5|None|No|Claws: 3~9 slash damage|
| [Will O' Wisp](Will-O'-Wisp)<br>*A gleaming orb of light*|3|4|3|None|None|4|No|Flicker: 1~2 light damage|
| [Wisp](Wisp)<br>*A ghostly wisp of air.*|1|1~2|1|None|None|None|No|Dot: shivering; Shiver: 0.2~1 spirit damage|
| [Wolf](Wolf)|4|13|12|None|None|None|No|Bite: 2~5 pierce damage|
| [Wombat](Wombat)<br>*Just wombatting around.*|2|7~10|3|None|None|None|No|Wombatting: 1~4 blunt damage|
| [Wooden Golem](Wooden-Golem)<br>*A golem crafted on a budget.*|7|32|28|None|5|None|No|Punch: 2~11 blunt damage|
| [Wraith](Wraith)<br>*A ghostly black form.*|9|30|40|None|10|None|No|Death Touch: 5~8 drain damage|
| [Wyvern](Wyvern)<br>*Not a dragon. But it will kill you like one.*|13|132|42|None|12|None|No|Claws: 20~30 slash damage|
| [Yeti](Yeti)|10|60|25|None|11|None|No|Claws: 8~12 slash damage|
| [Yog-Sothoth](Yog-Sothoth)|90|None|None|None|None|None|No||
| [Ythogtha](Ythogtha)|30|None|None|None|None|None|No||
| [Zombie](Zombie)<br>*Slow and rotting corpse.*|3|4|4|None|-1|None|No|Smash: 3~8 blunt damage|
| [Zoth-Ommog](Zoth-Ommog)<br>*Third son of cthulhu with a razor fanged, reptilian head.*|30|None|None|None|None|None|No||
|<span style="font-size:2em;">⚔️</span> [Armored Dummy](Armored-Dummy)<br>*Any normal attack leaves no mark*|1|10|20|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Dodging Dummy](Dodging-Dummy)<br>*Any normal means of attack is useless*|0|1|None|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Dummy1](Dummy1)<br>*Even fists would do*|0|10|None|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Dummy](Dummy)<br>*Can't even hit back*|1|10|3|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Hard Dummy](Hard-Dummy)<br>*Fists don't work against this guy*|0|5|None|None|None|-10|No||
|<span style="font-size:2em;">⚔️</span> [Instructor](Instructor)<br>*Is he toying with you?*|1|50|None|None|None|3|No|Mockery: 0~2 slashing damage|
|<span style="font-size:2em;">⚔️</span> [Spellabsorber](Spellabsorber)<br>*Use a weapon for this guy*|0|5|None|None|None|-10|No||
|<span style="font-size:2em;">❄️</span> [Avalanche](Avalanche)<br>*The mass of snow seems to aim directly for you as it tumbles down the mountain*|2|100|-10|None|None|None|No|Chilling: None ice damage|
|<span style="font-size:2em;">❄️</span> [Blizzard Guardian](Blizzard-Guardian)<br>*Winter holds its own secrets, and summons guardians to watch them.*|9|50|55|None|12|None|No|Freeze: 10~20 ice damage|
|<span style="font-size:2em;">❄️</span> [Frost Elemental](Frost-Elemental)<br>*A spiral of deadly icicles*|1|20|4|None|None|None|No|Icicles: 5~8 ice damage|
|<span style="font-size:2em;">❄️</span> [Frost Golem](Frost-Golem)<br>*A hulking Golem sculpted in ice*|1|50|8|2|None|10|No|Snowball: 3~5 ice damage|
|<span style="font-size:2em;">❄️</span> [Ice Troll](Ice-Troll)<br>*A huge dumb looking creature with a club of ice*|3|40|10|None|None|None|No|Smash: 10~15 blunt damage|
|<span style="font-size:2em;">❄️</span> [Icy Winds](Icy-Winds)<br>*A chilling wind*|2|30|5|None|None|None|No|Icy Winds: 2~3 ice damage|
|<span style="font-size:2em;">❄️</span> [Yeti](Yeti)<br>*A mythical creature, found only in legends*|5|200|20|None|None|1|No|Icybreath: 35~55 ice damage|
