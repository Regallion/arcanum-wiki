**Description**: The mage does not appear inclined to speak.

**Effects**

- [Befuddlement](Befuddlement): 1~5

- [Frustration](Frustration): 1~5

- [Weariness](Weariness): 2~5

- [Concentration Exp](Concentration): 1

- [Composure Exp](Composure): 1

**Symbol**: ⭐

