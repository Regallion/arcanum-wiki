```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Wind Lore ≥ 10"])
  t1 ---> t1r ---> windmage
  subgraph tier2 [Tier 2]
    windmage([Wind Mage])
  end
```
**Description**: To walk on the wind

**Tags**: T_Tier2

**Action Description**: Become a wind mage.

**Requirements**: [Tier 1](tier1) > 0 and [Wind Lore](Wind-Lore) ≥ 10

**Cost to acquire**

- [Research](Research): 1000

- [Arcana](Arcana): 15

- [Tomes](Tomes): 10

- [Air Gem](Air-Gem): 10

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 2](tier2): True

- [Wind Lore Max](Wind-Lore): 2

- [Wind Lore Rate](Wind-Lore): 10%

- [Dodge](Dodge): 10%

- [Mana Max](Mana): 2

**Flavor**: There's a storm coming

