**Description**: Magical gems will focus the power of the tree of rites.

**Level**: 2

**Effects**

- [Unease](Unease): 0~2

- [Frost](Frost): 0~1

**Symbol**: ❄️

