```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["Wind Lore > 7,<br>Pyromancy > 7,<br>Water Lore > 7,<br>Geomancy > 7"])
  t2 ---> t2r ---> highelemental
  subgraph tier3 [Tier 3]
    highelemental([Highelemental])
  end
```
**Tags**: T_Tier3

**Description**: Lord of the elements

**Requirements**: [Tier 2](tier2) > 0 and [Wind Lore](Wind-Lore) > 7 and [Pyromancy](Pyromancy) > 7 and [Water Lore](Water-Lore) > 7 and [Geomancy](Geomancy) > 7

**Cost to acquire**

- [Arcana](Arcana): 15

- [Tomes](Tomes): 10

- [Air Gem](Air-Gem): 20

- [Fire Gem](Fire-Gem): 20

- [Earth Gem](Earth-Gem): 20

- [Water Gem](Water-Gem): 20

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 3](tier3): True

- [Wind Lore Max](Wind-Lore): 2

- [Pyromancy Max](Pyromancy): 2

- [Geomancy Max](Geomancy): 2

- [Water Lore Max](Water-Lore): 2

- [Research Max](Research): 25

- Elemental Max: 2

- Elemental Rate: 0.2

