**Value**: 0

**Only Integer Values**: True

**Statistics**: True

**Locked**: False

**Hidden**: True

**Modifiers**

- Spelllist Max: 1

- [Stamina Max](Stamina): 1

- tohit: 1

- [Life Max](Life): 1

- Allies Max: :5

- [Speed](Speed): :4

**Result**

- Hallpoints: 0.1

- [Skill Points](Skill-Points): :3

