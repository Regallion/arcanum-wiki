**Description**: Unusually, prepared by baking special herbs into a brownie.

**Requirements**: [Potions](Potions) ≥ 1

**Cost to Buy**

- [Gold](Gold): 25

- [Research](Research): 50

**Cost to acquire**

- [Herbs](Herbs): 10

**Effect of using**

- Dot: {'duration': 120, 'effect': {'breath': 3, 'water': 0.1}}

