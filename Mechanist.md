```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["Mechanical Magic ≥ 5"])
  t2 ---> t2r ---> mechanist
  subgraph tier3 [Tier 3]
    mechanist([Mechanist])
  end
```
**Description**: Steel and magic

**Tags**: T_Tier3

**Requirements**: [Tier 2](tier2) > 0 and [Mechanical Magic](Mechanical-Magic) ≥ 5 and [Tier 3](tier3) = 0

**Cost to acquire**

- [Research](Research): 3000

- [Arcana](Arcana): 25

- [Blood Gem](Blood-Gem): 20

- [Air Gem](Air-Gem): 20

- [Tomes](Tomes): 10

**Modifiers**

- [Tier 3](tier3): True

- [Puppetry Max](Puppetry): 1

- Minions Keep: machina

- [Animation Max](Animation): 2

- [Mechanical Magic Max](Mechanical-Magic): 2

- Allies Max: 5

- [Clockwork Home Space Max](Clockwork-Home): 100

**Flavor**: What is a man? An inefficient form of labor.

