**Level**: 1

**Length**: 15

**Symbol**: 📖

**Requirements**: Event: [Promotion](evt_helper)

**Description**: A good apprentice spends all their free time in the library. After chores, of course.

**Run Cost**

- [Stamina](Stamina): 0.2

**Result**

- [Arcana](Arcana): 0.1

- [Research](Research): 10

**Loot**

- [Scrolls](Scrolls): 1~4

**Encounters**

- [Bookworm](Bookworm)

- [Occult Tapestry](Occult-Tapestry)

- [Magical Primer](Magical-Primer)

- [Dusty Chest](Dusty-Chest)

- [Magical Primer](Magical-Primer)

- [Workbook](Workbook)

