```mermaid
graph LR
  t5{Tier 5 Class}
  t5r(["Astronomy ≥ 15,<br>Planes Lore ≥ 15"])
  t5 ---> t5r ---> astralseer
  subgraph tier6 [Tier 6]
    astralseer([Astral Seer])
  end
```
**Description**: The stars are an open book, to those who can read.

**Tags**: T_Tier6

**Requirements**: [Tier 5](tier5) > 0 and [Astronomy](Astronomy) ≥ 15 and [Planes Lore](Planes-Lore) ≥ 15

**Cost to acquire**

- [Research](Research): 17000

- [Arcana](Arcana): 75

- [Tomes](Tomes): 50

- [Star Shard](Star-Shard): 3

- [Starcharts](Starcharts): 50

**Modifiers**

- [Tier 6](tier6): True

- [Spellcraft Max](Spellcraft): 1

- [Planes Lore Max](Planes-Lore): 2

- [Planes Lore Rate](Planes-Lore): 2%

- bonuses: 2

- [Astronomy Max](Astronomy): 2

- [Astronomy Rate](Astronomy): 1%

- [Scrying Max](Scrying): 1

- [Scrying Rate](Scrying): 2%

- [Star Shard Rate](Star-Shard): 5%

- [Mysticism Max](Mysticism): 2

- [Mysticism Rate](Mysticism): 0.5

- [Crafting Max](Crafting): 2

