**Description**: A batch of ice cream, for children

**Requirements**: [Potions](Potions) ≥ 1 and ([Witchcraft](Witchcraft) + [Thanophage](Thanophage)) > 0

**Cost to Buy**

- [Gold](Gold): 250

- [Ice](Ice): 5

**Cost to acquire**

- [Herbs](Herbs): 15

- [Ice](Ice): 5

**Gains from selling**: {'gold': 100}

**Effect of using**

- Dot: {'id': 'icecreamlure', 'name': 'ice cream lure', 'duration': 600, 'effect': {'eatchildren.effect.stress': -0.2, 'eatchildren.effect.prismatic': 0.1, 'eatchildren.effect.stamina': 0.3, 'eatchildren.effect.hp': 1}}

