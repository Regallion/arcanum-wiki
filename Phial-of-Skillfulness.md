**Description**: Converts passive information into short-term benefit

**Requirements**: [Potions](Potions) ≥ 9

**Level**: 5

**Cost to Buy**

- [Gold](Gold): 2500

- [Research](Research): 5000

- [Skill Points](Skill-Points): 100

**Cost to acquire**

- [Skill Points](Skill-Points): 50

- [Potion Base](Potion-Base): 1

**Effect of using**

- [Research](Research): 2000

- Manas: 250

- [Stamina](Stamina): 250

