**Level**: 2

**Length**: 20

**Requirements**: [Musty Library](Musty-Library)

**Description**: Tradition tells of a spring with enchanted waters. Investigations might prove revealing.

**Run Cost**

- [Stamina](Stamina): 0.2

**Result**

- [Arcana](Arcana): 0.1

- [Research](Research): 10

**Loot**

- [Herbs](Herbs): 2~5

- [Lillit's Cord](Lillit's-Cord): 5%

**Encounters**

- [Mana Tree](Mana-Tree)

- [Heathered Copse](Heathered-Copse)

- [Mystic Waters](Mystic-Waters)

- [Mana Tree](Mana-Tree)

- [Hidden Cache](Hidden-Cache)

- [Starry Night](Starry-Night)

