**Description**: In the woods at night, goblins hold a market of their own.

**Effects**

- [Gold](Gold): -10

- [Frustration](Frustration): 1~3

- [Madness](Madness): 3~4

**Result**

- [Herbs](Herbs): 4~10

- [Gems](Gems): 5~10

- [Arcane Gem](Arcane-Gem): 40%

**Symbol**: 🌼

