**Description**: Cures most common ailments and fortifies the body.

**Requirements**: [Potions](Potions) ≥ 8

**Level**: 10

**Cost to Buy**

- [Gold](Gold): 2500

- [Research](Research): 3000

**Cost to acquire**

- [Herbs](Herbs): 50

- [Nature](Nature): 10

- [Nature Gem](Nature-Gem): 15

- [Gems](Gems): 5

- [Potion Base](Potion-Base): 1

**Effect of using**

- Action: {'cure': ['paralysis', 'enrage', 'charmed', 'fear', 'silence', 'sick'], 'targets': 'self'}

- Dot: {'duration': 300, 'effect': {'hp': 1}, 'mod': {'hp.max': 100}}

