**Description**: Ominous shapes drift and flow beneath the murky waters.

**Effects**

- [Befuddlement](Befuddlement): 0~2

- [Unease](Unease): 2~3

- [Madness](Madness): 1~2

- [Water Lore Exp](Water-Lore): 2

- [Shadowlore Exp](Shadowlore): 1

- [Arcana Max](Arcana): 0.05

