**Level**: 2

**Description**: You rest in the shade amid flowers of heather.

**Effects**

- [Frustration](Frustration): -2~0

- [Stamina](Stamina): 1

- [Mana](Mana): 1

