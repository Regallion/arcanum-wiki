**Description**: The old tree has lain dormant for centuries. Now it draws life back into its branches.

**Level**: 2

**Run Cost**

- [Life](Life): 2

**Effects**

- [Weariness](Weariness): 1~3

- [Frost](Frost): 0~1

**Symbol**: ❄️

