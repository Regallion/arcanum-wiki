**Description**: The mirror shows scenes from long ago.

**Effects**

- [World Lore Exp](World-Lore): 1

- [Befuddlement](Befuddlement): 1~2

- [Unease](Unease): 1~2

