**Description**: One of a dozen mossy portals at the edges of the menagerie. Their use is unknown.

**Effects**

- [Unease](Unease): 3~4

- [Befuddlement](Befuddlement): 3~4

**Result**

- [Arcana](Arcana): 0.01

