```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["Vile > 0,<br>Life ≥ 100,<br>Level ≥ 17"])
  t2 ---> t2r ---> dreadlord
  subgraph tier3 [Tier 3]
    dreadlord([Dreadlord])
  end
```
**Description**: Harbinger of destruction

**Tags**: T_Tier3

**Requirements**: [Tier 2](tier2) > 0 and Event: [Vile](evil) > 0 and [Life](Life) ≥ 100 and [Level](Level) ≥ 17 and [Tier 3](tier3) = 0

**Cost to acquire**

- [Research](Research): 1500

- [Bones](Bones): 100

- [Bodies](Bodies): 50

- [Arcana](Arcana): 20

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 3](tier3): True

- tohit: 10%

- [Life Max](Life): 10%

- [Bladelore Max](Bladelore): 1

- [Shadowlore Max](Shadowlore): 2

- [Shadowlore Rate](Shadowlore): 1%

- [Arcane Body Max](Arcane-Body): 1

- [Greater Arcane Body Max](Greater-Arcane-Body): 1

