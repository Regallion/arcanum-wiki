**Level**: 5

**Length**: 30

**Description**: The small and tranquil island is inhabited by an old hermit named thyffr

**Run Cost**

- [Stamina](Stamina): 0.22

**Result**

- [Arcana](Arcana): 0.1

- [Research](Research): 10

**Loot**

- [Scrolls](Scrolls): 1~4

- [Herbs](Herbs): 2~5

**Encounters**

- [Dusty Chest](Dusty-Chest)

- [Bright Vista](Bright-Vista)

- [Talking To Thyffr](Talking-To-Thyffr)

- [Starry Night](Starry-Night)

- [Mystic Waters](Mystic-Waters)

