```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Enchanting ≥ 6,<br>Charms ≥ 3"])
  t1 ---> t1r ---> enchanter
  subgraph tier2 [Tier 2]
    enchanter([Enchanter])
  end
```
**Description**: A master of charms and enchantments.

**Requirements**: [Tier 1](tier1) > 0 and [Enchanting](Enchanting) ≥ 6 and [Charms](Charms) ≥ 3

**Tags**: T_Tier2

**Cost to acquire**

- [Research](Research): 1200

- [Arcane Gem](Arcane-Gem): 10

- [Tomes](Tomes): 5

- [Arcana](Arcana): 20

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 2](tier2): True

- [Research Max](Research): 15

- [Enchanting Max](Enchanting): 3

- [Arcana Max](Arcana): 2

- [Nature Lore Max](Nature-Lore): 2

- [Mana Max](Mana): 3

- [Wind Lore Max](Wind-Lore): 2

- [Water Lore Max](Water-Lore): 2

