**Description**: What was theirs is now yours.

**Level**: 25

**Requirements**: [Potions](Potions) ≥ 999 and Event: [Vile](evil) > 0

**Cost to Buy**

- [Gold](Gold): 5000

- [Tomes](Tomes): 50

- [Research](Research): 5000

- [Souls](Souls): 150

**Cost to acquire**

- [Herbs](Herbs): 50

- [Research](Research): 15000

- [Bodies](Bodies): 100

- [Souls](Souls): 100

- [Blood Gem](Blood-Gem): 50

- [Potion Base](Potion-Base): 5

**Effect of using**

- Effect: {'hp.max': 1, 'element.max': 0.1}

