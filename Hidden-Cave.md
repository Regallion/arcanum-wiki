**Description**: The shifting sands reveal a rocky cave. Perhaps some artifacts remain within.

**Effects**

- [Unease](Unease): 1~3

- [Void](Void): 0.1

**Result**

- [Codices](Codices): 2~4

- [Tomes](Tomes): 0~3

- [Rune Stones](Rune-Stones): 10%

