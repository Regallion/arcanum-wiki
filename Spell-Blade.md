```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["Enchanting ≥ 7,<br>player.tohit ≥ 10"])
  t2 ---> t2r ---> spellblade
  subgraph tier3 [Tier 3]
    spellblade([Spell Blade])
  end
```
**Tags**: T_Tier3

**Requirements**: [Tier 2](tier2) > 0 and [Enchanting](Enchanting) ≥ 7 and Tohit ≥ 10 and [Tier 3](tier3) = 0

**Cost to acquire**

- [Research](Research): 3000

- [Arcana](Arcana): 20

- [Gold](Gold): 1000

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 3](tier3): True

- tohit: 15%

- [World Lore Max](World-Lore): 1

- [Enchanting Max](Enchanting): 2

- [Bladelore Max](Bladelore): 2

- [Anatomy Max](Anatomy): 1

- [Greater Arcane Body Max](Greater-Arcane-Body): 1

