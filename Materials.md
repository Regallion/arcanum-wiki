This page is a default list of the items in this category.<br>
If it doesn't meet your needs, please let a developer (probably 'simple') know, and they'll get around to updating it.<br>

[Adamant](Adamant)<br>
[Basilisk Hide](Basilisk-Hide)<br>
[Bone](Bone)<br>
[Bronze](Bronze)<br>
[Coldsteel](Coldsteel)<br>
[Cotton](Cotton)<br>
[Crimsonice](Crimsonice)<br>
[Dragonscale](Dragonscale)<br>
[Ebonwood](Ebonwood)<br>
[Ethereal](Ethereal)<br>
[Glass](Glass)<br>
[Gold](Gold)<br>
[Idrasil](Idrasil)<br>
[Iron](Iron)<br>
[Ivory](Ivory)<br>
[Leather](Leather)<br>
[Mithril](Mithril)<br>
[Permafrost](Permafrost)<br>
[Quicksteel](Quicksteel)<br>
[Ruby](Ruby)<br>
[Sackcloth](Sackcloth)<br>
[Silk](Silk)<br>
[Steel](Steel)<br>
[Stone](Stone)<br>
[Wood](Wood)