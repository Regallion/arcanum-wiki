```mermaid
graph LR
  t4{Tier 4 Class}
  t4r(["Warlock > 0,<br>Demonology ≥ 20"])
  t4 ---> t4r ---> archlock
  subgraph tier5 [Tier 5]
    archlock([Archlock])
  end
```
**Description**: Master of demons and demonic magic

**Tags**: T_Tier5

**Requirements**: [Warlock](Warlock) > 0 and [Demonology](Demonology) ≥ 20

**Cost to acquire**

- [Research](Research): 15000

- [Souls](Souls): 75

- [Bodies](Bodies): 10

- [Tomes](Tomes): 25

- [Arcana](Arcana): 25

- [Rune Stones](Rune-Stones): 10

**Modifiers**

- [Tier 5](tier5): True

- [Lore Max](Lore): 2

- [Necromancy Max](Necromancy): 1

- [Lore Rate](Lore): 10%

- [Pyromancy Max](Pyromancy): 3

- [Pyromancy Rate](Pyromancy): 20%

- [Demonology Max](Demonology): 5

- [Demonology Rate](Demonology): 20%

**Flavor**: In the War of the Locks the Seven raged for Power Absolute, and Dhrune trembled in their wake.

