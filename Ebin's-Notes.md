**Description**: A detailed study of the properties of ice.

**Effects**

- [Befuddlement](Befuddlement): 1~3

- [Cryomancy Exp](Cryomancy): 1

- [Cryomancy Max](Cryomancy): 0.001

**Result**

- [Essence of Winter](Essence-of-Winter): 0.1

- Event: [A Cold Legend](evt_w_chillforge): 10%

**Symbol**: ❄️

