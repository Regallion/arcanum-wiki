**Description**: The mirror shows scenes from far away.

**Effects**

- [Scrying Exp](Scrying): 1

- Travel Exp: 1

- [Befuddlement](Befuddlement): 1~2

