```mermaid
graph LR
  t5{Tier 5 Class}
  t5r(["(Highelemental + Kell + Heresiarch + Thaumaturge) > 0"])
  t5 ---> t5r ---> c_avatar
  subgraph tier6 [Tier 6]
    c_avatar([Avatar])
  end
```
**Description**: An incarnation of raw, elemental forces.

**Tags**: T_Tier6

**Requirements**: [Tier 5](tier5) > 0 and ([Highelemental](Highelemental) + [Kell](Kell) + [Heresiarch](Heresiarch) + [Thaumaturge](Thaumaturge)) > 0

**Cost to acquire**

- [Research](Research): 20000

- [Star Shard](Star-Shard): 3

- [Arcane Gem](Arcane-Gem): 70

- [Air Gem](Air-Gem): 40

- [Earth Gem](Earth-Gem): 40

- [Fire Gem](Fire-Gem): 40

- [Water Gem](Water-Gem): 40

- [Skill Points](Skill-Points): 10

- [Arcana](Arcana): 35

- [Rune Stones](Rune-Stones): 15

**Modifiers**

- [Tier 6](tier6): True

- [Wind Lore Max](Wind-Lore): 2

- [Wind Lore Rate](Wind-Lore): 10%

- [Geomancy Max](Geomancy): 3

- [Geomancy Rate](Geomancy): 15%

- [Water Lore Max](Water-Lore): 2

- [Water Lore Rate](Water-Lore): 10%

- [Pyromancy Max](Pyromancy): 1

- [Pyromancy Rate](Pyromancy): 15%

