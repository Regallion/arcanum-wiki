**Description**: Legend has it that Thurn himself crafted the ten leagues of clockworks in the Orrem Sands. Year by year, it ticks away the endless Aeons.

**Effects**

- [Madness](Madness): 0~3

- [Unease](Unease): 0~2

- [Befuddlement](Befuddlement): 1~3

- [Chronomancy Exp](Chronomancy): 1

- [Crafting Exp](Crafting): 1

- [World Lore Exp](World-Lore): 1

**Result**

- [World Lore Max](World-Lore): 0.001

**Flavor**: Its vast face is nothing to what is reputed to lie buried below.

