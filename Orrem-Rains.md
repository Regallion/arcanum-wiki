**Description**: Rare but torrential, the Orrem rains are accounted a wonder of the world.

**Effects**

- [Stamina](Stamina): 1

- [Water Lore Exp](Water-Lore): 1

- [Water](Water): 0.5

