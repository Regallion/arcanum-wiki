```mermaid
graph LR
  t-1{Tier -1 Class}
  t-1r([study<=0,<br>Neophyte,<br>Arcana >= 5])
  t-1 ---> t-1r ---> savant
  subgraph tier0 [Tier 0]
    savant([Mystic Savant])
  end
```
