**Description**: An odd crystal pulses rhythmically in the darkness. .... . .-.. .-.. ---

**Effects**

- [Weariness](Weariness): 1~3

- [Unease](Unease): 1~3

- [Befuddlement](Befuddlement): 1~3

- [Automata Sculpting Exp](Automata-Sculpting): 5

**Result**

- [Rune Stones](Rune-Stones): 15%

**Symbol**: None

