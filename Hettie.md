**Description**: An ancient gryffon lives atop the Rithel's solitary turret, and Hettie spends her days grooming and feeding the beast.

**Effects**

- [Weariness](Weariness): 1~3

- [Befuddlement](Befuddlement): 1~2

**Result**

- [Magic Beasts Max](Magic-Beasts): 0.001

