**Description**: The Delki of the woods know many things, but are seldom coherent.

**Effects**

- [Frustration](Frustration): 1~2

- [Weariness](Weariness): 1~3

- [Nature Lore Exp](Nature-Lore): 1

- [Research](Research): 1

- [Lore Exp](Lore): 1

**Result**

- [Nature Gem](Nature-Gem): 10%

- [Arcana](Arcana): 0.01

