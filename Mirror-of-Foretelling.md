**Description**: Visions of the future play in your reflection.

**Effects**

- [Divination Exp](Divination): 1

- [Madness](Madness): 1~3

- [Unease](Unease): 2~3

**Result**

