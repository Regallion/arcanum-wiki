```mermaid
graph LR
  t0{Tier 0 Class}
  t0r([Trickery >= 4,<br>tier1==0])
  t0 ---> t0r ---> trickster
  subgraph tier1 [Tier 1]
    trickster([Arcane Trickster])
  end
```
