**Description**: Causes you to become more attuned to the world's elements. Harmful to one's physical body.

**Level**: 25

**Requirements**: [Potions](Potions) ≥ 999

**Cost to Buy**

- [Gold](Gold): 5000

- [Tomes](Tomes): 50

- [Research](Research): 5000

- [Fire Gem](Fire-Gem): 50

- [Air Gem](Air-Gem): 50

- [Water Gem](Water-Gem): 50

- [Earth Gem](Earth-Gem): 50

**Cost to acquire**

- [Herbs](Herbs): 50

- [Life](Life): 150

- [Research](Research): 15000

- [Gold](Gold): 10000

- [Fire Gem](Fire-Gem): 50

- [Air Gem](Air-Gem): 50

- [Water Gem](Water-Gem): 50

- [Earth Gem](Earth-Gem): 50

- [Potion Base](Potion-Base): 5

**Effect of using**

- Effect: {'hp.max': -4, 'element.max': 0.25}

