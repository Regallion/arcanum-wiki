**Description**: The old stonemasons often chiselled spells into their works.

**Effects**

- [Geomancy Exp](Geomancy): 1

- [Befuddlement](Befuddlement): 0~2

- [Unease](Unease): 0~2

- [World Lore Exp](World-Lore): 1

**Result**

- [Geomancy Max](Geomancy): 0.0001

