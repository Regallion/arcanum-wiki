**Description**: All features of the riders are hidden in robes. They communicate with gutteral sounds and gestures.

**Effects**

- [Unease](Unease): 1~3

**Result**

- [Travel Skill Max](Travel): 0.001

