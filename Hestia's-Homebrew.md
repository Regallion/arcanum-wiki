**Requirements**: [Witchcraft](Witchcraft)

**Level**: 4

**Cost to Buy**

- [Gold](Gold): 25

- [Research](Research): 150

**Cost to acquire**

- [Herbs](Herbs): 15

**Effect of using**

- Dot: {'duration': 50, 'effect': {'hp': 2, 'nature': 0.5}}

