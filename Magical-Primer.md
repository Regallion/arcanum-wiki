**Description**: An introduction to magical theory.

**Effects**

- [Befuddlement](Befuddlement): -1~2

- [Lore Exp](Lore): 1

**Result**

- [Research](Research): 1

- [Arcana](Arcana): 0.01

