```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Nature Lore ≥ 12"])
  t1 ---> t1r ---> warden
  subgraph tier2 [Tier 2]
    warden([Warden])
  end
```
**Description**: A keeper of the forests, mountains, and hills. Only those living in the wild may become wardens.

**Tags**: T_Tier2

**Requirements**: [Tier 1](tier1) > 0 and [Nature Lore](Nature-Lore) ≥ 12

**Need**: (G.Outdoors+G.Lodge)>0

**Cost to acquire**

- [Research](Research): 1500

- [Nature Gem](Nature-Gem): 15

- [Herbs](Herbs): 50

- [Arcana](Arcana): 15

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 2](tier2): True

- [Research Max](Research): 5

- [Nature Lore Max](Nature-Lore): 3

- [Nature Lore Rate](Nature-Lore): 1+15%

- [Animal Handling Max](Animal-Handling): 3

- [Potions Max](Potions): 1

