**Description**: A flask of metal that stays liquid even in the coldest weather. Converts organic materials to more liquid metal. Becomes inert after some time.

**Requirements**: Puppet_Enhancedlabs > 0 and [Potions](Potions) > 0

**Level**: 10

**Cost to Buy**

- [Gold](Gold): 2500

- [Research](Research): 3000

**Cost to acquire**

- [Herbs](Herbs): 50

- [Gold](Gold): 2500

- [Gems](Gems): 25

- [Mana](Mana): 15

- [Potion Base](Potion-Base): 1

**Effect of using**

- Dot: {'id': 'metallicize', 'name': 'liquidation', 'duration': 120, 'effect': {'bodies': -1, 'herbs': -1, 'hp': -2}, 'mod': {'gatherherbs.result.herbs': -0.2, 'gatherherbs.result.gold': 4, 'murder.result.bodies': -1, 'murder.result.gold': 200}}

