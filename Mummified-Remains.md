**Description**: A corpse preserved by the stale air

**Effects**

- [Madness](Madness): 0~2

- [Unease](Unease): 1~3

- [Embalming Exp](Embalming): 1

- [Spirit Lore Exp](Spirit-Lore): 1

- [Anatomy Exp](Anatomy): 1

**Result**

- [Anatomy Max](Anatomy): 0.001

**Loot**

- [Gold](Gold): 0~100

- [Bone Dust](Bone-Dust): 0~2

- [Gems](Gems): 0~2

- [Bodies](Bodies): 30%

