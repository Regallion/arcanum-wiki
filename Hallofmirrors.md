**Level**: 12

**Requirements**: [Scrying](Scrying) ≥ 999

**Length**: 50

**Symbol**: 📖

**Run Cost**

- [Stamina](Stamina): 0.4

**Result**

- [Arcana](Arcana): 0.1

- [Research](Research): 10

**Loot**

- [Scrolls](Scrolls): 1~4

**Encounters**

- [Occult Tapestry](Occult-Tapestry)

- [Dusty Chest](Dusty-Chest)

- [Mirror of Introspection](Mirror-of-Introspection)

- [Mirror of False Reflection](Mirror-of-False-Reflection)

- [Mirror of Foretelling](Mirror-of-Foretelling)

- [Seething Mirror](Seething-Mirror)

- [Reflecting Hallways](Reflecting-Hallways)

- [Void Mirror](Void-Mirror)

- [Mirror of History](Mirror-of-History)

- [A Distant Mirror](A-Distant-Mirror)

- [Water Mirror](Water-Mirror)

