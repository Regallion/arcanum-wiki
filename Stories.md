**Description**: Listening to elders recite their winter legends.

**Effects**

- [Befuddlement](Befuddlement): 1~2

- [Unease](Unease): 0~2

- [World Lore Exp](World-Lore): 3

**Level**: 2

**Result**

- [Essence of Winter](Essence-of-Winter): 0.1

- Event: [Legend Tells](evt_w_legend): 10%

**Symbol**: ❄️

