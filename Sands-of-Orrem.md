**Requirements**: [World Lore](World-Lore) ≥ 7 and [Scrying](Scrying) ≥ 5

**Start Announcement**

- Name: Cities of Orrem

- Desc: A vision in a crystal orb reveals to you the ruins of the lost cities of Orrem.

**Description**: Only a small trading post marks the boundary of the lost cities of Orrem. Weathered masonry jutting from the sands beyond bespeak monuments of unimaginable proportions.

**Length**: 400

**Level**: 19

**Run Cost**

- [Stamina](Stamina): 4

**Result**

- Title: Nomad

- [Notoriety](Notoriety): 1

**Loot**

- [Jazid's Compass](Jazid's-Compass): 5%

**Encounters**

- [Aeon Clock](Aeon-Clock)

- [Caravan](Caravan)

- [Oasis](Oasis)

- [Orrem Rains](Orrem-Rains)

- [Winds of Madness](Winds-of-Madness)

- [Hidden Cave](Hidden-Cave)

- [Sandstorm](Sandstorm)

- [Mirage](Mirage)

- [Eldar Statue](Eldar-Statue)

- [Strange Bones](Strange-Bones)

- [Crumbled Statue](Crumbled-Statue)

- [Starry Night](Starry-Night)

- [Dusty Chest](Dusty-Chest)

- [Lost Trading Post](Lost-Trading-Post)

- [Scrap Pile](Scrap-Pile)

- [Pile of Wood?](Pile-of-Wood_)

