**Description**: A grim name for an otherwise respectable mage.

**Effects**

- [Madness](Madness): 1~5

- [Befuddlement](Befuddlement): 2~5

- [World Lore Exp](World-Lore): 2

- [Shadowlore Exp](Shadowlore): 2

**Symbol**: ⭐

