**Description**: Phrenesis was forever preoccupied with the cycles of death and rebirth.

**Effects**

- Stress: -0.1

- [Life](Life): -4

**Symbol**: ⭐

