```mermaid
graph LR
  t4{Tier 4 Class}
  t4r(["Spirit ≥ 15,<br>Divination ≥ 17"])
  t4 ---> t4r ---> heresiarch
  subgraph tier5 [Tier 5]
    heresiarch([Heresiarch])
  end
```
**Description**: A reckoning comes.

**Requirements**: [Tier 4](tier4) > 0 and [Spirit](Spirit) ≥ 15 and [Divination](Divination) ≥ 17

**Tags**: T_Tier5

**Cost to acquire**

- [Research](Research): 15000

- [Arcana](Arcana): 25

- [Tomes](Tomes): 15

- [Spirit Gem](Spirit-Gem): 50

- [Rune Stones](Rune-Stones): 10

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 5](tier5): True

- [Spirit Lore Max](Spirit-Lore): 3

- [Spirit Lore Rate](Spirit-Lore): 12%

- [Divination Max](Divination): 3

- [Divination Rate](Divination): 10%

- [Scrying Max](Scrying): 2

- [Lore Max](Lore): 2

- [Lore Rate](Lore): 10%

**Flavor**: It waits in time

