**Level**: 4

**Length**: 30

**Requirements**: [Tier 0](tier0)

**Description**: An ancient crypt of crumbling stone.

**Run Cost**

- [Stamina](Stamina): 0.35

**Result**

- [Arcana](Arcana): 0.15

- [Research](Research): 20

**Loot**

- [Scrolls](Scrolls): 2~4

**Encounters**

- [Occult Tapestry](Occult-Tapestry)

- [Mummified Remains](Mummified-Remains)

- [Sarcophagus](Sarcophagus)

- [Plague Rats](Plague-Rats)

- [Strange Bones](Strange-Bones)

- [Embalming Apparatus](Embalming-Apparatus)

- [Iron Chest](Iron-Chest)

- [Eerie Moans](Eerie-Moans)

- [Pile of Wood?](Pile-of-Wood_)

