```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["Vile > 0"])
  t2 ---> t2r ---> thanophage
  subgraph tier3 [Tier 3]
    thanophage([Thanophage])
  end
```
**Tags**: T_Tier3

**Description**: Drain corpses of their lingering life-force to prolong your bleak existence.

**Requirements**: [Tier 2](tier2) > 0 and Event: [Vile](evil) > 0 and [Tier 3](tier3) = 0

**Cost to acquire**

- [Research](Research): 1500

- [Bones](Bones): 100

- [Bodies](Bodies): 50

- [Bone Dust](Bone-Dust): 25

- [Arcana](Arcana): 15

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 3](tier3): True

- [Evil](Evil): 20

- [Lore Max](Lore): 2

- [Research Max](Research): 25

- [Embalming Max](Embalming): 1

- [Reanimation Max](Reanimation): 2

- [Reanimation Rate](Reanimation): 5%

- [Shadowlore Max](Shadowlore): 2

- [Shadowlore Rate](Shadowlore): 5%

