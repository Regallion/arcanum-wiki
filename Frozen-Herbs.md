**Description**: A few herbs still survive in the frozen loam.

**Level**: 3

**Effects**

- [Frustration](Frustration): 1~3

- [Weariness](Weariness): 1~4

**Loot**

- [Essence of Winter](Essence-of-Winter): 0.1

- [Herbs](Herbs): 2~5

- [Snowdrop](Snowdrop): 20%

- [Icethorn](Icethorn): 10%

- [Manabell](Manabell): 1%

**Symbol**: ❄️

