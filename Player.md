This page is a default list of the items in this category.<br>
If it doesn't meet your needs, please let a developer (probably 'simple') know, and they'll get around to updating it.<br>

[Age](Age)<br>
[Breath](Breath)<br>
[Damage Reduction](Damage-Reduction)<br>
[Distance](Distance)<br>
[Dodge](Dodge)<br>
[Evil](Evil)<br>
[Exp](Exp)<br>
[Level](Level)<br>
[Life](Life)<br>
[Luck](Luck)<br>
[Notoriety](Notoriety)<br>
[Skill Points](Skill-Points)<br>
[Speed](Speed)<br>
[Spellcraft Power](Spellcraft-Power)<br>
[Stamina](Stamina)<br>
[Virtue](Virtue)