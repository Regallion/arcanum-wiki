```mermaid
graph LR
  t5{Tier 5 Class}
  t5r(["Spell Blade > 0"])
  t5 ---> t5r ---> eldritchknight
  subgraph tier6 [Tier 6]
    eldritchknight([Eldritch Knight])
  end
```
**Tags**: T_Tier6

**Description**: You have reached the pinnacle of blending steel and sorcery, and your sword strokes seem to cleave the air itself.

**Requirements**: [Tier 5](tier5) > 0 and [Spell Blade](Spell-Blade) > 0 and [Tier 6](tier6) = 0

**Cost to acquire**

- [Research](Research): 15000

- [Arcana](Arcana): 20

- [Gold](Gold): 7500

- [Air Gem](Air-Gem): 40

- [Fire Gem](Fire-Gem): 20

- [Earth Gem](Earth-Gem): 20

- [Water Gem](Water-Gem): 20

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 6](tier6): True

- tohit: 15%

- [World Lore Max](World-Lore): 1

- [Enchanting Max](Enchanting): 1

- [Bladelore Max](Bladelore): 2

- Elemental Max: 1

- [Anatomy Max](Anatomy): 1

- [Greater Arcane Body Max](Greater-Arcane-Body): 1

