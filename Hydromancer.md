```mermaid
graph LR
  t1{Tier 1 Class}
  t1r(["Water Lore ≥ 10"])
  t1 ---> t1r ---> hydromancer
  subgraph tier2 [Tier 2]
    hydromancer([Hydromancer])
  end
```
**Description**: Master of water

**Tags**: T_Tier2

**Action Description**: Become a hydromancer.

**Requirements**: [Tier 1](tier1) > 0 and [Water Lore](Water-Lore) ≥ 10

**Cost to acquire**

- [Research](Research): 1000

- [Arcana](Arcana): 15

- [Tomes](Tomes): 10

- [Water Gem](Water-Gem): 10

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 2](tier2): True

- [Water Lore Max](Water-Lore): 2

- [Water Lore Rate](Water-Lore): 10%

- [Dodge](Dodge): 10%

- [Mana Max](Mana): 2

