**Description**: Conjuring a fire elemental at the magic circle. That should stave away the cold!

**Effects**

- [Mana](Mana): 0.5~1

- [Pyromancy Exp](Pyromancy): 3

- [Conjuration Exp](Conjuration): 5

- [Madness](Madness): 2~3

**Level**: 2

**Result**

- [Frost Rate](Frost): -0.1

**Symbol**: ❄️

