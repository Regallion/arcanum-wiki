**Description**: A mage of the wilds makes a perplexing teacher.

**Effects**

- [Madness](Madness): 1~5

- [Weariness](Weariness): 2~5

- [Nature Lore Exp](Nature-Lore): 2

- [Animal Handling Exp](Animal-Handling): 2

**Symbol**: ⭐

