**Level**: 20

**Requirements**: [Travel Skill](Travel) ≥ 5 and [Travel Task](Travel) ≥ 100

**Description**: Tenwick's trail runs a circuit about the notable areas of Dhrune. On foot it takes the better part of a year to complete.

**Length**: 10000

**Run Cost**

- [Stamina](Stamina): 2

**Result**

- Title: Dhrunic Wayfarer

**Loot**

- [Tenwick's Walking Stick](Tenwick's-Walking-Stick): 1

**Encounters**

- [Bright Vista](Bright-Vista)

- [Pod of Poglers](Pod-of-Poglers)

- [Agolith](Agolith)

- [Ancient Battlefield](Ancient-Battlefield)

- [Black Cat](Black-Cat)

- [Oak Chest](Oak-Chest)

- [Babbling Delki](Babbling-Delki)

- [Crumbled Statue](Crumbled-Statue)

- [Eldar Statue](Eldar-Statue)

- [Markhul's Gap](Markhul's-Gap)

- [Blood Grass](Blood-Grass)

- [Gibbering](Gibbering)

- [Affable Gnome](Affable-Gnome)

- [Thandhrun Pass](Thandhrun-Pass)

- [The Old Stone](The-Old-Stone)

- [Sandstorm](Sandstorm)

- [Star Shrine](Star-Shrine)

- [Tenwick](Tenwick)

- [Wyrd Sister](Wyrd-Sister)

- [Haunted Glade](Haunted-Glade)

- [Foggy Dale](Foggy-Dale)

- [Hidden Cache](Hidden-Cache)

- [Mana Tree](Mana-Tree)

- [Murky Waters](Murky-Waters)

- [Mystic Waters](Mystic-Waters)

- [Somber Sunset](Somber-Sunset)

- [Starry Night](Starry-Night)

- [Strange Bones](Strange-Bones)

