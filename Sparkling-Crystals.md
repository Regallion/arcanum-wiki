**Description**: A vein of crystals embedded in the cave wall, sparkling in the dark.

**Level**: 3

**Effects**

- [Frustration](Frustration): 1~3

- [Weariness](Weariness): 1~4

- [Geomancy Exp](Geomancy): 3

**Loot**

- [Gems](Gems): 1~3

- [Water Gem](Water-Gem): 0~1

- [Fire Gem](Fire-Gem): 20%

**Symbol**: ❄️

