|Tag Name|Tagged Items|
|:-------|:-----------|
|airsource|[Casement Window](home/Furnitures/Casement-Window), [Cloud Chamber](home/Furnitures/Cloud-Chamber), [Evanescence](home/Upgrades/Evanescence), [Lattice Window](home/Furnitures/Lattice-Window), [Swirling Globe](home/Upgrades/Swirling-Globe), [Tower](home/Homes/Tower), [Windchime](home/Upgrades/Windchime)|<br>
|armssource|[Armory](home/Furnitures/Armory), [Weapon Rack](home/Furnitures/Weapon-Rack)|<br>
|automata|[Autocaster](home/Monsters/Autocaster)|<br>
|bed|[Burning Coals](home/Furnitures/Burning-Coals), [Cot](home/Furnitures/Cot), [Fourposter](home/Furnitures/Fourposter), [Wooden Bed](home/Furnitures/Wooden-Bed)|<br>
|book|[Annals of Arazor](home/Tasks/Annals-of-Arazor), [Annals of Orrem](home/Tasks/Annals-of-Orrem), [Coporis Fabrica](home/Tasks/Coporis-Fabrica)|<br>
|candle|[Heart Candle](home/Upgrades/Heart-Candle), [Holy Candle](home/Upgrades/Holy-Candle), [Somber Candle](home/Upgrades/Somber-Candle), [Unholy Candle](home/Upgrades/Unholy-Candle), [Wax Candle](home/Upgrades/Wax-Candle)|<br>
|d_axe|[Axe](home/Weapons/Axe)|<br>
|d_axes|[Axe](home/Weapons/Axe), [Battleaxe](home/Weapons/Battleaxe)|<br>
|d_battleaxe|[Battleaxe](home/Weapons/Battleaxe)|<br>
|d_broomstick|[Broomstick](home/Weapons/Broomstick)|<br>
|d_cane|[Cane](home/Weapons/Cane)|<br>
|d_club|[Club](home/Weapons/Club)|<br>
|d_dagger|[Dagger](home/Weapons/Dagger)|<br>
|d_fetish|[Fetish](home/Weapons/Fetish)|<br>
|d_greatstaff|[Greatstaff](home/Weapons/Greatstaff)|<br>
|d_knife|[Knife](home/Weapons/Knife)|<br>
|d_longsword|[Longsword](home/Weapons/Longsword)|<br>
|d_mace|[Mace](home/Weapons/Mace)|<br>
|d_magic|[Fetish](home/Weapons/Fetish), [Greatstaff](home/Weapons/Greatstaff), [Orb](home/Weapons/Orb), [Rod](home/Weapons/Rod), [Wand](home/Weapons/Wand)|<br>
|d_orb|[Orb](home/Weapons/Orb)|<br>
|d_rod|[Rod](home/Weapons/Rod)|<br>
|d_shortsword|[Shortsword](home/Weapons/Shortsword)|<br>
|d_spear|[Spear](home/Weapons/Spear)|<br>
|d_staff|[Staff](home/Weapons/Staff)|<br>
|d_staves|[Greatstaff](home/Weapons/Greatstaff), [Staff](home/Weapons/Staff)|<br>
|d_sword|[Sword](home/Weapons/Sword)|<br>
|d_swords|[Longsword](home/Weapons/Longsword), [Shortsword](home/Weapons/Shortsword), [Sword](home/Weapons/Sword)|<br>
|d_wand|[Wand](home/Weapons/Wand)|<br>
|d_warhammer|[Warhammer](home/Weapons/Warhammer)|<br>
|earthsource|[Cavern](home/Homes/Cavern), [Earth Soul](home/Upgrades/Earth-Soul), [Grotesque](home/Furnitures/Grotesque), [Heart of Stone](home/Upgrades/Heart-of-Stone), [Sand Garden](home/Furnitures/Sand-Garden), [Terrarium](home/Furnitures/Terrarium), [Vivarium](home/Furnitures/Vivarium)|<br>
|element|[Air](home/Resources/Air), [Earth](home/Resources/Earth), [Fire](home/Resources/Fire), [Water](home/Resources/Water)|<br>
|elemental|[Aeromancy](home/Skills/Aeromancy), [Aquamancy](home/Skills/Aquamancy), [Geomancy](home/Skills/Geomancy), [Pyromancy](home/Skills/Pyromancy)|<br>
|elementalgems|[Air Gem](home/Resources/Air-Gem), [Earth Gem](home/Resources/Earth-Gem), [Fire Gem](home/Resources/Fire-Gem), [Water Gem](home/Resources/Water-Gem)|<br>
|elementalrunes|[Air Runes](home/Resources/Air-Runes), [Earth Runes](home/Resources/Earth-Runes), [Flame Runes](home/Resources/Flame-Runes), [Water Runes](home/Resources/Water-Runes)|<br>
|enchantsource|[Enchanting Table](home/Furnitures/Enchanting-Table)|<br>
|familiar|[Cat Familiar](home/Upgrades/Cat-Familiar), [Imp Familiar](home/Upgrades/Imp-Familiar), [Kobold Familiar](home/Upgrades/Kobold-Familiar), [Owl Familiar](home/Upgrades/Owl-Familiar), [Tome Familiar](home/Upgrades/Tome-Familiar)|<br>
|firesource|[Bonfire](home/Furnitures/Bonfire), [Brazier](home/Furnitures/Brazier), [Campfire](home/Furnitures/Campfire), [Fireplace](home/Furnitures/Fireplace), [Inner Fire](home/Upgrades/Inner-Fire), [Lake of Fire](home/Furnitures/Lake-of-Fire), [Volcanic Lair](home/Homes/Volcanic-Lair)|<br>
|flag|[Soulflag](home/Rares/Soulflag)|<br>
|gemimbue|[Coagulate Gem (Blood)](home/Tasks/Coagulate-Gem-(Blood)), [Imbue Gem (Air)](home/Tasks/Imbue-Gem-(Air)), [Imbue Gem (Arcane)](home/Tasks/Imbue-Gem-(Arcane)), [Imbue Gem (Chaos)](home/Tasks/Imbue-Gem-(Chaos)), [Imbue Gem (Fire)](home/Tasks/Imbue-Gem-(Fire)), [Imbue Gem (Light)](home/Tasks/Imbue-Gem-(Light)), [Imbue Gem (Nature)](home/Tasks/Imbue-Gem-(Nature)), [Imbue Gem (Shadow)](home/Tasks/Imbue-Gem-(Shadow)), [Imbue Gem (Spirit)](home/Tasks/Imbue-Gem-(Spirit)), [Imbue Gem (Water)](home/Tasks/Imbue-Gem-(Water)), [Imbue Stone (Earth)](home/Tasks/Imbue-Stone-(Earth))|<br>
|herb|[Belladonna](home/Reagents/Belladonna), [Blackrot](home/Reagents/Blackrot), [Fire Thistle](home/Reagents/Fire-Thistle), [Goldfel](home/Reagents/Goldfel), [Icethorn](home/Reagents/Icethorn), [Lavender](home/Reagents/Lavender), [Marigold](home/Reagents/Marigold), [Milkweed](home/Reagents/Milkweed), [Nightshade](home/Reagents/Nightshade), [Oleander](home/Reagents/Oleander), [Snowdrop](home/Resources/Snowdrop), [Stinging Nettle](home/Reagents/Stinging-Nettle)|<br>
|lightsource|[Casement Window](home/Furnitures/Casement-Window), [Lattice Window](home/Furnitures/Lattice-Window), [Pavilion](home/Homes/Pavilion), [Prism](home/Upgrades/Prism), [Shrine](home/Furnitures/Shrine)|<br>
|magicgems|[Air Gem](home/Resources/Air-Gem), [Arcane Gem](home/Resources/Arcane-Gem), [Blood Gem](home/Resources/Blood-Gem), [Chaos Gem](home/Resources/Chaos-Gem), [Earth Gem](home/Resources/Earth-Gem), [Fire Gem](home/Resources/Fire-Gem), [Light Gem](home/Resources/Light-Gem), [Nature Gem](home/Resources/Nature-Gem), [Shadow Gem](home/Resources/Shadow-Gem), [Spirit Gem](home/Resources/Spirit-Gem), [Time Gem](home/Resources/Time-Gem), [Void Gem](home/Resources/Void-Gem), [Water Gem](home/Resources/Water-Gem)|<br>
|manas|[Air](home/Resources/Air), [Chaos](home/Resources/Chaos), [Earth](home/Resources/Earth), [Fire](home/Resources/Fire), [Ice](home/Resources/Ice), [Light](home/Resources/Light), [Mana](home/Resources/Mana), [Nature](home/Resources/Nature), [Shadow](home/Resources/Shadow), [Spirit](home/Resources/Spirit), [Tempus](home/Resources/Tempus), [Void](home/Resources/Void), [Water](home/Resources/Water)|<br>
|naturesource|[Garden](home/Furnitures/Garden), [Vivarium](home/Furnitures/Vivarium)|<br>
|outdoors|[Camp](home/Homes/Camp), [Cave](home/Homes/Cave), [Cove](home/Homes/Cove), [Enchanted Grove](home/Homes/Enchanted-Grove), [Idrasil Seedling](home/Homes/Idrasil-Seedling), [Shrouded Isle](home/Homes/Shrouded-Isle), [Tinker's Wagon](home/Homes/Tinker's-Wagon), [Wild's Encampment](home/Homes/Wild's-Encampment)|<br>
|patron|[Bloodbane's Anvil](home/Furnitures/Bloodbane's-Anvil), [Kanna's Dervish Dance](home/Spells/Kanna's-Dervish-Dance), [Nathaniel's Sanctuary](home/Furnitures/Nathaniel's-Sanctuary), [Wild's Encampment](home/Homes/Wild's-Encampment)|<br>
|plantsource|[Creeping Vine](home/Furnitures/Creeping-Vine), [Flower Pot](home/Furnitures/Flower-Pot), [Garden](home/Furnitures/Garden), [Potted Milkweed](home/Furnitures/Potted-Milkweed), [Wild's Encampment](home/Homes/Wild's-Encampment)|<br>
|poison|[Belladonna](home/Reagents/Belladonna), [Milkweed](home/Reagents/Milkweed), [Nightshade](home/Reagents/Nightshade), [Oleander](home/Reagents/Oleander)|<br>
|potsource|[Cauldron](home/Upgrades/Cauldron), [Cooking Pot](home/Upgrades/Cooking-Pot), [Potions Room](home/Furnitures/Potions-Room)|<br>
|primaticrunes|[Air Runes](home/Resources/Air-Runes), [Earth Runes](home/Resources/Earth-Runes), [Flame Runes](home/Resources/Flame-Runes), [Light Runes](home/Resources/Light-Runes), [Mana Runes](home/Resources/Mana-Runes), [Nature Runes](home/Resources/Nature-Runes), [Shadow Runes](home/Resources/Shadow-Runes), [Spirit Runes](home/Resources/Spirit-Runes), [Water Runes](home/Resources/Water-Runes)|<br>
|primordial|[Chaos](home/Resources/Chaos), [Tempus](home/Resources/Tempus), [Void](home/Resources/Void)|<br>
|primordialgems|[Chaos Gem](home/Resources/Chaos-Gem), [Time Gem](home/Resources/Time-Gem), [Void Gem](home/Resources/Void-Gem)|<br>
|primordialrunes|[Chaos Runes](home/Resources/Chaos-Runes), [Time Runes](home/Resources/Time-Runes)|<br>
|prismatic|[Air](home/Resources/Air), [Earth](home/Resources/Earth), [Fire](home/Resources/Fire), [Light](home/Resources/Light), [Mana](home/Resources/Mana), [Nature](home/Resources/Nature), [Shadow](home/Resources/Shadow), [Spirit](home/Resources/Spirit), [Water](home/Resources/Water)|<br>
|prismaticgems|[Air Gem](home/Resources/Air-Gem), [Arcane Gem](home/Resources/Arcane-Gem), [Earth Gem](home/Resources/Earth-Gem), [Fire Gem](home/Resources/Fire-Gem), [Light Gem](home/Resources/Light-Gem), [Nature Gem](home/Resources/Nature-Gem), [Shadow Gem](home/Resources/Shadow-Gem), [Spirit Gem](home/Resources/Spirit-Gem), [Water Gem](home/Resources/Water-Gem)|<br>
|prison|[Binding Circle](home/Furnitures/Binding-Circle), [Crypt](home/Furnitures/Crypt), [Demonic Pentagram](home/Furnitures/Demonic-Pentagram), [Dungeon](home/Furnitures/Dungeon), [Iron Cell](home/Furnitures/Iron-Cell)|<br>
|reagent|[Belladonna](home/Reagents/Belladonna), [Blackrot](home/Reagents/Blackrot), [Crocodile Tears](home/Reagents/Crocodile-Tears), [Dragonscale](home/Reagents/Dragonscale), [Eye of Newt](home/Reagents/Eye-of-Newt), [Fire Thistle](home/Reagents/Fire-Thistle), [Goldfel](home/Reagents/Goldfel), [Icethorn](home/Reagents/Icethorn), [Ichor](home/Resources/Ichor), [Lavender](home/Reagents/Lavender), [Marigold](home/Reagents/Marigold), [Milkweed](home/Reagents/Milkweed), [Nightshade](home/Reagents/Nightshade), [Oleander](home/Reagents/Oleander), [Snowdrop](home/Resources/Snowdrop), [Stinging Nettle](home/Reagents/Stinging-Nettle)|<br>
|rez|[Greater Reanimate](home/Spells/Greater-Reanimate), [Magic Tending](home/Spells/Magic-Tending), [Mend Wood](home/Spells/Mend-Wood), [Reanimate](home/Spells/Reanimate), [Repair Machina](home/Spells/Repair-Machina), [Revive](home/Spells/Revive), [Tune Automata](home/Spells/Tune-Automata), [Wild Tending](home/Spells/Wild-Tending)|<br>
|shadowsource|[Blood Fountain](home/Furnitures/Blood-Fountain), [Dark Shrine](home/Furnitures/Dark-Shrine), [Graveyard](home/Furnitures/Graveyard), [Iron Cell](home/Furnitures/Iron-Cell), [Ossuary](home/Furnitures/Ossuary), [Rusty Cage](home/Furnitures/Rusty-Cage), [Torture Chamber](home/Furnitures/Torture-Chamber)|<br>
|soulart|[Soul Card: Soul Lure](home/Spells/Soul-Card:-Soul-Lure)|<br>
|spiritart|[Spirit Art: Soul River](home/Spells/Spirit-Art:-Soul-River), [Spirit Art: Soulstrike](home/Spells/Spirit-Art:-Soulstrike)|<br>
|spiritsource|[Brazier](home/Furnitures/Brazier), [Cairn](home/Furnitures/Cairn), [Crystal Ball](home/Upgrades/Crystal-Ball), [Dream Catcher](home/Upgrades/Dream-Catcher), [Graveyard](home/Furnitures/Graveyard), [Haunted Manse](home/Homes/Haunted-Manse), [Ossuary](home/Furnitures/Ossuary), [Reliquary](home/Furnitures/Reliquary), [Third Eye](home/Upgrades/Third-Eye), [Windchime](home/Upgrades/Windchime)|<br>
|stables|[Menagerie](home/Furnitures/Menagerie), [Stables](home/Furnitures/Stables)|<br>
|starsource|[Camp](home/Homes/Camp), [Orrery](home/Furnitures/Orrery), [Sextant](home/Furnitures/Sextant), [Telescope](home/Furnitures/Telescope), [Ziggurat](home/Furnitures/Ziggurat)|<br>
|steed|[Demure Gelding](home/Upgrades/Demure-Gelding), [Fine Bay](home/Upgrades/Fine-Bay), [Fire Charger](home/Upgrades/Fire-Charger), [Gryffon](home/Upgrades/Gryffon), [Mule](home/Upgrades/Mule), [Old Nag](home/Upgrades/Old-Nag), [Pegasus](home/Upgrades/Pegasus), [Skeletal Charger](home/Upgrades/Skeletal-Charger)|<br>
|stress|[Befuddlement](home/Stressors/Befuddlement), [Frustration](home/Stressors/Frustration), [Madness](home/Stressors/Madness), [Unease](home/Stressors/Unease), [Weariness](home/Stressors/Weariness)|<br>
|swordart|[Lost Sword Art: Flying Swords](home/Spells/Lost-Sword-Art:-Flying-Swords), [Lost Sword Art: Giant Slayer](home/Spells/Lost-Sword-Art:-Giant-Slayer)|<br>
|t_ankleguard|[Wraps of the Ecdysiast](home/Rares/Wraps-of-the-Ecdysiast)|<br>
|t_artifact|[Titan's Hammer](home/Rares/Titan's-Hammer)|<br>
|t_automata|[Autoslayer](home/Monsters/Autoslayer)|<br>
|t_axe|[Axe](home/Weapons/Axe)|<br>
|t_axes|[Axe](home/Weapons/Axe), [Battleaxe](home/Weapons/Battleaxe)|<br>
|t_battleaxe|[Battleaxe](home/Weapons/Battleaxe)|<br>
|t_broomstick|[Broomstick](home/Weapons/Broomstick)|<br>
|t_cane|[Cane](home/Weapons/Cane), [Tenwick's Walking Stick](home/Rares/Tenwick's-Walking-Stick)|<br>
|t_club|[Club](home/Weapons/Club)|<br>
|t_cord|[Lillit's Cord](home/Rares/Lillit's-Cord)|<br>
|t_crown|[Crown of Karnivex](home/Rares/Crown-of-Karnivex)|<br>
|t_dagger|[Dagger](home/Rares/Dagger), [Dagger](home/Weapons/Dagger), [Icy Dagger](home/Rares/Icy-Dagger), [Necrotic Dagger](home/Rares/Necrotic-Dagger)|<br>
|t_dart|[Homing Dart](home/Rares/Homing-Dart)|<br>
|t_earring|[Veldran's Earring](home/Rares/Veldran's-Earring)|<br>
|t_event|[❄️Orb of Winters](home/Rares/❄️Orb-of-Winters)|<br>
|t_fetish|[Fetish](home/Weapons/Fetish)|<br>
|t_gloves|[Gloves of the Botanist](home/Rares/Gloves-of-the-Botanist)|<br>
|t_greatstaff|[Greatstaff](home/Weapons/Greatstaff)|<br>
|t_hall|[Wizard's Hall](home/Upgrades/Wizard's-Hall)|<br>
|t_job|[Falconer](home/Classes/Falconer), [Herbalist](home/Classes/Herbalist), [Scribe](home/Classes/Scribe)|<br>
|t_knife|[Knife](home/Weapons/Knife)|<br>
|t_longsword|[Longsword](home/Weapons/Longsword), [Vorpal Blade](home/Rares/Vorpal-Blade)|<br>
|t_mace|[Mace](home/Weapons/Mace)|<br>
|t_machina|[Mecha-Charger](home/Monsters/Mecha-Charger), [Mecha-Mender](home/Monsters/Mecha-Mender)|<br>
|t_magic|[Fetish](home/Weapons/Fetish), [Greatstaff](home/Weapons/Greatstaff), [Orb](home/Weapons/Orb), [Rod](home/Weapons/Rod), [Wand](home/Weapons/Wand)|<br>
|t_mine|[Mineshaft](home/Furnitures/Mineshaft)|<br>
|t_orb|[Orb](home/Weapons/Orb), [❄️Orb of Winters](home/Rares/❄️Orb-of-Winters)|<br>
|t_pets|[Birdcage](home/Furnitures/Birdcage), [Rusty Cage](home/Furnitures/Rusty-Cage), [Terrarium](home/Furnitures/Terrarium), [Tinker's Wagon](home/Homes/Tinker's-Wagon), [Vivarium](home/Furnitures/Vivarium), [Wild's Encampment](home/Homes/Wild's-Encampment)|<br>
|t_puppet|[Puppet Aggressor](home/Monsters/Puppet-Aggressor), [Puppet Bulwark](home/Monsters/Puppet-Bulwark)|<br>
|t_rest|[Chant](home/Tasks/Chant), [Commune](home/Tasks/Commune), [Eat Children](home/Tasks/Eat-Children), [Indulge](home/Tasks/Indulge), [Relax](home/Tasks/Relax), [Slumber](home/Tasks/Slumber), [Spring Rites](home/Tasks/Spring-Rites), [Time Siphon](home/Tasks/Time-Siphon), [Warm Yourself](home/Tasks/Warm-Yourself)|<br>
|t_ring|[Ring of the Patron](home/Rares/Ring-of-the-Patron)|<br>
|t_ritual|[Greater Ritual of Cultivation](home/Tasks/Greater-Ritual-of-Cultivation), [Greater Ritual of Cursing](home/Tasks/Greater-Ritual-of-Cursing), [Greater Ritual of Energy](home/Tasks/Greater-Ritual-of-Energy), [Greater Ritual of Healing](home/Tasks/Greater-Ritual-of-Healing), [Greater Ritual of the Arcane](home/Tasks/Greater-Ritual-of-the-Arcane), [Ritual of Air](home/Tasks/Ritual-of-Air), [Ritual of Cultivation](home/Tasks/Ritual-of-Cultivation), [Ritual of Cursing](home/Tasks/Ritual-of-Cursing), [Ritual of Dawn](home/Tasks/Ritual-of-Dawn), [Ritual of Dreams](home/Tasks/Ritual-of-Dreams), [Ritual of Dusk](home/Tasks/Ritual-of-Dusk), [Ritual of Earth](home/Tasks/Ritual-of-Earth), [Ritual of Fire](home/Tasks/Ritual-of-Fire), [Ritual of Healing](home/Tasks/Ritual-of-Healing), [Ritual of Light Binding](home/Tasks/Ritual-of-Light-Binding), [Ritual of Mana](home/Tasks/Ritual-of-Mana), [Ritual of Mandalas](home/Tasks/Ritual-of-Mandalas), [Ritual of Penance](home/Tasks/Ritual-of-Penance), [Ritual of Refreshment](home/Tasks/Ritual-of-Refreshment), [Ritual of Time](home/Tasks/Ritual-of-Time), [Ritual of Void](home/Tasks/Ritual-of-Void), [Ritual of Water](home/Tasks/Ritual-of-Water), [Ritual of the Arcane](home/Tasks/Ritual-of-the-Arcane)|<br>
|t_rod|[Rod](home/Weapons/Rod)|<br>
|t_runes|[Air Runes](home/Resources/Air-Runes), [Earth Runes](home/Resources/Earth-Runes), [Flame Runes](home/Resources/Flame-Runes), [Light Runes](home/Resources/Light-Runes), [Mana Runes](home/Resources/Mana-Runes), [Nature Runes](home/Resources/Nature-Runes), [Rune Stones](home/Resources/Rune-Stones), [Shadow Runes](home/Resources/Shadow-Runes), [Spirit Runes](home/Resources/Spirit-Runes), [Water Runes](home/Resources/Water-Runes)|<br>
|t_school|[Aeromancy](home/Skills/Aeromancy), [Aquamancy](home/Skills/Aquamancy), [Chronomancy](home/Skills/Chronomancy), [Geomancy](home/Skills/Geomancy), [Lumenology](home/Skills/Lumenology), [Mage Lore](home/Skills/Mage-Lore), [Nature Studies](home/Skills/Nature-Studies), [Pyromancy](home/Skills/Pyromancy), [Spirit Communion](home/Skills/Spirit-Communion), [Umbramancy](home/Skills/Umbramancy)|<br>
|t_shortsword|[Shortsword](home/Weapons/Shortsword)|<br>
|t_spear|[Spear](home/Weapons/Spear)|<br>
|t_staff|[Staff](home/Weapons/Staff)|<br>
|t_staves|[Greatstaff](home/Weapons/Greatstaff), [Staff](home/Weapons/Staff)|<br>
|t_sword|[Sword](home/Weapons/Sword)|<br>
|t_swords|[Longsword](home/Weapons/Longsword), [Shortsword](home/Weapons/Shortsword), [Sword](home/Weapons/Sword), [Vorpal Blade](home/Rares/Vorpal-Blade)|<br>
|t_tier0|[Adept](home/Classes/Adept), [Blue Adept](home/Classes/Blue-Adept), [Mystic Savant](home/Classes/Mystic-Savant)|<br>
|t_tier1|[Arcane Trickster](home/Classes/Arcane-Trickster), [Dark Magician](home/Classes/Dark-Magician), [Elementalist](home/Classes/Elementalist), [Magician](home/Classes/Magician), [Oracle](home/Classes/Oracle), [Puppeteer](home/Classes/Puppeteer), [Reanimator](home/Classes/Reanimator), [Witchcraft](home/Classes/Witchcraft)|<br>
|t_tier2|[Alchemist](home/Classes/Alchemist), [Battle Mage](home/Classes/Battle-Mage), [Bonemonger](home/Classes/Bonemonger), [Enchanter](home/Classes/Enchanter), [Geomancer](home/Classes/Geomancer), [Hydromancer](home/Classes/Hydromancer), [Mage](home/Classes/Mage), [Mystic Madcap](home/Classes/Mystic-Madcap), [Pyromancer](home/Classes/Pyromancer), [Ritualist](home/Classes/Ritualist), [Seer](home/Classes/Seer), [Warden](home/Classes/Warden), [Wind Mage](home/Classes/Wind-Mage)|<br>
|t_tier3|[Blood Mage](home/Classes/Blood-Mage), [Doomsayer](home/Classes/Doomsayer), [Dreadlord](home/Classes/Dreadlord), [Druid](home/Classes/Druid), [High Mage](home/Classes/High-Mage), [Highelemental](home/Classes/Highelemental), [Mechanist](home/Classes/Mechanist), [Spell Blade](home/Classes/Spell-Blade), [Stormcaller](home/Classes/Stormcaller), [Summoner](home/Classes/Summoner), [Thanophage](home/Classes/Thanophage)|<br>
|t_tier4|[Bladeweaver](home/Classes/Bladeweaver), [Earthshaker](home/Classes/Earthshaker), [Fey](home/Classes/Fey), [Necromancer](home/Classes/Necromancer), [Shian Anchorite](home/Classes/Shian-Anchorite), [Sorcerer](home/Classes/Sorcerer), [Thaumaturge](home/Classes/Thaumaturge), [Warlock](home/Classes/Warlock), [Wizard](home/Classes/Wizard)|<br>
|t_tier5|[Arcane Dervish](home/Classes/Arcane-Dervish), [Archlock](home/Classes/Archlock), [Dhrunic Wizard](home/Classes/Dhrunic-Wizard), [Grey Necromancer](home/Classes/Grey-Necromancer), [Heresiarch](home/Classes/Heresiarch), [Kell](home/Classes/Kell), [Master Ritualist](home/Classes/Master-Ritualist), [Mechamancer](home/Classes/Mechamancer)|<br>
|t_tier6|[Astral Seer](home/Classes/Astral-Seer), [Avatar](home/Classes/Avatar), [Death Lock](home/Classes/Death-Lock), [Eldritch Knight](home/Classes/Eldritch-Knight), [High Kell](home/Classes/High-Kell), [Mythic Wizard](home/Classes/Mythic-Wizard), [Titan](home/Classes/Titan), [Vile Necromancer](home/Classes/Vile-Necromancer)|<br>
|t_training|[Dagger](home/Rares/Dagger), [Homing Dart](home/Rares/Homing-Dart), [Wooden Hammer](home/Rares/Wooden-Hammer)|<br>
|t_wand|[Wand](home/Weapons/Wand)|<br>
|t_warhammer|[Titan's Hammer](home/Rares/Titan's-Hammer), [Warhammer](home/Weapons/Warhammer), [Wooden Hammer](home/Rares/Wooden-Hammer)|<br>
|timesource|[Clepsydra](home/Furnitures/Clepsydra), [Metronome](home/Upgrades/Metronome), [Pendulum](home/Upgrades/Pendulum)|<br>
|tricksource|[Bag of Tricks](home/Upgrades/Bag-of-Tricks), [Card Deck](home/Upgrades/Card-Deck), [Three-Sided Coin](home/Upgrades/Three-Sided-Coin)|<br>
|watersource|[Basin](home/Furnitures/Basin), [Bottomless Pitcher](home/Upgrades/Bottomless-Pitcher), [Cataract](home/Homes/Cataract), [Fountain](home/Furnitures/Fountain), [Gargoyle](home/Furnitures/Gargoyle), [Pond](home/Furnitures/Pond)|<br>
|weavesource|[Loom](home/Furnitures/Loom), [Spinning Wheel](home/Furnitures/Spinning-Wheel)|<br>
|workspace|[Crafting Table](home/Furnitures/Crafting-Table), [Enchanting Table](home/Furnitures/Enchanting-Table), [Machina Workshop](home/Furnitures/Machina-Workshop), [Shaping Laboratory](home/Furnitures/Shaping-Laboratory), [Workbench](home/Furnitures/Workbench), [Workshop](home/Furnitures/Workshop), [Writing Desk](home/Furnitures/Writing-Desk)|