**Tags**: T_Tier0

**Description**: Your master behind you, you set off into the world alone. You had better find a new place to stay.

**Modifiers**: [Tier 0](tier0)

