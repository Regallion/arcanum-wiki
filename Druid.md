```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["Nature Lore ≥ 15"])
  t2 ---> t2r ---> druid
  subgraph tier3 [Tier 3]
    druid([Druid])
  end
```
**Description**: A spellcaster dedicated to the natural world

**Tags**: T_Tier3

**Requirements**: [Tier 2](tier2) > 0 and [Nature Lore](Nature-Lore) ≥ 15

**Cost to acquire**

- [Research](Research): 1000

- [Nature Gem](Nature-Gem): 20

- [Herbs](Herbs): 50

- [Arcana](Arcana): 20

**Result**

- [Skill Points](Skill-Points): 1

**Modifiers**

- [Tier 3](tier3): True

- [Nature Lore Max](Nature-Lore): 3

- [Nature Lore Rate](Nature-Lore): 0.2+5%

- [Animal Handling Max](Animal-Handling): 2

- [Herbalism Max](Herbalism): 2

- [Herbalism Rate](Herbalism): 10%

- [Potions Max](Potions): 1

- [Potions Rate](Potions): 0.4

