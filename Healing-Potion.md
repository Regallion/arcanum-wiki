**Requirements**: [Potions](Potions) ≥ 9

**Cost to Buy**

- [Gold](Gold): 100

- [Light Gem](Light-Gem): 1

- [Research](Research): 500

**Level**: 7

**Cost to acquire**

- [Herbs](Herbs): 25

- [Gems](Gems): 3

**Effect of using**

- [Life](Life): 200

