**Description**: A fox-girl delightedly teaches you some practical ways to use your weapon more effectively

**Effects**

- [Befuddlement](Befuddlement): 0~2

- [Weariness](Weariness): 2~5

- [Frustration](Frustration): 2~5

- [Bladelore Exp](Bladelore): 3

- [Enchanting Exp](Enchanting): 3

**Result**

- [Bladelore Max](Bladelore): 0.001

**Symbol**: ⭐

