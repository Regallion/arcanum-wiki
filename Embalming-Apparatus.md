**Description**: Fluids and recepticles for the preservation of corpses.

**Effects**

- [Befuddlement](Befuddlement): -1~2

- [Unease](Unease): 0~2

- [Reanimation Exp](Reanimation): 1

- [Embalming Exp](Embalming): 2

**Result**

- [Bone Dust](Bone-Dust): 0~3

