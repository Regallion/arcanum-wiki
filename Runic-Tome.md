**Description**: Even deciphering the runes is a tedious chore.

**Effects**

- [Befuddlement](Befuddlement): -1~4

- [Frustration](Frustration): 0~4

- [Unease](Unease): 0~2

- [Lore Exp](Lore): 2

**Result**

- [Codices](Codices): 1

- [Arcana](Arcana): 0.01

