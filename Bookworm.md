**Description**: A studious annelid to choose a home within a tome's pages.

**Effects**

- [Befuddlement](Befuddlement): 1~2

- [Lore Exp](Lore): 3

