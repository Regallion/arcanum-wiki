**Description**: Within a ramble of mundane details, thyffr often discloses some genuine history.

**Effects**

- [Frustration](Frustration): 0~2

- [Weariness](Weariness): 1~3

- [World Lore Exp](World-Lore): 2

- [Research](Research): 1

**Result**

- [Arcana](Arcana): 0.01

