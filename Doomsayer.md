```mermaid
graph LR
  t2{Tier 2 Class}
  t2r(["Spirit ≥ 12,<br>Divination ≥ 12"])
  t2 ---> t2r ---> doomsayer
  subgraph tier3 [Tier 3]
    doomsayer([Doomsayer])
  end
```
**Description**: Harbinger of ruin

**Requirements**: [Tier 2](tier2) > 0 and [Spirit](Spirit) ≥ 12 and [Divination](Divination) ≥ 12

**Tags**: T_Tier3

**Cost to acquire**

- [Research](Research): 2000

- [Arcana](Arcana): 15

- [Tomes](Tomes): 7

- [Spirit Gem](Spirit-Gem): 15

**Result**

- [Skill Points](Skill-Points): 2

**Modifiers**

- [Tier 3](tier3): True

- [Spirit Lore Max](Spirit-Lore): 2

- [Divination Max](Divination): 3

- [Planes Lore Max](Planes-Lore): 1

- [Scrying Max](Scrying): 2

- [Lore Max](Lore): 2

- [Lore Rate](Lore): 10%

- [Mana Max](Mana): 1

**Flavor**: You call it an empire, but in the gloaming I see only heaps of crumbled stone.

