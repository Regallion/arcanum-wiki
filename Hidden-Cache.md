**Description**: You sense something buried nearby.

**Effects**

- [Frustration](Frustration): 1~2

- [Weariness](Weariness): 0~2

- [Befuddlement](Befuddlement): -1~2

- [Nature Lore Exp](Nature-Lore): 1

**Loot**

- [Gold](Gold): 0~50

- [Gems](Gems): 2~5

- [Scrolls](Scrolls): 2~5

- [Codices](Codices): 0~4

