**Description**: Holding it seems dangerous. Not holding it, even more so.

**Requirements**: [Potions](Potions) ≥ 999

**Level**: 5

**Cost to Buy**

- [Gold](Gold): 1500

- [Research](Research): 1200

**Cost to acquire**

- [Fire](Fire): 5

- [Air](Air): 5

- [Herbs](Herbs): 25

- [Water](Water): 5

**Effect of using**

- Attack: {'targets': 'all', 'dmg': '50~90'}

