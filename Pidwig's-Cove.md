**Level**: 9

**Requirements**: [World Lore](World-Lore) ≥ 5 and [Eryl Eyot](Eryl-Eyot) > 0

**Length**: 25

**Description**: The half-goblin Pidwig has sailed the waters around his bay for hundreds of years. In that time he has collected a myriad of interesting artifacts.

**Run Cost**

- [Stamina](Stamina): 0.3

**Result**

- [Arcana](Arcana): 0.1

**Loot**

- [Gold](Gold): 20~30

- [Gems](Gems): 4~10

**Encounters**

- [Dusty Chest](Dusty-Chest)

- [Pidwig's Starcharts](Pidwig's-Starcharts)

- [Talk To Pidwig](Talk-To-Pidwig)

- [Murky Waters](Murky-Waters)

- [Pidwig's Hoard](Pidwig's-Hoard)

- [Dusty Chest](Dusty-Chest)

- [Somber Sunset](Somber-Sunset)

- [Starry Night](Starry-Night)

