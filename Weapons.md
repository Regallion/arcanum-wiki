This page is a default list of the items in this category.<br>
If it doesn't meet your needs, please let a developer (probably 'simple') know, and they'll get around to updating it.<br>

[Axe](Axe)<br>
[Battleaxe](Battleaxe)<br>
[Broomstick](Broomstick)<br>
[Cane](Cane)<br>
[Club](Club)<br>
[Dagger](Dagger)<br>
[Fetish](Fetish)<br>
[Greatstaff](Greatstaff)<br>
[Knife](Knife)<br>
[Longsword](Longsword)<br>
[Mace](Mace)<br>
[Orb](Orb)<br>
[Rod](Rod)<br>
[Shortsword](Shortsword)<br>
[Spear](Spear)<br>
[Staff](Staff)<br>
[Sword](Sword)<br>
[Wand](Wand)<br>
[Warhammer](Warhammer)