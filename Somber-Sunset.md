**Description**: Lapping waves in the dying sun hint at secrets beyond your reach.

**Effects**

- [Frustration](Frustration): -1~-2

- [Weariness](Weariness): -1~-2

- [Madness](Madness): -1~-2

- [Water Lore Exp](Water-Lore): 1

- [Mysticism Exp](Mysticism): 1

- [Divination Exp](Divination): 1

